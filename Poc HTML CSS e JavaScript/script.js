document.addEventListener("DOMContentLoaded", function () {
  const videoContainer = document.getElementById("videoContainer");
  const transcriptContainer = document.getElementById("transcriptContainer");

  let player;
  let transcript = [];

  // Função para carregar a transcrição do URL usando um proxy para contornar o CORS
  async function loadTranscript(url) {
    try {
      const proxyUrl = "https://api.allorigins.win/raw?url=";
      const response = await fetch(proxyUrl + encodeURIComponent(url));
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      transcript = await response.json();
      displayFullTranscript();
    } catch (error) {
      console.error("Failed to load transcript:", error);
    }
  }

  // Inicializar o player do Vimeo
  function initializePlayer() {
    player = new Vimeo.Player(videoContainer, {
      id: 530868434, // Substitua pelo ID do seu vídeo
      width: 640,
    });

    player.on("play", startKaraoke);
    player.on("timeupdate", (data) => updateTranscript(data.seconds));
  }

  // Exibir a transcrição completa na tela
  function displayFullTranscript() {
    let text = "";
    for (let i = 0; i < transcript.length; i++) {
      const content = transcript[i].alternatives[0].content;
      text += `<span id="word-${i}">${content} </span>`;
    }
    transcriptContainer.innerHTML = text;
  }

  // Iniciar o karaokê desde o início do vídeo
  function startKaraoke() {
    updateTranscript(0);
  }

  // Atualizar a transcrição com base no tempo atual
  function updateTranscript(currentTime) {
    if (currentTime === undefined) {
      return;
    }

    for (let i = 0; i < transcript.length; i++) {
      const start = parseFloat(transcript[i].start_time);
      const wordElement = document.getElementById(`word-${i}`);

      if (!isNaN(start) && currentTime >= start) {
        wordElement.classList.add("highlight");
      } else {
        wordElement.classList.remove("highlight");
      }
    }
  }

  // Carregar a transcrição e inicializar o player
  async function init() {
    await loadTranscript(
      "https://gitlab.com/pocs_project/poctranscption/-/raw/main/Trancribe.json"
    ); // Substitua pelo URL correto
    initializePlayer();
  }

  init();
});
