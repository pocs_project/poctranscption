import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VimeoPlayerComponent } from './vimeo-player/vimeo-player.component';

const routes: Routes = [
  { path: '', component: VimeoPlayerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
