import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import Player from '@vimeo/player';

@Component({
  selector: 'app-vimeo-player',
  templateUrl: './vimeo-player.component.html',
  styleUrls: ['./vimeo-player.component.css'],
})
export class VimeoPlayerComponent implements OnInit {
  @ViewChild('videoContainer', { static: true }) videoContainer!: ElementRef;
  player!: Player;
  transcript = [
    {
      type: 'pronunciation',
      alternatives: [
        {
          confidence: '0.996',
          content: 'gestão',
        },
      ],
      start_time: '14.199',
      end_time: '14.729',
    },

  ];

  displayText = '';
  currentWordIndex = 0;

  ngOnInit(): void {
    // Inicialize o player do Vimeo
    this.player = new Player(this.videoContainer.nativeElement, {
      id: 530868434, // Substitua pelo ID do seu vídeo
      width: 640,
    });

    // Carregue a transcrição do vídeo (se necessário, inclua a lógica de carregamento da transcrição)

    // Escute o evento 'play' para iniciar a exibição do texto
    this.player.on('play', () => {
      this.startKaraoke();
    });

    // Escute os eventos de atualização de tempo para atualizar a transcrição
    this.player.on('timeupdate', (data: any) => {
      this.updateTranscript(data.seconds);
    });
  }

  startKaraoke(): void {
    // Inicie o karaokê desde o início do vídeo
    this.updateTranscript(0);
  }

  updateTranscript(currentTime: number): void {
    // Constrói o texto completo, destacando as palavras que já passaram
    let text = '';
    for (let i = 0; i < this.transcript.length; i++) {
      const start = this.transcript[i].start_time;
      if (start != null && currentTime >= parseFloat(start)) {
        text += `<b>${this.transcript[i].alternatives[0].content}</b> `;
      } else {
        text += `${this.transcript[i].alternatives[0].content} `;
      }
    }

    // Exibe o texto completo
    this.displayText = text;
  }
}
