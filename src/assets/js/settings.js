//####################################################
//                   ILANG ->
//####################################################
var ILangSettings = {
    UserID: 0,
    UserName: '',
    OrganizationID: 0,
    UserOrganizationID: 0,
    Version: '',
    ClientIP: '',
    SynchronousEnabled: false,
    SignalRLogEnabled: false,
    GoogleAnalyticsKey: 'chavedoGoogle',
    AccusoftViewerEnabled: true,
    JavaScriptDomainName: 'ulife.com.br',
    IsUserAdmin: false,
    IsUserStudent:false,
    Sites: {
        ILang: function () {
            return ILangSettings.getSecureURL('https://teste1.ulife.com.br')
        },
        Student: function () {
            return ILangSettings.getSecureURL('https://teste1student.ulife.com.br')
        },
        StudentAPI: function () {
            return ILangSettings.getSecureURL('https://teste1apistudent.ulife.com.br')
        },
        Static: function () {
            return ILangSettings.getSecureURL('https://staticteste1.ulife.com.br')
        },
        API: function () {
            return ILangSettings.getSecureURL('https://teste1api.ulife.com.br')
        },
        SignalR: function () {
            return ILangSettings.getSecureURL('https://teste1signalr.ulife.com.br')
        },
        Social: function () {
            return ILangSettings.getSecureURL('https://teste1social.ulife.com.br')
        },
        SocialAPI: function () {
            return ILangSettings.getSecureURL('https://teste1apisocial.ulife.com.br')
        },
        Admin: function () {
            return ILangSettings.getSecureURL('https://teste1admin.ulife.com.br')
        },
        AdminAPI: function () {
            return ILangSettings.getSecureURL('https://teste1apiadmin.ulife.com.br')
        },
        Assessment: function () {
            return ILangSettings.getSecureURL('https://teste1assessment.ulife.com.br')
        }
    },
    Logout: {
        AutenticationTimeout: 0,
        Timer: null,
        LoginUrl: '',
        StartCount: function () {
            if (this.AutenticationTimeout > 0) {
                window.clearTimeout(this.Timer);

                this.Timer = null;

                this.Timer = window.setTimeout(function () {
                    if (document.getElementById('logoutWarningDialog') != undefined) {
                        getAngularScopeByElementId('logoutWarningDialog').show();
                    }
                }, this.AutenticationTimeout);
            }
        }
    },
    Browser: {
        showBrowserSupportDialog: function () {
            getAngularScopeByElementId('unsupportedBrowserWarningDialog').show();
        },
        BrowserName: null
    },
    init: function () {
        this.Logout.StartCount();
        this.Browser.BrowserName = GetBrowserName();
    },
    getSecureURL: function(url) {
        if ('https:' == document.location.protocol) {
            url = url.replace('http://', 'https://');
        }
        return url;
    }
}
