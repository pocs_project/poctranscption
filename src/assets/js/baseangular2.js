﻿/// <reference path="angular.min.js" />
var keyboardCodes = { ENTER: 13, KEY_UP: 38, KEY_DOWN: 40 };
var keyCode = { KEY_ENTER: 13, KEY_DOWN: 40, KEY_UP: 38, KEY_ESC: 27, KEY_RIGHT: 39, BACKSPACE: 8, KEY_SPACE: 32 };
var _searchTypes = { Big: 0, Small: 1 };
var includPositions = { Bottom: 0, Top: 1 };
var dropTypes = { Default: 'Dropdown_Enum', Select: 'Dropdown_Select', OrganizationSelector: 'SelectOrganization', Checklist: 'Dropdown_Checklist', SelectOrganizationLeftMenu: 'SelectOrganizationLeftMenu', SelectOrganizationLeftMenu_Old: 'SelectOrganizationLeftMenu_Old' };
var sortTypes = { Default: 'SortBar', SortBarUpDown: 'SortBarUpDown' };
var tabTypes = { Blue: 'TabBlue', Default: 'MultiTab', Gray: 'TabGray', EM: 'MultiTabEM' };
var dialogType = { Global: 'GDMulti', Modal: 'ModalDialog', Drawer: 'DrawerDialog' };
var dialogSize = { Auto: 'dlgAutoHeight', Medium: 'dlgMed', Small: 'dlgSmall', Full: 'dlgFull' };
var uploaderTypes = { Default: 'Default', Bto: 'BtoUploader', Mobile: 'Html5Mobile', Label:'Html5Label' };
var ILangApp = angular.module('ILangApp', [])
    .config(function ($sceDelegateProvider, $compileProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            ILangSettings.Sites.ILang() + '/**',
            ILangSettings.Sites.Student() + '/**',
            ILangSettings.Sites.API() + '/**',
            ILangSettings.Sites.Static() + '/**',
            ILangSettings.Sites.AdminAPI() + '/**',
            ILangSettings.Sites.StudentAPI() + '/**',
            ILangSettings.Sites.SocialAPI() + '/**',
            getSecureURL(ILangSettings.Sites.ILang()) + '/**',
            getSecureURL(ILangSettings.Sites.Student()) + '/**',
            getSecureURL(ILangSettings.Sites.API()) + '/**',
            getSecureURL(ILangSettings.Sites.Static()) + '/**'
        ]);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
    });

window.trackObj = {};
function setListDefaultValue($scope, $attrs) {
    var enumValue = null;
    var key = $attrs.key;

    if ($attrs.hasOwnProperty('trackHash'))
        $scope.serverParameters.drops[key].trackHash = true;

    if ($scope.serverParameters.drops[key].trackHash && $scope.track.read($attrs.key) != null) {
        enumValue = $scope.track.read($attrs.key);
    } else if ($attrs.hasOwnProperty("default")) {
        var name = $attrs.default.split('.')[1];
        enumValue = $scope.serverParameters.enums[key][name];
    } else {
        var k = $scope.serverParameters.drops[key].list.notEquals({ fixedOption:true}).first({ selected: true });
        if (k != null) {
            enumValue = k.value;
        } else if ($scope.serverParameters.drops[key].defaultValueIfEmpty != null) {
            $scope.serverParameters.drops[key].list.push($scope.serverParameters.drops[key].defaultValueIfEmpty);
            enumValue = $scope.serverParameters.drops[key].defaultValueIfEmpty.value;
        }

    }

    if (enumValue != null) {
        $scope.serverParameters.drops[key].selectedId = enumValue;
        $scope.serverParameters.drops[key].selected = enumValue;
        if ($scope.serverParameters.mainScope == true &&
            $scope.serverParameters.drops[key].trackHash == true &&
            enumValue != null &&
            enumValue != '' &&
            enumValue != '-1' &&
            enumValue != '0' &&
            enumValue != 'Todos' &&
            compareIfSafe($scope, 'serverParameters.drops.' + key + '.defaultValueIfEmpty.value', enumValue) == false) {
            $scope.track.write(key, enumValue);
        }

        if ($scope.serverParameters.drops[key].isCheckboxList == false) {
            $($scope.serverParameters.drops[key].list).each(function (a, item) {
                item.selected = item.value == enumValue;
                if (item.selected)
                    $scope.serverParameters.drops[key].selectedText = item.description;
            });
        } else {
            $scope.df__dropChecklistSetText($scope.serverParameters.drops[key]);
        }
    }
}



//####################################################
//              <-    UTIL
//                    DIRECTIVES     ->
//####################################################
ILangApp.directive('ilangSearch', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) { return generalTemplateUrl(elem, attrs, 'Search', 'ElasticSearchBox') },
        controller: function ($scope, $element, $attrs) {
            $scope.focusoninput = true;
            $scope.appendList = getPropertyByNameOrDefault($attrs, 'appendList', true) == true;
            $scope.outterCssClass = getPropertyByName($attrs, 'outtercss') == null ? "srchContainer" : $attrs.outtercss;
            var sc = getRootScope($scope);
            if (isSafeToUse(sc, 'smallContext') && isSafeToUse(sc, 'track') && sc.track.read('searchTxt') != null) {
                sc.smallContext.searchTxt = sc.track.read('searchTxt');
            }
            var hybrid = getPropertyByName($attrs, 'hybridSearch') == 'true';
            if (hybrid == true) {
                sc.jsParameters.hybridSearch = hybrid;
            }
            $scope.onIlangSearchLoad = function () {
                var _focus = compareIfSafe($attrs, 'focusonload', 'true');
                if (_focus == true) {
                    //
                    window.setTimeout(function () {
                        $($element).find('input[type="text"]').eq(0).focus();
                    }, 500);


                }

            };
        },

        compile: function (Telement, tAttras, aa) {
            $(Telement).find('small-list').attr('template', tAttras.template);
            if (typeof (tAttras.placeholder) != 'undefined')
                $(Telement).find('input[placeholder]').attr('placeholder', tAttras.placeholder);


        }
    };
});

ILangApp.directive('listSearch', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) { return generalTemplateUrl(elem, attrs, 'Search', 'Default') },
        scope: {
            searchfn: '&',
            action: '@',
            dto: '='
        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            $scope.outterCssClass = getPropertyByNameOrDefault($attrs, 'outtercss', 'srchContainer');
            registerInputBehavior(me, $attrs, $timeout);
        },
        compile: function (tElement, tAttrs, transclude) {
            if (typeof (tAttrs.placeholder) != 'undefined')
                $(tElement).find('input[placeholder]').attr('placeholder', tAttrs.placeholder);

        }
    };
}]);

ILangApp.directive('mailList', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) {
            var name = getPropertyByName(attrs, 'template');
            if (name != null) {
                if (name.indexOf('tpl') >= 0) {
                    if (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl') {
                        return getStaticUrl('/Templates/Search/MailList.html');
                    } else {
                        return getAngularTemplate('Search', name.replace('tpl/', ''));
                    }
                } else {
                    return getStaticUrl('/Templates/Search/MailList.html');
                }
            } else {
                return getStaticUrl('/Templates/Search/MailList.html');
            }
        },
        //templateUrl: function (elem, attrs) {
        //    if (typeof (attrs.template) != 'undefined') {
        //        return getStaticUrl('/Templates/Search/' + attrs.template + '.html');
        //    } else {
        //        return getStaticUrl('/Templates/Search/MailList.html');
        //    }
        //},
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.onSelectionChange = null;
            me.emptyField = false;
            me.focusOnInput = false;
            me.tagElement = null;
            me.trackId = $attrs.hasOwnProperty('track');
            me.key = "mailListSelected";
            me.splitChart = '-';
            registerInputBehavior(me, $attrs, $timeout);
            me.doSearch = function (BackTodefault) {
                if (BackTodefault == true)
                    return;
                var actionName = me.key + '_read';

                me.rootScope.action(actionName, { SearchCriteria: me.searchTxt }, function (result) {
                    if (result.Success == true) {
                        me.searchActive = true;
                        me.searchList = result.Result.removeDuplicate(me.selectList, function (item) { return item.EntityID; });
                        me.isSearchBoxActive = true;
                        me.smallListHoverItem = -1;
                    }

                }, undefined, { showLoading: false });
            };
            me.removeItemFromList = function (item) {
                me.selectList.remove({ EntityID: item.EntityID });
                if (me.trackId == true && compareIfSafe(localStorage, 'isEnabled', true)) {
                    me.track.remove(me.key, item.EntityID, me.splitChart);
                    me.saveListOnLocalStorage();
                }
                me.updateIfNecessary();
            }
            me.removeAll = function () {
                me.selectList = [];
                if (me.trackId == true && compareIfSafe(localStorage, 'isEnabled', true)) {
                    me.track.clean(me.key);
                    me.saveListOnLocalStorage();
                }
                me.updateIfNecessary();
            };
            me.onload = function () {
                me.tagElement = $($element).find('.tagCont').eq(0);
                try {
                    if (me.trackId == true && compareIfSafe(localStorage, 'isEnabled', true)) {
                        var data = me.track.read(me.key);
                        if (data != null) {
                            var idList = data.toString().split(me.splitChart).select({ id: function (item) { return parseInt(item); } });
                            if (idList != null) {
                                var txtStorage = window.sessionStorage.getItem(me.key);
                                if (txtStorage != null && txtStorage != '') {
                                    var storage = JSON.parse(txtStorage);
                                    if (storage != null) {
                                        idList.foreach(function (item) {
                                            var lookup = storage.first({ EntityID: item.id });
                                            if (lookup != null && !me.selectList.any({ EntityID: item.id })) {
                                                me.selectList.push(lookup);
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }
                    me.setInputFocus();
                } catch (e) {

                }
            };
            me.addItem = function (item) {
                me.selectList.push(item);
                me.emptyField = false;
                if (me.trackId == true && compareIfSafe(localStorage, 'isEnabled', true)) {
                    me.track.add(me.key, item.EntityID, me.splitChart);
                    me.saveListOnLocalStorage();
                }

                me.updateIfNecessary();
            },
                me.saveListOnLocalStorage = function () {
                    if (me.trackId == true && compareIfSafe(localStorage, 'isEnabled', true)) {
                        try {
                            window.sessionStorage.setItem(me.key, JSON.stringify(me.selectList.select({
                                EntityID: 'EntityID',
                                Type: 'Type',
                                Name: 'Name',
                                ImageUrl: 'ImageUrl'
                            })));
                        } catch (e) { }

                    }
                };
            me.onItemIsSelected = function (item) {
                me.searchList.remove({ EntityID: item.EntityID });
                me.addItem(item);
                me.closeSearchBox();
            }
            me.getList = function () {
                return me.selectList;
            }
            me.backToDefault = function () {
                me.closeSearchBox();
            };
            me.updateIfNecessary = function () {
                if (isSafeToUse(me, 'onSelectionChange') && typeof (me.onSelectionChange) == 'function') {
                    var searchItens = me.selectList.select({ EntityID: 'EntityID', Type: 'Type' });
                    me.onSelectionChange(me, searchItens);
                }
            }
            me.setInputFocus = function () {
                me.focusOnInput = true;
            };
            if ($attrs.hasOwnProperty("onselectionchange")) {
                me.onSelectionChange = new Function('$scope', 'searchItens', '$scope.' + $attrs.onselectionchange + "({searchCriteria: '', searchItens: searchItens});");
            }

            if (isSafeToUse(me, 'track') && me.track.read('hashTxt') != null && me.track.read('hashId') != null) {
                var hashtag = {
                    EntityID: me.track.read('hashId'),
                    Name: me.track.read('hashTxt'),
                    Type: me.serverParameters.enums.AngularAutoCompleteType.Tag
                };
                me.selectList.push(hashtag);
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            $(tElement).find('li[listResult]')
                .attr('ng-class', "{hover: $parent.isSearchBoxActive == true && $index == $parent.smallListHoverItem }")
                .attr('ng-click', '$parent.itemClick(user)');
        }
    };
}]);

ILangApp.directive('contentEditable', ['$timeout', function ($timeout) {
    return {
        replace: true,
        templateUrl: getStaticUrl('/Templates/Search/ContentEditable.html'),
        restrict: 'E',
        scope: {
            insertfn: '&',
            onFocus: '&',
            comment: '=',
            selectedList: '=',
            feedId: '=',
            placeHolder: '@'
        },
        link: function (scope, elm, attrs, ctrl) {
            // view to model
            elm.scope = scope;
            scope.element = elm;
            var input = $(elm).find('textarea');
            elm.scope.textArea = input;

        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.inputtext = '';
            me.textCoef = 1;
            me.paddingLeft = 0;
            me._placeHolder = getPropertyByNameOrDefault(me, 'placeHolder', "Comente aqui...");
            me.text = '';
            me.element;
            me.selectedItems = [];
            me.result = {
                show: false,
                left: 0,
                top: 0,
                list: [],
                isUserBrowsingOverResult: false,
                index: 0,
                busy: false,
                close: function (apply) {
                    this.isUserBrowsingOverResult = false;
                    this.index = 0;
                    this.show = false;
                    this.setFocus();
                    //if ((apply == null || apply == true) && !me.$$phase)
                    //    me.$apply();
                },
                clean: function () {
                    this.isUserBrowsingOverResult = false;
                    this.index = 0;
                    this.list = [];
                    this.busy = false;
                    this.show = false;
                    this.hasLoaded = false;
                },
                cleanText: function () {
                    if (isSafeToUse(me, 'element')) {
                        $(me.element).find('textarea').val('');
                    }
                },
                searchTxt: '',
                hasLoaded: false,
                setFocus: function () {
                    this.focus = true;
                    $(me.element).find('textarea').focus();

                },
                focus: false
            };
            me.blur = function (event) {
                me.createTags();
                if (me.selectedList == undefined)
                    me.selectedList = [];
            }
            me.itemHasBeenSelected = false;
            me.lines = [];
            me.words = [];
            me.wordsObj = [];
            me.currentTag = null;
            me.currentTxt = '';
            me.textArea = null;
            me.userIsTypeingName = false;
            me.lastWord = '';
            me.searchTxt = '';
            me.stopWords = ['ola', 'olá', 'oi', ',', '.', 'o', 'a', '!', '?', '/', ';', ':', '-', 'dia', 'bom'];
            me.lastWasSpaceOrEmpty = true
            me.mainTag = null;
            me.pendingSearch = '';
            me.navigationKeysCode = { KEY_DOWN: 40, KEY_UP: 38, KEY_LEFT: 37, KEY_RIGHT: 39 };
            me.root = getRootScope(me);
            me.action = getPropertyByName(me, 'root.serverParameters.actions._contentEditableReadAction');
            me.preKeyUp = function (event) {

                if (event.keyCode == keyCode.KEY_ESC) {
                    //ESC Cancel
                    me.result.close();
                } else if (event.keyCode == keyCode.BACKSPACE && me.result.isUserBrowsingOverResult) {
                    //back to default
                    me.result.clean();
                } else if (objToArray(me.navigationKeysCode).indexOf(event.keyCode) >= 0) {
                    me.userBrowsingResult(event.keyCode);
                    event.stopPropagation();
                    event.preventDefault();

                    return;
                } else if (event.keyCode == keyCode.KEY_ENTER) {
                    if (me.result.isUserBrowsingOverResult) {
                        //select item
                        me.itemSelected();
                        return;
                    } else {
                        //scope.insert()
                    }
                    if (isSafeToUse(me, 'textAreaAdjust'))
                        me.textAreaAdjust(event);

                } else {
                    if (me.text.trim() == '') {
                        me.wordsObj = [];
                        me.selectedList = [];
                        me.result.clean();
                    }

                    var lastChar = me.text.last();

                    if (lastChar == null || lastChar.trim() == '' || lastChar == ' ' || lastChar == ',' || me.itemHasBeenSelected == true) {
                        me.itemHasBeenSelected = false;
                        return false;
                    }
                    console.log('result.show: ', me.result.show);
                    var words = me.text.trim().split(' ');
                    if (words == null && me.lastWord.length == 0)
                        return;
                    me.words = words;
                    me.lines = me.text.split(/\r?\n/);
                    me.lastWord = words.last().replace(/[\r\n]/g, "");//.trimLeft();

                    me.searchTxt = me.lastWord;

                    if (me.lastWord == null || me.lastWord.trim() == '') {
                        me.result.clean();
                        input.val('');
                        return;
                    }

                    me.keyUp(event, me.element);
                }
            }
            me.keyUp = function (event, elm) {
                me.element = elm;

                if (me.text != null && me.text.length >= 1 && me.lastWasSpaceOrEmpty) {

                    var firstChart = me.lastWord.substr(0, 1);

                    if (firstChart == firstChart.toUpperCase() && firstChart.trim() != "") {

                        if (me.lastWord.length == 1) {
                            try {
                                var canvas = document.createElement('canvas');
                                var ctx = canvas.getContext("2d");
                                ctx.font = me.textArea.css('font-size') + " " + me.textArea.css('font-family');
                                var lines = me.text.split(/\r?\n/)
                                var str = lines.last();//me.text;
                                console.log('lastLine: ', str);
                                me.paddingLeft = ctx.measureText(str).width;
                            } catch (e) {
                                me.paddingLeft = me.text.length * 7;
                            }
                        } else if (me.lastWord.length == 2 || me.lastWord.length == 3 || me.lastWord.length == 4) {
                            me.search(me.lastWord, function (data) {
                                me.result.show = true;
                                me.result.left = parseInt(event.currentTarget.offsetLeft) + parseInt(me.paddingLeft * me.textCoef);
                                me.result.top = event.currentTarget.offsetTop + 20;
                                //if (!me.$$phase)
                                //    me.$apply();
                            });
                        }
                    } else {
                        me.result.clean();
                    }
                } else {
                    me.lastWasSpaceOrEmpty = event.keyCode == keyCode.KEY_SPACE;
                }
            };
            me.keyPress = function (e) {
                if (e.keyCode === 13) {
                    if (me.text != null) {
                        //   me.text += ' ';
                    }
                }
            }
            me.serverSearch = function (txt, after) {
                if (me.result.busy == true)
                    return;

                me.result.busy = true;
                if (isSafeToUse(me, 'root.action') && isSafeToUse(me, 'action.name')) {
                    me.root.action(me.action.name, { SearchCriteria: txt, EntityID: me.feedId, PageIndex: 1 }, function (data) {
                        if (compareIfSafe(data, 'Success', true)) {
                            me.result.busy = false;
                            if (data.Result.length > 0) {
                                me.result.list = data.Result.equals({ Type: me.root.serverParameters.enums.AngularAutoCompleteType.User });
                                me.result.hasLoaded = txt.length >= 4 && data.HasMorePages == false;
                            } else {
                                me.result.hasLoaded = false;
                            }

                            if (typeof (after) == 'function')
                                after(data);

                            if (me.pendingSearch != '' && me.pendingSearch != txt && me.pendingSearch.length > txt.length)
                                me.serverSearch(me.pendingSearch, after);
                        }
                    });
                }
            }
            me.localSearch = function (txt, after) {
                me.result.searchTxt = txt;
            };
            me.search = function (txt, after) {
                if (me.words.length >= 2) {
                    var before = me.words[me.words.length - 2].substr(0, 1);
                    if (before != "\n" && before.toUpperCase() == before.substr(0, 1) && me.stopWords.indexOf(before.toLowerCase().trim()) == -1) {
                        txt = me.words[me.words.length - 2] + ' ' + txt;
                        console.log('2 words: ', txt);
                        me.result.busy = false;
                        me.result.hasLoaded = false;
                    }
                }

                if (me.result.busy == true) {
                    console.log('pending: ', txt);
                    me.pendingSearch = txt;
                    me.localSearch(txt, after);
                } else if (me.result.hasLoaded == true) {
                    console.log('localSearch')
                    me.localSearch(txt, after)
                } else {
                    console.log('serverSearch')
                    me.serverSearch(txt, after);
                }
            }
            me.userBrowsingResult = function (key) {
                if (me.result.isUserBrowsingOverResult == false) {
                    //first time
                    me.result.isUserBrowsingOverResult = true;
                    me.result.index = 0;
                } else {
                    if (key == me.navigationKeysCode.KEY_DOWN) {
                        if (me.result.index < me.result.list.length - 1) {
                            me.result.index++;
                        } else {
                            me.result.index = 0;
                        }
                    } else if (key == me.navigationKeysCode.KEY_UP) {
                        if (me.result.index == 0) {
                            me.result.index = me.result.list.length - 1;
                        } else {
                            me.result.index--;
                        }
                        return;
                    }
                }

            }
            me.itemClicked = function (item, index) {
                me.result.index = index;
                me.itemSelected(item, false);
            };
            me.itemSelected = function (element, apply) {
                var list = me.getFilteredList();
                var item = list[me.result.index];

                me.insertSelectedItem(item, apply);
            };
            me.cleanAll = function () {
                me.result.clean();
                me.result.close();
                me.text = "";
                me.selectedList = [];
                me.selectedItems = [];
                me.lines = [];
                me.comment = "";
            };
            me.insertSelectedItem = function (item, apply) {
                if (item != null) {
                    if (me.selectedList == undefined || me.selectedList == null)
                        me.selectedList = [];
                    me.selectedList.push(item.EntityID);
                    me.selectedItems.push(item);

                    me.itemHasBeenSelected = true;
                    me.result.clean();

                    var txt = me.text;//me.mainTag.val();
                    me.words.foreach(function (item, index) {
                        me.wordsObj[index] = me.words[index];
                    });
                    var indexWords = Math.max(0, me.words.length - 1)
                    me.words[indexWords] = item.Name + ' ';
                    me.wordsObj[Math.max(0, me.wordsObj.length - 1)] = item;
                    //lines
                    var lastLine = me.lines.last();
                    var lastLineWords;
                    if (lastLine != null) {
                        lastLineWords = lastLine.split(' ');
                        lastLineWords[lastLineWords.length - 1] = item.Name + ' ';
                    } else {
                        lastLineWords = [];
                        lastLineWords[0] = item.Name + ' ';
                    }

                    me.lines[Math.max(0, me.lines.length - 1)] = lastLineWords.join(' ');

                    var txt1 = '';
                    for (var i = 0; i < me.lines.length; i++) {
                        txt1 += me.lines[i]
                        if (i < me.lines.length - 1)
                            txt1 += '\n';
                    }

                    //var newText = me.words.join(' ');
                    me.text = txt1;
                    me.createTags();

                    me.result.close(true);
                }
            }
            me.createTags = function () {

                if (me.selectedList == null || me.selectedList.length == 0) {
                    me.comment = me.text;
                    me.selectedItems = [];
                } else {
                    var txt = me.text;

                    me.selectedItems = me.selectedItems.distinct('EntityID');
                    for (var i = 0; i < me.selectedItems.length; i++) {
                        var tagHtml = '';
                        var item = me.selectedItems[i];
                        if (txt.indexOf(item.Name) >= 0) {
                            var newTag = $('<userinfo-picture></userinfo-picture>')
                                .attr('name', item.Name)
                                .attr('userid', item.EntityID)
                                .attr('image', item.ImageUrl)
                                .attr('type', "Name");

                            tagHtml = me.getElementHtml(newTag);
                            txt = txt.replaceAll(item.Name, tagHtml)
                        }
                    }

                    me.comment = txt;
                }

            };
            me.getElementHtml = function (element) {
                var k = $('<a></a>');
                $(element).appendTo(k);
                return k.html();
            }
            me.setCursorToEnd = function (remove) {
                if (me.mainTag == null)
                    return;

                $($timeout(function () {
                    $(me.mainTag).focus();
                    try {
                        var tmp = $('<em></em>', { html: '&nbsp;' }).appendTo(me.mainTag),
                            node = tmp.get(0),
                            range = null,
                            sel = null;

                        if (document.selection) {
                            range = document.body.createTextRange();
                            range.moveToElementText(node);
                            range.select();
                        } else if (window.getSelection) {
                            range = document.createRange();
                            var nodeMf = me.mainTag.find('.userTagTextarea').last().get(0);

                            if (typeof (nodeMf) != 'undefined')
                                range.setStartAfter(nodeMf);

                            range.insertNode(node);
                            range.collapse(false);
                            sel = window.getSelection();
                            sel.removeAllRanges();
                            sel.addRange(range);
                        }
                        if (remove == true)
                            tmp.remove();
                    } catch (e) {

                    }


                    return this;
                }, 20));
            }
            me.getCursorPosition = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();
                    var count = 0;
                    var currentNode = sel.focusNode;
                    var cursorPosition = 0;
                    var previous = currentNode.previousSibling == null ? currentNode.parentNode.previousSibling : currentNode.previousSibling;
                    if (previous != null) {
                        while (previous) {
                            if (previous.nodeType == 3) {
                                count += previous.length;
                            } else if (previous.nodeType == 1) {
                                count += previous.outerHTML.length;
                                count -= 2;
                            }
                            previous = previous.previousSibling;
                        }
                        if (currentNode.parentNode.tagName.toUpperCase() == "EM") {
                            cursorPosition = count + currentNode.parentNode.outerHTML.length;
                        } else {
                            cursorPosition = sel.focusOffset + count;
                        }

                    } else {
                        cursorPosition = sel.focusOffset - 1;
                    }

                    tmp.remove();
                    return cursorPosition;
                }
            };
            me.getLastTypedChar = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();

                    var currentNode = sel.focusNode;
                    var last = currentNode.wholeText.substring(sel.focusOffset - 1, sel.focusOffset);

                    tmp.remove();
                    return last;
                }
            };
            me.getLastTypedWord = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();

                    var currentNode = sel.focusNode;
                    var lastBlock = currentNode.wholeText;
                    var last = lastBlock.split(' ');
                    tmp.remove();
                    if (last != null && last.length > 0) {
                        return last.last();
                    } else {
                        return '';
                    }

                }
            };
            me.insert = function () {
                if (typeof (me.insertfn) == 'function') {
                    me.insertfn(me.inputtext);
                }
            };
            me.getFilteredList = function () {
                var list = [];
                for (var i = 0; i < me.result.list.length; i++) {
                    var item = me.result.list[i]
                    if (me.filterNoAccent(item)) {
                        list.push(item);
                    }
                }
                return list;
            }
            me.filterNoAccent = function (item) {
                if (me.searchTxt == null || me.searchTxt == '')
                    return true;

                var search = me.searchTxt.toLowerCase().removeAccent();
                return item.Name.toLowerCase().indexOf(search) >= 0 ||
                    item.NameNoAccent.toLowerCase().indexOf(search) >= 0;
            };

            me.click = function (event) {
                if (typeof (me.onFocus) == 'function') {
                    me.onFocus();
                }
            }
            me.onLoad = function () {
                me.$watch('comment', function (value) {
                    if (value == null || value == '' || value.length == 0) {
                        me.result.cleanText();
                        me.selectedItems = [];
                        me.selectedList = [];
                        me.text = '';
                        if (isSafeToUse(me, 'textAreaAdjust'))
                            me.textAreaAdjust({ keyCode: keyCode.KEY_ESC });
                    }
                });
                me.$parent.$parent._contentEditable = me;
                me.$parent._contentEditable = me;
            };
            me.editHtml = function (html) {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                me.mainTag.html(html);
                //me.setCursorToEnd(true);
            };
        }

    };
}])

ILangApp.directive('contentEditableAt', ['$timeout', function ($timeout) {
    return {
        replace: true,
        templateUrl: getStaticUrl('/Templates/Search/ContentEditable.html'),
        restrict: 'E',
        scope: {
            insertfn: '&',
            onFocus: '&',
            comment: '=',
            selectedList: '=',
            feedId: '=',
            placeHolder: '@'
        },
        link: function (scope, elm, attrs, ctrl) {
            // view to model
            elm.scope = scope;
            scope.element = elm;
            var input = $(elm).find('textarea');
            elm.scope.textArea = input;

        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.inputtext = '';
            me.textCoef = 1;
            me.paddingLeft = 0;
            me._placeHolder = getPropertyByNameOrDefault(me, 'placeHolder', "Comente aqui...");
            me.text = '';
            me.element;
            me.selectedItems = [];
            me.result = {
                show: false,
                left: 0,
                top: 0,
                list: [],
                isUserBrowsingOverResult: false,
                index: 0,
                busy: false,
                close: function (apply) {
                    this.isUserBrowsingOverResult = false;
                    this.index = 0;
                    this.show = false;
                    this.setFocus();
                    //if ((apply == null || apply == true) && !me.$$phase)
                    //    me.$apply();
                },
                clean: function () {
                    this.isUserBrowsingOverResult = false;
                    this.index = 0;
                    this.list = [];
                    this.busy = false;
                    this.show = false;
                    this.hasLoaded = false;
                },
                cleanText: function () {
                    if (isSafeToUse(me, 'element')) {
                        $(me.element).find('textarea').val('');
                    }
                },
                searchTxt: '',
                hasLoaded: false,
                setFocus: function () {
                    this.focus = true;
                    $(me.element).find('textarea').focus();

                },
                focus: false
            };
            me.blur = function (event) {
                me.createTags();
                if (me.selectedList == undefined)
                    me.selectedList = [];
            }
            me.itemHasBeenSelected = false;
            me.lines = [];
            me.words = [];
            me.wordsObj = [];
            me.currentTag = null;
            me.currentTxt = '';
            me.textArea = null;
            me.userIsTypeingName = false;
            me.lastWord = '';
            me.searchTxt = '';
            me.inSearchState = false;
            me.stopWords = ['ola', 'olá', 'oi', ',', '.', 'o', 'a', '!', '?', '/', ';', ':', '-', 'dia', 'bom'];
            me.lastWasSpaceOrEmpty = true
            me.mainTag = null;
            me.pendingSearch = '';
            me.navigationKeysCode = { KEY_DOWN: 40, KEY_UP: 38, KEY_LEFT: 37, KEY_RIGHT: 39 };
            me.root = getRootScope(me);
            me.action = getPropertyByName(me, 'root.serverParameters.actions._contentEditableReadAction');
            me.preKeyUp = function (event) {

                if (event.keyCode == keyCode.KEY_ESC) {
                    //ESC Cancel
                    me.inSearchState = false;
                    me.result.close();
                } else if (event.keyCode == keyCode.BACKSPACE && me.result.isUserBrowsingOverResult) {
                    //back to default
                    me.result.clean();
                } else if (objToArray(me.navigationKeysCode).indexOf(event.keyCode) >= 0) {
                    me.userBrowsingResult(event.keyCode);
                    event.stopPropagation();
                    event.preventDefault();

                    return;
                } else if (event.keyCode == keyCode.KEY_ENTER) {
                    if (me.result.isUserBrowsingOverResult) {
                        //select item
                        me.itemSelected();
                        return;
                    } else {
                        //scope.insert()
                    }
                    if (isSafeToUse(me, 'textAreaAdjust'))
                        me.textAreaAdjust(event);

                } else {
                    if (me.text.trim() == '') {
                        me.wordsObj = [];
                        me.selectedList = [];
                        me.result.clean();
                    }

                    var lastChar = me.text.last();

                    if (lastChar == null || lastChar.trim() == '' || lastChar == ' ' || lastChar == ',' || me.itemHasBeenSelected == true) {
                        me.itemHasBeenSelected = false;
                        return false;
                    }

                    var words = me.text.trim().split(' ');
                    if (words == null && me.lastWord.length == 0)
                        return;
                    me.words = words;
                    me.lines = me.text.split(/\r?\n/);
                    me.lastWord = words.last().replace(/[\r\n]/g, "");//.trimLeft();

                    me.searchTxt = me.lastWord;

                    if (me.lastWord == null || me.lastWord.trim() == '') {
                        me.result.clean();
                        input.val('');
                        return;
                    }

                    me.keyUp(event, me.element);
                }
            }
            me.keyUp = function (event, elm) {
                me.element = elm;

                if (me.text != null && me.text.length >= 1 && me.lastWasSpaceOrEmpty) {

                    var firstChart = me.lastWord.substr(0, 1);

                    if ((firstChart == "@" && firstChart.trim() != "") || me.inSearchState == true) {
                        me.inSearchState = true;
                        if (me.lastWord.length == 1) {
                            try {
                                var canvas = document.createElement('canvas');
                                var ctx = canvas.getContext("2d");
                                ctx.font = me.textArea.css('font-size') + " " + me.textArea.css('font-family');
                                var lines = me.text.split(/\r?\n/)
                                var str = lines.last();//me.text;
                                console.log('lastLine: ', str);
                                me.paddingLeft = ctx.measureText(str).width;
                            } catch (e) {
                                me.paddingLeft = me.text.length * 7;
                            }
                        } else if (me.lastWord.length >= 2) {
                            me.search(me.lastWord, function (data) {
                                me.result.show = true;
                                me.result.left = parseInt(event.currentTarget.offsetLeft) + parseInt(me.paddingLeft * me.textCoef);
                                me.result.top = event.currentTarget.offsetTop + 20;
                                //if (!me.$$phase)
                                //    me.$apply();
                            });
                        }
                    } else {
                        me.result.clean();
                    }
                } else {
                    me.lastWasSpaceOrEmpty = event.keyCode == keyCode.KEY_SPACE;
                }
            };
            me.keyPress = function (e) {
                if (e.keyCode === 13) {
                    if (me.text != null) {
                        //   me.text += ' ';
                    }
                }
            }
            me.serverSearch = function (txt, after) {
                if (me.result.busy == true)
                    return;

                me.result.busy = true;
                if (isSafeToUse(me, 'root')) {
                    me.root.action(me.action.name, { SearchCriteria: txt, EntityID: me.feedId, PageIndex: 1 }, function (data) {
                        if (compareIfSafe(data, 'Success', true)) {
                            me.result.busy = false;
                            if (data.Result.length > 0) {
                                me.result.list = data.Result.equals({ Type: me.root.serverParameters.enums.AngularAutoCompleteType.User });
                                me.result.hasLoaded = txt.length >= 4 && data.HasMorePages == false;
                            } else {
                                me.result.hasLoaded = false;
                            }

                            if (typeof (after) == 'function')
                                after(data);

                            if (me.pendingSearch != '' && me.pendingSearch != txt && me.pendingSearch.length > txt.length)
                                me.serverSearch(me.pendingSearch, after);
                        }
                    });
                }
            }
            me.localSearch = function (txt, after) {
                me.result.searchTxt = txt;
            };
            me.search = function (txt, after) {
                if (me.words.length >= 2 && txt.indexOf('@') == -1) {
                    var before = me.words[me.words.length - 2].substr(0, 1);
                    if (before != "\n" && before.toUpperCase() == before.substr(0, 1) && me.stopWords.indexOf(before.toLowerCase().trim()) == -1) {
                        txt = me.words[me.words.length - 2] + ' ' + txt;
                        console.log('2 words: ', txt);
                        me.result.busy = false;
                        me.result.hasLoaded = false;
                    }
                }
                txt = txt.replace('@', '');
                if (me.result.busy == true) {
                    console.log('pending: ', txt);
                    me.pendingSearch = txt;
                    me.localSearch(txt, after);
                } else if (me.result.hasLoaded == true) {
                    console.log('localSearch', txt)
                    me.localSearch(txt, after)
                } else {
                    console.log('serverSearch', txt);
                    me.serverSearch(txt, after);
                }
            }
            me.userBrowsingResult = function (key) {
                if (me.result.isUserBrowsingOverResult == false) {
                    //first time
                    me.result.isUserBrowsingOverResult = true;
                    me.result.index = 0;
                } else {
                    if (key == me.navigationKeysCode.KEY_DOWN) {
                        if (me.result.index < me.result.list.length - 1) {
                            me.result.index++;
                        } else {
                            me.result.index = 0;
                        }
                    } else if (key == me.navigationKeysCode.KEY_UP) {
                        if (me.result.index == 0) {
                            me.result.index = me.result.list.length - 1;
                        } else {
                            me.result.index--;
                        }
                        return;
                    }
                }

            }
            me.itemClicked = function (item, index) {
                me.result.index = index;
                me.itemSelected(item, false);
            };
            me.itemSelected = function (element, apply) {
                me.inSearchState = false;
                var list = me.getFilteredList();
                var item = list[me.result.index];

                me.insertSelectedItem(item, apply);
            };
            me.insertSelectedItem = function (item, apply) {
                if (item != null) {
                    if (me.selectedList == undefined || me.selectedList == null)
                        me.selectedList = [];
                    me.selectedList.push(item.EntityID);
                    me.selectedItems.push(item);

                    me.itemHasBeenSelected = true;
                    me.result.clean();

                    var txt = me.text;//me.mainTag.val();
                    me.words.foreach(function (item, index) {
                        me.wordsObj[index] = me.words[index];
                    });
                    var indexWords = Math.max(0, me.words.length - 1)
                    me.words[indexWords] = item.Name + ' ';
                    me.wordsObj[Math.max(0, me.wordsObj.length - 1)] = item;
                    //lines
                    var lastLine = me.lines.last();
                    var lastLineWords;
                    if (lastLine != null) {
                        lastLineWords = lastLine.split(' ');
                        lastLineWords[lastLineWords.length - 1] = item.Name + ' ';
                    } else {
                        lastLineWords = [];
                        lastLineWords[0] = item.Name + ' ';
                    }

                    me.lines[Math.max(0, me.lines.length - 1)] = lastLineWords.join(' ');

                    var txt1 = '';
                    for (var i = 0; i < me.lines.length; i++) {
                        txt1 += me.lines[i]
                        if (i < me.lines.length - 1)
                            txt1 += '\n';
                    }

                    //var newText = me.words.join(' ');
                    me.text = txt1;
                    me.createTags();

                    me.result.close(true);
                }
            }
            me.createTags = function () {

                if (me.selectedList == null || me.selectedList.length == 0) {
                    me.comment = me.text;
                    me.selectedItems = [];
                } else {
                    var txt = me.text;

                    me.selectedItems = me.selectedItems.distinct('EntityID');
                    for (var i = 0; i < me.selectedItems.length; i++) {
                        var tagHtml = '';
                        var item = me.selectedItems[i];
                        if (txt.indexOf(item.Name) >= 0) {
                            var newTag = $('<userinfo-picture></userinfo-picture>')
                                .attr('name', item.Name)
                                .attr('userid', item.EntityID)
                                .attr('image', item.ImageUrl)
                                .attr('type', "Name");

                            tagHtml = me.getElementHtml(newTag);
                            txt = txt.replaceAll(item.Name, tagHtml)
                        }
                    }

                    me.comment = txt;
                }

            };
            me.getElementHtml = function (element) {
                var k = $('<a></a>');
                $(element).appendTo(k);
                return k.html();
            }
            me.setCursorToEnd = function (remove) {
                if (me.mainTag == null)
                    return;

                $($timeout(function () {
                    $(me.mainTag).focus();
                    try {
                        var tmp = $('<em></em>', { html: '&nbsp;' }).appendTo(me.mainTag),
                            node = tmp.get(0),
                            range = null,
                            sel = null;

                        if (document.selection) {
                            range = document.body.createTextRange();
                            range.moveToElementText(node);
                            range.select();
                        } else if (window.getSelection) {
                            range = document.createRange();
                            var nodeMf = me.mainTag.find('.userTagTextarea').last().get(0);

                            if (typeof (nodeMf) != 'undefined')
                                range.setStartAfter(nodeMf);

                            range.insertNode(node);
                            range.collapse(false);
                            sel = window.getSelection();
                            sel.removeAllRanges();
                            sel.addRange(range);
                        }
                        if (remove == true)
                            tmp.remove();
                    } catch (e) {

                    }


                    return this;
                }, 20));
            }
            me.getCursorPosition = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();
                    var count = 0;
                    var currentNode = sel.focusNode;
                    var cursorPosition = 0;
                    var previous = currentNode.previousSibling == null ? currentNode.parentNode.previousSibling : currentNode.previousSibling;
                    if (previous != null) {
                        while (previous) {
                            if (previous.nodeType == 3) {
                                count += previous.length;
                            } else if (previous.nodeType == 1) {
                                count += previous.outerHTML.length;
                                count -= 2;
                            }
                            previous = previous.previousSibling;
                        }
                        if (currentNode.parentNode.tagName.toUpperCase() == "EM") {
                            cursorPosition = count + currentNode.parentNode.outerHTML.length;
                        } else {
                            cursorPosition = sel.focusOffset + count;
                        }

                    } else {
                        cursorPosition = sel.focusOffset - 1;
                    }

                    tmp.remove();
                    return cursorPosition;
                }
            };
            me.getLastTypedChar = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();

                    var currentNode = sel.focusNode;
                    var last = currentNode.wholeText.substring(sel.focusOffset - 1, sel.focusOffset);

                    tmp.remove();
                    return last;
                }
            };
            me.getLastTypedWord = function () {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                var tmp = $('<span />').appendTo(me.mainTag),
                    node = tmp.get(0),
                    range = null,
                    sel = null;
                if (window.getSelection) {
                    range = document.createRange();
                    range.selectNode(node);
                    sel = window.getSelection();

                    var currentNode = sel.focusNode;
                    var lastBlock = currentNode.wholeText;
                    var last = lastBlock.split(' ');
                    tmp.remove();
                    if (last != null && last.length > 0) {
                        return last.last();
                    } else {
                        return '';
                    }

                }
            };
            me.insert = function () {
                if (typeof (me.insertfn) == 'function') {
                    me.insertfn(me.inputtext);
                }
            };
            me.getFilteredList = function () {
                var list = [];
                for (var i = 0; i < me.result.list.length; i++) {
                    var item = me.result.list[i]
                    if (me.filterNoAccent(item)) {
                        list.push(item);
                    }
                }
                return list;
            }
            me.filterNoAccent = function (item) {
                if (me.searchTxt == null || me.searchTxt == '')
                    return true;

                var search = me.searchTxt.toLowerCase().removeAccent();
                return item.Name.toLowerCase().indexOf(search) >= 0 ||
                    item.NameNoAccent.toLowerCase().indexOf(search) >= 0;
            };

            me.click = function (event) {
                if (typeof (me.onFocus) == 'function') {
                    me.onFocus();
                }
            }
            me.onLoad = function () {
                me.$watch('comment', function (value) {
                    if (value == null || value == '' || value.length == 0) {
                        me.result.cleanText();
                        me.selectedItems = [];
                        me.selectedList = [];
                        if (isSafeToUse(me, 'textAreaAdjust'))
                            me.textAreaAdjust({ keyCode: keyCode.KEY_ESC });
                    }
                });
                me.$parent.$parent._contentEditable = me;
            };
            me.editHtml = function (html) {
                if (me.mainTag == null)
                    me.mainTag = $(me.element).find('div[contenteditable="true"]');

                me.mainTag.html(html);
                //me.setCursorToEnd(true);
            };
        }

    };
}])

ILangApp.directive('ilangMention', ['$q', function ($q) {
    return {
        restrict: 'E',
        replace: true,
        //templateUrl: getStaticUrl('/Templates/Search/TextAreaMention.html'),
        templateUrl: function () { return getAngularTemplate('Search', 'TextAreaMention'); },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.simplePeople = [];
            me.busy = false;
            me.hasLoaded = false;
            me.feedUserComment = '';
            me.feedUserHtmlTooltip = null;
            me.macros = {};
            me.selecteds = null;
            me.PageIndex = 1;
            me.HasMorePages = null;
            me.lastSavedData = null;
            me.promise = null;
            me.root = getRootScope(me);
            me.index = me.$parent.$parent.$index;
            me.action = getPropertyByName(me, 'root.serverParameters.actions._contentEditableReadAction');
            me.newTxt = null;
            me.getList = function (txt, after) {
                if (me.busy == false) {
                    me.busy = true;
                    me.first = false;
                    var param = { SearchCriteria: txt, PageIndex: me.PageIndex };
                    var extraParam = getPropertyByName($attrs, 'param');
                    if (extraParam != null && extraParam.length > 0) {
                        var blocks = extraParam.split(',');
                        for (var i = 0; i < blocks.length; i++) {
                            param[blocks[i].split(':')[0].trim()] = blocks[0].split(':')[1].trim();
                        }
                    }

                    if (isSafeToUse(me, 'root')) {
                        me.root.action(me.action.name, param, function (data) {
                            if (compareIfSafe(data, 'Success', true)) {
                                me.busy = false;
                                me.HasMorePages = data.HasMorePages;
                                if (data.Result.length > 0) {

                                    var list = data.Result.equals({ Type: me.root.serverParameters.enums.AngularAutoCompleteType.User });
                                    //list = list.hasTextNA({ Name: me.newTxt, NameNoAccent: me.newTxt.removeAccent() });
                                    me.simplePeople = list.select({
                                        label: 'Name',
                                        name: 'Name',
                                        imageUrl: 'ImageUrl',
                                        id: 'EntityID'
                                    });

                                    me.lastSavedData = me.simplePeople;
                                    me.hasLoaded = true;
                                } else {
                                    console.log('no data');
                                    me.simplePeople = me.lastSavedData || [];
                                    me.hasLoaded = false;
                                }

                                if (typeof (after) == 'function')
                                    after(data);
                            }
                        });
                    }
                }
            };
            me.searchPeople = function (txt, after) {
                me.newTxt = txt;
                if (me.busy == false && txt.trim() != '') {

                    if (txt.trim().length >= 3) {
                        clearTimeout(me.promise);
                        me.promise = window.setTimeout(function () {
                            me.getList(txt, after);
                        }, 150);
                    }
                }
            };

            me.getPeopleText = function (item) {

                if (me.selecteds == null)
                    me.selecteds = [];

                me.selecteds.push({ item: item, label: '@' + item.label });
                return '@' + item.label;
            };
            me.getSelectedsIDs = function () {
                if (me.selecteds != null && me.selecteds.length > 0) {
                    var list = me.selecteds.select('item').select('id');
                    return list;
                } else {
                    return null;
                }
            }
            me.getPeopleTextRaw = function (item, a, b, c) {
                return '@' + item.label;
            };
            me.getTag = function (item) {
                var _item = item.item;
                var newTag = $('<userinfo-picture></userinfo-picture>')
                    .attr('name', _item.name)
                    .attr('userid', _item.id)
                    .attr('image', _item.imageUrl)
                    .attr('type', "Name");
                var tagHtml = me.getElementHtml(newTag);
                return tagHtml;

            };
            me.getElementHtml = function (element) {
                var k = $('<a></a>');
                $(element).appendTo(k);
                return k.html();
            }
            me.onfous = function () {
                try {
                    if (typeof ($attrs.onfocusfn) == 'string') {
                        if (isSafeToUse(me, 'parent.' + $attrs.onfocusfn))
                            me.parent[$attrs.onfocusfn]($attrs.id);
                    }
                } catch (e) {

                }


            }
            me.onthisload = function () {
                if (typeof ($attrs.comment) == 'string') {
                    me.parent = getScopeByProp(me, $attrs.comment.split('.')[0]);
                    if (me.parent != null)
                        me.parent.mention = me;
                }

            }
            me.getHtml = function () {
                if (me.selecteds != null) {
                    me.feedUserHtmlTooltip = me.feedUserComment;
                    for (var i = 0; i < me.selecteds.length; i++) {
                        var item = me.selecteds[i];
                        var tag = me.getTag(item);
                        me.feedUserHtmlTooltip = me.feedUserHtmlTooltip.replace(item.label, tag);
                    }
                    if (me.parent != null) {
                        setIfSafe(parent, $attrs.comment, me.feedUserHtmlTooltip);
                    }
                }
                if (me.feedUserHtmlTooltip == null) {
                    return me.feedUserComment;
                } else {
                    return me.feedUserHtmlTooltip;
                }
            }
            me.clean = function () {
                me.feedUserComment = '';
                me.feedUserHtmlTooltip = null;
            }
        }
    };
}]);

function registerInputBehavior(me, attr, timeout) {
    me.searchTxt = '';
    me.searchList = [];
    me.selectList = [];
    me.smallListHoverItem = -1;
    me.lastSearchCriteria = null;
    me.isSearchBoxActive = false;
    me.promiseSearch = null;
    me.searchActive = false;
    me.startSearchTypeAmount = 3;
    me.hybridSearch = compareIfSafe(attr, 'hybridSearch', 'true');
    me.clientSearch = '';
    me.searchSummary = '';
    me.key = getPropertyByName(attr, 'key');
    me.rootScope = getRootScope(me);
    me.rootScope.searchCtrl = me;
    me.forbidenKey = { CAPS_LOCK: 20, SHIFT: 16, CTRL: 17, ALT: 18, CLEAR: 144, COMMAND: 91 };
    me.keyCode = { KEY_ENTER: 13, KEY_DOWN: 40, KEY_UP: 38, KEY_ESC: 27, KEY_RIGHT: 39 };
    me.didSearch = false;

    me.backToDefault = function () {
        me.searchTxt = '';
        me.didSearch = false;
        me.lastSearchCriteria = null;
        if (me.promiseSearch != null)
            timeout.cancel(me.promiseSearch);

        me.doSearch(true);
    };
    me.doSearch = function (BackTodefault) {
        me.isSearchBoxActive = false;
        if (typeof (me.action) != 'undefined' && me.action != '') {
            me.rootScope.action(me.action, { searchCriteria: me.searchTxt }, function (result) {

            });
        }
        if (typeof (me.searchfn) == 'function') {
            me.searchfn({ searchParameters: { searchCriteria: me.searchTxt, searchSummaryFn: me.bindSearchSummary, backTodefault: BackTodefault == null ? false : BackTodefault } });
        }
    };
    me.bindSearchSummary = function (result) {
        if (me.key != null)
            me.rootScope[me.key] = me.searchTxt;

        var totalCount = getPropertyByName(result, 'TotalCount');
        me.searchSummary = me.rootScope.df__searchSummary(totalCount, me.searchTxt);
    };

    me.cleanSearch = function (search) {
        me.searchTxt = '';
        me.didSearch = false;
        me.lastSearchCriteria = null
        me.searchList = [];
        me.selectList = [];
        if (search == null || search == true)
            me.doSearch(true);

    };
    me.cleanResultList = function () {
        me.smallListHoverItem = -1;
        me.didSearch = false;
        me.isSearchBoxActive = false;
        me.searchActive = false;
        me.lastSearchCriteria = null
    }
    me.closeSearchBox = function () {
        me.searchTxt = '';
        me.cleanResultList();
    };
    me.keyScrolling = function (key) {
        me.userIsNavigatingOnSearchOptions = false;
        if (key == keyCode.KEY_ENTER) {
            if (typeof (me.onItemIsSelected) == 'function') {
                me.onItemIsSelected(me.searchList[me.smallListHoverItem]);
            } else {
                me.itemHasBeenSelected(me.searchList[me.smallListHoverItem]);
                me.closeSearchBox();
            }

            return;
        }
        var incremento;
        if (key == keyCode.KEY_DOWN) {
            incremento = 1;
        } else if (key == keyCode.KEY_UP) {
            incremento = -1;
        }
        if (incremento == -1 && me.smallListHoverItem <= 0) {
            me.smallListHoverItem = me.searchList.length - 1;
        } else if (incremento == 1 && me.smallListHoverItem == me.searchList.length - 1) {
            me.smallListHoverItem = 0
        } else {
            me.smallListHoverItem += incremento;
        }
    };
    me.inputSearchFocused = function (event) {

        me.isSearchBoxActive = true;
        if (me.searchTxt != '' && me.searchList.length > 0) {
            me.searchActive = true;
        }

        event.stopPropagation();
        event.preventDefault();
    };
    me.inputSearchBlur = function (event) {
        me.smallListHoverItem = -1;
        //me.searchActive = false;
        event.stopPropagation();
        event.preventDefault();
    };

    me.inputSearchTyping = function (event) {

        if (objToArray(me.forbidenKey).indexOf(event.keyCode) >= 0) {
            return false;
        }
        if (me.searchTxt == '' && event.keyCode == keyCode.KEY_ENTER) {
            return false;
        }
        if (event.keyCode == keyCode.KEY_ESC) {
            if (me.searchTxt == '') {
                return false;
            } else {
                me.backToDefault();
                return false;
            }
        }
        if (event.keyCode == keyCode.BACKSPACE && me.promiseSearch != null) {
            timeout.cancel(me.promiseSearch);
        }
        //ENTER + EMPTY SEARCH
        if (me.searchTxt == '' && event.keyCode != keyCode.KEY_ENTER) {
            me.backToDefault();
            event.stopPropagation();
            event.preventDefault();
            return false;
        }
        //scrolling
        if (me.isSearchBoxActive == true && (event.keyCode == keyCode.KEY_DOWN || event.keyCode == keyCode.KEY_UP || (event.keyCode == keyCode.KEY_ENTER && me.smallListHoverItem >= 0))) {
            me.keyScrolling(event.keyCode);
            return;
        }
        var fn = function () {

            if (me.searchTxt == '' || me.searchTxt.length < me.startSearchTypeAmount)
                return;

            me.cleanResultList();
            me.didSearch = true;
            me.doSearch();
            me.lastSearchCriteria = me.searchTxt;
            event.stopPropagation();
            event.preventDefault();
        }
        //ENTER
        if (event.keyCode == keyCode.KEY_ENTER) {
            fn();
            return false;
        }

        //NO SEARCH 
        if (me.searchTxt.length < me.startSearchTypeAmount) {
            return;
        } else if (me.lastSearchCriteria != me.searchTxt) {
            if (me.hybridSearch) {
                //search on server at 3 character and then just client search
                if (me.searchTxt.length == me.startSearchTypeAmount) {
                    fn();
                    return;
                } else if (me.searchTxt.length > me.startSearchTypeAmount) {
                    if (me.didSearch == false) {
                        fn();
                        return
                    }
                    me.clientSearch = me.searchTxt
                }
            } else {
                if (me.promiseSearch != null)
                    timeout.cancel(me.promiseSearch);

                me.promiseSearch = timeout(function () {
                    fn();
                    return;
                }, 300);
            }
        }
    }
    me.inputSearchPress = function (event) {
        if (event.keyCode == keyCode.KEY_ENTER) {
            event.stopPropagation();
            event.preventDefault();
        }
        return;
    };
    me.onItemIsSelected = function (item) {
        me.searchList.remove({ EntityID: item.EntityID });
        me.selectList.push(item);
        me.closeSearchBox();
    }
    me.itemClick = function (item) {
        me.onItemIsSelected(item);
    }
    me.filterNoAccent = function (item) {
        if (me.hybridSearch == false)
            return true;

        var search = me.searchTxt.toLowerCase().removeAccent();
        return item.Name.toLowerCase().indexOf(search) >= 0 ||
            item.NameNoAccent.toLowerCase().indexOf(search) >= 0;
    }
}

ILangApp.directive('highligthList', function () {
    return {
        restrict: 'A',
        scope: {
            searchtxt: '@'
        },
        link: function (scope, element, attrs) {
            if (isSafeToUse(attrs, 'highligthList')) {
                var txt = attrs.highligthList.toLowerCase();
                var search = scope.searchtxt.toLowerCase();
                var index = txt.indexOf(search);
                if (index >= 0) {
                    var normal = attrs.highligthList.substr(index, search.length);
                    var before = attrs.highligthList.substr(0, index);
                    var after = attrs.highligthList.substr(index + search.length, txt.length);
                    var str = before + '<strong>' + normal + '</strong>' + after;
                    var html = $.parseHTML(str)
                    $(element).html(html);
                } else {
                    $(element).text(attrs.highligthList);
                }

            }
        }
    };
});

ILangApp.directive('highlightSearch', function () {
    //No Atributo key, passar o mesmo key do elemento <list-search/>
    return {
        restrict: 'A',
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.rootScope = getRootScope($scope);
            me.key = $attrs.highlightSearch;

            if (me.key == undefined || me.key == null)
                return;

            me.element = $($element);

            if (isSafeToUse(me.rootScope, me.key) || true) {
                me.rootScope.$watch(me.key, function (value) {
                    if (value != undefined && value != null)
                        me.element.highlight(value);
                });
            }
        }
    };
});

ILangApp.directive('focusOn', function () {
    return function (scope, elem, attr) {
        scope.$on('focusOn', function (e, name) {
            if (name === attr.focusOn) {
                elem[0].focus();
            }
        });
    };
});

ILangApp.directive('focusOnLoad', function () {
    return {
        restrict: 'A',
        link: function (scope, tElement, attrs) {
            $(tElement).focus();
        }
    };
});

ILangApp.directive('selectBrstates', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '='
        },
        controller: function ($scope, $element, $attrs) {
            $scope.value = parseInt($attrs.selected);
        },
        templateUrl: getStaticUrl('/Templates/Select/Dropdown_Brazil_States.html')
    };
});

ILangApp.directive('selectInteger', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '='
        },
        controller: function ($scope, $element, $attrs) {
            $scope.arr = [];
            var start = parseInt($attrs.start);
            var end = parseInt($attrs.end);
            //$scope.selected = parseInt($attrs.selected);
            for (var i = start; i <= end; i++) {
                $scope.arr.push(i);
            }
        },
        template: function (a, b, c) {
            return '<select ng-model="model" ng-options="i for i in arr"></select>';
        }
    };
});

ILangApp.directive('smallList', ['$templateCache', function ($templateCache) {
    return {
        restrict: 'E',
        replace: true,
        controller: function ($scope, $element, $attrs) {
            var sc = getRootScope($scope);
            if (sc != null && isSafeToUse(sc, 'smallContext.enabledList'))
                sc.smallContext.enabledList = typeof ($attrs.template) != 'undefined';
        },
        compile: function (tElement, tAttrs, transclude) {
            if (typeof (tAttrs.template) != 'undefined' && tAttrs.template.indexOf('tpl') == -1) {
                var html = $.parseHTML($('#' + tAttrs.template).html().trim());
                $(html).attr('ng-repeat', 'item in smallContext.list');
                $(html).attr('ng-class', "{active: smallContext.isSearchBoxActive == true && $index == smallContext.smallListHoverItem }");

                tElement.html(html);
            }
        }
    };
}]);

ILangApp.directive('bigList', function () {
    var id = 0;
    return {
        restrict: 'E',
        replace: true,
        controller: function ($scope, $element, $attrs) {

            if (typeof ($attrs.listSize) != 'undefined') {
                var ps = parseInt($attrs.listSize);
                var k = $scope.bigContext.listSize = ps;
            }
            if ($attrs.hasOwnProperty('trackHash')) {
                $scope.bigContext.trackHash = true;
            }

        },
        compile: function (tElement, tAttrs, transclude) {
            tElement.html($('#' + tAttrs.template).html());
        }
    };
});

ILangApp.directive('legendFilter', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Filter/LegendBar.html'),
        controller: function ($scope, $element, $attrs) {
            var key = $attrs.key;

            if (compareIfSafe($attrs, 'trackHash', 'true'))
                $scope.serverParameters.filters[key].trackHash = true;
            if (compareIfSafe($attrs, 'trackHash', 'false'))
                $scope.serverParameters.filters[key].trackHash = false;


            $($scope.serverParameters.filters[key].list).each(function (a, item) {
                item.selected = true;
            });

            if ($attrs.hasOwnProperty("alias")) {
                $scope.serverParameters.filters[key].alias = $attrs.alias
            } else {
                $scope.serverParameters.filters[key].alias = key;
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var key = tAttrs.key;
            var data = 'serverParameters.filters["' + tAttrs.key + '"].list';
            var drop = 'serverParameters.filters["' + tAttrs.key + '"]';

            var a = $(tElement).find('[repeater]').eq(0)
            a.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            if (getPropertyByName(tAttrs, 'click') != null) {
                a.attr('ng-click', tAttrs.click + '(item, ' + drop + ')');
            } else {
                var input = $(tElement).find('input').eq(0)
                input.attr('ng-change', 'df__filterMulti(item, ' + drop + ')');
            }
            $(tElement).find('input').eq(0).attr('id', key + '_' + "{{$index}}");
            $(tElement).find('label').eq(0).attr('for', key + '_' + "{{$index}}");

        }
    };
});

ILangApp.directive('statusBar', function () {
    //Filter Bar <filter-bar></filter-bar>
    //key -> the name of the enum passed on the server using the method addFilter<enum>()
    //label -> change the text before the links. Default:"Status:"

    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) { return generalTemplateUrl(elem, attrs, 'Filter', 'StatusBar') },
        controller: function ($scope, $element, $attrs) {
            var key = $attrs.key;
            var enumValue = null;
            if (compareIfSafe($attrs, 'trackHash', 'true'))
                $scope.serverParameters.filters[key].trackHash = true;
            if (compareIfSafe($attrs, 'trackHash', 'false'))
                $scope.serverParameters.filters[key].trackHash = false;


            if ($scope.track.read($attrs.key) != null) {
                enumValue = $scope.track.read($attrs.key);
            } else if ($attrs.hasOwnProperty("default")) {
                var name = $attrs.default;
                enumValue = $scope.serverParameters.enums[key][name];
            }

            if (enumValue != null) {
                $scope.serverParameters.filters[key].selectedId = enumValue;
                $scope.serverParameters.filters[key].selected = enumValue;

                $($scope.serverParameters.filters[key].list).each(function (a, item) {
                    item.selected = item.value == enumValue;
                });
            }

            if ($attrs.hasOwnProperty("alias")) {
                $scope.serverParameters.filters[key].alias = $attrs.alias
            } else {
                $scope.serverParameters.filters[key].alias = key;
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var data = 'serverParameters.filters["' + tAttrs.key + '"].list';
            var drop = 'serverParameters.filters["' + tAttrs.key + '"]';

            var a = $(tElement).find('a').eq(0)
            a.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            if (getPropertyByName(tAttrs, 'click') != null) {
                a.attr('ng-click', tAttrs.click + '(item, ' + drop + ')');
            } else {
                a.attr('ng-click', 'df__filter(item, ' + drop + ')');
            }


            if (typeof (tAttrs.label) != 'undefined') {
                $(tElement).find('em').eq(0).text(tAttrs.label);
            }
        }
    };
});

ILangApp.directive('statusBarBatchSheet', function () {
    //Filter Bar <filter-bar></filter-bar>
    //key -> the name of the enum passed on the server using the method addFilter<enum>()
    //label -> change the text before the links. Default:"Status:"

    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) { return generalTemplateUrl(elem, attrs, 'Filter', 'StatusBarBatchSheet') },
        controller: function ($scope, $element, $attrs) {
            var key = $attrs.key;
            var enumValue = null;
            if (compareIfSafe($attrs, 'trackHash', 'true'))
                $scope.serverParameters.filters[key].trackHash = true;
            if (compareIfSafe($attrs, 'trackHash', 'false'))
                $scope.serverParameters.filters[key].trackHash = false;


            if ($scope.track.read($attrs.key) != null) {
                enumValue = $scope.track.read($attrs.key);
            } else if ($attrs.hasOwnProperty("default")) {
                var name = $attrs.default;
                enumValue = $scope.serverParameters.enums[key][name];
            }

            if (enumValue != null) {
                $scope.serverParameters.filters[key].selectedId = enumValue;
                $scope.serverParameters.filters[key].selected = enumValue;

                $($scope.serverParameters.filters[key].list).each(function (a, item) {
                    item.selected = item.value == enumValue;
                });
            }

            if ($attrs.hasOwnProperty("alias")) {
                $scope.serverParameters.filters[key].alias = $attrs.alias
            } else {
                $scope.serverParameters.filters[key].alias = key;
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var data = 'serverParameters.filters["' + tAttrs.key + '"].list';
            var drop = 'serverParameters.filters["' + tAttrs.key + '"]';

            var a = $(tElement).find('a').eq(0)
            a.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            if (getPropertyByName(tAttrs, 'click') != null) {
                a.attr('ng-click', tAttrs.click + '(item, ' + drop + ')');
            } else {
                a.attr('ng-click', 'df__filter(item, ' + drop + ')');
            }


            if (typeof (tAttrs.label) != 'undefined') {
                $(tElement).find('em').eq(0).text(tAttrs.label);
            }
        }
    };
});

ILangApp.directive('sortBar', function () {
    //Sort Bar <sort-bar></sort-bar>
    //key -> the name of the enum passed on the server using the method addFilter<enum>()
    //label -> change the text before the links. Default:"Ordenar por:"

    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) { return generalTemplateUrl(elem, attrs, 'Filter', 'SortBar') },
        compile: function (tElement, tAttrs, transclude) {
            var data = 'serverParameters.filters["' + tAttrs.key + '"].list';
            var drop = 'serverParameters.filters["' + tAttrs.key + '"]';

            var a = $(tElement).find('a').eq(0)
            a.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            a.attr('ng-click', 'df__sort(item, ' + drop + ')');

            if (typeof (tAttrs.label) != 'undefined') {
                $(tElement).find('[barLabel]').eq(0).text(tAttrs.label);
            }
        }
    };
});

ILangApp.directive('selectSource', function ($http) {
    //<select-source source="getPages" opt-id="Key"  opt-text="Value" value="basicInfo.BDay" parameters="getPageParameters()"></select-source>
    return {
        restrict: 'E',
        replace: true,
        scope: {
            source: '@',
            parameters: '&',
            delay: '@',
            optText: '@',
            optId: '@',
            value: '=',
            par: '@'
        },
        controller: function ($scope, $element, $attrs) {
            $scope.arr = [];
            var sourceName = $attrs.source;
            var config = $scope.$parent.parameters.source[sourceName];
            var data = config.data || {};
            var otherParameters = $scope.parameters();
            for (i in otherParameters) {
                if (typeof (otherParameters[i]) != 'undefined')
                    data[i.toString()] = otherParameters[i];
            }
            var wait = 200;
            if ($attrs.delay != undefined)
                wait = parseInt($attrs.delay);

            window.setTimeout(function () {
                $http({
                    method: 'GET',
                    url: config.url,
                    params: data
                }).success(function (result) {
                    $scope.arr = result.Result;
                }).error(function () { });
            }, wait);
        },
        template: function (a, b, c) {
            return '<select ng-model="value" ng-options="item.' + b.optId + ' as item.' + b.optText + ' for item in arr"></select>';
        }
    }
});

ILangApp.directive('showMore', function () {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: getStaticUrl('/Templates/Link/showMore.html'),
        compile: function (tElement, tAttrs, transclude) {
            if (tAttrs.hasOwnProperty('click')) {
                if (tAttrs.click.indexOf('(') < 0)
                    tAttrs.click += '()';

                $(tElement).find('a').attr('ng-click', tAttrs.click);
            }
            if (tAttrs.hasOwnProperty('text')) {
                $(tElement).find('a').text(tAttrs.text);
            }
        }
    };
});

ILangApp.directive('globalDialog', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: function (element, attrs) {
            var str = '<div ng-class="{socialDialog:true, hide: serverParameters.dialogs.' + attrs.key + '.visible == false}">'
            str += '  <div class="sdBox dlgNoFrame dlgAutoHeight" >';
            str += '     <a id="imgClose" title="Fechar" class="ilangIcon sdClose" onclick="javascript:if (typeof (GD) != \'undefined\') {GD_AddOrEditQuestion.Close();}else{CloseDialog(true);}return false;" href="javascript:void(0);" ></a>';
            str += '     <div class="sdTop">';
            str += '        <p class="fb phm aLeft nw">';
            str += '            <span class="black lblTitle">' + attrs.title + '</span>';
            if (angular.isDefined(attrs.subtitle)) {
                str += '            <span class="lblSubTitle gray fms">' + attrs.subtitle + '</span>';
            } else if (angular.isDefined(attrs.subtitlefield)) {
                str += '            <span class="lblSubTitle gray fms" ng-bind="' + attrs.subtitlefield + '"></span>';
            }
            str += '        </p>';
            str += '     </div>'
            str += '     <div class="sdContent">';
            str += '         <div class="overY" ng-multi-transclude="dialog-body"></div>';
            str += '     </div>';
            str += '     <div class="sdBot" ng-multi-transclude="dialog-footer"></div>';
            str += '   </div>';
            str += '</div>';
            return str;
        },
        controller: function ($scope, $element, $attrs) {
            if (typeof ($attrs.key) != 'undefined') {
                $scope.serverParameters.dialogs = $scope.serverParameters.dialogs || {};
                if (!$scope.serverParameters.dialogs.hasOwnProperty($attrs.key))
                    $scope.serverParameters.dialogs[$attrs.key] = { visible: false };
            }

        }
    };
});

ILangApp.directive('globalDialog', function () {
    return {
        restrict: 'A',
        link: function (scope, tElement, attrs) {
            var outer = $(tElement[0])
            var inner = outer.contents();

            outer.addClass('socialDialog');

            var d10 = $('<div></div>', { 'class': 'sdBox dlgNoFrame dlgAutoHeight' }).appendTo(outer);
            var img = $('<a/>', { title: 'Fechar', 'class': 'ilangIcon sdClose' }).appendTo(d10);
            var d20 = $('<div></div>', { 'class': 'sdTop' }).appendTo(d10);
            var d21 = $('<div></div>', { 'class': 'sdContent' }).appendTo(d10);
            var d22 = $('<div></div>', { 'class': 'sdBot' }).appendTo(d10);;
            var d30 = $('<div></div>', { 'class': 'overY' }).appendTo(d21);
            $(inner).appendTo(d30);

            $(img).click(function () { $(this).parent().parent().addClass('hide'); });
        }
    };
});

ILangApp.directive('tabBar', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) {
            if (typeof (attrs.template) != 'undefined') {
                try {
                    var t;
                    if (attrs.template.indexOf('tpl/') >= 0) {
                        var name = getPropertyByName(attrs, 'template');
                        if (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl') {
                            return getStaticUrl('/Templates/Tabs/MultiTab.html');
                        } else {
                            name = name.replace('tpl/', '');
                            return '/Tabs_' + name + '.tpl';
                        }
                    } else {
                        if (attrs.template.indexOf('tabTypes') >= 0) {
                            t = eval(attrs.template);
                        } else {
                            t = eval('tabTypes.' + attrs.template);
                        }
                        return getStaticUrl('/Templates/Tabs/' + t + '.html');
                    }
                } catch (e) {
                    return getStaticUrl('/Templates/Tabs/MultiTab.html');
                }
            } else {
                return getStaticUrl('/Templates/Tabs/MultiTab.html');
            }

        },
        controller: function ($scope, $element, $attrs) {
            var value = null;
            $scope.tabCssClass = getPropertyByNameOrDefault($attrs, "css", "ilTabs");
            if ($attrs.hasOwnProperty('trackHash') && $scope.serverParameters.filters.hasOwnProperty($attrs.key))
                $scope.serverParameters.filters[$attrs.key].trackHash = getPropertyByNameOrDefault($attrs, 'trackHash', 'true') == 'true';

            if ($attrs.hasOwnProperty('trackHash') && $scope.track.read($attrs.key) != null) {
                value = $scope.track.read($attrs.key);
            } else if (typeof ($attrs.default) != 'undefined') {
                value = $scope.serverParameters.enums[$attrs.key][$attrs.default];
            }
            if ($attrs.hasOwnProperty("onchange")) {
                $scope.serverParameters.filters[$attrs.key].onChange = $attrs.onchange;
            }

            if (value != null) {
                $scope.serverParameters.filters[$attrs.key].selected = value;

                for (i in $scope.serverParameters.filters[$attrs.key].list) {
                    $scope.serverParameters.filters[$attrs.key].list[i].selected = $scope.serverParameters.filters[$attrs.key].list[i].value == value;
                }
                if ($attrs.hasOwnProperty('onload') && $scope.hasOwnProperty($attrs.onload))
                    $scope[$attrs.onload](value);
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var way = 'serverParameters.filters["' + tAttrs.key + '"]';
            var data = 'serverParameters.filters["' + tAttrs.key + '"].list';

            if ($(tElement).find('li').length == 0) {
                var li = $(tElement).find('[repeat]').eq(0)
                li.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            } else {
                var li = $(tElement).find('li').eq(0)
                li.attr('ng-repeat', 'item in ' + data + ' | filter:{ visible: true }');
            }

            var a = $(tElement).find('a').eq(0);
            a.attr('ng-click', 'df__changeTab(item, ' + way + ')');

            $(tElement).find('li[il-click]').attr('ng-click', 'df__changeTab(item, ' + way + ')');
        }
    };
});

ILangApp.directive('tabItem', function () {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        template: function (element, attrs) {
            var key = attrs.key.split('.')[0];
            var item = attrs.key.split('.')[1];

            return '<div ng-show="serverParameters.filters.' + key + '.selected == serverParameters.enums.' + attrs.key + '" ng-transclude></div>';
        },
        controller: function ($scope, $element, $attrs) {

            if (typeof ($attrs.onselected) != 'undefined') {
                var key = $attrs.key.split('.')[0];
                var name = $attrs.key.split('.')[1];

                var filter = $scope.serverParameters.filters[key].list.first({ name: name });

                if (filter != null)
                    filter.onSelected = $attrs.onselected;
            }
        }
    };
});

ILangApp.directive('userTooltip', function () {
    return {
        restrict: 'E',
        replace: true,
        //templateUrl: getStaticUrl('/Templates/Tooltip/UserInfo.html'),
        templateUrl: function () { return getAngularTemplate('Tooltip', 'UserInfo') },
        controller: function ($scope, $element) {
            // O Diego Fiorotto dise que o elemento precisa ser filho do Body.
            $($element).appendTo('body');
        }
    };
});

ILangApp.directive('confirmationDialog', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Dialog/ConfirmationDialog.html'),
        controller: function ($scope, $element) {
            var me = $scope;
            me.confirmationDialog = {
                visible: false,
                onBtoClick: null,
                htmlContent: '',
                title: 'Titulo',
                showCancelButton: true,
                open: function () {
                    me.confirmationDialog.visible = true;
                },
                close: function () {
                    me.confirmationDialog.visible = false;
                },
                click: function () {
                    if (me.confirmationDialog.onBtoClick != null)
                        me.confirmationDialog.onBtoClick();
                }
            }
        }
    };
});

function getRootScope(currentScope) {
    var max = 100;
    while (max-- > 0 && !currentScope.hasOwnProperty('IamTheRootScope')) {
        if (currentScope != null && typeof (currentScope.$parent) != 'undefined')
            currentScope = currentScope.$parent;
    }
    return currentScope;
}
function getScopeByProp(currentScope, prop) {
    var max = 100;
    if (prop.indexOf('.') > 0) {
        while (max-- > 0 && isSafeToUse(currentScope, prop) == false) {
            if (currentScope != null && typeof (currentScope.$parent) != 'undefined' && currentScope.$parent != null) {
                currentScope = currentScope.$parent;
            } else {
                return null;
            }
        }

    } else {
        while (max-- > 0 && !currentScope.hasOwnProperty(prop)) {
            if (currentScope != null && typeof (currentScope.$parent) != 'undefined' && currentScope.$parent != null) {
                currentScope = currentScope.$parent;
            } else {
                return null;
            }
        }
    }
    return currentScope;
}
function registerCommonFunctionUserInfo($scope, $element, $attrs) {

}
function getAngularTemplate(path, template) {
    var h = window.location.href;
    if (h.indexOf('student') >= 0 || h.indexOf('admin') >= 0 || h.indexOf('social') >= 0) {
        return '/' + path + '_' + template + '.tpl';
    } else {
        return getStaticUrl('/Templates/' + path + '/' + template + '.html');
    }
}
function generalTemplateUrl(elem, attrs, path, defaultFile) {
    if (typeof (attrs.template) != 'undefined') {
        try {
            var t;
            var name = getPropertyByName(attrs, 'template');
            if (name != null) {
                if (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl') {
                    return '/' + path + '_' + defaultFile + '.tpl';
                } else if (name.indexOf('tpl/') >= 0) {
                    var k = name.replace('tpl/', '');
                    return '/' + path + '_' + k + '.tpl';
                } else {
                    return getStaticUrl('/Templates/' + path + '/' + name + '.html');
                }
            } else {
                return getStaticUrl('/Templates/' + path + '/' + name + '.html');
            }
        } catch (e) {
            return getStaticUrl('/Templates/' + path + '/' + name + '.html');
        }
    } else {
        return getStaticUrl('/Templates/' + path + '/' + defaultFile + '.html');
    }
}
window.__lastUserInfoOpened = null;
ILangApp.directive('userInfo', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            userid: '@',
            name: '@',
            duration: '@'
        },
        template: '<a ng-mouseover="onMouseOver($event)" ng-mouseleave="onMouseLeave($event)">{{name}}<user-tooltip></user-tooltip></a>',
        controller: function ($scope, $element, $attrs) {
            $scope.visible = false;
            $scope.e = $element;
            $scope.data = null;
            $scope.timer = null;
            $scope.isVisible = false;
            $scope.show = function () {
                if (window.__lastUserInfoOpened != null)
                    window.__lastUserInfoOpened.close();
                $timeout(function () {
                    if ($($scope.e).is(':hover')) {
                        $("div[data-visible='y']").each(function (index, item) {
                            var id = $(item).attr('id');
                            var scope = angular.element($('#' + id)).scope();
                            if (scope && scope.close)
                                scope.close();
                        });
                        $scope.visible = true;
                        window.__lastUserInfoOpened = $scope;
                    }
                }, 100);


            };
            $scope.close = function () {
                if ($('.ilUserTooltip:hover').length == 0) {
                    $scope.visible = false;
                } else {
                    $scope.setTimeToClose();
                }

            };
            $scope.setTimeToClose = function () {
                $scope.duration = ($scope.duration != undefined ? parseInt($scope.duration) : 1000);
                $scope.timer = $timeout(function () {
                    $scope.close();
                    // console.log('time');
                    if (!$scope.$$phase)
                        $scope.$apply();
                }, $scope.duration);
            };
            $scope.onMouseOver = function (event) {
                if ($scope.data != null && $scope.isVisible) {
                    window.clearTimeout($scope.timer);
                    return;
                }

                var X = event.clientX;
                var Y = event.clientY;

                if ($attrs.userid && $scope.data == null) {
                    var id = parseInt($attrs.userid);
                    var idx = $attrs.useridcrypto;
                    $scope.$parent.$parent.ajax.get(ILangSettings.Sites.API() + '/User/GetUserInfo', { userid: idx }, function (result) {
                        $scope.data = result;
                        $scope.data["X"] = X;
                        $scope.data["Y"] = Y;
                        $scope.show();
                    });
                } else if ($scope.data != null) {
                    $timeout.cancel($scope.timer);
                    $scope.show();
                }
            };
            $scope.onMouseLeave = function (event) {
                $scope.setTimeToClose();
            };
            $scope.startNewChat = function () {
                var chatScope = angular.element('#ilangChatElement').scope();
                chatScope.openChat({ userId: $attrs.userid, chatid: null });
                $scope.close();
            };
        }
    }
});

ILangApp.directive('userpicTooltip', ['$timeout', '$window', function ($timeout, $window) {
    return {
        restrict: 'E',
        replace: true,
        //templateUrl: getStaticUrl('/Templates/Tooltip/Default.html'),
        templateUrl: function () { return getAngularTemplate('Tooltip', 'Default') },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.visible = false;
            me.showMsgIcon = $window.ILangSettings.IsUserStudent == true && $window.location.href.indexOf($window.ILangSettings.Sites.Student()) >= 0;
            me.tooltipScope = {
                x: 0,
                y: 0,
                visible: false,
                data: {
                    UserThumbnailUrl: null
                }
            };
            me.doNotClose = false;
            me.cancelFn = null;
            me.set = function (info, cancelFn) {
                me.cancelFn = cancelFn;
                for (var i in info) {
                    me.tooltipScope[i] = info[i];
                }
                me.tooltipScope.visible = true;
                $(window).on('scroll', function (a, b) {
                    $scope.close(true);
                });
            };
            me.close = function (forced) {
                try {
                    if (forced == true) {
                        me.tooltipScope.visible = false;
                        if (!me.$$phase)
                            me.$apply();
                    } else if (!me.doNotClose && !$($element).is(':hover')) {
                        me.tooltipScope.visible = false;
                    }
                } catch (e) {

                }

                //$(window).unbind('scroll');

            };
            me.onMouseOver = function (event) {
                if (typeof (me.cancelFn) == 'function')
                    me.cancelFn();
            };
            me.onMouseLeave = function (event) {
                $timeout(function () {
                    me.close();
                }, 300);
            };
            me.sendMsg = function (tooltipScope) {
                if (tooltipScope.data != null) {
                    var user = tooltipScope.data;
                    var t = tooltipScope.data.IsAdminUser == true ? 2 : 1;
                    openComposeMesageDialog({ EntityID: user.UserOrganizationId, Name: user.UserName, Type: t, ImageUrl: user.UserMiniatureUrl }, false);
                }
            };
            $scope.startNewChat = function () {
                var chatScope = angular.element('#ilangChatElement').scope();
                chatScope.openChat({ userId: me.tooltipScope.data.UserId, chatid: null });
                me.close();
            };

            $(window).on('scroll', function (a, b) {
                if (isSafeToUse($scope, 'close'))
                    $scope.close(true);
            });
        }
    };
}]);

ILangApp.directive('userinfoPicture', ['$timeout', function ($timeout) {
    //Pra utilizar esta diretiva, a pagina tem que ter a diretiva userpicTooltip
    return {
        restrict: 'E',
        replace: true,
        scope: {
            userid: '@',
            name: '@',
            duration: '@',
            image: '@',
            type: '@',
            extraCss: '@',
            useron: '='
        },
        template: function (element, attrs) {
            if (attrs.hasOwnProperty('type') && attrs.type == 'Name') {
                return '<a class="fdUserName {{extraCss}}"  ng-mouseover="onMouseOver($event)" ng-mouseleave="onMouseLeave($event)">{{name}}</a>';
            } else {
                return '<span class="pic picDefault" ng-class="{userOn: useron == true}" ng-style="{\'background-image\':\'url({{image}})\'}" ng-mouseover="onMouseOver($event)" ng-mouseleave="onMouseLeave($event)"></span>';
            }
        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            $scope.visible = false;
            $scope.e = $element;
            $scope.timer = null;
            $scope.isVisible = false;
            $scope.root = getRootScope($scope);
            $scope._tooltipScope = null;
            $scope.previouslyLoaded = {};
            $scope.currentId = null;
            $scope.show = function (info) {
                if (me._tooltipScope == null)
                    me._tooltipScope = getScopeByProp(me, 'tooltipScope');
                if (me._tooltipScope != null) {
                    me._tooltipScope.set(info, me.cancelTimer);
                }

            };
            $scope.close = function () {
                if (me._tooltipScope != null) {
                    me._tooltipScope.close();
                }
            };
            $scope.lock = function (value) {
                if (me._tooltipScope == null)
                    me._tooltipScope = getScopeByProp(me, 'tooltipScope');
                if (me._tooltipScope != null) {
                    me._tooltipScope.doNotClose = value;
                }
            };

            $scope.cancelTimer = function () {
                $timeout.cancel($scope.timer);
            };
            $scope.onMouseOver = function (event) {
                me.lock(true);
                $timeout.cancel($scope.timer);
                var X = event.clientX;
                var Y = event.clientY;
                if ($attrs.userid == '')
                    return;

                var id = parseInt($attrs.userid);
                var idx = $attrs.useridcrypto;
                $scope.currentId = id;
                $timeout(function () {
                    if ($($scope.e).is(':hover')) {
                        if ($attrs.userid && !me.previouslyLoaded.hasOwnProperty(id)) {
                            $scope.root.ajax.get(ILangSettings.Sites.API() + '/User/GetUserInfo', { UserId: idx }, function (result) {
                                me.previouslyLoaded[id] = { x: X, y: Y, data: result };
                                $scope.show({ x: X, y: Y, data: result });
                            });
                        } else {
                            $timeout.cancel($scope.timer);
                            var prev = me.previouslyLoaded[id];
                            prev.x = X;
                            prev.y = Y;
                            $scope.show(prev);
                        }
                    }
                }, 100);

            };
            $scope.onMouseLeave = function (event) {
                me.lock(false);
                var t = ($scope.duration != undefined ? parseInt($scope.duration) : 500);
                $scope.timer = $timeout(function () {
                    $scope.close();
                }, t);
            };
            $scope.startNewChat = function () {
                var chatScope = angular.element('#ilangChatElement').scope();
                chatScope.openChat({ userId: $attrs.userid, chatid: null });
                $scope.close();
            };
        }
    }
}]);

ILangApp.directive('editText', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Forms/EditText.html'),
        scope: {
            label: '@',
            value: '@',
            onsave: '=',
            parameters: '@',
            oncancel: '=',
            action: '='
        },
        controller: function ($scope, $element, $attrs) {
            $scope.editMode = false;
            $scope.lastValue = $scope.value;
            $scope.toogleMode = function () {
                $scope.editMode = !$scope.editMode;
            };
            $scope.cancel = function () {
                if (typeof ($attrs.oncancel) != 'undefined') {
                    if ($scope.$parent.$parent.hasOwnProperty($attrs.oncancel)) {
                        $scope.$parent.$parent[$attrs.oncancel]($scope.value, $scope.parameters);
                    }
                }
                $scope.undo();
            };
            $scope.confirm = function () {
                $scope.lastValue = $scope.value;
                $scope.editMode = false;
            };
            $scope.undo = function () {
                $scope.editMode = false;
                $scope.value = $scope.lastValue || '';
            };
            $scope.save = function () {
                if (typeof ($attrs.onsave) != 'undefined' && $scope.$parent.$parent.hasOwnProperty($attrs.onsave)) {
                    $scope.$parent.$parent[$attrs.onsave]($scope.value, $scope.oldValue, $scope.parameters, $scope.confirm, $scope.undo);
                } else if (typeof ($attrs.action) != 'undefined' && $scope.$parent.$parent.serverParameters.actions.hasOwnProperty($attrs.action)) {
                    $scope.$parent.$parent.action($attrs.action, { text: $scope.value, p: $scope.parameters }, $scope.confirm, $scope.cancel);
                } else {
                    $scope.confirm();
                }
            };
        }
    };
});

ILangApp.directive('multitextEditable', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Forms/MultiText.html'),
        scope: {
            dto: '='
        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            console.log($scope.dto);
        }
    };
});

ILangApp.directive('ilangSelect', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) {

            var name = getPropertyByName(attrs, 'template');
            if (name != null) {
                if ($('script[id*="Select_Dropdown_Select"]').length >= 1 && (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl')) {
                    return '/Select_Dropdown_Select.tpl';
                } else if (name.indexOf('tpl/') >= 0) {
                    var k = name.replace('tpl', '');
                    return k + '.tpl';
                } else {
                    return getStaticUrl('/Templates/Select/Dropdown_Select.html');
                }
            } else {
                return getStaticUrl('/Templates/Select/Dropdown_Select.html');
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var way = 'serverParameters.drops.' + tAttrs.key;
            var data = way + '.list';

            var model = way + '.selected';
            if (tAttrs.hasOwnProperty('model'))
                model = tAttrs.model;

            if (tAttrs.hasOwnProperty("model")) {
                $(tElement).find('select')
                    .attr('ng-disabled', tAttrs.disabled)
                    .attr('ng-class', '{bgLoading:' + way + '.loadingList}')
                    .attr('ng-model', tAttrs.model)
                    .attr('ng-options', 'item.value as item.description for item in ' + data)
                    .attr('ng-click', 'df__dropClick(' + way + ', $event)');
            } else {
                $(tElement).find('select')
                    .attr('ng-disabled', tAttrs.disabled)
                    .attr('ng-class', '{bgLoading:' + way + '.loadingList}')
                    .attr('ng-model', model)
                    .attr('ng-options', 'item.value as item.description for item in ' + data)
                    .attr('ng-change', 'df__dropSelected(null,' + way + ')')
                    .attr('ng-click', 'df__dropClick(' + way + ', $event)');
            }

            if (tAttrs.hasOwnProperty('placeholder')) {
                $(tElement).find('select').append('<option value="" selected="selected" disabled="disabled">' + tAttrs.placeholder + '</option>')
            }

        },
        controller: function ($scope, $element, $attrs, $document, $rootScope) {
            var key = $attrs.key;
            if (isSafeToUse($scope, 'serverParameters.drops.' + key) == false)
                return;

            if ($attrs.hasOwnProperty("filterLoad")) {
                $scope.serverParameters.drops[key].loadFilter = true;
            }

            if ($attrs.hasOwnProperty("alias")) {
                $scope.serverParameters.drops[key].alias = $attrs.alias;
            } else if ($scope.serverParameters.drops.hasOwnProperty(key) && $scope.serverParameters.drops[key].hasOwnProperty('alias') && $scope.serverParameters.drops[key].alias == null) {
                $scope.serverParameters.drops[key].alias = key;
            }

            if ($scope.serverParameters.drops[key].loadAction != null) {
                $scope.serverParameters.drops[key].loaded = false;
                if ($scope.serverParameters.drops[key].loadOnStart) {
                    $scope.df__loadDrop($scope.serverParameters.drops[key], function () { setListDefaultValue($scope, $attrs); });
                }
            } else {
                $scope.serverParameters.drops[key].loaded = true;
                setListDefaultValue($scope, $attrs);
            }
        }
    };
});
ILangApp.directive('ilangSource', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="ng-hide">',
        controller: function ($scope, $element, $attrs, $document, $rootScope) {
            var key = $attrs.key;
            if ($scope.serverParameters.sources[key].loadAction != null) {
                $scope.serverParameters.sources[key].loaded = false;
                if ($scope.serverParameters.sources[key].loadOnStart) {
                    var root = getRootScope($scope);
                    root.df__loadSource($scope.serverParameters.sources[key], function () { });
                } else {
                    //setListDefaultValue($scope, $attrs);
                }
            } else {
                $scope.serverParameters.sources[key].loaded = true;
            }
        }
    }
});
ILangApp.directive('ilangDrop', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, attrs) {
            if (typeof (attrs.template) != 'undefined') {
                try {
                    var t;
                    var name = getPropertyByName(attrs, 'template');
                    if (name != null) {
                        if (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl') {
                            return getStaticUrl('/Templates/Select/Dropdown_Enum.html');
                        } else if (name.indexOf('tpl/') >= 0) {
                            var k = name.replace('tpl', '');
                            if ($('script[id*="' + k.replace('/', '') + '"]').length >= 1) {
                                return k + '.tpl';
                            } else if (compareIfSafe(attrs, 'getTemplateFromServer', "true")) {
                                var templateName = k.replace('/Select_', '');
                                return getStaticUrl('/Templates/Select/' + templateName + '.html');
                            } else {
                                return getStaticUrl('/Templates/Select/Dropdown_Select.html');
                            }

                        } else if (attrs.template.indexOf('dropTypes') >= 0) {
                            t = eval(attrs.template);
                        } else {
                            t = eval('dropTypes.' + attrs.template);
                        }
                        return getStaticUrl('/Templates/Select/' + t + '.html');
                    } else {
                        return getStaticUrl('/Templates/Select/Dropdown_Enum.html');
                    }
                    return getStaticUrl('/Templates/Select/Dropdown_Enum.html');
                } catch (e) {
                    return getStaticUrl('/Templates/Select/Dropdown_Enum.html');
                }
            } else {
                return getStaticUrl('/Templates/Select/Dropdown_Enum.html');
            }
        },
        //templateUrl: getStaticUrl('/Templates/Select/Dropdown_Checklist.html'),

        controller: function ($scope, $element, $attrs, $document, $rootScope) {
            var key = $attrs.key;
            if (isSafeToUse($scope, 'serverParameters.drops.' + key) == false)
                return;


            if ($attrs.hasOwnProperty("filterLoad")) {
                $scope.serverParameters.drops[key].loadFilter = true;
            }

            if ($attrs.hasOwnProperty("alias")) {
                $scope.serverParameters.drops[key].alias = $attrs.alias;
            } else if ($scope.serverParameters.drops.hasOwnProperty(key) && $scope.serverParameters.drops[key].hasOwnProperty('alias') && $scope.serverParameters.drops[key].alias == null) {
                $scope.serverParameters.drops[key].alias = key;
            }

            if ($attrs.hasOwnProperty("onchange")) {
                $scope.serverParameters.drops[key].onChange = $attrs.onchange;
            }

            if ($attrs.hasOwnProperty("showSearchbox")) {
                $scope.serverParameters.drops[key].showSearchBox = $attrs.showSearchbox == "true";
            } else {
                $attrs.showSearchbox = $scope.serverParameters.drops[key].showSearchBox;
            }
            if ($scope.serverParameters.drops[key].showSearchBox) {
                $($element).find('input[inputSearch]').parent().show();
            } else {
                $($element).find('input[inputSearch]').parent().hide();
            }

            if ($scope.serverParameters.drops[key].loadAction != null) {
                $scope.serverParameters.drops[key].loaded = false;
                if ($scope.serverParameters.drops[key].loadOnStart) {
                    var root = getRootScope($scope);
                    root.df__loadDrop($scope.serverParameters.drops[key], function () { setListDefaultValue($scope, $attrs); });
                } else {
                    setListDefaultValue($scope, $attrs);
                }
            } else {
                $scope.serverParameters.drops[key].loaded = true;
                setListDefaultValue($scope, $attrs);
            }

            if ($attrs.hasOwnProperty('customModel')) {
                if ($scope.hasOwnProperty('customModels') == false)
                    $scope.customModels = {};
                var copy = {};
                copyAllPropersTo(copy, $scope.serverParameters.drops[key]);

                $scope.customModels[$attrs.customModel] = copy;
            }

            $rootScope.$on("document_click", function () {
                if (isSafeToUse($scope, 'serverParameters.drops')) {
                    var drop = $scope.serverParameters.drops[key];
                    if (drop != null) {
                        drop.active = false;
                        if (!$scope.$$phase)
                            $scope.$apply();
                    }
                }

            });
        },
        compile: function (tElement, tAttrs, transclude) {
            var way = 'serverParameters.drops.' + tAttrs.key;
            if (typeof (tAttrs.customModel) != 'undefined') {
                way = 'customModels.' + tAttrs.customModel;
            }
            var data = way + '.list';

            if (typeof (tAttrs.title) != 'undefined') {
                var _dtitle = tAttrs.title;
                if (_dtitle.indexOf('R.') == 0) {
                    try {
                        _dtitle = eval(_dtitle);
                    } catch { }
                }
                $(tElement).find('span[il-title]').text(_dtitle);
                $(tElement).attr('title', _dtitle);
            } else {
                //$(tElement).find('span[il-title]').remove();
                $(tElement).find('span[il-title]').attr('ng-bind', way + '.title').attr('ng-show', way + '.title != ""');
            }

            $(tElement).find('select')
                .attr('ng-model', way + '.selected')
                .attr('ng-options', "i.value as i.description for i in " + data)
                .attr('ng-disabled', way + '.disabled')
                .attr('ng-change', 'df__dropSelectChange(' + way + ')')

            $(tElement).parent().find('.ilDropdown').attr('ng-class', "{disabled:" + way + '.disabled}');
            if (tAttrs.hasOwnProperty('onchangedrop'))
                $(tElement).find('select').attr('ng-change', tAttrs.onchangedrop);
            if (tAttrs.hasOwnProperty('onchange'))
                $(tElement).find('select').attr('ng-change', tAttrs.onchange);
            if (tAttrs.hasOwnProperty('model'))
                $(tElement).find('select').attr('ng-model', tAttrs.model);
            if (tAttrs.hasOwnProperty('extraclass'))
                $(tElement).find('select').addClass(tAttrs.extraclass);

            //ng-model="model" ng-options="i for i in arr"
            var li = $(tElement).find('[il-target]').eq(0)
            li.attr('ng-repeat', 'item in ' + data + ' | filter:{ fixedOption:false, visible:true}');

            $(tElement).find('li[li-target-alloptions]').eq(0).attr('ng-repeat', 'item in ' + data + ' | filter:{ fixedOption:true}');
            $(tElement).find('[il-classActive]').attr('ng-class', '{active: ' + way + '.active }');
            $(tElement).find('a[il-clicktoggle]').attr('ng-click', 'df__dropClickToggle(item, ' + way + ', $event);');
            $(tElement).find('div[il-showHide]')
                .attr('ng-show', way + '.active')
                .attr('ng-mouseover', 'df__dropCancelHover(item, ' + way + ')');

            $(tElement).find('[il-selectedtext]')
                .attr('ng-bind-html', way + '.selectedText | trusted')
                .attr('ng-attr-title', '{{' + way + '.selectedText}}')
                .attr('ng-if', way + '.selectedText != null');

            $(tElement).find('a[il-repeatElement]')
                .attr('ng-click', 'df__dropSelected(item, ' + way + ')')
                .attr('ng-class', '{active : item.selected && ' + way + '.hoverIndex != item.index, mouseover :' + way + '.hoverIndex == item.index && ' + way + '.keyboardNavigation == true}');

            $(tElement).find('[il-ckecklist-item]')
                .attr('ng-click', 'df__dropChecked(item, ' + way + ', $event)');

            if ($(tElement).attr('il-drop-load') != undefined)
                $(tElement).attr('ng-init', 'df__dropChecklistSetText(' + way + ')');

            $(tElement).find('[ulBgLoading]').attr('ng-class', '{bgLoading:' + way + '.loadingList}');

            var uniqueID = '{{' + way + '.key}}_{{item.value}}';
            $(tElement).find('[il-uniqueid-id]').attr('id', uniqueID);
            $(tElement).find('[il-uniqueid-for]').attr('for', uniqueID);

            $(tElement).find('[il-checklist-selectall]').attr('ng-click', 'df__dropSelectAll(' + way + ')');
            $(tElement).find('[il-checklist-cleanselection]').attr('ng-click', 'df__dropCleanSelection(' + way + ')');
            $(tElement).find('[il-checklist-apply]')
                .attr('ng-click', 'df__dropApply(' + way + ')')
                .attr('ng-disabled', way + '.disableApplyButton');

            $(tElement).find('.paginacao')
                .attr('ng-if', way + '.showSearchBox == true  && ' + way + '.hasMorePage == true')
                .attr('ng-click', 'df__dropShowMorePage(' + way + ')');

            var emBindText = $(tElement).find('[em-bind-text]');
            if (emBindText.length != 0) {
                if (tAttrs.hasOwnProperty('bindAsHtml')) {
                    emBindText.attr('ng-bind-html', 'item.description | trusted');
                } else {
                    emBindText.attr('ng-bind', 'item.description');
                }
            }

            if (tAttrs.hasOwnProperty("showSearchbox") && tAttrs.showSearchbox.toString() == "false") {
                $(tElement).find('input[inputSearch]').parent().hide();
            } else {
                $(tElement).find('input[inputSearch]')
                    .attr('ng-keyup', 'df__dropBoxSearching(' + way + ',$event)')
                    .attr('ng-model', way + '.searchTxt')
                    .attr('ng-click', 'df__dropBoxFocus(' + way + ',$event)')
                    .attr('focus-me', way + '.focusOnTextBox == true');

                li.attr('ng-class', '{active:' + way + '.keyboardNavigation == true && $index == ' + way + '.hoverIndex}')
            }

            var frameset = getPropertyByName(tAttrs, 'framset');
            if (frameset != null) {
                try {
                    var framesetObj = $.parseJSON(frameset);
                    $(tElement).find('[il-frameset-label]').text(framesetObj.Title).show();
                    $(tElement).find('[il-frameset-icon]').addClass(framesetObj.icon).show();
                } catch (err) { }
            }
        }
    };
});

ILangApp.directive('myDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function () {
            element.datepicker({
                dateFormat: 'dd/mm/yy',
                minDate: 0,
                onSelect: function (dateText, inst) {
                    if (!scope.$$phase)
                        scope.$apply(function (scope) {
                            // Change binded variable
                            ngModel.assign(scope, dateText);
                        });
                }
            });
        });
    }
});

ILangApp.directive('hour', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.mask('00:00', { placeholde: '00:00' });
        }
    };
});

ILangApp.directive('logoutWarningDialog', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getAngularTemplate('Dialog', 'LogoutWarningDialog'); }
    };
});

ILangApp.directive('initialPageSelect', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getAngularTemplate('Dialog', 'InitialPageSelect'); }
    };
});

ILangApp.directive('organizationSelector', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Dialog/OrganizationSelector.html')
    };
});

ILangApp.directive('focusMe', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                if (!scope.$$phase && isSafeToUse(model, 'assign'))
                    scope.$apply(model.assign(scope, false));
            });
        }
    };
});

ILangApp.directive('elapsedTime', ['$interval', function ($interval) {
    var k = {};
    return {
        restrict: 'A',
        scope: { elapsedTime: '@' },
        link: function (scope, element, attrs) {

            scope.time = { year: 360 * 24 * 60 * 60, month: 30 * 24 * 60 * 60, day: 24 * 60 * 60, hour: 60 * 60, minute: 60 };
            $interval(function () {
                var now = new Date().getTime();
                var scopeElapsedTime = null;
                if (typeof (scope.elapsedTime) == 'number') {
                    scopeElapsedTime = scope.elapsedTime;
                } else if (!isNaN(scope.elapsedTime)) {
                    scopeElapsedTime = parseInt(scope.elapsedTime);
                } else {
                    try {
                        scopeElapsedTime = Date(scope.elapsedTime).getTime();
                    } catch (e) {
                        scopeElapsedTime = null;
                    }
                }

                if (scopeElapsedTime == null)
                    return;

                var str = 'Há ';
                var seconds = parseInt((now - parseInt(scopeElapsedTime)) / 1000);

                var _year = seconds > scope.time.year ? parseInt(seconds / scope.time.year) : 0;
                var _month = seconds > scope.time.month ? parseInt(seconds / scope.time.month) : 0;
                var _day = seconds > scope.time.day ? parseInt(seconds / scope.time.day) : 0;
                var _hour = seconds > scope.time.hour ? parseInt(seconds / scope.time.hour) : 0;
                var _minute = parseInt(seconds / scope.time.minute);

                if (_year > 0) {
                    str += _year + (_year > 1 ? ' anos' : ' ano ');
                } else if (_month > 0) {
                    str += _month + (_month > 1 ? ' meses' : ' mês  ');
                } else if (_day > 0) {
                    str += _day + (_day > 1 ? ' dias' : ' dia ');
                } else if (_hour > 0) {
                    str += _hour + (_hour > 1 ? ' horas' : ' hora ');
                } else if (_minute > 0) {
                    str += _minute + (_minute > 1 ? ' minutos' : ' minuto ');
                } else {
                    str = 'Há menos de 1 minuto';
                }
                element.text(str);
            }, 30000);
        }
    };
}]);

ILangApp.directive('autoComplete', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: getStaticUrl('/Templates/Search/AutoComplete.html'),
        controller: function ($scope, $element, $attrs) {
            var key = $attrs.key
            $scope.ph = $attrs.hasOwnProperty('placeholder') ? $attrs.placeholder : "Busque ou insira um novo autor...2";
            $scope.hideSeatch = false;
            var idAlias = $attrs.hasOwnProperty('idValue') ? $attrs.idValue : 'Id';
            $scope.$parent[key] = new $scope.contextParameters(key, 5, false);
            $scope.$parent[key].tagList = [];
            $scope.$parent[key].readOnly = false;
            $scope.$parent[key].hideSearchBar = false;
            if ($attrs.hasOwnProperty('onafterwrite') && $scope.hasOwnProperty($attrs.onafterwrite))
                $scope.$parent[key].onAfterWrite = $scope[$attrs.onafterwrite];

            $scope.$parent[key].allowOneSelection = $attrs.hasOwnProperty('allowOneSelection')


            $scope.$parent[key].searchFn = function () {
                var context = $scope.$parent[key];
                $scope.action(context.key + '_read', { SearchCriteria: context.searchTxt }, function (result) {
                    if (result.Success) {
                        context.isSearchBoxActive = true;
                        context.list = result.Result.take(context.listSize).select({ ID: idAlias, Name: 'Name' });
                    }
                });
            };

            $scope.$parent[key].onItemIsSelected = function (index) {
                var context = $scope.$parent[key];
                var i = (index == null) ? context.smallListHoverItem : index;
                var item = context.list[i];

                var fn = function () {
                    context.tagList.push(item);
                    $scope.closeSearchBox($scope.$parent[key]);
                    if ($scope.$parent[key].allowOneSelection)
                        $scope.$parent[key].hideSearchBar = true;
                }

                if ($scope.serverParameters.actions.hasOwnProperty(key + '_associate')) {
                    $scope.action(key + '_associate', item, function (result) {
                        if (result.Success) {
                            fn();
                        }
                    });
                } else {
                    fn();
                }

            };
            $scope.$parent[key].addNewItem = function (id, name) {
                var context = $scope.$parent[key];
                if (context.tagList == undefined || context.tagList == null)
                    context.tagList = [];
                context.tagList.push({ ID: id, Name: name });
            }

            $scope.$parent[key].onEnterIsPressed = function () {
                var context = $scope.$parent[key];
                $scope.action(key + '_write', { Name: context.searchTxt }, function (result) {
                    if (result.Success) {
                        var fn = function () {
                            var id = 0;
                            if (isSafeToUse(result, 'Result'))
                                id = result.Result[idAlias];

                            context.tagList.push({ ID: id, Name: context.searchTxt });
                            $scope.closeSearchBox($scope.$parent[key]);
                            if ($scope.$parent[key].allowOneSelection)
                                $scope.$parent[key].hideSearchBar = true;
                        }
                        if (typeof (context.onAfterWrite) == 'function') {
                            context.onAfterWrite(result, context.searchTxt, function () {
                                fn()
                            });
                        } else {
                            fn();
                        }

                    }
                });
            };

            $scope.$parent[key].remove = function (item) {
                var fn = function () {
                    $scope.$parent[key].tagList.remove({ ID: item.ID });
                };
                if ($scope.serverParameters.actions.hasOwnProperty(key + '_remove')) {
                    $scope.action(key + '_remove', item, function (result) {
                        $scope.$parent[key].hideSearchBar = false;
                        if (result.Success)
                            fn();
                    });
                } else {
                    fn();
                }
            };

            $scope.onAutoCompleteLoad = function () {
                if ($attrs.hasOwnProperty('onCtrlInit') && typeof (getRootScope($scope)[$attrs.onCtrlInit]) == 'function') {
                    getRootScope($scope)[$attrs.onCtrlInit]();
                }
            };
            $scope.$parent[key].setHideSearch = function (value) {
                $scope.$parent[key].hideSearchBar = value;
            }
        },
        compile: function (tElement, tAttrs, transclude) {
            var key = tAttrs.key;
            $(tElement).find('.srchCreate')
                .attr('ng-class', '{active: ' + key + '.isSearchBoxActive == true}')
                .attr('ng-hide', key + '.hideSearchBar');

            $(tElement).find('input')
                .attr('ng-model', key + '.searchTxt')
                .attr('ng-keyup', 'inputSearchTyping($event, ' + key + ')')
                .attr('ng-focus', 'inputSearchFocused($event, ' + key + ')')
                .attr('ng-blur', 'inputSearchBlur($event, ' + key + ')');


            $(tElement).find('.card')
                .attr('ng-repeat', 'item in ' + key + '.list')
                .attr('ng-class', '{active: ' + key + '.isSearchBoxActive == true && $index == ' + key + '.smallListHoverItem }')
                .attr('ng-click', key + '.onItemIsSelected($index)');

            $(tElement).find('.topic')
                .attr('ng-repeat', 'tag in ' + key + '.tagList');



            $(tElement).find('.close').attr('ng-click', key + '.remove(tag)')

            if (tAttrs.hasOwnProperty('placeholder'))
                $(tElement).find('input').attr('placeholder', tAttrs.placeholder);

            if (tAttrs.hasOwnProperty('fullwidth'))
                $(tElement).find('.srch').addClass('inputFullWidth');

        }
    };
});

ILangApp.directive('usermediaViewer', function () {
    return {
        replace: true,
        restrict: 'AE',
        templateUrl: getStaticUrl('/Templates/UserMedia/UserMediaViewer.html'),
        scope: {
            id: '=',
            usermedialist: '='
        },
        controller: function ($scope, $element) {
            var me = $scope;
            me.elm = $element;
            me.create = function () {
                if ($scope.usermedialist != null) {
                    $scope.usermediaInstancesList = [];
                    var umi = new UserMediaViewer('um_' + $scope.id, 'umhiden_' + $scope.id, true, '');
                    for (var i = 0; i < $scope.usermedialist.length; i++) {
                        umi.addUserMedia($scope.usermedialist[i]);
                    }
                    $scope.usermediaInstancesList.push(umi);
                }
            };
            me.update = function (userMediaID) {
                var item = $scope.usermediaInstancesList.first({ inputElementID: 'umhiden_' + $scope.id });
                if (isSafeToUse(item, addUserMedia))
                    item.addUserMedia(userMediaID);


            }
            var parent = getScopeByProp(me, 'IamUsermediaLineScope');
            if (parent != null) {
                parent.update = function (userMediaID) {
                    me.update(userMediaID);
                }
            }

        },
        link: function ($scope, $element, $attrs) {
            $scope.create();
        }
    };
});

ILangApp.directive('uniqueUserList', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Dialog/UniqueUserList.html'),
        //templateUrl: function () { return getAngularTemplate('Dialog', 'UserList'); },
        controller: function ($scope, $element, $attrs) {
            $scope.uniqueUserList = {};
            var me = $scope.uniqueUserList;
            me.list = [];
            me.evaluationList = [];
            me.showChatlink = false;
            me.showMsglink = false;
            me.showShowmorebutton = false;
            me.show = false;
            me.title = '';
            me.number = '';
            me.total = 0;
            me.percent = 0;
            me.loading = false;
            me.showFeedtext = false;
            me.showViewersPercent = false;

            var sc = getRootScope($scope);
            me.elem = $element;
            me.showDialog = false;
            sc.uniqueUserList = me;

            me.showMore = function () {
                sc.uniqueUserList.showMore();
            };

            me.showViewedUsers = function () {
                me.onlyViewedUsers = !me.onlyViewedUsers;
                me.clean;
                me.get('feedUsers', me.parameters);
            };

            me.searchTimeout = null;

            me.searchCriteria = '';

            me.searchCriteriaChange = function () {
                if ($scope.searchTimeout)
                    clearInterval($scope.searchTimeout);

                me.searchTimeout = setTimeout(function () {
                    me.searchCriteria = $scope.searchCriteria;
                    me.get('feedUsers', {
                        FeedId: me.parameters.FeedId,
                        searchCriteria: me.searchCriteria
                    });
                }, 1000);
            }
            me.searchUserByname = function (parameters) {
                me.clean();
                me.searchCriteria = parameters.searchCriteria;
                me.get(me.actionName, undefined, false);
            };

            me.showDialog = false;
            me.loading = false;
            me.list = [];
            me.actionName = null;
            me.parameters = null;
            me.showShowMoreButton = false;
            me.searchCriteria = '';
            me.index = 1;
            me.onlyViewedUsers = true;
            me.show = function () {
                $(me.elem).removeAttr('style');
                me.showDialog = true;
                $scope.loading = false;
            };
            me.clean = function () {
                me.list = [];
                me.index = 1;
            };

            me.showMore = function () {
                me.index++;
                me.get("showMore");
            };
            me.pop = function (result, include) {
                if (include) {
                    me.list.include(result.Result);
                } else {
                    me.list = result.Result;
                }
                me.showShowMoreButton = result.HasMorePages;
                me.show();
            };
            me.setParameters = function (param) {
                param = param || {};
                param.PageIndex = me.index;
                param.PageSize = 20;
                param.SearchCriteria = me.searchCriteria;
                param.onlyViewedUsers = me.onlyViewedUsers;
                return param;
            };
            me.get = function (action, param, include) {
                if (action == undefined && param == undefined)
                    return;

                if (action != undefined && action != null && action != "showMore") {
                    me.clean();
                    me.actionName = action;
                }

                if (param != undefined && param != null) {
                    param = me.setParameters(param);
                    me.parameters = param;
                } else {
                    me.parameters.PageIndex = me.index;
                    param = me.parameters;
                    param.SearchCriteria = me.searchCriteria;
                    param.onlyViewedUsers = me.onlyViewedUsers;
                }

                var root = getRootScope($scope);

                root.action(me.actionName, param, function (result) {
                    me.pop(result, trueIfUndefined(include));
                });
            };
            me.title;
            me.viewCount;
            me.total;
            me.percent;

        }
    };
});

ILangApp.directive('userList', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Dialog/UserList.html'),
        //templateUrl: function () { return getAngularTemplate('Dialog', 'UserList'); },
        scope: {
            list: '=',
            showChatlink: '@',
            showMsglink: '@',
            showShowmorebutton: '=',
            show: '=',
            title: '@',
            number: '@',
            total: '@',
            percent: '@',
            loading: '=',
            showFeedtext: '=',
            showViewersPercent: '='
        },
        controller: function ($scope, $element, $attrs) {
            var sc = $scope.$parent;
            $scope.elem = $element;
            $scope.showDialog = false;

            $scope.showMore = function () {
                sc.userList.showMore();
            };

            $scope.searchTimeout = null;

            $scope.searchCriteria = '';

            $scope.searchCriteriaChange = function () {
                if ($scope.searchTimeout)
                    clearInterval($scope.searchTimeout);

                $scope.searchTimeout = setTimeout(function () {
                    sc.userList.searchCriteria = $scope.searchCriteria;
                    sc.userList.get('feedUsers', {
                        FeedId: sc.userList.parameters.FeedId,
                        searchCriteria: sc.userList.searchCriteria
                    });
                }, 1000);
            }
            $scope.searchUserByname = function (parameters) {
                sc.userList.clean();
                sc.userList.searchCriteria = parameters.searchCriteria;
                sc.userList.get(undefined, undefined, false);
            };
            sc.userList = {
                showDialog: false,
                loading: false,
                list: [],
                action: null,
                parameters: null,
                showShowMoreButton: false,
                searchCriteria: '',
                show: function () {
                    $($scope.elem).removeAttr('style');
                    $scope.showDialog = true;
                    this.loading = false;
                },
                clean: function () {
                    this.list = [];
                    this.index = 1;
                },
                index: 1,
                showMore: function () {
                    this.index++;
                    this.get("showMore");
                },
                pop: function (result, include) {
                    if (include) {
                        this.list.include(result.Result);
                    } else {
                        this.list = result.Result;
                    }
                    this.showShowMoreButton = result.HasMorePages;
                    this.show();
                },
                setParameters: function (param) {
                    param = param || {};
                    param.PageIndex = this.index;
                    param.PageSize = 20;
                    param.SearchCriteria = this.searchCriteria;
                    return param;
                },
                get: function (action, param, include) {
                    if (action == undefined && param == undefined)
                        return;

                    if (action != undefined && action != null && action != "showMore") {
                        this.clean();
                        this.action = action;
                    }

                    if (param != undefined && param != null) {
                        param = this.setParameters(param);
                        this.parameters = param;
                    } else {
                        this.parameters.PageIndex = this.index;
                        param = this.parameters;
                        param.SearchCriteria = this.searchCriteria;
                    }

                    var root = getRootScope($scope);

                    root.action(this.action, param, function (result) {
                        sc.userList.pop(result, trueIfUndefined(include));
                    });
                },
                title: function (_title) {
                    if (_title == null) {
                        return $scope.title;
                    } else {
                        $scope.title = _title;
                    }
                },
                viewCount: function (_count) {
                    if (_count == null) {
                        return $scope.number;
                    } else {
                        $scope.number = parseInt(_count);
                    }
                },
                total: function (_count) {
                    if (_count == null) {
                        return $scope.total;
                    } else {
                        $scope.total = parseInt(_count);
                    }
                },
                percent: function (_count) {
                    if (_count == null) {
                        return $scope.percent;
                    } else {
                        $scope.percent = parseInt(_count);
                    }
                }
            };
        }
    };
});

ILangApp.directive('conclusionUserList', function () {
    return {
        restrict: 'E',
        replace: true,
        //templateUrl: getStaticUrl('/Templates/Dialog/ConclusionUserList.html'),
        templateUrl: function () { return getAngularTemplate('Dialog', 'ConclusionUserList'); },
        scope: {
            list: '=',
            showChatlink: '@',
            showMsglink: '@',
            showShowmorebutton: '=',
            show: '=',
            title: '@',
            number: '@',
            total: '@',
            percent: '@',
            complementPercent: '@',
            announcementNavigatorUrl: '@',
            loading: '=',
            showFeedtext: '=',
            feedId: '@'
        },
        controller: function ($scope, $element, $attrs) {
            var sc = $scope.$parent;
            $scope.elem = $element;
            $scope.showDialog = false;
            $scope.deliveredTabActive = false;

            $scope.showMore = function () {
                sc.conclusionUserList.showMore();
            };

            $scope.toggleDeliveredTabActive = function () {
                var param = {
                    FeedId: $scope.feedId
                };
                sc.conclusionUserList.index = 1;
                if ($scope.deliveredTabActive)
                    sc.conclusionUserList.get('feedNonDeliverers', param);
                else
                    sc.conclusionUserList.get('feedDeliverers', param);
                $scope.deliveredTabActive = !$scope.deliveredTabActive;
            }

            sc.conclusionUserList = {
                showDialog: false,
                loading: false,
                list: [],
                action: null,
                parameters: null,
                showShowMoreButton: false,
                feedId: 0,
                show: function () {
                    $($scope.elem).removeAttr('style');
                    $scope.showDialog = true;
                    this.loading = false;
                },
                clean: function () {
                    this.list = [];
                    this.index = 1;
                },
                index: 1,
                showMore: function () {
                    this.index++;
                    this.get();
                },
                pop: function (result) {
                    this.list.include(result.Result);
                    this.showShowMoreButton = result.HasMorePages;
                    this.show();
                },
                setParameters: function (param) {
                    param = param || {};
                    param.PageIndex = this.index;
                    param.PageSize = 20;
                    return param;
                },
                get: function (action, param) {

                    if (action != undefined && action != null) {
                        this.clean();
                        this.action = action;
                    }

                    if (param != undefined && param != null) {
                        param = this.setParameters(param);
                        this.parameters = param;
                    } else {
                        this.parameters.PageIndex = this.index;
                        param = this.parameters;
                    }

                    var root = getRootScope($scope);

                    root.action(this.action, param, function (result) {
                        sc.conclusionUserList.pop(result);
                    });
                },
                feedId: function (_feedId) {
                    if (_feedId == null) {
                        return $scope.feedId;
                    } else {
                        $scope.feedId = _feedId;
                    }
                },
                title: function (_title) {
                    if (_title == null) {
                        return $scope.title;
                    } else {
                        $scope.title = _title;
                    }
                },
                viewCount: function (_count) {
                    if (_count == null) {
                        return $scope.number;
                    } else {
                        $scope.number = parseInt(_count);
                    }
                },
                deliveryCount: function (_count) {
                    if (_count == null) {
                        return $scope.number;
                    } else {
                        $scope.number = parseInt(_count);
                    }
                },
                total: function (_count) {
                    if (_count == null) {
                        return $scope.total;
                    } else {
                        $scope.total = parseInt(_count);
                    }
                },
                percent: function (_count) {
                    if (_count == null) {
                        return $scope.percent;
                    } else {
                        $scope.percent = parseInt(_count);
                    }
                },
                complementPercent: function (_count) {
                    if (_count == null) {
                        return $scope.complementPercent;
                    } else {
                        $scope.complementPercent = parseInt(_count);
                    }
                },
                announcementNavigatorUrl: function (_url) {
                    if (_url == null) {
                        return $scope.announcementNavigatorUrl;
                    } else {
                        $scope.announcementNavigatorUrl = _url;
                    }
                }
            };
        }
    };
});

ILangApp.directive('scopeDialog', function() {
    return {
        replace: true,
        templateUrl: getStaticUrl('/Templates/Dialog/scopeDialog.html'),
        transclude: true,
        restrict: 'E',
        scope: {
            isopened: '=',
            title: '@',
            subtitle: '@',
            list: '='
        },
        compile: function(tElement, tAttrs, transclude) {
            if (typeof (tAttrs.size) != 'undefined') {
                try {
                    var t;
                    if (tAttrs.size.indexOf('dialogSize') >= 0) {
                        t = eval(tAttrs.size);
                    } else {
                        t = eval('dialogSize.' + tAttrs.size);
                    }
                    $(tElement).find('.sdBox').addClass(t);
                } catch (e) {
                }
            }
        }
    };
});

ILangApp.directive('ilangDialog', function () {
    return {
        replace: true,
        transclude: true,
        restrict: 'AE',
        templateUrl: function (elem, attrs) {
            if (typeof (attrs.template) != 'undefined') {
                try {
                    var t;
                    var name = getPropertyByName(attrs, 'template');
                    if (name.indexOf('tpl') >= 0) {
                        if (name == 'tpl/' || name.toLowerCase() == 'tpl/default' || name == 'tpl') {
                            return '/Dialog_GDMulti.tpl';
                        } else {
                            return '/Dialog_' + name.replace('tpl/', '') + '.tpl';
                        }
                    } else {
                        if (attrs.template.indexOf('dialogType') >= 0) {
                            t = eval(attrs.template);
                        } else {
                            t = eval('dialogType.' + attrs.template);
                        }
                        return getStaticUrl('/Templates/Dialog/' + t + '.html');
                    }

                } catch (e) {
                    return getStaticUrl('/Templates/Dialog/GDMulti.html')
                }
            } else {
                return getStaticUrl('/Templates/Dialog/GDMulti.html')
            }
        },
        link: function ($scope, $element, $attrs, controller) {

        },
        compile: function (tElement, tAttrs, transclude) {
            var p = getPropertyByName(tAttrs, 'key');
            if (p != null) {
                $(tElement).attr('ng-show', 'serverParameters.dialogs.' + p + '.visible == true');
            }
            if (typeof (tAttrs.size) != 'undefined') {
                try {
                    var t;
                    if (tAttrs.size.indexOf('dialogSize') >= 0) {
                        t = eval(tAttrs.size);
                    } else {
                        t = eval('dialogSize.' + tAttrs.size);
                    }
                    $(tElement).find('.sdBox').addClass(t);
                } catch (e) {

                }
            }
        }
    };
});

ILangApp.directive('dialogBody', function () {
    return {
        replace: true,
        transclude: true,
        restrict: 'E',
        //templateUrl: getStaticUrl('/Templates/Dialog/GDBody.html')
        templateUrl: function () { return getAngularTemplate('Dialog', 'GDBody'); }
    };
});

ILangApp.directive('dialogFooter', function () {
    return {
        replace: true,
        transclude: true,
        restrict: 'E',
        //templateUrl: getStaticUrl('/Templates/Dialog/GDFooter.html'),
        templateUrl: function () { return getAngularTemplate('Dialog', 'GDFooter'); }
    };
});

ILangApp.directive('myTransclude', function () {
    return {
        replace: true,
        restrict: 'E',
        compile: function (tElement, tAttrs, transclude) {
            return function (scope, iElement, iAttrs) {
                transclude(scope, function (clone) {
                    iElement.append(clone);
                });
            };
        }
    };
});

ILangApp.directive('watchHeight', function () {
    return {
        restrict: 'A',
        controller: function ($scope, $element, $attrs, $timeout, $window) {
            $scope.updateHeight = function () {
                $scope.elHeight = $element[0].offsetHeight;
                $scope.elHeightPorc = (parseInt($element[0].offsetHeight) / parseInt($(window).height()));
            };
            $scope.setUpdate = function () {
                $timeout(function () {
                    $scope.updateHeight();
                }, 1000);
            };
            $scope.setUpdate();
        },
        link: function (scope, ele, attrs) {
            var el = ele.find('iframe');
            if (el.length > 0) {
                $(el[0]).load(function () {
                    scope.setUpdate();
                });
            }
        }
    };
});

ILangApp.directive('showLoading', function () {
    return function (scope, element, attrs) {
        if (scope.$last && typeof (window.ToggleLoading) == 'function')
            ToggleLoading(false);
    }
});

ILangApp.directive('onLastbind', function () {
    return function (scope, element, attrs) {
        if (scope.$last && typeof (attrs.onLastbind) != 'undefined' && attrs.onLastbind != '' && scope.$parent.hasOwnProperty(attrs.onLastbind))
            scope.$parent[attrs.onLastbind]();
    }
});

ILangApp.directive('afterRender', function ($timeout) {
    return {
        restrict: 'A',
        terminal: true,
        transclude: false,
        link: function (scope, element, attrs) {
            $timeout(scope.$eval(attrs.afterRender), 0);
        }
    };
});

ILangApp.directive('fileUploader', function ($timeout) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: getStaticUrl('/Templates/Uploader/Default.html'),
        //scope:{key:'='},
        link: {
            post: function (scope, element, attr, controller) {
                scope.key = parseInt(attr.key);
                $timeout(function () {
                    var sc = scope.$parent.$parent
                    if (typeof (sc.serverParameters) == 'undefined' || typeof (sc.serverParameters.uploaderParameters) == 'undefined')
                        return;

                    var key = scope.key;

                    var param = sc.serverParameters.uploaderParameters;
                    if (param == null)
                        return

                    sc.uploader = new CustomUpload(
                        {
                            upload_url: param.UploadHandlerUrl,
                            file_size_limit: 10240,
                            file_types: '*.*',
                            file_queue_limit: param.QueueLimitSize,
                            button_placeholder_id: 'spanButton' + key,
                            button_image_url: param.ButtonImageUrl,
                            button_width: param.Width,
                            flash_url: param.SWFUPLOADURL,
                            resources_url: param.RESOURCESURL,
                            containerId: 'divU' + key,
                            progressTarget: 'fsUP' + key,
                            cancelButtonId: 'lnkCancel_' + key,
                            clienttUploadSuccess: function (file) { if (typeof (sc.clienttUploadSuccess) != 'undefined') { sc.clienttUploadSuccess(file) } else { console.warn('Uploader threw the event: clienttUploadSuccess', file) } },
                            clientStartUpload: function (file) { if (typeof (sc.clientStartUpload) != 'undefined') { sc.clientStartUpload(file) } else { console.warn('Uploader threw the event: clientStartUpload', file) } },
                            clientCancelUpload: function (file) { if (typeof (sc.clientCancelUpload) != 'undefined') { sc.clientCancelUpload(file) } else { console.warn('Uploader threw the event: clientCancelUpload', file) } },
                            serverUploadSuccess: function (file) { if (typeof (sc.serverUploadSuccess) != 'undefined') { sc.serverUploadSuccess(file) } else { console.warn('Uploader threw the event: serverUploadSuccess', file) } },
                            fileIsToBigMessage: 'Arquivo excedeu o tamanho suportado (10MB).',
                            uploadProgressMode: param.UploadProgessMode,
                            uploadTypeMode: param.UploadTypeMode,
                            fileLocation: param.FileLocation
                        });
                })

            }
        }

    };
});

ILangApp.directive('autoHeight', function () {
    return {
        restrict: 'A',
        controller: function ($scope, $element) {
            $scope.textAreaAdjust = function (event) {
                if (event.keyCode == keyCode.KEY_ESC) {
                    $($element).val('');
                    return;
                }
                let el = $($element);
                if (el.length > 0) {
                    let scrollMargin = 2;
                    let h = el[0].scrollHeight + scrollMargin + "px";

                    el.height(h);
                }

            };
            $scope.onEnterPressed = function (event) {
                if (event.keyCode == keyCode.KEY_ENTER) {
                    $scope.autoResize();
                }
            }
            $scope.autoResize = function () {
                let element = $element[0];
                let margin = 10;

                element.style.height = 'auto'; // Reset height to auto
                element.style.height = (element.scrollHeight + margin) + 'px'; // Set height based on content
            }

        }
    }
});

ILangApp.directive('backImg', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (isSafeToUse(attrs, 'backImg')) {
                var url = attrs.backImg;
                element.css({
                    'background-image': 'url(' + url + ')',
                    'background-size': 'cover'
                });
            }
        }
    };
});

ILangApp.directive('singularPlural', function () {
    return {
        restric: 'A',
        scope: {
            singularPlural: '@',
            singular: '@',
            plural: '@',
            template: '@'
        },
        link: function ($scope, $element, $attrs, controller) {
            var me = $scope;

            var fn = function () {
                if (!isNaN(me.singularPlural)) {
                    var text = '';
                    var sp = '';

                    if (me.singularPlural == 1) {
                        sp = me.singular;
                    } else {
                        sp = me.plural;
                    }

                    if (typeof (me.template) != 'undefined') {
                        text = me.template.replace('{n}', me.singularPlural).replace('{t}', sp)
                    } else {
                        text = me.singularPlural + ' ' + sp;
                    }
                    $element.text(text);

                }
            };
            $scope.$watch(function (sc) { return sc.singularPlural }, function (newValue) {
                fn();
            })
            fn();
        }
    };
});
ILangApp.directive('singularPlural2', function () {
    return {
        restric: 'A',
        scope: {
            singularPlural2: '@',
            singular: '@',
            plural: '@',
            template: '@',
            when: '@'
        },
        link: function ($scope, $element, $attrs, controller) {
            var me = $scope;

            var fn = function (value) {
                if (!isNaN(value)) {
                    var text = '';
                    var sp = '';

                    if (value == 1) {
                        sp = me.singular;
                    } else {
                        sp = me.plural;
                    }

                    if ($scope.when != null && $scope.when != '') {
                        try {
                            eval('var _when = ' + $scope.when + ';');
                            if (_when.hasOwnProperty(value)) {
                                sp = _when[value];
                                $element.text(sp);
                                return;
                            }

                        } catch (e) { }
                    }

                    if (typeof (me.template) != 'undefined') {
                        text = me.template.replace('{n}', value).replace('{t}', sp)
                    } else {
                        text = value + ' ' + sp;
                    }
                    $element.text(text);

                }
            };
            $scope.$watch(function (sc) {
                return getPropertyByName(sc, '$parent.' + $scope.singularPlural2)
            }, function (newValue) {
                fn(newValue);
            })
            fn();
        }
    };
});

ILangApp.directive('videoHeader', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/Header/VideoHeader.html'),
        controller: function ($scope, $element, $attrs, $document, $rootScope) {
            $scope.openVideoDialog = function () {
                if (isSafeToUse($scope, 'serverParameters.videoHeader.ShowDialog')) {
                    $scope.serverParameters.videoHeader.ShowDialog = true;
                    $scope.serverParameters.videoHeader.IframeHtml = '<iframe  width="100%" height="100%" src="' + $scope.serverParameters.videoHeader.VideoUrl + '" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>'
                }
            };
            $scope.closeVideoDialog = function () {
                $scope.serverParameters.videoHeader.IframeHtml = '';
                $scope.serverParameters.videoHeader.ShowDialog = false;
            }
        }
    };
})
//ILangApp.directive('userInfo', function () {
//    return function (scope, element, attrs) {
//        //alert(element);
//    }
//});

ILangApp.directive('compile', function ($compile) {
    // directive factory creates a link function
    return function (scope, element, attrs) {
        scope.$watch(
            function (scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function (value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
});
function uploadCommonFunction(me, $element, $attrs, properties) {
    me.message = null;
    me.maxFiles = 50;
    me.progress = 0;
    me.inputValue = null;
    me.success = null;
    me.startUpload = false;
    me.inputElement = null;
    me.upload_properties = properties;
    me.filesSize = 0;
    me._up
    me.filesName = '';
    me.maxFileSize = me.maxfilesize || 300000000;
    me.rootScope = getRootScope(me);
    me.showProgressBar = true;
    me.FileSelectHandler = function (element, event) {
        if (me.startUpload == true)
            return;

        if (event != null)
            me.FileDragHover(event);

        // fetch FileList object
        var files = [];
        if (isSafeToUse(element, 'files')) {
            files = element.files;
        } else if (isSafeToUse(element, 'dataTransfer.files')) {
            files = element.dataTransfer.files;
        } else if (isSafeToUse(element, 'originalEvent.dataTransfer.files')) {
            files = element.originalEvent.dataTransfer.files;
        }
        if (isSafeToUse(me, 'upload_properties.allowMultiFiles')) {
            if (me.upload_properties.allowMultiFiles == false && files.length > 1) {
                me.startUpload = false;
                showTopErrorMsg('Por enquanto não é permitido o upload de multiplos aquivos!', 5000);
                return;
            }
        }
        var listFiles = objToArray(files);
        me.filesSize = listFiles.sum('size');
        if (me.customfilename != undefined && me.customfilename.length > 0) {

            var fileExtension = listFiles[0].name.split('.').pop();
            me.filesName = me.customfilename + '.' + fileExtension;
        }
        else {
            me.filesName = listFiles.length == 1 ? listFiles[0].name : listFiles.selectToArray('name').join(', ');
        }

        if (typeof (me.onstart) == 'function')
            me.onstart(listFiles);

        me.showProgressBar = true;
        me.startUpload = true;
        if (!me.$$phase)
            me.$apply('startUpload');


        // process all File objects
        if (files.length == 1) {
            me.ParseFile(files[0]);
            me.UploadFile(files[0]);
            if (!me.$$phase)
                me.$apply();
        } else {
            me.uploadManyQueue(files, 0);
        }

    };
    me.uploadManyQueue = function (list, index) {
        if (index > list.length - 1) {
            if (!me.$$phase)
                me.$apply();
            return false;
        }

        me.progress = parseInt((index * 100 / list.length));
        console.log(index, ' de ', list.length)
        me.UploadFileMany(list[index], index == list.length - 1, function (result) {
            return me.uploadManyQueue(list, index + 1);
        })
    }
    me.FileDragHover = function (e) {
        e.stopPropagation();
        e.preventDefault();
        //e.target.className = (e.type == "dragover" ? "hover" : "");
    };
    me.Output = function (msg) {
        me.message = msg;
    }
    me.ParseFile = function (file) {

        me.Output(
            "<p>File information: <strong>" + file.name +
            "</strong> type: <strong>" + file.type +
            "</strong> size: <strong>" + file.size +
            "</strong> bytes</p>"
        );

        // display text
        if (file.type.indexOf("text") == 0) {
            var reader = new FileReader();
            reader.onload = function (e) {
                me.Output(
                    "<p><strong>" + file.name + ":</strong></p><pre>" +
                    e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
                    "</pre>"
                );
            }
            reader.readAsText(file);
        }

        // display an image
        if (file.type.indexOf("image") == 0) {
            var reader = new FileReader();
            reader.onload = function (e) {
                me.Output(
                    "<p><strong>" + file.name + ":</strong><br />" +
                    '<img src="' + e.target.result + '" /></p>'
                );
            }
            reader.readAsDataURL(file);
        }
    }
    me.cleanInput = function () {
        if (me.inputElement != null)
            me.inputElement.val('');
        me.showProgressBar = true;
    };
    me.UploadFileMany = function (file, isLastFile, after) {
        var xhr = new XMLHttpRequest();
        if (xhr.upload && file.size <= me.maxFileSize) {

            xhr.onreadystatechange = function (e) {
                if (xhr.readyState == 4) {
                    me.showProgressBar = false;
                    me.success = (xhr.status == 200 ? "success" : "failure");
                    if (typeof (me.onsuccess) == 'function')
                        me.onsuccess({
                            result: {
                                alwaysCreateNew: true,
                                file: file,
                                isLastFile: isLastFile,
                                response: me.strToObj(xhr.response),
                                after: function () {
                                    if (isLastFile) {
                                        me.startUpload = false;
                                        me.cleanInput();
                                    }
                                    if (typeof (after) == 'function')
                                        after(me.strToObj(xhr.response));


                                }
                            }
                        });

                }
            };

            xhr.onerror = function (e) {
                console.error(e.statusText);
                me.success = 'failure';
                me.cleanInput();
            };

            // start upload

            var __handlerUrl = me.getHandler();
            var _auth = me.getAuth();
            xhr.open("POST", __handlerUrl, true);
            xhr.setRequestHeader("X_FILENAME", unescape(encodeURIComponent(file.name)));
            xhr.setRequestHeader("auth", _auth);
            //xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
            //xhr.setRequestHeader('Content-Type', 'undefined');
            xhr.send(file);
        }
    };
    me.UploadFile = function (file) {
        var xhr = new XMLHttpRequest();
        if (xhr.upload && file.size <= me.maxFileSize) {

            // progress bar
            xhr.upload.addEventListener("progress", function (e) {
                me.progress = parseInt((e.loaded * 100 / e.total));
                if (!me.$$phase)
                    me.$apply();
            }, false);

            xhr.onreadystatechange = function (e) {
                if (xhr.readyState == 4) {
                    me.showProgressBar = false;
                    me.success = (xhr.status == 200 ? "success" : "failure");
                    if (typeof (me.onsuccess) == 'function')
                        me.onsuccess({
                            result: {
                                alwaysCreateNew: false,
                                file: file,
                                isLastFile: true,
                                response: me.strToObj(xhr.response),
                                after: function () {
                                    me.startUpload = false;
                                    me.cleanInput();

                                    //if (!me.$$phase)
                                    //    me.$apply();
                                }
                            }
                        });

                }
            };

            xhr.onerror = function (e) {
                console.error(e.statusText);
                me.success = 'failure';
                me.cleanInput();
            };

            // start upload

            var __handlerUrl = me.getHandler();
            var _auth = me.getAuth();
            xhr.open("POST", __handlerUrl, true);
            var filename = file.name;
            if (me.customfilename != undefined && me.customfilename.length > 0) {
                var fileExtension = filename.split('.').pop();
                filename = me.customfilename + '.' + fileExtension;
            }
            xhr.setRequestHeader("X_FILENAME", unescape(encodeURIComponent(filename)));
            xhr.setRequestHeader("auth", _auth);
            //xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
            //xhr.setRequestHeader('Content-Type', 'undefined');
            xhr.send(file);
        }
    };
    me.strToObj = function (str) {
        return Function(
            'return ' + str + ';'
        )();
    };
    me.getHandler = function () {
        var h = getPropertyByName(me.rootScope, 'serverParameters.html5UploaderParameters.UploadHandlerUrl');
        return h == null ? '/Controls/CustomUpload2/HtmlFileUpload.ashx' : h;
    };
    me.getAuth = function () {
        return getPropertyByName(me.rootScope, 'serverParameters.html5UploaderParameters.EncryptedUserId')
    };
    me.onInit = function () {
        if (getPropertyByName(me, 'multiplefiles') == true) {
            $($element).find('input').attr('multiple', 'multiple');
        }
        if (me.buttonText == undefined || me.buttonText == null)
            me.buttonText = 'ENVIAR RESPOSTA';

        if ($element != null) {
            me.inputElement = $($element).find('input[type="file"]');
        }
    };
    me.cancel = function () {
        if (typeof (me.oncancel) == 'function')
            me.oncancel();
    };
}
ILangApp.directive('html5Uploader', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function (elem, attrs) {
            if (typeof (attrs.template) != 'undefined') {
                try {
                    var t;
                    if (attrs.template.indexOf('uploaderTypes') >= 0) {
                        t = eval(attrs.template);
                    } else {
                        t = eval('uploaderTypes.' + attrs.template);
                    }
                    return getStaticUrl('/Templates/Uploader/' + t + '.html');
                } catch (e) {
                    return getStaticUrl('/Templates/Uploader/Html5Default.html');
                }
            } else {
                return getStaticUrl('/Templates/Uploader/Html5Default.html');
            }
        },
        scope: {
            maxFilesize: '@',
            multiplefiles: '=',
            hidesupportedformat: '=',
            onsuccess: '&',
            buttontext: '@',
            filetypes: '@',
            accept: '@',
            oncancel: '&',
            onstart: '&',
            identity: '@',
            customfilename: '@'
        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            uploadCommonFunction(me, $element, $attrs)
        }
    };
});

ILangApp.directive('dragDropUploader', ['$http', function ($http) {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            onfinish: '&',
            oninit: '&',
            onprogress: '&',
            feedbackparameter: '@',
            cardclass:'@'
        },

        link: function (scope, element, attrs, ngModel) {
            var getElement = function (scope, element) {
                if (element.hasClass(scope.cardclass))
                    return element;
                return element.find('.' + scope.cardclass);
            }
            element.on('drop', function (e) {
                var el = getElement(scope, element);
                el.removeClass('dragHover');
                e.preventDefault();
                e.stopPropagation();
                if (e.originalEvent.dataTransfer) {
                    if (e.originalEvent.dataTransfer.files.length > 0) {
                        scope.OnDrop(e);//e.originalEvent.dataTransfer.files
                    }
                }
                return false;
            });
            element.on('dragover', function (e) {
                // element.find('.cardAttach').addClass('dragHover');
                var el = getElement(scope, element);
                el.addClass('dragHover');
                e.preventDefault();
                e.stopPropagation();
                return false;

            });

            //element.on('dragenter', function (e) {
            //    //element.find('.cardAttach').addClass('dragHover');
            //    console.log('enter')
            //    scope.fileIsOver = true;
            //    e.preventDefault();
            //    e.stopPropagation();
            //    return false;
            //});
            element.on('dragleave', function (e) {
                var el = getElement(scope, element);
                el.removeClass('dragHover')
                e.preventDefault();
                e.stopPropagation();
                scope.fileIsOver = false;
                return false;

            });
            element.find('input[type="file"]').on('change', function (e) {
                var ele = element.find('input[type="file"]')[0]
                scope.FileSelectHandler(ele, e);
            });


        },
        controller: function($scope, $element, $attrs){
            var me = $scope;
            me.uploading = false;
            uploadCommonFunction(me, $element, $attrs, {allowMultiFiles:true})
            me.fileIsOver = false;
            me.filesToUpload = [];
            me.OnDrop = function (e) {
                me.FileSelectHandler(e, null);
            };
            me.onstart = function (filesToUpload) {
                me.uploading = true;
                me.filesToUpload = filesToUpload;
                me.files = [];
                if (typeof (me.oninit) == 'function') {
                    if (typeof (me.feedbackparameter) != 'undefined') {
                        me.oninit({ list: me.filesToUpload, feedbackparameter: me.feedbackparameter  });
                    } else {
                        me.oninit({ list: me.filesToUpload });
                    }
                }
            };
            me.files = [];
            me.onsuccess = function (result) {
                me.files.push(result);
                if (result.result.isLastFile == true) {
                    me.uploading = false;
                    if (typeof (result.result.after) == 'function')
                        result.result.after();
                    if (typeof (me.onfinish) == 'function') {
                        if (typeof (me.feedbackparameter) != 'undefined') {
                            me.onfinish({ result: me.files, feedbackparameter: me.feedbackparameter });
                        } else {
                            me.onfinish({ result: me.files });
                        }

                    }
                } else {
                    if (typeof (result.result.after) == 'function') {
                        result.result.after();
                    }
                    if (typeof (me.onprogress) == 'function') {
                        if (typeof (me.feedbackparameter) != 'undefined') {
                            me.onprogress({ index: me.files.length, total: me.filesToUpload.length, feedbackparameter: me.feedbackparameter  });
                        } else {
                            me.onprogress({ index: me.files.length, total: me.filesToUpload.length });
                        }
                    }

                }

            }

        }
    };
}]);

ILangApp.directive('html5DisciplineContent', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function (elem, attrs) {
            if (typeof (attrs.template) != 'undefined') {
                try {
                    var t;
                    if (attrs.template.indexOf('uploaderTypes') >= 0) {
                        t = eval(attrs.template);
                    } else {
                        t = eval('uploaderTypes.' + attrs.template);
                    }
                    return getStaticUrl('/Templates/Uploader/' + t + '.html');
                } catch (e) {
                    return getStaticUrl('/Templates/Uploader/Html5DisciplineContent.html');
                }
            } else {
                return getStaticUrl('/Templates/Uploader/html5DisciplineContent.html');
            }
        },
        scope: {
            maxFilesize: '@',
            multiplefiles: '=',
            hidesupportedformat: '=',
            onsuccess: '&',
            buttontext: '@',
            filetypes: '@',
            oncancel: '&'
        },
        controller: function ($scope, $element, $attrs) {
            var me = $scope;
            me.message = null;
            me.maxFiles = 50;
            me.progress = 0;
            me.inputValue = null;
            me.success = null;
            me.startUpload = false;
            me.inputElement = null;
            me.filesSize = 0;
            me.filesName = '';
            me.maxFileSize = 100000000;
            me.rootScope = getRootScope($scope);
            me.showProgressBar = true;
            me.FileSelectHandler = function (element, event) {
                if (me.startUpload == true)
                    return;
                me.startUpload = true;
                if (!$scope.$$phase)
                    $scope.$apply('startUpload');

                me.FileDragHover(event);

                // fetch FileList object
                var files = element.files || element.dataTransfer.files;

                var listFiles = objToArray(files);
                me.filesSize = listFiles.sum('size');
                me.filesName = listFiles.length == 1 ? listFiles[0].name : listFiles.selectToArray('name').join(', ');
                if (!$scope.$$phase)
                    $scope.$apply();
                // process all File objects
                for (var i = 0, f; f = files[i]; i++) {
                    me.ParseFile(f);
                    me.UploadFile(f, i == (files.length - 1));
                }

            };
            me.FileDragHover = function (e) {
                e.stopPropagation();
                e.preventDefault();
                //e.target.className = (e.type == "dragover" ? "hover" : "");
            };
            me.Output = function (msg) {
                me.message = msg;
            }
            me.ParseFile = function (file) {

                me.Output(
                    "<p>File information: <strong>" + file.name +
                    "</strong> type: <strong>" + file.type +
                    "</strong> size: <strong>" + file.size +
                    "</strong> bytes</p>"
                );

                // display text
                if (file.type.indexOf("text") == 0) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        me.Output(
                            "<p><strong>" + file.name + ":</strong></p><pre>" +
                            e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
                            "</pre>"
                        );
                    }
                    reader.readAsText(file);
                }

                // display an image
                if (file.type.indexOf("image") == 0) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        me.Output(
                            "<p><strong>" + file.name + ":</strong><br />" +
                            '<img src="' + e.target.result + '" /></p>'
                        );
                    }
                    reader.readAsDataURL(file);
                }
            }
            me.cleanInput = function () {
                if (me.inputElement != null)
                    me.inputElement.val('');
                me.showProgressBar = true;
            };
            me.UploadFile = function (file, lastFile) {
                var xhr = new XMLHttpRequest();
                if (xhr.upload && file.size <= me.maxFileSize) {
                    me.showLoading();
                    xhr.onreadystatechange = function (e) {
                        if (xhr.readyState == 4) {
                            me.showProgressBar = false;
                            me.success = (xhr.status == 200 ? "success" : "failure");
                            if (typeof (me.onsuccess) == 'function')
                                me.onsuccess({
                                    result: {
                                        file: file,
                                        response: me.strToObj(xhr.response),
                                        after: function () {
                                            me.startUpload = false;
                                            me.cleanInput();
                                            me.hideLoading();
                                        }
                                    }
                                });

                        }
                    };

                    xhr.onerror = function (e) {
                        console.error(e.statusText);
                        me.success = 'failure';
                        me.cleanInput();
                    };

                    // start upload

                    var __handlerUrl = me.getHandler();
                    var _auth = me.getAuth();
                    xhr.open("POST", __handlerUrl, true);
                    xhr.setRequestHeader("X_FILENAME", unescape(encodeURIComponent(file.name)));
                    xhr.setRequestHeader("auth", _auth);
                    //xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
                    //xhr.setRequestHeader('Content-Type', 'undefined');
                    xhr.send(file);
                }
                else {
                    me.startUpload = false;
                    showNegativeMsg("O tamanho da imagem não pode ser maior que 100mb.", 100000);
                }
            };
            me.strToObj = function (str) {
                return Function(
                    'return ' + str + ';'
                )();
            };
            me.getHandler = function () {
                return '/Controls/CustomUpload2/HtmlFileCompressUpload.ashx';
            };
            me.getAuth = function () {
                return getPropertyByName(me.rootScope, 'serverParameters.html5UploaderParameters.EncryptedUserId')
            };
            me.onInit = function () {
                if (getPropertyByName(me, 'multiplefiles') == true) {
                    $($element).find('input').attr('multiple', 'multiple');
                }

                if ($element != null) {
                    me.inputElement = $($element).find('input[type="file"]');
                }
            };
            me.cancel = function () {
                if (typeof ($scope.oncancel) == 'function')
                    $scope.oncancel();
            };
            me.showLoading = function () {
                let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content') ;
                load = pc.find('.bgLoading');
                load.show();
            };
            me.hideLoading = function () {
                let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content');
                load = pc.find('.bgLoading');
                load.hide();
            };
        }
    };
});

ILangApp.directive('circleGraphic', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: getStaticUrl('/Templates/KPI/CircleGraphic.html'),
        scope: {
            title: '@',
            value: '=',
            extraclass: '@'
        }, controller: function ($scope) {
            if ($scope.value == null || $scope.value == '')
                $scope.value = 0;
        }
    };
});

ILangApp.directive('blindAttr', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var value = attr.blindAttr == "false";
            if (value == true) {
                $(element).attr('aria-hidden', "true").attr('role', "presentation");
            } else {
                $(element).removeAttr('aria-hidden').removeAttr('role');
            }
        }
    };
});

ILangApp.directive('ulifeTinymce', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            unique: '@'
        },
        template: '<textarea id="{{unique}}" class="inputFullWidth" placeholder="Comente essa postagem...2"></textarea>',
        link: function ($scope, $element, attr, parentDirectCtrl) {
            window.setTimeout(function () {
                InitializeTinyMCEOptionSimpleMode($scope.unique, function () { console.log('blur'); })
                //InitializeTinyMCE($scope.unique)
            }, 200);

        }
    };
})

ILangApp.directive("formatDate", function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, modelCtrl) {
            modelCtrl.$formatters.push(function (modelValue) {
                return new Date(modelValue);
            })
        }
    }
});
ILangApp.directive("formatTime", function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, modelCtrl) {
            modelCtrl.$formatters.push(function (modelValue) {
                return new Date('1970-01-01T' + modelValue);
            })
        }
    }
});
ILangApp.directive('ngConfirmMsg', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmMsg || "Tem certeza?";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }])

ILangApp.directive('ulifeResource', function($timeout, $parse) {
    return {
        link: function(scope, element, attrs) {
            var _rname = $(element).attr('ulife-resource');
            var _rvalue = eval(_rname);
            $(element).text(_rvalue);
        }
    };
});
//####################################################
//              <-    DIRECTIVES
//                    CONTROLLERS            ->
//####################################################
ILangApp.controller('logoutWarningDialogController', function ($scope, $element, $attrs, $interval) {
    $scope.element = $element;
    $scope.defaultCountdown = parseInt($attrs.secondscountdown);
    $scope.remainingSeconds = $scope.defaultCountdown;
    $scope.promise = null;
    $scope.show = function () {
        var me = this;

        $(me.element).removeClass('hide');

        me.remainingSeconds = me.defaultCountdown;

        var callAtInterval = function () {
            me.remainingSeconds--;

            if (me.remainingSeconds <= 0) {
                $interval.cancel(me.promisse);
                me.doLogout();
            }
        };

        me.promisse = $interval(callAtInterval, 1000);
    };
    $scope.keepConnected = function () {
        $interval.cancel(this.promisse);
        $(this.element).addClass('hide');
        this.remainingSeconds = this.defaultCountdown;
        ILangSettings.Logout.StartCount();
    };
    $scope.doLogout = function () {
        document.location.href = ILangSettings.Logout.LoginUrl;
    };
});

ILangApp.controller('initialPageSelectController', function ($scope, $element, $http, $attrs, $interval) {
    $scope.element = $element;
    $scope.page;
    $scope.selectPage = function (pageId) {
        if (pageId == 'cardQb') {
            $('#cardQb').addClass('active');
            $('#cardDc').removeClass('active');
            $scope.page = ILangSettings.Sites.ILang() + "/QuestionPool/QuestionBucket.aspx";
        }
        else {
            $('#cardDc').addClass('active');
            $('#cardQb').removeClass('active');
            $scope.page = ILangSettings.Sites.Admin() + '/Discipline';
        }
    };
    $scope.confirmSelectedPage = function () {
        var me = this;
        if (me.page == undefined || me.page == '') {
            showNegativeMsg('Por gentileza, selecione uma opção para prosseguir.', 4000);
        }
        else {
            var ajaxConfig = { url: ILangSettings.Sites.AdminAPI() + '/UserSettings/InitialPageSelect', cache: false };
            ajaxConfig.method = 'POST';
            ajaxConfig.withCredentials = true;
            ajaxConfig.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
            ajaxConfig.data = 'Page=' + me.page;
            $http(ajaxConfig).success(function (result, status) {
                if (result.Success) {
                    window.location = result.Result;
                    me.close();
                }
                else {
                    showNegativeMsg(result.ErrorMessage, 4000);
                }
            });
        }
    };
    $scope.redirectPageInfo = function () {
        var me = this;
        var ajaxConfig = { url: ILangSettings.Sites.AdminAPI() + '/UserSettings/RedirectInfoPage', cache: false };
        ajaxConfig.method = 'POST';
        ajaxConfig.withCredentials = true;
        ajaxConfig.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
        ajaxConfig.data = '';

        $http(ajaxConfig).success(function (result, status) {
            if (result.Success) {
                me.close();
                window.location = ILangSettings.Sites.ILang() + "/Community/FullProfile.aspx";
            }
            else {
                showTopMsg(result.ErrorMessage, 4000);
            }
        });
    };
    $scope.close = function () {
        var me = this;

        $(me.element).addClass('hide');
    };
});

ILangApp.directive('ngMultiTransclude', function () {
    return {
        controller: function ($scope, $element, $attrs, $transclude) {
            if (!$transclude) {
                throw {
                    name: 'DirectiveError',
                    message: 'ng-multi-transclude found without parent requesting transclusion'
                };
            }
            this.$transclude = $transclude;
        },

        link: function ($scope, $element, $attrs, controller) {
            var selector = '[name=' + $attrs.ngMultiTransclude + ']';
            var attach = function (clone) {
                var $part = clone.find(selector).addBack(selector);
                $element.html('');
                $element.append($part);
            };

            if (controller.$transclude.$$element) {
                attach(controller.$transclude.$$element);
            }
            else {
                controller.$transclude(function (clone) {
                    controller.$transclude.$$element = clone;
                    attach(clone);
                });
            }
        }
    };
});

ILangApp.directive('ngRightClick', function ($parse) {
    return function (scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function (event) {
            scope.$apply(function () {
                event.preventDefault();
                fn(scope, { $event: event });
            });
        });
    };
});

//####################################################
//              <-    DIRECTIVES
//                    FILTER            ->
//####################################################

ILangApp.filter('trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
ILangApp.filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
}])
ILangApp.filter('maxLength', function () {
    return function (text, maxLength) {
        maxLength = maxLength || 40;
        if (text != undefined && text != null) {
            return text.substr(0, maxLength);
        }

    };
});
ILangApp.filter('cutAtLength', function () {
    return function (text, maxLength, separator) {
        maxLength = maxLength || 40;
        if (text != undefined && text != null) {
            if (text.length > maxLength) {
                separator = separator || ',';
                var arr = text.split(separator);
                var str = arr.shift();
                for (var i = 0; i < arr.length; i++) {
                    var next = arr[i]
                    if ((str.length + next.length) <= maxLength) {
                        str += separator + arr.shift();
                    }
                } var plus = '';
                if (arr.length > 0) {
                    plus = ' +' + arr.length;
                }
                if (str.length > maxLength)
                    str = str.substr(0, maxLength) + '...'

                return str + plus;
            } else {
                return text;
            }
        }

    };
});
ILangApp.filter('highlight', function ($sce) {
    return function (text, phrase) {
        if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
            '<span class="highlighted">$1</span>')

        return $sce.trustAsHtml(text)
    }
});
ILangApp.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        if (input == '')
            return '';
        if (decimals == -1)
            return input;
        return $filter('number')(input, decimals) + '%';
    };
}]);
ILangApp.filter('rounded', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input, decimals);
    };
}]);

//####################################################
//              <-     FILTER
//                     FACTORY      ->
//####################################################
ILangApp.factory(
    "traceService",
    function () {
        return ({
            print: function () { return 1; }
        });
    }
);
var __lastLogError = {
    Time: null,
    Msg: null
};
ILangApp.factory('$exceptionHandler', ['$log', 'traceService', function ($log, traceService) {
    return function (exception, cause) {
        try {
            console.log(exception);
            var msg;
            var customMsg = false;
            var showRefreshBto = true;
            var _closeAfter = null;
            if (typeof (exception) == 'string') {
                msg = exception;
            } else {
                msg = getPropertyByName(exception, 'message');
                customMsg = compareIfSafe(exception, 'custom', true);
                _closeAfter = getPropertyByName(exception, 'closeAfter', null);
                var _refreshBto = getPropertyByName(exception, 'showRefreshBto');
                if (_refreshBto != null)
                    showRefreshBto = _refreshBto;
            }

            if (__lastLogError.Time == null) {
                __lastLogError.Time = (new Date()).getTime();
                __lastLogError.Msg = msg;
            } else if (__lastLogError.Msg == msg) {
                var now = (new Date()).getTime();
                if (now - __lastLogError.Time < 1000 * 5) {
                    return;
                } else {
                    __lastLogError.Time = (new Date()).getTime();
                }
            }

            var failedToLoadTemplateMsg = msg != null &&
                msg.trim() != '' &&
                msg.toLowerCase() != 'null' &&
                (msg.toLowerCase().indexOf('failed to load template') >= 0 ||
                    msg.toLowerCase().indexOf('error loading template') >= 0 ||
                    msg.toLowerCase().indexOf('[$compile:tpload]') >= 0 ||
                    msg.toLowerCase().indexOf('invalid argument') >= 0 ||
                    msg.toLowerCase().indexOf('argumento invalido') >= 0 ||
                    msg.toLowerCase().indexOf('argumento inválido') >= 0);

            var fn = function (result) {
                if (failedToLoadTemplateMsg == true)
                    return;

                var errorID = getPropertyByName(result, 'Result');

                var type = null;
                if (customMsg == true) {
                    type = 'business'
                } else if (msg != null) {
                    if (msg.indexOf('chat parou de funcionar') >= 0) {
                        type = 'chat';
                    } else if (msg.indexOf('SignalR') >= 0) {
                        type = 'SignalR';
                    }
                }

                if (type == 'business') {
                    showTopErrorMsg(customMsg ? msg : null, _closeAfter, errorID, showRefreshBto);
                }
                logErrorOnHiddenField(msg == null ? JSON.stringify(exception) : msg, errorID || 0, type); //<-- fullstory
                $log.error(exception);
                console.error(exception);
            }

            if (!customMsg && isSafeToUse(ILang, 'ErrorHandling') && isSafeToUse(document, 'location.href')) {
                ILang.ErrorHandling.HTTPHandler.Send(msg, document.location.href, 0, failedToLoadTemplateMsg == false, function (result) {
                    fn(result);
                });
            } else {
                fn(null);
            }
        } catch (e) {
            console.log('e:', e);
            console.log('exception: ', exception);
        }

    };
}]);

ILangApp.factory('focus', function ($rootScope, $timeout) {
    return function (name) {
        $timeout(function () {
            $rootScope.$broadcast('focusOn', name);
        });
    }
});

ILangApp.run(function ($rootScope, $document) {
    $('div[ilang-cloak]').each(function (index, item) {
        $(item).show();
    });

    $document.on('click', function (event) {
        if (event.which == 1) {
            $rootScope.$broadcast('document_click');
        }
    });

    $('body').on('OnlineStarted', function () {
        ILangHub.server.getCurrentChatRooms();
    });

});

//####################################################
//                <-   FACTORY
//                     GENERIC ANGULAR  ->
//####################################################
function baseController(base, http, jsParameters, services) {
    base.jsParameters = jsParameters || {};
    base.services = services || {};
    //default parameters
    base.defaultParameters = {
        infiniteScrolling: true,
        startSearchTypeAmount: 3,
        doSmallSearch: true,
        inputSearchDelayToStartSearch: 500,
        keepSearchCriteriaOnLocalStorage: false,
        itemsPerPage: 18,
        scrollPercentOfPageTriggerSearch: 0.95,
        scrollTriggerIncrease: 0.0,
        bigSearchListSize: 20,
        smallSearchListSize: 5,
        pageIndexInitialValue: 1,
        trackPageIndex: false,
        ajaxCache: false,
        isDialog: false,
        defaultErrorMsg: 'ocorreu um erro ao tentar acessar a lista.',
        includPosition: includPositions.Bottom,
        autoShowLoading: true,
        showTotalCountOnSumaryWhenNoSearch: true,
        useKnockoutMapping: false,
        resultList: true,
        showDialogCloseButton: true,
        fixedParameter: {},
        dialogVisibility: false,
        unshift: false,
        unshitOnShowMore: false,
        smallListHoverItem: -1,
        onItemIsSelected: null,
        reverse: false,
        rewriteHash: true,
        hybridSearch: false,
        listLimitPageSize: 40,
        ilangSearchEnabled: true,
        showNotTreatedErro: true,
        trackPageIndex: true,
        firstRequestListSize: null,
        makeListDistinctBy: null
    };
    for (k in base.defaultParameters) {
        if (!base.jsParameters.hasOwnProperty(k)) {
            base.jsParameters[k] = base.defaultParameters[k];
        }
    }

    //Properties
    base.IamTheRootScope = 1;
    base.contextParameters = function (key, defaultListSize, infiniteScrolling) {
        this.key = key;
        this.hasMorePages = false;
        this.totalCount = null;
        this.list = [];
        this.tagList = [];
        this.dto = {};
        this.pageIndex = 1;
        this.listSize = defaultListSize || 20;
        this.smallSearchIsRunning = false;
        this.searchboxLoading = false;
        this.emptyResult = false;
        this.infiniteScrolling = infiniteScrolling;
        this.searchParameters = {};
        this.sortParameters = {};
        this.trackHash = false;
        this.enabledList = true;
        this.searchTxt = '';
        this.isSearchBoxActive = false;
        this.smallListHoverItem = -1;
        this.searchFn = null;
        this.loading = false;
        this.firstLoading = false
        this.done = false;
        this.clean = function () {
            this.list = null;
            this.tagList = null;
        }
    };
    //Search
    base.bigContext = new base.contextParameters('bigContext', base.jsParameters.bigSearchListSize, base.jsParameters.infiniteScrolling);
    base.smallContext = new base.contextParameters('smallContext', base.jsParameters.smallSearchListSize, false);

    base.currentContext = null;

    base.smallContext.isSearchBoxActive = false;
    base.searchSummary = base.jsParameters.searchSummaryInitialValue || '';

    base.searchTimeManager = null;

    //small list
    base.userIsNavigatingOnSearchOptions = false;
    base.smallContext.smallListHoverItem = -1;
    base.searchTypes = _searchTypes.Big;

    base.loading = false;
    base.busy = false;
    base.divContext = null;

    //watch
    base.setResultTypes = function (id) {
        base._overwriteResultType = id;
    }
    base.setLoadSettings = function (handler, data, id, mainScope, _serverParameters) {
        var serverParameters = _serverParameters || {};
        serverParameters.contextID = id;
        if (serverParameters.hasOwnProperty('actions') == false)
            serverParameters.actions = {};

        serverParameters.actions.load = {};
        serverParameters.actions.load.handler = handler;
        //serverParameters.actions.load.data = data;
        serverParameters.fixedParameters = data;

        serverParameters.enums = {};
        serverParameters.enums.resultType = {};
        serverParameters.enums.resultType.List = 2;
        serverParameters.enums.resultType.DtoList = 3;
        serverParameters.enums.resultType.DtoAndList = 4;


        serverParameters.enums.actionType = {
            DELETE: 3,
            GET: 0,
            POST: 1,
            PUT: 2
        };
        serverParameters.mainScope = mainScope;
        serverParameters.actions.load.resultType = serverParameters.enums.resultType.List //base.serverParameters.enums.resultType.List ;
        if (isSafeToUse(base, '_overwriteResultType')) {
            serverParameters.actions.load.resultType = base._overwriteResultType;
        }


        base.load(serverParameters);
    };
    base.load = function (serverParameters) {
        base.serverParameters = serverParameters;
        if (base.serverParameters.scopeEnums != null) {
            copyAllPropersTo(base, base.serverParameters.scopeEnums);
            delete base.serverParameters.scopeEnums;
        }
        if (base.serverParameters.scopeParameters != null) {
            copyAllPropersTo(base, base.serverParameters.scopeParameters);
            delete base.serverParameters.scopeParameters;
        }
        if (base.serverParameters.windowEnums != null) {
            copyAllPropersTo(window, base.serverParameters.windowEnums);
            delete base.serverParameters.windowEnums;
        }
        if (typeof (window.R) == 'object') {
            base.R = {};
            copyAllPropersTo(base.R, window.R);
        }

        if (typeof (base.serverParameters.selfDialog) != 'undefined' && base.serverParameters.selfDialog != null)
            base.serverParameters.selfDialog.Visible = (base.serverParameters.selfDialog.Visible == "true");

        if (compareIfSafe(base.serverParameters.parameters, '_isLocal', true)) {
            console.log('serverParameters', serverParameters);
        }

        base.divContext = $('#' + base.serverParameters.contextID);
        window.setTimeout(function () {

            //serverParameters
            var data = {};
            if (typeof (serverParameters.actions) != 'undefined' && typeof (serverParameters.actions.load) != 'undefined') {
                if (typeof (serverParameters.actions.load.data) != 'undefined') {
                    data = serverParameters.actions.load.data || {};
                } else if (typeof (serverParameters.actions.load.parametersFunction) != 'undefined') {
                    try {
                        data = eval('base.' + parametersFunction + '();');
                    } catch (e) {
                        data = {};
                    }
                }
            }

            //data passed on serverParameters
            base.jsParameters.fixedParameters = base.jsParameters.fixedParameters || {};


            //filter
            if (typeof (serverParameters.filters) != 'undefined') {
                for (i in serverParameters.filters) {
                    data[i] = serverParameters.filters[i].selected;

                    if (typeof (serverParameters.filters[i].list) != 'undefined') {
                        $(serverParameters.filters[i].list).each(function(filterIndex, filterItem) {
                            if (filterItem.description.indexOf('R.') == 0) {
                                try {
                                    filterItem.description = eval(filterItem.description);
                                } catch { }
                            }
                        });
                    }
                }
            }
            //parametersFirtsLoad

            //hash
            if (base.bigContext.trackHash == true || base.serverParameters.trackAllElements == true) {
                base.track.populate();
                for (i in base.track.obj) {
                    if (base.serverParameters.filters.hasOwnProperty(i)) {
                        base.serverParameters.filters[i].selected = base.track.obj[i];
                        if (base.serverParameters.filters[i].alias == null) {
                            base.serverParameters.filters[i].alias = base.divContext.find('[key="' + i + '"]').attr('alias');
                        }
                    } else if (base.serverParameters.drops.hasOwnProperty(i)) {

                        if (base.serverParameters.drops[i].list.any({ value: base.track.obj[i] }) || base.serverParameters.drops[i].loadAction != null) {
                            base.serverParameters.drops[i].selected = base.track.obj[i];
                        } else {
                            base.track.clean(i);
                        }

                        if (base.serverParameters.drops[i].alias == null) {
                            base.serverParameters.drops[i].alias = base.divContext.find('[key="' + i + '"]').attr('alias');
                        }
                    }
                }
            }

            if (typeof (base.jsParameters.onBeforeLoad) == 'function')
                base.jsParameters.onBeforeLoad(base);

            if (typeof (serverParameters.actions) != 'undefined' && typeof (serverParameters.actions.load) != 'undefined' && compareIfSafe(serverParameters, 'multiRequestOnLoad', true) == false) {

                if (base.serverParameters.mainScope && base.serverParameters.hideTopLoading == false) {
                    base.$watch('bigContext.loading', function (newValue, oldValue) {

                        if (typeof (window.ToggleLoading) == 'function')
                            ToggleLoading(newValue);
                    });
                }

                base.divContext.find('big-list').each(function () {
                    $.each(this.attributes, function () {
                        if (this.specified) {
                            var name = '';
                            if (this.name.indexOf('-') > 0) {
                                $(this.name.split('-')).each(function (index, item) {
                                    if (name == '') {
                                        name = item.substr(0, 1) + item.substr(1, item.length).toLowerCase();
                                    } else {
                                        name += item.substr(0, 1).toUpperCase() + item.substr(1, item.length).toLowerCase();
                                    }
                                });
                            } else {
                                name = this.name;
                            }
                            if (base.bigContext.hasOwnProperty(name)) {
                                if (isNaN(this.value)) {
                                    if (this.value == "true") {
                                        base.bigContext[name] = true;
                                    } else if (this.value == "false") {
                                        base.bigContext[name] = false;
                                    } else {
                                        base.bigContext[name] = this.value;
                                    }
                                } else {
                                    base.bigContext[name] = parseFloat(this.value);
                                }

                            }
                        }
                    });
                });

                base.divContext.find('small-list').each(function () {
                    $.each(this.attributes, function () {
                        if (this.specified) {
                            var name = '';
                            if (this.name.indexOf('-') > 0) {
                                $(this.name.split('-')).each(function (index, item) {
                                    if (name == '') {
                                        name = item.substr(0, 1) + item.substr(1, item.length).toLowerCase();
                                    } else {
                                        name += item.substr(0, 1).toUpperCase() + item.substr(1, item.length).toLowerCase();
                                    }
                                });
                            } else {
                                name = this.name;
                            }
                            if (base.smallContext.hasOwnProperty(name)) {
                                base.smallContext[name] = setParameters(name);
                            }
                        }
                    });
                });


                if (base.serverParameters.requestOnLoad == undefined || base.serverParameters.requestOnLoad == null || base.serverParameters.requestOnLoad == true) {
                    base.bigContext.firstLoading = true;
                    data['FirstLoad'] = true;
                    base.bigSearch(data, function () { base.bigContext.firstLoading = false; base.bigContext.done = true; });
                }


                //verify show more has been added
                base.bigContext.infiniteScrolling = base.bigContext.infiniteScrolling && base.divContext.find('show-more').length <= 0;

                if (base.bigContext.infiniteScrolling == true) {
                    if (base.jsParameters.scrollingContextId != undefined || base.serverParameters.isDialog) {
                        var dialogY;
                        var contextID = base.serverParameters.parameters.contextID || base.serverParameters.contextID;
                        if (base.jsParameters.scrollingContextId != undefined) {
                            var __scrollingContextId = (base.jsParameters.scrollingContextId.indexOf('#') >= 0) ? base.jsParameters.scrollingContextId : '#' + base.jsParameters.scrollingContextId;
                            dialogY = $('#' + contextID).find(__scrollingContextId);
                        } else {
                            dialogY = $('#' + contextID).find('.overY');
                        }

                        if (dialogY.length > 0) {
                            dialogY.scroll(function () {
                                if (!base.loading && !base.busy && base.bigContext.hasMorePages == true) {
                                    var s = $(this).scrollTop();
                                    var d = $(this)[0].scrollHeight;
                                    var c = $(this).height();
                                    var percentage = (s / (d - c));
                                    if (percentage > (base.jsParameters.scrollPercentOfPageTriggerSearch) && base.bigContext.infiniteScrolling == true) {
                                        base.df__showMore();
                                    }

                                }
                            });
                        }
                    }
                    else {
                        $(window).on('scroll', function (a) {
                            if (!base.loading && !base.busy && base.bigContext.hasMorePages == true && base.divContext.is(':visible')) {
                                var s = $(window).scrollTop();
                                var d = $(document).height();
                                var c = $(window).height();
                                var percentage = (s / (d - c));
                                if (percentage > (base.jsParameters.scrollPercentOfPageTriggerSearch) && base.bigContext.infiniteScrolling == true && base.currentContext.bigListLoading == false) {
                                    base.df__showMore();
                                }

                            }
                        });
                    }
                }
            } else if (serverParameters.multiRequestActionList != null) {
                base.loadSearch = base.multiRequestSearch;
                if (serverParameters.multiRequestOnLoad == true) {
                    base.multiRequestFirstLoading = true;
                    base.bigSearch(data, function () { base.multiRequestFirstLoading = false; base.bigContext.done = true; });
                }
            }
            else {
                base.currentContext = base.bigContext;
            }
            $('#' + base.serverParameters.contextID).removeAttr('ng-init');
            base.divContext.removeClass('hideAllCont');//.removeClass('ng-hide');
            base.divContext.removeAttr('style');
            base.divContext.find('.sdContent').removeClass('hideAllCont');
            base.divContext.find('.socialDialog ').removeAttr('style');
        }, 50);
    };
    base.df__searchSummary = function (totalCount, searchCriteria) {
        var totCount = totalCount == null ? base.currentContext.totalCount : totalCount;
        var sc = searchCriteria == null ? base.smallContext.searchTxt : searchCriteria

        if (typeof (base.jsParameters.onSearchSummary) != 'undefined') {
            return base.jsParameters.onSearchSummary(base);
        } else if (base.searchTypes != _searchTypes.Small) {
            if ((totCount == null || totCount == 0) && sc == '') {
                return 'Nenhum registro';
            }
            if (totCount == null) {
                return base.jsParameters.searchSummaryDefaultText || '';
            }
            if (totCount == 0) {
                return base.jsParameters.searchSummaryNoResult || 'Nenhum resultado para <b>' + sc + '</b>';
            }
            if (totCount > 0 && sc != '') {
                return base.jsParameters.searchSummaryText || totCount + ' resultados para <b>' + sc + '</b>';
            } else {
                if (base.jsParameters.showTotalCountOnSumaryWhenNoSearch == true) {
                    return totCount + (totCount > 1 ? ' registros' : ' registro');
                } else {
                    return '';
                }
            }

        }
    }
    base.loadSearch = function (before, after, _parameters, config) {

        var data = base.getFixedParameters(_parameters);



        if (base.smallContext.searchTxt != null && base.smallContext.searchTxt != '' && base.searchTypes == _searchTypes.Big)
            base.track.write('searchTxt', base.smallContext.searchTxt);

        //search
        if (typeof (data.eventType) == 'undefined')
            data.eventType == 'search';

        if (base.track.read('searchTxt') != null && base.bigContext.trackHash == true) {
            base.smallContext.searchTxt = base.track.read('searchTxt');
            data.SearchCriteria = base.track.read('searchTxt');
        } else if (!data.hasOwnProperty("SearchCriteria")) {
            data.SearchCriteria = base.smallContext.searchTxt;
        }


        if (!data.hasOwnProperty("PageIndex"))
            data.PageIndex = base.currentContext.PageIndex;

        if (!data.hasOwnProperty("PageSize")) {
            if (base.jsParameters.firstRequestListSize != null && base.bigContext.firstLoading == true) {
                data.PageSize = base.jsParameters.firstRequestListSize;
            } else {
                data.PageSize = base.currentContext.listSize;
            }
        }




        data = base.verifyParameterAlias(data);

        if (typeof (before) == 'function')
            before();

        base.loading = true;
        if (base.currentContext != null) {
            base.currentContext.loading = true;
        }
        base.busy = true;

        if (base.serverParameters.mainScope) {
            //console.time(base.serverParameters.contextID);
            base.currentContext.bigListLoading = true;
        }
        var successCallback = function (result) {
            if (typeof (base.jsParameters.onRequestIsDone) == 'function') {
                base.jsParameters.onRequestIsDone(result);
                return;
            }

            if (result.Success) {
                var list = (typeof (base.jsParameters.resultWay) != 'undefined' && typeof (base.jsParameters.resultWay.List) != 'undefined') ? eval(base.jsParameters.resultWay.List) : result.Result;
                var hasMorePages = (typeof (base.jsParameters.resultWay) != 'undefined' && typeof (base.jsParameters.resultWay.List) != 'undefined') ? eval(base.jsParameters.resultWay.HasMorePage) : result.HasMorePages;
                var totalCount = (typeof (base.jsParameters.resultWay) != 'undefined' && typeof (base.jsParameters.resultWay.List) != 'undefined') ? eval(base.jsParameters.resultWay.TotalCount) : result.TotalCount;

               if (base.jsParameters.makeListDistinctBy != null) {
                    var list = base.df__distinctList(list, base.currentContext.list);
                }

                if (base.jsParameters.reverse)
                    list = list.reverse();

                if (typeof (base.serverParameters.actions.load.resultType) != 'undefined' && base.serverParameters.actions.load.resultType == base.serverParameters.enums.resultType.List ) {

                    for (var i = 0; i < list.length; i++) {
                        if (typeof (base.jsParameters.onInsertItem) == 'function') {
                            var item = base.jsParameters.onInsertItem(list[i]);
                            base.currentContext.list.push(item);
                        } else {
                            if (base.jsParameters.unshift == false) {
                                base.currentContext.list.push(list[i]);
                            } else {
                                base.currentContext.list.unshift(list[i]);
                            }
                        }
                    }
                    base.currentContext.hasMorePages = hasMorePages;
                    base.currentContext.totalCount = totalCount;
                    base.currentContext.emptyResult = (list == null || list.length == 0) && base.smallContext.searchTxt != "";
                    
                } else if (typeof (base.serverParameters.actions.load.resultType) != 'undefined' && base.serverParameters.actions.load.resultType == base.serverParameters.enums.resultType.DtoList) {
                    var _list = getPropertyByName(result, 'Result') || getPropertyByName(result, 'List');
                    for (var i = 0; i < _list.length; i++) {
                        if (typeof (base.jsParameters.onInsertItem) == 'function') {
                            var item = base.jsParameters.onInsertItem(_list[i]);
                            base.currentContext.list.push(item);
                        } else if (typeof (base.jsParameters.onManageListAppend) == 'function') {
                            base.currentContext.list = base.jsParameters.onManageListAppend(base.currentContext.list, _list[i])
                        }
                        else {
                            if (base.jsParameters.unshift == false) {
                                base.currentContext.list.push(_list[i]);
                            } else {
                                base.currentContext.list.unshift(_list[i]);
                            }
                        }
                    }
                    base.currentContext.hasMorePages = hasMorePages;
                    base.currentContext.totalCount = totalCount;
                    base.currentContext.emptyResult = (base.currentContext.list == null || base.currentContext.list.length == 0) && base.smallContext.searchTxt != "";

                    for (var i in result) {
                        base.currentContext.dto[i] = result[i];
                    }
                } else {
                    base.currentContext.dto = (list == undefined || list == null) ? result : list;
                }

                //searchSummary
                base.searchSummary = base.df__searchSummary();

                if (typeof (base.jsParameters.onLoadSuccess) == 'function')
                    base.jsParameters.onLoadSuccess(result, base);

            } else {
                if (typeof (base.jsParameters.onLoadFail) == 'function')
                    base.jsParameters.onLoadFail(result, base);
            }
            base.loading = false;
            if (base.currentContext != null) {
                base.currentContext.loading = false;
            }
            base.busy = false;

            if (typeof (after) == 'function')
                after(result);

            if (base.serverParameters.mainScope) {
                //console.timeEnd(base.serverParameters.contextID);
                base.currentContext.bigListLoading = false;
            }


            if (typeof (base.jsParameters.onAfterLoad) == 'function')
                base.jsParameters.onAfterLoad(result, base);

            base.updateFilterCount();
        };

        var errorCallback = function (a, b, c) {
            base.loading = false;
            if (base.currentContext != null) {
                base.currentContext.loading = false;
                base.currentContext.bigListLoading = true;
            }
            base.busy = false;
            if (typeof (base.jsParameters.onLoadError) == 'function') {
                base.jsParameters.onLoadError(base, a, b, c);
            } else if (a != null && a.hasOwnProperty('ErrorMessage') && base.jsParameters.showNotTreatedErro == true) {
                throw {
                    message: a.ErrorMessage,
                    showRefreshBto: a.ErrorType == 1,
                    custom: a.ErrorType == 2,
                    closeAfter: a.ErrorType == 2 ? 5000 : null
                }
            }
        };

        var type = base.serverParameters.actions.load.type;
        if (typeof (type) == 'undefined') {
            type = base.serverParameters.enums.actionType.GET;
        }

        if (type == base.serverParameters.enums.actionType.GET) {
            base.ajax.get(
                base.serverParameters.actions.load.handler,
                data,
                successCallback,
                errorCallback,
                {
                    showLoading: base.df__getJsParametersByName('autoShowLoading', true),
                    trackRewrite: base.jsParameters.rewriteHash
                });
        }
        else if (type == base.serverParameters.enums.actionType.POST) {
            base.ajax.post(
                base.serverParameters.actions.load.handler,
                data,
                successCallback,
                errorCallback,
                {
                    showLoading: base.df__getJsParametersByName('autoShowLoading', true),
                    trackRewrite: base.jsParameters.rewriteHash
                });
        }
    }
    base.bigSearch = function (customParameter, after, clean, updateFilterCount) {
        base.smallContext.isSearchBoxActive = false;
        base.searchTypes = _searchTypes.Big;
        base.currentContext = base.bigContext;

        var _parameter = customParameter || {};

        _parameter = base.df__getAllFilters(customParameter);

        if (clean == undefined || clean == true)
            base.cleanList();


        base.loadSearch(
            function () { },
            function (result) {
                if (typeof (after) == 'function') {
                    after(base, result);
                }
                if (typeof (base.jsParameters.onBigSearchAfterLoad) == 'function')
                    base.jsParameters.onBigSearchAfterLoad(result, base);
            },
            _parameter
        );
    };
    base.df__getAllFilters = function (parameter) {
        var _parameter = parameter || {};
        _parameter = base.getFixedParameters(_parameter);
        _parameter = base.df__getSearchCriteriaFilter(_parameter);
        //searchParameters
        if (typeof (base.bigContext.searchParameters) != 'undefined') {
            $(base.bigContext.searchParameters).each(function (index, _param) {
                $(Object.keys(_param)).each(function (a, item) {
                    if (!_parameter.hasOwnProperty(item))
                        _parameter[item] = base.bigContext.searchParameters[item];
                });
            });
        }

        //sortParameters
        if (typeof (base.bigContext.sortParameters) != 'undefined') {
            $(base.bigContext.sortParameters).each(function (index, _param) {
                $(Object.keys(_param)).each(function (a, item) {
                    if (!_parameter.hasOwnProperty(item))
                        _parameter[item] = base.bigContext.sortParameters[item];
                });
            });
        }

        //fixedParameters
        if (typeof (base.serverParameters.fixedParameters) != 'undefined') {
            $(base.serverParameters.fixedParameters).each(function (index, _param) {
                $(Object.keys(_param)).each(function (a, item) {
                    if (!_parameter.hasOwnProperty(item)) {
                        _parameter[item] = base.serverParameters.fixedParameters[item];
                    }

                });
            });
        }

        //filters
        for (f in base.serverParameters.filters) {
            var filter = base.serverParameters.filters[f];
            if (typeof (filter.loadFilter) != 'undefined' && filter.loadFilter == true) {
                var a = typeof (filter.alias) == 'undefined' ? filter.key : filter.alias;
                _parameter[a] = filter.selected;
            }
        }

        //drops
        for (f in base.serverParameters.drops) {
            var drop = base.serverParameters.drops[f];
            if (typeof (drop.loadFilter) != 'undefined' && drop.loadFilter == true && _parameter.hasOwnProperty(drop.alias) == false) {
                if (drop.isCheckboxList == true)
                    base.df__dropChecklistSetText(drop);

                var dropalias = drop.alias == null || drop.alias == "" ? drop.key : drop.alias;
                _parameter[dropalias] = base.df__dropGetSelected(drop);

                // _parameter[drop.alias] = drop.selected;
            }
        }
        return _parameter;
    }
    base.df__getSearchCriteriaFilter = function (parameter) {
        var _parameter = parameter || {};
        if (base.track.read('searchTxt') != null && base.bigContext.trackHash == true) {
            base.smallContext.searchTxt = base.track.read('searchTxt');
            _parameter.SearchCriteria = base.track.read('searchTxt');
        } else if (!_parameter.hasOwnProperty("SearchCriteria")) {
            _parameter.SearchCriteria = base.smallContext.searchTxt;
        }
        return _parameter;
    };
    base.contextSearch = function (context) {
        if (typeof (context.searchFn) == 'function') {
            context.searchFn();
        }
    };
    base.smallSearch = function (context) {
        if (context != null) {
            base.contextSearch(context);
            return;
        }

        var _parameter = {};
        base.searchTypes = _searchTypes.Small;
        base.currentContext = base.smallContext;

        base.currentContext.smallSearchIsRunning = true;

        if (typeof (base.jsParameters.onSmallSearchBeforeLoad) == 'function')
            base.jsParameters.onSmallSearchBeforeLoad(base);

        base.loadSearch(
            function () { base.currentContext.searchboxLoading = true; base.cleanSmallList(); base.smallContext.isSearchBoxActive = true; },
            function () {
                base.currentContext.searchboxLoading = false;
                base.currentContext.smallSearchIsRunning = false;
                if (typeof (base.jsParameters.onSmallSearchAfterLoad) != 'undefined')
                    me.parameters.onSmallSearchAfterLoad(me);
            },
            _parameter,
            { doNotUpdateIndex: false });
    };
    base.multiRequestSearch = function (before, after, _parameters, config) {
        if (base.serverParameters.multiRequestActionList == null || base.serverParameters.multiRequestActionList.length == 0)
            return;

        if (typeof (base.multiResult) == 'undefined')
            base.multiResult = {};

        var data = base.getFixedParameters(_parameters);

        if (base.smallContext.searchTxt != null && base.smallContext.searchTxt != '' && base.searchTypes == _searchTypes.Big)
            base.track.write('searchTxt', base.smallContext.searchTxt);

        //search
        if (typeof (data.eventType) == 'undefined')
            data.eventType == 'search';

        data = base.df__getSearchCriteriaFilter(data);


        if (!data.hasOwnProperty("PageIndex"))
            data.PageIndex = base.currentContext.PageIndex;

        if (!data.hasOwnProperty("PageSize"))
            data.PageSize = base.currentContext.listSize;


        data = base.verifyParameterAlias(data);

        if (typeof (before) == 'function')
            before();

        base.loading = true;
        if (base.currentContext != null) {
            base.currentContext.loading = true;
        }
        base.busy = true;


        if (base.serverParameters.multiRequestActionAsync == true) {
            base.multiRequestActionAsync(_parameters, after);
        } else {
            base.multiRequestActionSync(_parameters, after, 0);
        }
    };
    base.multiRequestActionAsync = function (data, after) {
        for (var i = 0; i < base.serverParameters.multiRequestActionList.length; i++) {
            var act = base.serverParameters.multiRequestActionList[i];
            base.action(act, data, function (result, status, param) {
                base.multiResult[param.act] = result.Result;
                if (isSafeToUse(base, 'jsParameters.multiRequestActionAsyncAfterLoad.' + param.act) && (base.multiRequestFirstLoading == undefined || base.multiRequestFirstLoading == false))
                    base.jsParameters.multiRequestActionAsyncAfterLoad[param.act](result.Result);
                if (typeof (after) == 'function' && param.i == base.serverParameters.multiRequestActionList.length - 1)
                    after();
            }, undefined, null, { act: act, i: i });
        }
    };
    base.multiRequestActionSync = function (data, after, count) {
        if (base.serverParameters.multiRequestActionList.length - 1 < count) {
            if (typeof (after) == 'function')
                after();

            return;
        } else {
            var act = base.serverParameters.multiRequestActionList[count];
            base.action(act, data, function (result) {
                base.multiResult[act] = result.Result;
                count++;
                base.multiRequestActionSync(data, after, count)
            });
        }
    };
    base.cleanSmallList = function () {
        base.smallContext.list = [];
    }
    base.cleanList = function () {
        base.bigContext.PageIndex = base.jsParameters.pageIndexInitialValue;
        base.bigContext.smallPageIndex = base.jsParameters.pageIndexInitialValue;
        base.bigContext.list = [];
        if (base.jsParameters.trackPageIndex)
            base.track.write('pageIndex', base.bigContext.PageIndex);
    }
    base.actionNoLoading = function (key, parameters, onSuccess, onError, config, anyParameters) {
        var cfg = config || { showLoading: false };
        base.action(key, parameters, onSuccess, onError, cfg, anyParameters)
    }
    base.action = function (key, parameters, onSuccess, onError, config, anyParameters) {
        var act = base.serverParameters.actions[key];
        if (act != undefined && act != null) {
            if (typeof (base.serverParameters.actions[key].data) != 'undefined') {
                parameters = parameters || {};
                for (i in base.serverParameters.actions[key].data) {
                    if (!parameters.hasOwnProperty(i))
                        parameters[i] = setParameters(base.serverParameters.actions[key].data[i]);
                }
                if (typeof (base.serverParameters.fixedParameters) != 'undefined' && base.serverParameters.fixedParameters != null) {
                    for (i in base.serverParameters.fixedParameters) {
                        if (typeof (base.serverParameters.fixedParameters[i]) != 'function' && !parameters.hasOwnProperty(i)) {
                            parameters[i] = base.serverParameters.fixedParameters[i];
                        }
                    }
                }
            }
            if (isSafeToUse(base, act.parametersFunction)) {
                copyAllPropersTo(parameters, base[act.parametersFunction]());
            }

            switch (act.type) {
                case base.serverParameters.enums.actionType.GET:
                    base.ajax.get(act.handler, parameters, onSuccess, onError, config, anyParameters);
                    break;
                case base.serverParameters.enums.actionType.PUT:
                    base.ajax.put(act.handler, parameters, onSuccess, onError, config, anyParameters);
                    break;
                case base.serverParameters.enums.actionType.DELETE:
                    base.ajax.delete(act.handler, parameters, onSuccess, onError, config, anyParameters);
                    break;
                default:
                    base.ajax.post(act.handler, parameters, onSuccess, onError, config, anyParameters);
                    break;
            }

        }
    };
    base.getFixedParameters = function (data) {
        var _data = data || {};
        if (typeof (base.jsParameters.fixedParameters) != 'undefined') {
            $(base.jsParameters.fixedParameters).each(function (index, _param) {
                $(Object.keys(_param)).each(function (a, item) {
                    if (!_data.hasOwnProperty(item))
                        _data[item] = base.jsParameters.fixedParameters[item];
                });
            });
        }
        if (isSafeToUse(base, 'serverParameters.fixedParametersFunction')) {
            base.serverParameters.fixedParametersFunction.everyEach(function (item, key) {
                data[key] = base[item]();
            });
        }
        return _data;
    };
    base.verifyParameterAlias = function (data) {
        //parameters alias -> server
        if (typeof (base.serverParameters) != 'undefined') {
            if (typeof (base.serverParameters.parametersAlias) != 'undefined') {
                $(base.serverParameters.parametersAlias).each(function (index, item) {
                    if (data.hasOwnProperty(item.name)) {
                        data[item.alias] = data[item.name];
                        delete data[item.name];
                    }
                });
            }
            //parameters alias -> server
            if (typeof (base.jsParameters.parametersAlias) != 'undefined') {
                $(base.jsParameters.parametersAlias).each(function (index, item) {
                    if (data.hasOwnProperty(item.name)) {
                        data[item.alias] = data[item.name];
                        delete data[item.name];
                    }
                });
            }
        }

        return data;
    };
    base.updateFilterCountLoading = false;
    base.updateFilterCount = function (index) {
        if (base.updateFilterCountLoading)
            return;

        base.updateFilterCountLoading = true;
        var _index = index || 0;
        var filters = [];

        for (i in base.serverParameters.filters) {
            if (base.serverParameters.filters[i].hasOwnProperty("updateAction") && base.serverParameters.filters[i].updateAction != null) {
                filters.push(base.serverParameters.filters[i]);
            }
        }

        if (filters.length <= 0)
            return;

        var item = filters[_index];

        if (!item.updateAction)
            return;


        var data = base.getFixedParameters();
        data["searchCriteria"] = base.smallContext.searchTxt;

        base.action(item.updateAction, data, function (result) {
            if (result) {
                for (i in result.Result) {
                    for (k in item.list) {
                        if (item.list[k].name == i) {
                            item.list[k].count = result.Result[i];
                        }
                    }
                }
                base.updateFilterCountLoading = false;
                if (filters.length > _index + 1) {
                    base.updateFilterCount(_index + 1);
                }
            }
        });

    };
    base.ajax = {
        success: null,
        errorMsg: null,
        get: function (httpUrl, parameters, onSuccess, onError, config, anyParameters) {
            let ajaxConfig = { url: httpUrl, cache: false };
            ajaxConfig.method = 'GET';
            ajaxConfig.withCredentials = true;
            if (parameters == null) {
                ajaxConfig.params = {};
            }
            else {
                ajaxConfig.params = base.verifyParameterAlias(parameters);
            }
            ajaxConfig.cache = false;
            ajaxConfig.params._t = new Date().getTime();
            return this.request(ajaxConfig, onSuccess, onError, config, anyParameters);
        },
        post: function (httpUrl, parameters, onSuccess, onError, config, anyParameters) {
            let ajaxConfig = { url: httpUrl, cache: false };
            ajaxConfig.method = 'POST';
            ajaxConfig.withCredentials = (config == undefined || config.withCredentials == undefined ? true : config.withCredentials);
            ajaxConfig.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
            ajaxConfig.data = $.param(base.verifyParameterAlias(parameters));
            return this.request(ajaxConfig, onSuccess, onError, config, anyParameters);
        },
        put: function (httpUrl, parameters, onSuccess, onError, config, anyParameters) {
            let ajaxConfig = { url: httpUrl, cache: false };
            ajaxConfig.method = 'PUT';
            ajaxConfig.withCredentials = true;
            ajaxConfig.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
            ajaxConfig.data = $.param(base.verifyParameterAlias(parameters));
            return this.request(ajaxConfig, onSuccess, onError, config, anyParameters);
        },
        delete: function (httpUrl, parameters, onSuccess, onError, config, anyParameters) {
            let ajaxConfig = { url: httpUrl, cache: false };
            ajaxConfig.method = 'DELETE';
            ajaxConfig.withCredentials = true;
            if (parameters == null) {
                ajaxConfig.params = null;
            }
            else {
                ajaxConfig.params = base.verifyParameterAlias(parameters);
            }
            return this.request(ajaxConfig, onSuccess, onError, config, anyParameters);
        },
        request: function (ajaxConfig, onSuccess, onError, config, anyParameters) {

            if (typeof (config) == 'undefined' || config == null) {
                config = { showLoading: true };
            }
            if (typeof (config.showLoading) == 'undefined') {
                config.showLoading = true;
            }
            if (typeof (config.trackRewrite) == 'undefined') {
                config.trackRewrite = true;
            }

            base.loading = config.showLoading;
            base.busy = config.showLoading;
            if (base.currentContext != null) {
                base.currentContext.loading = config.showLoading;
            }

            base.disableControls();

            http(ajaxConfig).success(function (result, status) {
                base.ajax.success = result.Success;
                base.ajax.errorMsg = getPropertyByName(result, 'ErrorMessage');
                base.loading = false;
                base.busy = false;
                if (base.currentContext != null) {
                    base.currentContext.loading = false;
                    base.currentContext.firstLoading = false;
                }
                base.enableControls();
                if (config.trackRewrite) {
                    base.track.rewrite();
                }
                if (typeof (result.Success) == 'undefined' || (result.Success )) {
                    if (typeof (onSuccess) == 'function')
                        onSuccess(result, status, anyParameters);
                } else if (typeof (result.Success) != 'undefined' && result.Success == false) {
                    if (typeof (onError) == 'function') {
                        onError(result, status, anyParameters);
                    } else if (
                        compareIfSafe(base, 'serverParameters.enums.ServiceResultErrorType.Application', result.ErrorType) &&
                        isSafeToUse(result, 'ErrorMessage') &&
                        typeof (window.showTopErrorMsg) == 'function'
                    ) {
                        window.showTopErrorMsg(result.ErrorMessage, 5000);
                    } else {
                        console.error('Result Fail: ', result);
                    }
                }
            }).error(function (a, b, c) {
                base.loading = false;
                base.busy = false;
                if (base.currentContext != null) {
                    base.currentContext.loading = false;
                    base.currentContext.firstLoading = false;
                }
                base.enableControls();
                if (typeof (onError) == 'function')
                    onError(a, b, c);

                if (compareIfSafe(b, '', '401')) {
                    window.location = ILangSettings.Sites.ILang() + "/Login.aspx?redirectUrl=" + document.URL;
                }
            });
        },
        upload: function (httpUrl, file, auth, onSuccess, onFail) {
            var fd = new FormData().append('file', file);
            http.post(httpUrl, fd, {
                transformRequest: auth || angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function () { onSuccess(); }).error(function () { onerror(); });
        }
    };
    base.timeoutHandler = null;
    base.resetLoadingText = function () {
        if (typeof (ResetLoadingText) != 'undefined') {
            _previousLoadingText = 'Carregando...';
            ResetLoadingText();
        }
    };
    base.showContineLoading = function () {
        if (typeof (ShowContineLoading) != 'undefined') {
            ShowContineLoading();
        }
    };
    base.enableControls = function () {
        try {
            $('#divOverAll').hide();
            base.resetLoadingText();
            window.clearTimeout(base.timeoutHandler);
        } catch (e) {
        }
    };
    base.disableControls = function () {
        if ($('#divOverAll').length <= 0) {
            var div = $('<div></div>', { id: 'divOverAll' }).css({
                'width': '100%',
                'height': '100%',
                'left': '0px',
                'top': '0px',
                'position': 'absolute',
                'z-index': 999999999,
                'background-color': 'transparent',
                'cursor': 'progress'
            });
            div.bind('click', function () {
                base.showContineLoading();
                return false;
            });
            $(div).appendTo($('body'));
        }
        $('#divOverAll').show();
        base.timeoutHandler = window.setTimeout(function () { base.enableControls(); }, 5000);
    };
    base.track = {
        obj: window.trackObj,
        writer: function (txt, force) {
            if ((txt == null || txt == '' || base.serverParameters.mainScope == false) && force != true)
                return;

            if (typeof (base.services.location) != 'undefined') {
                base.services.location.hash(txt);
            } else {
                window.location.hash = txt;
            }
        },
        write: function (key, value) {
            this.obj[key] = value;
            this.writer($.param(this.obj));
        },
        add: function (key, value, splitChart) {
            splitChart = (splitChart == null) ? '-' : splitChart;
            if (this.obj.hasOwnProperty(key)) {
                var arr = this.obj[key].toString().split('-');
                if (arr == null) {
                    this.obj[key] = value;
                } else if (arr.indexOf(value.toString()) < 0) {
                    //this.obj[key] += splitChart + value;
                    arr.push(value);
                    this.obj[key] = arr.join(splitChart);
                }
            } else {
                this.obj[key] = value;
            }
            this.writer($.param(this.obj));
        },
        remove: function (key, value, splitChart) {
            splitChart = (splitChart == null) ? '-' : splitChart;
            if (this.obj.hasOwnProperty(key)) {
                var arr = this.obj[key].toString().split(splitChart).convertToInt();
                arr = arr.remove(value);
                if (arr.length > 0) {
                    this.obj[key] = arr.join(splitChart);
                    this.writer($.param(this.obj));
                } else {
                    this.clean(key);
                }


            }
        },
        read: function (key) {
            if (this.obj != null && this.obj.hasOwnProperty(key)) {
                return this.obj[key];
            } else {
                this.populate();
                return this.obj.hasOwnProperty(key) ? this.obj[key] : null;
            }
        },
        cleanAll: function () {
            this.obj = {};
            this.writer('', true);
        },
        clean: function (key) {
            delete this.obj[key];
            this.writer($.param(this.obj), true);
        },
        populate: function () {

            var h = '';
            if (typeof (base.services.location) != 'undefined') {
                h = base.services.location.hash();
            } else {
                h = window.location.hash;
            }

            if (h == '' || h == '#')
                return;

            var a = h.replace(/#/g, '').replace(/\//g, '').split('&');

            for (var i = 0; i < a.length; i++) {
                var k = a[i].split('=')[0].replace('%5B%5D', '');
                var v = a[i].split('=')[1];
                v = setParameters(v);
                this.obj[k] = v
            }
        },
        rewrite: function () {
            if (base.jsParameters.rewriteHash && compareIfSafe(base, 'serverParameters.mainScope', true)) {
                window.setTimeout(function () {
                    base.track.writer($.param(base.track.obj));
                }, 300);
            }
        }
    };
    base.page = {
        setTitle: function (title) {
            let el = $('.bodyContent').length > 0 ? $('.bodyContent .pageTitle h1') : $('.page-content main h1:first');
            if (el.length > 0)
                el.text(title);
        }
    };
    base.inputSearchFocused = function (event, context) {
        context = (context == null) ? base.smallContext : context;

        context.isSearchBoxActive = true;
        base.userIsNavigatingOnSearchOptions = false;
        context.smallListHoverItem = -1;
    };
    base.inputSearchTyping = function (event, context) {
        if (base.jsParameters.ilangSearchEnabled == false)
            return;

        context = (context == null) ? base.smallContext : context;

        base.searchTypes = _searchTypes.Small;
        //ESC
        if (event.keyCode == keyCode.KEY_ESC) {
            base.closeSearchBox(context);
            return;
        }
        //ENTER + EMPTY SEARCH
        if (context.searchTxt == '' && (event.keyCode == keyCode.KEY_ENTER || event.keyCode == keyCode.BACKSPACE)) {
            context.smallListHoverItem = -1;
            base.backToDefault();
            return;
        }
        //scrolling
        if (context.isSearchBoxActive == true && (event.keyCode == keyCode.KEY_DOWN || event.keyCode == keyCode.KEY_UP || (event.keyCode == keyCode.KEY_ENTER && context.smallListHoverItem >= 0))) {
            base.keyScrolling(event.keyCode, context);
            return;
        }

        //NO SEARCH 
        if (context.searchTxt.length < base.jsParameters.startSearchTypeAmount || base.userIsNavigatingOnSearchOptions == true) {
            return;
        }


        //Clean search timeout
        if (base.searchTimeManager != null)
            clearTimeout(base.searchTimeManager);

        if (compareIfSafe(base, 'jsParameters.hybridSearch', true)) {
            base.doHybridSearch(context.searchTxt, event.keyCode);
            return;
        }
        //ENTER
        if (event.keyCode == keyCode.KEY_ENTER) {
            context.smallListHoverItem = -1;

            if (typeof (base.jsParameters.onFilterSearchBeforeLoad) == 'function')
                base.jsParameters.onFilterSearchBeforeLoad(base);

            if (typeof (context.onEnterIsPressed) == 'function') {
                context.onEnterIsPressed();
            } else {
                base.bigSearch();
            }

            return;
        } else {
            if (base.jsParameters.inputSearchDelayToStartSearch > 0 && context.enabledList) {
                base.searchTimeManager = setTimeout(function () {
                    base.smallSearch(context);
                }, base.jsParameters.inputSearchDelayToStartSearch);
            }
        }

        return true;
    };
    base.inputSearchBlur = function (event, context) {
        context = (context == null) ? base.smallContext : context;

        if (context.searchTxt.toString().trim() == '')
            base.closeSearchBox();

        base.userIsNavigatingOnSearchOptions = false;
        context.smallListHoverItem = -1;
    };
    base.closeSearchBox = function (context) {
        context = (context == null) ? base.smallContext : context;

        context.searchTxt = '';
        context.smallListHoverItem = -1;
        context.isSearchBoxActive = false;
    };
    base.cleanSearch = function ($event, context) {
        context = (context == null) ? base.smallContext : context;

        context.smallListHoverItem = -1;
        base.track.clean('searchTxt');
        base.cleanSmallList(context);
        base.backToDefault(context);

    };
    base.backToDefault = function (context) {
        context = (context == null) ? base.smallContext : context;

        context.searchTxt = '';

        if (base.jsParameters.ilangSearchEnabled == true)
            base.bigSearch();
    };
    base.keyScrolling = function (key, context) {
        context = (context == null) ? base.smallContext : context;

        base.userIsNavigatingOnSearchOptions = false;
        if (key == keyCode.KEY_ENTER) {
            if (typeof (context.onItemIsSelected) == 'function') {
                context.onItemIsSelected();
            } else {
                base.itemHasBeenSelected(context.list[context.smallListHoverItem]);
                base.closeSearchBox(context);
            }

            return;
        }
        var incremento;
        if (key == keyCode.KEY_DOWN) {
            incremento = 1;
        } else if (key == keyCode.KEY_UP) {
            incremento = -1;
        }
        if (incremento == -1 && context.smallListHoverItem <= 0) {
            context.smallListHoverItem = context.list.length - 1;
        } else if (incremento == 1 && context.smallListHoverItem == context.list.length) {
            context.smallListHoverItem = 0
        } else {
            context.smallListHoverItem += incremento;
        }
    };
    base.doHybridSearch = function (txt, key) {
        if (base.busy == true)
            return;


        base.HybridSearchBusy = true;
        var fn = function () {
            if (isSafeToUse(base, 'services.filter')) {
                var item = base.services.filter('filter')(base.bigContext.list, base.smallContext.searchTxt);
                base.bigContext.resultLength = (item != null ? item.length : 0);
            }
        }
        if (txt.length == 3 || key == keyCode.KEY_ENTER || (base.bigContext.hasMorePages == true && txt.length >= 4)) {
            base.bigSearch({}, function () { fn(); }, true);
        } else if (txt.length > 3 && isSafeToUse(base, 'services.filter')) {
            fn();
        }
    }
    base.df__filterNoAccent = function (item) {
        if (me.searchTxt == null || me.searchTxt == '')
            return true;

        var search = me.searchTxt.toLowerCase().removeAccent();
        return item.Name.toLowerCase().indexOf(search) >= 0 ||
            item.NameNoAccent.toLowerCase().indexOf(search) >= 0;
    };
    base.itemHasBeenSelected = function () {
        if (typeof (base.jsParameters.itemHasBeenSelected) == 'function') {
            base.jsParameters.itemHasBeenSelected(base);
        }
    }
    base.getDialog = function (key) {
        if (typeof (base.serverParameters.dialogs) != 'undefined' && base.serverParameters.dialogs.hasOwnProperty(key)) {
            var id = base.serverParameters.dialogs[key]
            return angular.element($('#' + id)).scope();
        } else {
            return null;
        }

    };
    base.showDialog = function (key) {
        var dlg = base.getDialog(key);
        if (dlg != null && typeof (dlg.df__toggleVisibility) == 'function') {
            $('#' + dlg.serverParameters.contextID).removeClass('hide');
            dlg.df__toggleVisibility(true);
            parent.location.hash += ("openDlg___" + key);
        }
    };
    base.openDialog = function () {
        $('#' + base.serverParameters.contextID).removeClass('hide');
        base.df__toggleVisibility(true);
    };
    base.df__toggleInfiniteScrolling = function (value) {
        if (value == null) {
            base.bigContext.infiniteScrolling = !base.bigContext.infiniteScrolling;
        } else {
            base.bigContext.infiniteScrolling = value;
        }

    }
    base.df__pageListLimitBegin = function () {
        var size = 10;
        var n = base.jsParameters.listLimitPageSize / size;
        var i = (base.bigContext.PageIndex - n);
        var k = 0;
        if (i > 0) {
            var k = (base.bigContext.PageIndex - n) * size;
        }

        console.log('k: ', k);
        return k;
    }
    base.df__toggleVisibility = function (value, apply) {
        base.jsParameters.dialogVisibility = (value == undefined) ? !base.jsParameters.dialogVisibility : value;
        if (isSafeToUse(base, 'serverParameters.selfDialog'))
            base.serverParameters.selfDialog.Visible = (value == undefined) ? !base.jsParameters.dialogVisibility : value;
        if (apply == true && (!base.$$phase))
            base.$apply();
    };
    base.df__setDialogTitle = function (value) {
        if (typeof (base.serverParameters.selfDialog) != 'undefined')
            base.serverParameters.selfDialog.Title = value;
    };
    base.df__setDialogSubTitle = function (value) {
        if (typeof (base.serverParameters.selfDialog) != 'undefined')
            base.serverParameters.selfDialog.SubTitle = value;
    }
    base.df__setGlobalDialogVisibility = function (key, value) {
        if (base.serverParameters.dialogs.hasOwnProperty(key))
            base.serverParameters.dialogs[key].visible = value;
    }
    base.df__changeTab = function (item, tab, auto) {

        if (typeof (item.url) != 'undefined' && item.url != null) {
            window.location.href = item.url;
            return;
        }

        var key = item.key;
        var value = item.value;
        var name = item.name;
        var description = item.description;

        tab.selected = value;//base.serverParameters.filters[key].selected = value;

        if (tab.trackHash) {
            base.track.write(key, value);
        }

        for (var i = 0; i < tab.list.length; i++) {
            tab.list[i].selected = tab.list[i].value == value;
            if (tab.list[i].selected && (typeof (description) != 'undefined'))
                tab.list[i].description = description;
        }

        var selectedTab = tab.list.first({ value: value });// $(base.serverParameters.filters[key].list).filter(function (index, item) { return item.value == value });
        if (selectedTab != null) {
            if (typeof (selectedTab.onSelected) != 'undefined' && typeof (base[selectedTab.onSelected]) == 'function') {
                base[selectedTab.onSelected](key, value, name, selectedTab, tab);
            }
        }

        if (tab.onChange != null && typeof (base[tab.onChange]) != 'undefined') {
            base[tab.onChange](item, tab);
        }

        if (typeof (base.jsParameters.onChangeTab) == 'function')
            base.jsParameters.onChangeTab(key, value, name, selectedTab, base.serverParameters.filters[key], auto == null ? false : auto);

        if (tab.loadFilter == true) {
            base.bigSearch(undefined, function () {
                if (isSafeToUse(base, 'jsParameters.onTabChange.' + key))
                    base.jsParameters.onTabChange[key](item, tab);
            });
        }

    }
    base.df__filter = function (item, filter) {
        base.df__setSelectedFilter(item, filter);
        base.df_filterParam = { item: item, filter: filter };
        base.bigSearch({}, function () {
            if (typeof (base.jsParameters.onAfterFilter) == 'function') {
                base.jsParameters.onAfterFilter(base.df_filterParam.filter);
            }
        });
    }
    base.df__filterMulti = function (item, filter) {
        var filters = base.serverParameters.filters[item.key];
        var list = base.serverParameters.filters[item.key].list.equals({ visible: true, selected: true });
        if (list != null && list.length > 0) {
            filters.selected = list.select('value');
        } else {
            filters.selected = [];
        }


        base.df_filterParam = { item: item, filter: filter };
        base.bigSearch({}, function () {
            if (typeof (base.jsParameters.onAfterFilter) == 'function') {
                base.jsParameters.onAfterFilter(base.df_filterParam.filter);
            }
        });
    }
    base.df__setFilterCount = function (filter, data, selected) {
        if (filter == null || data == null)
            return;

        for (var i in data) {
            var item = filter.list.first({ value: i });
            if (item != null) {
                item.count = data[i];
            }
        }
        if (selected != null) {
            var item = filter.list.first({ value: selected });
            if (item != null)
                base.df__setSelectedFilter(item);
        }
    }
    base.df__changeFilterCountItem = function (filter, data) {
        if (filter == null || data == null)
            return

        for (var i in data) {
            var item = filter.list.first({ value: i });
            if (item != null) {
                item.count += data[i];
            }
        }
    }
    base.df__setSelectedFilter = function (item, filter) {
        var filters = base.serverParameters.filters[item.key];
        filters.selected = item.value;
        for (i in filters.list) {
            filters.list[i].selected = filters.list[i].value == item.value;
        }

        if (filters.trackHash) {
            base.track.write(filters.key, filters.selected);
        }
    };
    base.df__sort = function (item, filter) {
        if (base.jsParameters.hasOwnProperty('onSort') && typeof (base.jsParameters.onSort) == 'function') {
            base.jsParameters.onSort(item, filter, function () {
                base.df__setSelectedFilter(item, filter);
            });
        } else if (isSafeToUse(item, 'sortProperty')) {
            base.df__setSelectedFilter(item, filter);
            if (base.serverParameters.actions.load.resultType == base.serverParameters.enums.resultType.List) {
                base.df__sortAscDesc(item, base.bigContext.list);
            } else if (isSafeToUse(base, 'bigContext.dto')) {
                base.df__sortAscDesc(item, findFirstArray(base.bigContext.dto));
            }
        } else {
            base.df__filter(item, filter);
        }
    };
    base.df__sortAscDesc = function (item, list) {
        if (list == null)
            return;

        if (item.ascending == undefined) {
            item.ascending = true;
        } else {
            item.ascending = !item.ascending;
        }
        if (item.ascending) {
            list = list.orderBy(item.sortProperty);
        } else {
            list = list.orderDescBy(item.sortProperty);
        }
    };
    base.df__showMore = function (parameters, unshift, fn, reverse) {
        if (typeof (base.jsParameters.onShowMore) != 'undefined') {
            base.jsParameters.onShowMore(base);
        } else {
            base.bigContext.PageIndex++;

            if (base.jsParameters.scrollPercentOfPageTriggerSearch <= 0.97)
                base.jsParameters.scrollPercentOfPageTriggerSearch += base.jsParameters.scrollTriggerIncrease;//0.02;

            if (base.jsParameters.trackPageIndex == true)
                base.track.write('pageIndex', base.bigContext.PageIndex);
            base.smallContext.isSearchBoxActive = false;
            base.searchTypes = _searchTypes.Big;
            base.currentContext = base.bigContext;
            if (base.jsParameters.unshitOnShowMore == true || unshift == true)
                base.jsParameters.unshift = true;
            base.jsParameters.reverse = typeof (reverse) != 'undefined' ? reverse : false;
            base.bigSearch(parameters || {}, function () {
                base.jsParameters.unshift = false;
                base.jsParameters.reverse = false;
                if (typeof (fn) == 'function') {
                    fn();
                }
            }, false);
        }
    }
    base.df__dropSelectChange = function (drop) {
        base.df__dropVerifyPropagation(drop, { value: drop.selected})
    }
    base.df__dropChecked = function (checkedItem, drop, e,closeAfter) {
        if (closeAfter == null) { closeAfter = false;}
        base.df__dropApply(drop, closeAfter);
        base.df__dropChecklistSetText(drop);
        if (e != null)
            e.stopPropagation();
    };
    base.df__dropGetDependencesParameters = function (key, parameters) {
        if (!isSafeToUse(base.serverParameters, 'enums.AngularTriggerType'))
            return parameters;

        var param = parameters || {};
        var dependences = base.df__dropGetDependences(key);
        if (dependences != null) {
            dependences.foreach(function (item) {
                if (!param.hasOwnProperty(item.alias))
                    param[item.alias] = item.selectedId;
            });
        }
        return param;

    }
    base.df__dropGetDependences = function (key) {
        if (!isSafeToUse(base.serverParameters, 'enums.AngularTriggerType'))
            return null;

        return objToArray(base.serverParameters.drops).filter(function (item) {
            return compareIfSafe(item, 'trigger.type', base.serverParameters.enums.AngularTriggerType.LoadDrop) &&
                compareIfSafe(item, 'trigger.value', key);
        });
    };
    base.df__getDropCheckListCountSelectables = function (drop) {
        if (drop.searchTxt == null || drop.searchTxt == '') {
            return drop.list.notEquals({ value: -1 }).count({ fixedOption: false, visible: true })
        } else {
            return drop.list.notEquals({ value: -1 }).count({ fixedOption: false })
        }
    };
    base.df__getDropCheckListCountSelecteds = function (drop) {
        if (drop.searchTxt == null || drop.searchTxt == '') {
            return drop.list.notEquals({ value: -1 }).equals({ checkedItem: true, fixedOption: false, visible: true });
        } else {
            return drop.list.notEquals({ value: -1 }).equals({ checkedItem: true, fixedOption: false});
        }
    };
    base.df__dropChecklistSetText = function (drop) {
        if (drop == null)
            return;
        var selecteds = base.df__getDropCheckListCountSelecteds(drop);
        var countSelectables = base.df__getDropCheckListCountSelectables(drop);
        drop.disableApplyButton = selecteds.length == 0;
        var verifyJsFn = function (event, fn) {
            if (base.jsParameters.hasOwnProperty(event) && base.jsParameters[event].hasOwnProperty(drop.key)) {
                var dependences = base.df__dropGetDependences(drop.key);
                return base.jsParameters[event][drop.key](drop, selecteds, dependences);
            } else {
                if (typeof (fn) == 'function') {
                    return fn();
                } else {
                    if (drop.selectedTextFormat != null) {
                        return drop.selectedTextFormat
                            .replace('{0}', fn)
                            .replace('{selecteds}', selecteds.length)
                            .replace('{total}', drop.list.length);
                    } else {
                        return fn;
                    }

                }

            }
        };
        if (selecteds.length == 0) {
            drop.selectedText = verifyJsFn('onSelectCheckList_Empty', drop.emptyText != null ? drop.emptyText : 'Nenhum registro');
        } else if (selecteds.length == countSelectables) {
            drop.selectedText = verifyJsFn('onSelectCheckList_All', function () {
                if (isSafeToUse(drop, 'allItemsAreCheckedText')) {
                    return drop.allItemsAreCheckedText;
                } else {
                    return 'Todos';
                }
            });
        } else if (selecteds.length == 1) {
            drop.selectedText = verifyJsFn('onSelectCheckList_Single', selecteds[0].description);
        } else if (selecteds.length > 1) {
            var countVisibles = drop.list.notEquals({ value: -1 }).count({ fixedOption: false,visible: true });
            drop.selectedText = verifyJsFn('onSelectCheckList_Multi', selecteds.length + ' de ' + countVisibles + ' selecionados');
        }

        if ((selecteds.length == countSelectables && drop.alwaysSendAllItemValues == false) || (base.multiRequestFirstLoading == true && drop.sendDefaultValueOnFirstLoad == true)) {
            drop.selectedId = -1;
            drop.selected = -1;
        } else {
            if (selecteds.length == 0 && drop.defaultValueIfEmpty != null) {
                drop.selectedId = drop.defaultValueIfEmpty.value;
            } else {
                var __key = (drop.valueName != null && drop.list[0].hasOwnProperty(drop.valueName)) ? drop.valueName : 'value';
                drop.selectedId = selecteds.selectToArray(__key);
            }
            drop.selected = drop.selectedId;
        }

    };
    base.df__dropAllVisibleItemAreChecked = function (drop) {
        if (drop == null || drop.list == null || drop.list.length == 0)
            return false;
        var checked = drop.list.notEquals({ value: -1 }).count({ fixedOption: false, visible: true, checkedItem: true });
        var visibles = drop.list.notEquals({ value: -1 }).count({ fixedOption: false, visible: true });
        return checked == visibles;
    }
    base.df__getDrop = function (name) {
        if (typeof (name) == 'object')
            return drop;
        if (typeof (name) == 'string')
            return base.serverParameters.drops[name];
    }
    base.df__toogleDropDisabled = function (name, value) {
        var drop = base.df__getDrop(name);
        if (drop != null) {
            drop.disabled = value;
        }
    };
    base.df__dropSelectByIndex = function (drop, index) {
        var _drop = base.df__getDrop(drop);
        if (_drop != null) {
            if (_drop.list.length > 0) {
                var item = index == null ? _drop.list.first() : _drop.list[index];
                base.df__dropSelected(item, _drop);
            }
        }
    };
    base.df__dropSelected = function (selectedItem, drop) {
        drop.lastSelectedId = drop.selectedId;

        if (selectedItem == null) {
            selectedItem = drop.list.firstOrFirst({ value: drop.selected });

        }

        if (drop.trackHash == true && (drop.loadFromServer == true && drop.loadOnStart == false) == false) {
            base.track.write(drop.key, selectedItem.value);
        }
        $(drop.list).each(function (a, item) {
            item.selected = item.value == selectedItem.value;
            if (item.selected) {
                drop.selectedText = item.description;
                drop.selectedId = selectedItem.value;
                drop.selected = selectedItem.value;
            }
        });
        drop.active = false;
        base.df__dropVerifyPropagation(drop, selectedItem);
    };
    base.df__dropTrigger = function (drop, selectedItem) {
        if (!isSafeToUse(drop, 'trigger.type') || !isSafeToUse(base.serverParameters, 'enums.AngularTriggerType'))
            return;

        switch (drop.trigger.type) {
            case base.serverParameters.enums.AngularTriggerType.Function:
                if (isSafeToUse(base, drop.trigger.value) && typeof (base[drop.trigger.value]) == 'function') {
                    base[drop.trigger.value](drop, selectedItem);
                }
                break;
            case base.serverParameters.enums.AngularTriggerType.Action:
                if (isSafeToUse(base.actions, drop.trigger.value)) {
                    base.action(drop.trigger.value, drop);
                }
                break;
            case base.serverParameters.enums.AngularTriggerType.LoadDrop:
                if (isSafeToUse(base.serverParameters.drops, drop.trigger.value)) {
                    var targetDrop = base.serverParameters.drops[drop.trigger.value]
                    var param = {};
                    param[drop.alias] = drop.selectedId;
                    base.df__loadDrop(targetDrop, undefined, undefined, param);
                }
                break;
            default:
                break;
        }
    };
    base.df__dropCleanAllDropData = function () {
        if (base.serverParameters.drops != null) {
            objToArray(base.serverParameters.drops).foreach(function (item) {
                if (item.loadAction != null) { item.loaded = false; item.list = []; }
            });

        }
    }
    base.df__rollbackDrop = function (drop) {
        if (Array.isArray(drop)) {
            for (var i = 0; i < drop.length; i++) {
                var d = drop[i];
                if (base.serverParameters.drops.hasOwnProperty(d))
                    base.df__rollbackDrop(base.serverParameters.drops[d]);

            }
        } else if (drop.lastSelectedId != null) {
            var item = drop.list.first({ value: drop.lastSelectedId });
            if (item != null) {
                drop.list.set({ selected: false });
                drop.selectedId = item.value;
                drop.selected = item.value;
                item.selected = true;
            }
        }
    };
    base.df__dropBoxSearching = function (drop, event) {

        if (event.keyCode == keyCode.KEY_ENTER && drop.keyboardNavigation && (drop.hoverIndex != null && drop.hoverIndex >= 0)) {
            if (drop.searchFromServer == true) {
                var item = drop.list[drop.hoverIndex];
                drop.searchTxt = "";
                drop.hoverIndex = null;
                drop.keyboardNavigation = false;
                base.df__dropSelected(item, drop);

                return;
            } else {
                var item = drop.list.equals({ fixedOption: false, visible: true })[drop.hoverIndex];
                base.df__dropSelected(item, drop);
                base.df__dropCleanSearch(drop);
                drop.keyboardNavigation = false;
            }

        }
        if (event.keyCode == keyCode.KEY_ESC) {
            base.df__dropCleanSearch(drop, true);
            drop.keyboardNavigation = false;
            return;
        }
        if (event.keyCode == keyCode.KEY_DOWN || event.keyCode == keyCode.KEY_UP) {
            drop.keyboardNavigation = true;
            var listSize = drop.list.length;
            if (drop.hoverIndex == null || drop.hoverIndex == undefined) {
                drop.hoverIndex = 0;
                return true;
            } else {
                if (event.keyCode == keyCode.KEY_DOWN) {
                    drop.hoverIndex++;
                    if (drop.hoverIndex == listSize)
                        drop.hoverIndex = 0;

                } else if (event.keyCode == keyCode.KEY_UP) {
                    drop.hoverIndex--;
                    if (drop.hoverIndex < 0)
                        drop.hoverIndex = listSize - 1;
                }

            }
            return;
        }

        if (drop.searchTxt == null)
            return;

        if (drop.searchTxt.length == 0) {
            base.df__dropCleanSearch(drop);
            return;
        }

        if (drop.searchFromServer == false && drop.searchTxt.length >= drop.charactersBeforeSearch && event.keyCode != keyCode.KEY_ENTER) {
            var index = drop.list.count({ fixedOption: true });
            drop.list.setTo({ fixedOption: false }, { visible: false }).textContains({ description: drop.searchTxt.trim() }, { visible: true });
            drop.list.setTo({ visible: true }, function (item) { item.index = index++; });
            drop.hoverIndex = null;
            return;
        }

        if (drop.searchTxt.length < drop.charactersBeforeSearch && event.keyCode != keyCode.KEY_ENTER)
            return;

        if (drop.loadAction != undefined && drop.loadAction != null && drop.searchFromServer) {
            drop.hoverIndex = null;
            drop.PageIndex = 0;
            drop.list.remove({ fixedOption: false });
            base.df__loadDrop(drop);
        }
    };
    base.df__dropSelectAll = function (drop) {
        drop.list.set({ checkedItem: true });
        base.df__dropChecklistSetText(drop);
        base.df__checkDropPropagation(drop);
    };
    base.df__dropCleanSelection = function (drop) {
        drop.list.set({ checkedItem: false });
        drop.searchTxt = "";
        base.df__dropChecklistSetText(drop);
        base.df__checkDropPropagation(drop);

    };
    base.df__dropGetVisibles = function (drop) {
        if (drop == null || drop.list == null || drop.list.length == 0)
            return null;
        return drop.list.equals({ visible: true }).select([{ value: 'ID' }, { checkedItem:'Checked'}]);
    };
    base.df__dropGetSelected = function (drop) {
        var _drop;
        if (typeof (drop) == 'string') {
            _drop = getPropertyByName(base, 'serverParameters.drops.' + drop);
        } else { _drop = drop; }
        if (_drop != null) {
            if (_drop.isCheckboxList == true) {
                if (isSafeToUse(_drop, 'selected')) {
                    return _drop.selected;
                } else if (_drop.defaultValueIfEmpty != null) {
                    return _drop.defaultValueIfEmpty.value;
                } else {
                    return null;
                }
            } else if (_drop.valueName == null || _drop.valueName == '' || (_drop.loadFromServer == true && _drop.loaded == false)) {
                return _drop.selected;
            } else {
                var item = _drop.list.first({ selected: true });
                if (item != null) {
                    if (item.fixedOption == true) {
                        item.value;
                    } else {
                        return item[_drop.valueName];
                    }
                } else if (_drop.defaultValueIfEmpty != null) {
                    if (_drop.valueName == null || _drop.valueName == '' || (_drop.loadFromServer == true && _drop.loaded == false)) {
                        return _drop.defaultValueIfEmpty[_drop.valueName];
                    } else {
                        return _drop.defaultValueIfEmpty.value;
                    }
                }
                return null;

            }
        }
    };
    base.df__dropApply = function (drop, closeAfter) {
        base.df__dropChecklistSetText(drop);
        drop.active = closeAfter == null ? false : !closeAfter;
        base.df__checkDropPropagation(drop);
    };
    base.df__checkDropPropagation = function (drop) {
        var t = {};
        t[drop.valueName != null ? drop.valueName : 'value'] = drop.selectedId;
        base.df__dropVerifyPropagation(drop, t);
    }
    base.df__closDropChecklist = function (drop, event) {
        if (drop == null)
            return;

        var i = 0;
        drop.list.equals({ checkedItem: true }).foreach(y => {
            y.index = i;
            i++;
        });
        drop.list.equals({ checkedItem: false }).foreach(y => {
            y.index = i;
            i++;
        });
        drop.active = false;
        event.stopPropagation();
    };
    base.df__dropVerifyPropagation = function (drop, selectedItem) {
        if (drop.onChange != null && typeof (base[drop.onChange]) != 'undefined') {
            base[drop.onChange](selectedItem, drop);
        } else if (drop.trigger != null) {
            base.df__dropTrigger(drop, selectedItem);
        } else if (drop.loadFilter == true) {
            var t = getPropertyByNameOrDefault(base, 'serverParameters.actions.load.data', {});
            t[drop.alias] = drop.valueName != null ? selectedItem[drop.valueName] : selectedItem.value;
            base.bigSearch(t);
        }
    };
    base.df__dropShowMorePage = function (drop) {
        base.df__loadDrop(drop, function () { drop.active = true; });
    };
    base.df__loadSource = function (source, after, displayLoading, param) {

        if (typeof (displayLoading) == 'undefined') {
            displayLoading = true;
        }
        var parameters = param || {};
        parameters = base.getFixedParameters(parameters);

        parameters = base.df__dropGetDependencesParameters(source.key, parameters);

        source.loadingList = true;
        base.action(source.loadAction, parameters, function (result) {
            if (isSafeToUse(source, 'list') && source.list.any({ fixedOption: false }))
                source.list.remove({ fixedOption: false });

            if (result.Success) {
                var list = (source.resultListWay != null && source.resultListWay != '') ? getPropertyByName(result, source.resultListWay) : result.Result;

                base.df__populateDrop(source, list, after, result);
                if (source.trigger != null && source.trigger.triggerOnLoad == true)
                    base.df__dropTrigger(source);
            }
        }, null, { showLoading: displayLoading });
    }
    base.df__loadDrop = function (drop, after, displayLoading, param) {

        if (typeof (displayLoading) == 'undefined') {
            displayLoading = true;
        }
        var parameters = param || {};
        parameters.dsc = drop.searchTxt;
        parameters = base.getFixedParameters(parameters);

        parameters = base.df__dropGetDependencesParameters(drop.key, parameters);

        if (compareIfSafe(drop, 'showSearchBox', true)) {
            if (!drop.hasOwnProperty('PageIndex')) {
                drop.PageIndex = 1;
            } else {
                drop.PageIndex += 1;
            }
            parameters.PageIndex = drop.PageIndex;
            parameters.PageSize = drop.PagePageSize || 10;
        }

        drop.loadingList = true;
        base.action(drop.loadAction, parameters, function (result) {
            if (isSafeToUse(drop, 'list') && drop.list.any({ fixedOption: false }) && drop.showSearchBox == false)
                drop.list.remove({ fixedOption: false });

            if (result.Success) {
                var list = (drop.resultListWay != null && drop.resultListWay != '') ? getPropertyByName(result, drop.resultListWay) : result.Result;
                if (drop.showSearchBox == true)
                    drop.hasMorePage = getPropertyByName(result, 'HasMorePages') || false;//list.length >= (parameters.PageSize || 10);

                base.df__populateDrop(drop, list, after, result);
                if (drop.trigger != null && drop.trigger.triggerOnLoad == true)
                    base.df__dropTrigger(drop);
            }
        }, null, { showLoading: displayLoading });
    }
    base.df__populateDrop = function (drop, list, after, result) {
        var id = drop.valueName != null ? drop.valueName : 'id';
        if (list.length > 0 && !list[0].hasOwnProperty(id))
            id = "value";

        var name = drop.textname != null ? drop.textname : 'name';
        var additionalText = drop.additionalTextName != null ? drop.additionalTextName : 'additionalText';
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            var desc = getPropertyByName(item, 'name') == null ? item.description : item.name;
            var selected = item.hasOwnProperty('selected') ? item.selected : false;
            drop.list.push({
                visible: true,
                description: desc,
                value: item[id],
                selected: selected,
                fixedOption: false,
                additionalText: item[additionalText] || null,
                subTitle: item['subTitle'] ? item.subTitle : null,
                otherEntityID: item['otherEntityID'] ? item.otherEntityID : null,
            });
        }

        var _index = 0;
        drop.list.set(function (item) { item.index = _index++; });
        if (typeof (after) == 'function') {
            after();
        }

        drop.loaded = true;
        drop.loadingList = false;
        if (drop.isCheckboxList == false) {
            if (drop.selected != null && (drop.selectedText == '' || drop.selectedText == null)) {
                var selectedItem = drop.list.first({ value: drop.selected });
                if (selectedItem != null) {
                    drop.selectedText = selectedItem.description;
                    selectedItem.selected = true;
                }
            } else {
                var selectedItem = drop.list.first({ selected: true });
                if (selectedItem != null) {
                    drop.selectedText = selectedItem.description;
                    drop.selected = selectedItem.value;
                    drop.selectedId = selectedItem.value;
                    selectedItem.selected = true;
                }
            }
        } else {
            drop.list.set(function (item) { item.checkedItem = item.selected });
            base.df__dropChecklistSetText(drop);
        }

    };
    base.df__dropClick = function (drop, event) {
        if (drop != undefined && drop.loadAction != null && drop.loadOnStart == false && drop.loaded == false) {
            base.df__loadDrop(drop, function () { drop.loaded = true; }, false);
        }
        //event.preventDefault();
        event.stopPropagation();
        return false;
    };
    base.df__dropBoxFocus = function (drop, event) {
        drop.hoverIndex = null;
        event.stopPropagation();
    },
        base.df__dropCleanSearch = function (drop, close) {
            drop.searchTxt = "";
            drop.hoverIndex = null;
            drop.keyboardNavigation = false;

            if (drop.searchFromServer == false) {
                var _index = 0;
                drop.list.set(function (item) { item.visible = true; item.index = _index++; })

                if (close == true)
                    drop.active = false;

                return;
            } else {
                base.df__loadDrop(drop);
            }
        }
    base.df__dropCancelHover = function (item, drop) {
        drop.keyboardNavigation = false;
    }
    base.df__dropClickToggle = function (item, drop, event) {
        if (!drop.loaded) {
            base.df__loadDrop(drop, null, false);
        }
        drop.active = !drop.active;
        drop.focusOnTextBox = true;
        event.stopPropagation();
    }
    base.df__verifyRequiredFiled = function () {
        var ret = true;
        $('input[requiredField]').filter(function (a, b) { return $(b).is(':visible') }).each(function (index, item) {
            var way = $(item).attr('ng-model').split('.');
            var obj = base;
            for (var k = 0; k < way.length; k++) {
                var part = way[k];
                if (obj.hasOwnProperty(part)) {
                    obj = obj[part];
                }
            }
            if (obj == null || obj == "") {
                $(item).addClass('req').mouseup(function () {
                    $(this).removeClass('req');
                });
                ret = false;
            } else {
                ret = ret && true;
                $(item).removeClass('req').unbind('mouseup');
            }
        });

        return ret;
    };
    base.df__cleanRequiredFieldList = function () {
        base.requiredFilds = [];
    };
    base.df__getChatScope = function () {
        return angular.element('#ilangChatElement').scope();
    }
    base.df__anyDropSelected = function () {

        if (base.track.obj == undefined || base.track.obj == null || base.track.obj == [] || base.serverParameters.drops == null)
            return false;

        var drops = objToArray(base.serverParameters.drops).equals({ loadFilter: true }).select('key');
        if (drops == null || drops.length == 0)
            return false;


        for (var i = 0; i < drops.length; i++) {
            if (base.track.obj.hasOwnProperty(drops[i]))
                return true;
        }
        return false;
    };
    base.df_logError = function (msg) {
        if (typeof (ILang) != 'undefined' && typeof (ILang.ErrorHandling) != 'undefined')
            ILang.ErrorHandling.HTTPHandler.Send(msg, document.location.href, 0);
    };
    base.df__getJsParametersByName = function (name, defaultValue) {
        if (name == null || name == "")
            return defaultValue || null;

        if (base.jsParameters.hasOwnProperty(name)) {
            if (typeof (base.jsParameters[name]) == 'function') {
                return base.jsParameters[name](base);
            } else {
                return base.jsParameters[name];
            }
        }
        return defaultValue || null;
    }
    base.df__distinctList = function (addList, mainList) {
        if (addList == null || mainList == null || base.jsParameters.makeListDistinctBy == null || addList.length == 0 || mainList.length == 0)
            return addList;
        if (addList[0].hasOwnProperty(base.jsParameters.makeListDistinctBy) == false || mainList[0].hasOwnProperty(base.jsParameters.makeListDistinctBy) == false)
            return addList;
        var result = [];
        var filter = {};
        var listToBeDeleted = [];
        filter[base.jsParameters.makeListDistinctBy] = null;

        for (var i = 0; i < addList.length; i++) {
            filter[base.jsParameters.makeListDistinctBy] = addList[i][base.jsParameters.makeListDistinctBy];
            var itemToBeDeleted = mainList.first(filter);
            if (itemToBeDeleted == null) {
                result.push(addList[i]);
            }
        }
        return result;
    }

}
//####################################################
//                <-  GENERIC ANGULAR
//                    UTIL                  ->
//####################################################
function getDivByControllerName(name) {
    return $('div[ng-controller="' + name + '"]').attr('id');
}
function getAngularScope(elementScopeId) {
    var id = getDivByControllerName(elementScopeId);
    return getAngularScopeByElementId(id);
}
function getAngularScopeByElementId(elementId) {
    return angular.element($('#' + elementId)).scope();
}
function setParameters(value) {
    if (value == undefined)
        return null;

    if (isNaN(value)) {
        if (value.toLowerCase() == "true") {
            return true;
        } else if (value.toLowerCase() == "false") {
            return false;
        } else {
            return decodeURIComponent(value).replace(/\+/g, ' ');
        }
    } else {
        return parseFloat(value);
    }
}
function isNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null) return true;
    return input.replace(/\s/g, '').length < 1;
}
function getStaticUrl(file, after) {
    var url;
    if (file.indexOf('?') >= 0) {
        url = ILangSettings.Sites.Static() + file + '&v=' + ILangSettings.Version + (after != null ? after : '');
    } else {
        url = ILangSettings.Sites.Static() + file + '?v=' + ILangSettings.Version + (after != null ? after : '');
    }
    return getSecureURL(url);
}
function getSecureURL(url) {
    if ('https:' == document.location.protocol) {
        url = url.replace('http://', 'https://');
    }
    return url;
}
function getILangUrl(file, after) {
    if (file.indexOf('?') >= 0) {
        return ILangSettings.Sites.ILang() + file + '&v=' + ILangSettings.Version + (after != null ? after : '');
    } else {
        return ILangSettings.Sites.ILang() + file + '?v=' + ILangSettings.Version + (after != null ? after : '');
    }

}
function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null || str == "\n";
}
