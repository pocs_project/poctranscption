﻿class LtiService {
    constructor() {
        this._baseUrl = ILangSettings.Sites.StudentAPI();
        this._httpClient = new HttpClient();
    }

    async getLtiProviderResource(parameters) {
        const endpoint = '/lti-tool/';
        const uri = `${this._baseUrl}${endpoint}${parameters.LtiProviderResourceId}`;

        const response = await this._httpClient.get(uri, parameters);
        return response.json();
    }
}