﻿ILangApp.directive('lessonStructure', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('LessonStructure'); },
        scope: {
            allprops: '=',
            userImageUrl: '='
        }
        , controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;
            registerCommonFunction(me);
            me.exercisedto = me.allprops.Detail;
            me.propostaMet = { done: false, contA: null, contB: null, contC: null, header: null };
            me.onload = function () {
                if (me.exercisedto.FieldList != null) {
                    for (const item of me.exercisedto.FieldList) {
                        if (item.FieldName == 'Pré-Aula') {
                            item.DoNoShow = false;
                            item.IsPropostaMet = true;
                            me.propostaMet.header = item;
                            me.propostaMet.contA = item;
                        } else if (item.FieldName == 'Aula') {
                            item.DoNoShow = true;
                            me.propostaMet.contB = item;
                        } else if (item.FieldName == 'Pós-Aula') {
                            item.DoNoShow = true;
                            me.propostaMet.contC = item;
                        } else {
                            item.DoNoShow = false;
                            item.IsPropostaMet = false;
                        }

                    }
                }
            };
        }
    };
});