﻿ILangApp.directive('questionOpenended', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('Openended'); },
        scope: {
            allprops: '='
        }
        , controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;
            registerCommonFunction(me);
            me.exercisedto = me.allprops.Detail;
            me.serverParameters = me.$parent.serverParameters;
            me.enums = $scope.$parent.serverParameters.enums;
            me.root = getRootScope($scope);
            me.confirmPublication = false;
            me.savedText = '';
            me.busy = false;
            registerTinymceFunctions(me);
            me.getDefaultParameters = function () {
                return {
                    StudentLessonID: me.allprops.StudentLessonID,
                    StudentTrackID: me.exercisedto.StudentTrackID || 0,
                    ClassDisciplineID: parseInt(me.root.bigContext.dto.ClassDisciplineID),//me.serverParameters.actions.load.data.ClassDisciplineID
                    LessonID: parseInt(me.root.bigContext.dto.LessonID),//me.serverParameters.actions.load.data.LessonID
                    LessonCategoryID: parseInt(me.allprops.LessonCategoryID),
                }
            };
            me.cancel = function () {
                if (isEmptyOrSpaces(me.getText()) || me.savedText == '') {
                    return;
                }
                if (me.exercisedto.EditStatus != me.enums.StudentOpenEndedquestionEditStatus.Blank) {
                    me.exercisedto.Text = me.savedText;
                    me.setText();
                    me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                }
            };
            me.save = function () {
                if (!me.exercisedto.AllowStudentToPublish || me.studentIsSuspendedOnDiscipline)
                    return;

                if (isEmptyOrSpaces(me.getText())) {
                    showTopErrorMsg('O campo resposta não pode ser vazio..', 5000, null, false);
                    return;
                }

                if (me.busy)
                    return

                me.busy = true;

                let param = me.getDefaultParameters();
                param.Text = me.getText();
                me.$parent.action('saveOpenEnded', param,
                    function (result) {
                        if (result.Success) {
                            me.exercisedto.Text = param.Text;
                            me.savedText = param.Text;
                            me.exercisedto.StudentTrackID = result.Result;
                            me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                            me.updateAssignmentPost();
                        }
                        me.busy = false;
                    }, me.onError);

            };
            me.publish = function () {
                if (!me.exercisedto.AllowStudentToPublish || me.studentIsSuspendedOnDiscipline)
                    return;

                if (me.busy)
                    return

                me.busy = true;

                let param = me.getDefaultParameters();
                me.$parent.action('publishOpenEnded', param, function (result) {
                    if (result.Success) {
                        me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Published;
                        me.exercisedto.ShowStudentLessonCategoryPost = true;
                        me.confirmPublication = false;
                        me.updateAssignmentPost();
                        me.pushOnGoogleAnalytics();
                        me.root.checkLessonOnLeftMenu(me.allprops);
                    }
                    me.busy = false;
                }, me.onError);
            };
            me.confirmPublish = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                me.confirmPublication = true;
            };
            me.hideConfirmationLayer = function () {
                me.confirmPublication = false;
            };
            me.keepEditing = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                me.openEndedQuestionInitTinymce();
                me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Blank;
                me.busy = false;
            };
            me.getText = function () {
                return me.getEditorContent(me.txtFieldID(), 'html');
            };
            me.setText = function () {
                me.setEditorContent(me.txtFieldID(), me.savedText);
            };
            me.txtFieldID = function () {
                return 'if_' + me.allprops.LessonCategoryID;
            };
            me.onload = function () {
                me.savedText = me.exercisedto.Text;
            };
            me.deleteFile = function (file) {
                if (me.studentIsSuspendedOnDiscipline)
                    return

                let param = me.getDefaultParameters();
                param.ID = file.ID;
                if (me.busy)
                    return

                me.busy = true;
                me.$parent.action('deleteFile', param, function (result) {
                    if (result.Success) {
                        me.exercisedto.FileList.remove({ ID: file.ID });
                        if (me.exercisedto.FileList.length == 0)
                            me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Blank;

                        me.updateAssignmentPost();
                    }
                    me.busy = false;
                }, me.onError);
            };
            me.onError = function (error) {
                me.busy = false;
                me.generalError(error);
            };
            me.updateAssignmentPost = function () {
                if (isSafeToUse(me, 'assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
                    me.assignmentPostInstance.refresh();
                }
            };
            me.pushOnGoogleAnalytics = function () {
                if (!isSafeToUse(me, 'root'))
                    me.root = getRootScope($scope);
                me.root.pushOnGoogleAnalytics(me.allprops);
            };

            me.removeSpecialCharacters = function (customName) {
                let cleaner = customName.replace(/[^a-zA-Z0-9 ]/g, '');
                cleaner = cleaner.substr(0, 40);

                return cleaner;
            }

            me.customOpenEndedFileName = function () {
                let studentName = me.serverParameters.parameters.StudentName;
                let lessonCategoryName = me.allprops.LessonCategoryName;
                lessonCategoryName = me.removeSpecialCharacters(lessonCategoryName);

                let customName = lessonCategoryName + '_' + studentName;
                customName = customName.replace(/[^a-z0-9]/gi, '');

                return customName;
            };

            me.onUploadSuccess = function (response) {
                if (response.response.Success) {
                    let param = me.getDefaultParameters();

                    param.FileName = response.response.Result.FileName;
                    param.FileSize = response.file.size;
                    param.AttachmentPath = response.response.Result.VirtualPath;
                    me.$parent.action('uploadFile', param, function (result) {
                        me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                        me.exercisedto.FileList.push({
                            FileUrl: response.response.Result.VirtualPath,
                            FileName: response.response.Result.FileName,
                            AttachmentPath: response.response.Result.VirtualPath,
                            ID: result.Result
                        });
                        me.updateAssignmentPost();
                        response.after();
                    });
                } else {
                    throw getPropertyByName(response, 'response.ErrorMessage');
                }
            }

            me.openEndedQuestionInitTinymce = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return

                InitializeTinyMCEStudentSimpleMode(me.txtFieldID(), true);
            };

            changeFocusKeyDowIframe();
        }
    };
});