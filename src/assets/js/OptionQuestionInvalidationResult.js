﻿ILangApp.directive('optionQuestionInvalidationResult', () => {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => {
            return getStaticUrl('/Templates/Dialog/OptionQuestionInvalidationResult.html');
        }
    };
});
