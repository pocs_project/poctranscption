﻿function registerExerciseController($scope, $element, $attrs, $transclude, $timeout) {
    let me = $scope;
    me.showFeedbackCorrectAnswer = false;
    me.showOptionQuestionInvalidationDialog = false;
    me.showOptionQuestionInvalidationConfirmDialog = false;
    me.showComplementTextQuestionInvalidationDialog = false;
    me.showComplementCheckBoxQuestionInvalidationDialog = false;
    me.listResourceSettingsJustification = [];
    me.resourceStudentQuestion = [];
    me.busy = false;
    me.timeout = $timeout;
    registerCommonFunction(me);
    me.exercisedto = me.allprops.Detail;
    me.serverParameters = me.$parent.serverParameters;
    me.enums = me.serverParameters.enums;
    me.lockOptions = false;
    me.showResultTab = false;
    me.resultPageIsOn = false;
    me.closeSlideIsOn = false;
    me.answering = false;
    me.root = getRootScope($scope);
    me.skippedOrUnansweredQuestionCount = false;
    me.answeredQuestionCount = false;
    me.discardedQuestionCount = false;
    me.mustDiscardedQuestionCount = false;
    if ($('.pageContent').length > 0) {
        me.maxCards = Math.floor($('.pageContent').eq(0).width() / 50) - 2;
    } else {
        me.maxCards = Math.floor($('.page-content').eq(0).width() / 50) - 2;
    }
    me.firstCard = 1;
    me.lastCard = me.maxCards;
    me.CurrentQuestionIndex = 0;
    me.currentQuestion = null;
    me.starting = false;
    me.countDownManager = [];
    me.countMSecondsManager = [];
    me.dateNowLT = [];
    me.compareFaceDialog = false;
    me.compareFaceSuccess = false;
    me.streaming = false;
    me.localstream = null;
    me.video = null;
    me.canvas = null;
    me.photo = null;
    me.targetStudentPicture = null;
    me.compareTargetStudentPicture = null;
    me.dateLastPicture = null;
    me.confirmStartDialog = false;
    me.startDialog = true;
    me.confirmStartDialogCam = false;
    me.capturePicture = false;
    me.capturePictureError = false;
    me.videoWidth = 340;
    me.videoHeight = 280;
    me._categoryScope = null;
    me.showGradeClassAverage = false;
    me.selectQuestionWithDescriptorID = 0;
    me.isNotNextQuestion = false;

    const ERROR_DISPLAY_TIME = 5000;

    if (me.serverParameters.fixedParameters.isFullTratme) {
        me.$parent.getExternalDirectives().tratme.configureExeciseController(me);
    }
    me.getCategoryScope = () => {
        if (me._categoryScope === null) {
            me._categoryScope = getAngularScopeByElementId('ContentPlayerCtrl');
        }
        return me._categoryScope;
    };

    me.countSkippedQuestions = function () {
        let skippedCount = me.exercisedto.QuestionList.count({ IsCancelled: false, Status: me.enums.StudentQuestionStatus.Skipped });
        me.skippedOrUnansweredQuestionCount = skippedCount + me.exercisedto.QuestionList.count({ IsCancelled: false, Status: me.enums.StudentQuestionStatus.Unanswered });
        if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.File) {
            me.skippedOrUnansweredQuestionCount = me.skippedOrUnansweredQuestionCount + me.exercisedto.QuestionList.filter(d => d.FileUrl == null).count();
        }
        if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.TextAndFile) {
            me.skippedOrUnansweredQuestionCount = me.skippedOrUnansweredQuestionCount + me.exercisedto.QuestionList.filter(d => d.FileUrl == null && d.AnswerText != null && d.AnswerText.length == 0).count();
        }
    };

    me.countUnansweredQuestions = function () {
        return me.exercisedto.QuestionList.count({ IsCancelled: false, Status: me.enums.StudentQuestionStatus.Unanswered });
    }

    me.countMustAnswerQuestion = function () {
        return me.exercisedto.QuestionList.count() - me.exercisedto.DiscardedQuestionQuantity;
    }

    me.countAnsweredQuestions = function () {
        me.answeredQuestionCount = me.exercisedto.QuestionList.count({ Status: me.enums.StudentQuestionStatus.Answerered });
    };

    me.countDiscardedQuestions = function () {
        me.discardedQuestionCount = me.exercisedto.QuestionList.count({ Status: me.enums.StudentQuestionStatus.Discarded });
    };

    me.countMustDiscardQuestions = function () {
        let discardedQuestionQuantity = me.exercisedto.DiscardedQuestionQuantity;
        me.mustDiscardedQuestionCount = discardedQuestionQuantity - me.discardedQuestionCount;
        if (me.mustDiscardedQuestionCount < 0) {
            me.mustDiscardedQuestionCount = 0
        }
    };

    me.countQuestionStatus = function () {
        me.countSkippedQuestions();
        me.countAnsweredQuestions();
        me.countDiscardedQuestions();
        me.countMustDiscardQuestions();
    }

    me.showMessageUnansweredQuestion = function () {
        return me.countUnansweredQuestions() > 0;
    }

    me.showMessageErrorDiscardCount = function () {
        if (me.showMessageUnansweredQuestion()) {
            return false;
        }
        else {
            return me.discardedQuestionCount > me.exercisedto.DiscardedQuestionQuantity;
        }
    }

    me.showMessageErrorAnswerCount = function () {
        if (me.showMessageUnansweredQuestion()) {
            return false;
        }
        else {
            return me.answeredQuestionCount > me.countMustAnswerQuestion();
        }
    }

    me.showMessageResultPage = function () {
        return !(me.showMessageUnansweredQuestion() || me.showMessageErrorDiscardCount() || me.showMessageErrorAnswerCount());
    }

    me.answerSkipedOrUnansweredQuestion = function () {
        me.countSkippedQuestions();

        if (me.skippedOrUnansweredQuestionCount == 0)
            return;

        for (const element of me.exercisedto.QuestionList) {
            if (element.Status == me.enums.StudentQuestionStatus.Unanswered
                || element.Status == me.enums.StudentQuestionStatus.Skipped
                || (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.File && element.FileUrl == null)
                || (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.TextAndFile && element.FileUrl == null && element.AnswerText != null && element.AnswerText.length == 0)
            ) {
                me.select(element, element.QuestionIndex);
                break;
            }
        }
    }
    me.getIndexOfCurrentQuestion = function () {
        if (me.currentQuestion == null || me.exercisedto.QuestionList == null || me.exercisedto.QuestionList.length == 0)
            return 0;

        return me.exercisedto.QuestionList.indexOf(me.currentQuestion)
    };
    me.previousQuestion = function () {
        if (isSafeToUse(me, 'onBeforeChangeQuestion'))
            me.onBeforeChangeQuestion();
        me.CurrentQuestionIndex = me.getIndexOfCurrentQuestion();

        if (me.CurrentQuestionIndex >= 1) {
            me.CurrentQuestionIndex--;
            let cq = me.exercisedto.QuestionList[me.CurrentQuestionIndex];
            me.exercisedto.QuestionList.set({ Active: false });
            cq.Active = true;
            me.currentQuestion = cq;

            if (isSafeToUse(me, 'onChangeQuestion'))
                me.onChangeQuestion();

            if (me.currentQuestion.QuestionIndex <= Math.round((me.lastCard + me.firstCard) / 2) && me.firstCard > 0) {
                me.scrollLeft(1);
            }
            me.updateExerciseSetOpenQuestionGrade();
        }
    };
    me.goToFirstQuestion = function () {
        let cq = me.exercisedto.QuestionList.first({ SequentialNumber: 1 });
        if (cq != null)
            me.select(cq);
    };
    me.nextQuestion = function () {
        me.$applyAsync(function () {
            me.CurrentQuestionIndex = me.getIndexOfCurrentQuestion();
            if ((me.CurrentQuestionIndex < me.exercisedto.QuestionList.length - 1) && !me.verifyQuestionAnswered()) {
                //Se entrar nesse if é por que tem respostas em branco e não é a última questão, por isso passa para a próxima questão
                me.CurrentQuestionIndex++;
                me.setQuestionIndex();
                me.verifyHoriontalScroll();
            } else if (me.verifyQuestionAnswered()) {
                //Se tiver tudo respondido e clicar no botão salvar, independente da questão que ele estiver passa para a tela de finalizar a prova
                me.showCloseLayer()
            }
        });
    };
    me.setQuestionIndex = function () {
        if (isSafeToUse(me, 'onBeforeChangeQuestion'))
            me.onBeforeChangeQuestion();
        let cq = me.exercisedto.QuestionList[this.CurrentQuestionIndex];
        if (cq != null) {
            me.exercisedto.QuestionList.set({ Active: false });
            cq.Active = true;
            me.currentQuestion = cq;
            if (isSafeToUse(me, 'onChangeQuestion'))
                me.onChangeQuestion();

            me.updateExerciseSetOpenQuestionGrade();
        }
    };
    me.goToQuestion = function (question) {
        let cq = me.exercisedto.QuestionList.first({ SequentialNumber: question.SequencialNumber });
        if (cq != null)
            me.select(cq);
    };
    me.verifyHoriontalScroll = function () {
        if (me.currentQuestion.QuestionIndex >= Math.round((me.lastCard + me.firstCard) / 2) && me.lastCard <= me.exercisedto.QuestionList.length) {
            me.scrollRight(1);
        }
    };
    me.getQuestions = function () {
        return me.exercisedto.QuestionList.filter(function (item) {
            return item.QuestionIndex >= me.firstCard && item.QuestionIndex <= me.lastCard;
        });
    };
    me.scrollRight = function (step) {
        me.lastCard += step || me.maxCards;
        me.firstCard += step || me.maxCards;
    };
    me.scrollLeft = function (step) {
        me.lastCard -= step || me.maxCards;
        me.firstCard -= step || me.maxCards;
    };
    me.scrollEndRight = function () {
        let dif = me.lastCard - me.firstCard;
        me.lastCard = me.exercisedto.QuestionList.length;
        me.firstCard = me.lastCard - dif;
    };

    me.unMarkQuestion = function () {

        let check = document.querySelectorAll('[name="card"]');
        check.forEach(function (c) {
            if (c.checked) {
                c.checked = false;
            }
        });
    };

    me.checkedObservation = function () {

        let checkObservation = document.querySelector('[id="observation"]');
        checkObservation.checked = true;
        let checkQuestion = document.querySelector('[id="question"]');
        checkQuestion.checked = false;
    };

    me.showFeedbackAnswer = function () {

        let sca = me.exercisedto.QuestionList;
        sca.forEach(function (s) {
            if (s.ShowCorrectAnswer) {
                me.showFeedbackCorrectAnswer = true;
            }
        });
        return !!me.showFeedbackCorrectAnswer;
    };

    me.openDialogResourceQuestion = function (question) {
        me.currentQuestion = question;
        me.currentQuestion.SelectedOption = me.currentQuestion.AnsweredQuestionOptionID;
        me.showOptionQuestionInvalidationDialog = true;
        me.currentQuestion.StudentComment = null;
        me.$parent.action('getResourceSettingsJustification', '', (result) => {
            me.listResourceSettingsJustification = result;
        });
    };

    me.getValueIsDuplicated = function () {
        me.resourceStudentQuestion.StudentComment = '';
        $('[ng-value="question.QuestionID"]:checked').each(
            function () {
                me.resourceStudentQuestion.StudentComment += (this.value) + ';';
            });
    }

    me.openDialogResourceQuestionConfirm = function () {
        me.resourceStudentQuestion.ResourceSettingsJustificationID = me.currentQuestion.SelectedJustification.ResourceSettingsJustificationID;
        me.resourceStudentQuestion.QuestionID = me.currentQuestion.QuestionID;
        me.resourceStudentQuestion.CanComment = me.currentQuestion.SelectedJustification.CanComment;
        me.resourceStudentQuestion.IsDuplicated = me.currentQuestion.SelectedJustification.IsDuplicated;
        if (me.resourceStudentQuestion.IsDuplicated) {
            me.getValueIsDuplicated();
        } else if (me.resourceStudentQuestion.CanComment) {
            me.resourceStudentQuestion.StudentComment = me.currentQuestion.StudentComment;
        }
        me.resourceStudentQuestion.JustificationName = me.currentQuestion.SelectedJustification.JustificationName;
        me.showOptionQuestionInvalidationConfirmDialog = true;
    };

    me.complementQuestionInvalidationDialog = function (SelectedJustification) {
        me.showComplementTextQuestionInvalidationDialog = SelectedJustification.CanComment;
        me.showComplementCheckBoxQuestionInvalidationDialog = SelectedJustification.IsDuplicated;
    };

    me.disabledButton = function (SelectedJustification) {
        if (SelectedJustification == null) {
            return true;
        }

        if (!SelectedJustification.CanComment && !SelectedJustification.IsDuplicated) {
            return false;
        }

        if (SelectedJustification.CanComment  && (me.currentQuestion.StudentComment != null && me.currentQuestion.StudentComment != '')) {
            return false;
        } else if (SelectedJustification.CanComment && (me.currentQuestion.StudentComment == null || me.currentQuestion.StudentComment == '')) {
            return true;
        }

        if (SelectedJustification.IsDuplicated && $('[ng-value="question.QuestionID"]:checked').length > 0) {
            return false;
        } else if ($('[ng-value="question.QuestionID"]:checked').length == 0) {
            return true;
        }
    };
    me.saveResourceStudentQuestion = function () {
        let parameters = me.getDefaultParameters();
        parameters.ResourceSettingsJustificationID = me.resourceStudentQuestion.ResourceSettingsJustificationID;
        parameters.QuestionID = me.currentQuestion.QuestionID;
        parameters.StudentComment = me.resourceStudentQuestion.StudentComment;
        parameters.SequencialNumber = me.currentQuestion.QuestionIndex;
        me.$parent.action('saveResourceStudentQuestion', parameters, (result) => {
            if (result.Success) {
                me.currentQuestion.ResourceSettingsStatus = me.enums.ResourceSettingsStatus.Analyzing;
            }
        });
        me.showOptionQuestionInvalidationResultDialog = true;
        me.showOptionQuestionInvalidationConfirmDialog = false;
    };

    me.closeAllDialogsResourceQuestion = function () {
        me.resourceStudentQuestion.StudentComment = null;
        me.showOptionQuestionInvalidationResultDialog = false;
        me.showOptionQuestionInvalidationDialog = false;
        me.showComplementCheckBoxQuestionInvalidationDialog = false;
        me.showComplementTextQuestionInvalidationDialog = false;

    }

    me.ExerciseIsOpen = function () {
        return !me.exercisedto.ShowStudentLessonCategoryPost;
    }

    me.isValidOpenQuestion = function () {
        return me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion &&
            me.currentQuestion != null &&
            me.getHtml().trim() !== '';
    }

    me.select = async function (item, index) {
        if (me.ExerciseIsOpen() && me.isValidOpenQuestion()) {
            me.busy = false;
            me.currentQuestion.AnswerText = me.getHtml();
            me.isNotNextQuestion = true;
            await me.save();
        }

        if (me.$parent.serverParameters.parameters.enablePlayerAsync) {
            me.$applyAsync(me.changeQuestionDetails(item, index));
        }

        if (!me.$parent.serverParameters.parameters.enablePlayerAsync) {
            me.changeQuestionDetails(item, index);
        }
    };

    me.changeQuestionDetails = function (item, index) {
        me.currentQuestion = null;
        if (isSafeToUse(me, 'onBeforeChangeQuestion'))
            me.onBeforeChangeQuestion();


        me.resultPageIsOn = false;
        me.closeSlideIsOn = false;
        me.currentQuestion = item;
        me.CurrentQuestionIndex = me.getIndexOfCurrentQuestion();
        me.exercisedto.QuestionList.set({ Active: false });
        me.currentQuestion.Active = true;
        me.exercisedto.QuestionList[me.CurrentQuestionIndex].Active = true;

        if (me.currentQuestion.Status == me.enums.StudentQuestionStatus.Answerered || me.currentQuestion.Status == me.enums.StudentQuestionStatus.Discarded)
            me.currentQuestion.SelectedOption = me.currentQuestion.AnsweredQuestionOptionID;

        me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Question;

        if (isSafeToUse(me, 'onChangeQuestion'))
            me.onChangeQuestion();
        me.updateExerciseSetOpenQuestionGrade();
        if (me.currentQuestion.Options.length == 0 && me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined) {
            me.getFeedbackAnswer();
        }
    };

    me.goToResultPage = function (onlineExerciseClosedBySystem) {

        let categoryContext = me.getCategoryScope();

        me.currentQuestion = null;
        me.updateExerciseSetOpenQuestionGrade();
        if (me.busy)
            return;
        me.countQuestionStatus();
        me.busy = true;
        me.scrollEndRight();
        if (me.exercisedto.ResultSummary != null) {
            me.resultPageIsOn = true;
            me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Result;
            categoryContext.showCommentsForLessonCategory = true;
            me.busy = false;
            return;
        }

        let parameters = me.getDefaultParameters();
        if (onlineExerciseClosedBySystem) {
            parameters.OnlineExerciseClosedBySystem = onlineExerciseClosedBySystem;
        }

        me.$parent.action('closeExerciseSet', parameters, function (result) {
            if (result.Success) {
                me.resultPageIsOn = true;
                me.exercisedto.ResultSummary = result.Result.ResultSummary;
                me.exercisedto.FeedbackDescriptors = result.Result.FeedbackDescriptors;
                me.exercisedto.ExerciseSetTakeList = result.Result.ExerciseSetTakeList;
                me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Result;
                categoryContext.showCommentsForLessonCategory = true;
                me.exercisedto.ReadOnly = true;
                me.exercisedto.WaitingCorrection = result.Result.WaitingCorrection;
                me.exercisedto.StudentTrackID = result.Result.SelectedStudentTrackID;
                me.exercisedto.QuestionList.set({ ShowCorrectAnswer: result.Result.ShowCorrectAnswer });
                me.exercisedto.ShowStudentLessonCategoryPost = true;
                if (isSafeToUse(result, 'Result.QuestionAnswerKeys')) {
                    for (const item of result.Result.QuestionAnswerKeys) {
                        let q = me.exercisedto.QuestionList.first({ QuestionID: item.QuestionID });
                        if (q != null) {
                            q.CorrectQuestionOptionID = item.CorrectQuestionOptionID;
                            q.ExpectedAnswer = item.ExpectedAnswer;
                            q.ShowFeedback = item.ShowFeedBack;
                        }
                    }

                }
                me.$parent.$emit('closedExerciseSet', { result });

                me.getChart();
                me.updateAssignmentPost();
                me.pushOnGoogleAnalytics();
                me.root.checkLessonOnLeftMenu(me.allprops);
                if (isSafeToUse(me, 'onCloseExerciseSet'))
                    me.onCloseExerciseSet();

                if (me.serverParameters.parameters.goToFirstQuestionAfterExerciseSetIsClosed
                    && me.exercisedto.QuestionList.length == 1
                    && isSafeToUse(me, 'goToFirstQuestion')) {
                    me.goToFirstQuestion();
                }
            }
            me.busy = false;
        }, me.onError);

        if (me.exercisedto.CompareFaceContinuous && me.exercisedto.CompareFacesEnabled && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
            me.stopCam();
        }

        if (me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise && me.allprops.isVestib && me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Objective) {
            me.reloadPage();
        }
    };
    me.goToWaitingCorrectionPage = function () {
        if (me.busy)
            return;
        me.countQuestionStatus();
        me.scrollEndRight();
        me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Result;
        me.exercisedto.ShowStudentLessonCategoryPost = true;
    };
    me.showCloseLayer = function () {
        me.$applyAsync(function () {
            if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance && me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined) {
                window.parent.showHideGrades(false);
            }
            me.countQuestionStatus();
            if (me.exercisedto.ResultSummary != null) {
                me.goToResultPage();
                return;
            }

            me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Close;
            me.exercisedto.QuestionList.set({ Active: false });
            me.currentQuestion = null;
            me.showResultTab = true;
            me.closeSlideIsOn = true;
        });
    };
    me.showFinalScreenViaTab = async function () {
        if (me.busy)
            return;

        if (me.ExerciseIsOpen()) {
            await me.postAnswerAsync('answerQuestion', function (result) {
                if (result.Success) {
                    me.currentQuestion.Status = me.enums.StudentQuestionStatus.Answerered;
                    me.currentQuestion.IsAnswered = true;
                    if (me.exercisedto.DiscardedQuestionEnabled) {
                        me.currentQuestion.IsDiscarded = false;
                    }
                    me.currentQuestion.AnsweredQuestionOptionID = me.currentQuestion.SelectedOption;
                    me.countQuestionStatus();

                    if (isSafeToUse(me, 'onAfterAnswer')) {
                        me.onAfterAnswer();
                    }
                }
            });
        }
        me.busy = false;
        me.showCloseLayer();
    };
    me.getDefaultParameters = function () {
        return {
            StudentLessonID: me.allprops.StudentLessonID,
            StudentTrackID: me.exercisedto.StudentTrackID || 0,
            ClassDisciplineID: parseInt(me.root.bigContext.dto.ClassDisciplineID),
            LessonID: me.root.bigContext.dto.LessonID,//parseInt(me.serverParameters.actions.load.data.LessonID),
            LessonCategoryID: parseInt(me.allprops.LessonCategoryID),
            LessonCategoryType: me.allprops.LessonCategoryType
        }
    };

    me.postAnswerAsync = function (type, fn) {
        return new Promise((resolve, reject) => {
            if (me.busy || me.studentIsSuspendedOnDiscipline)
                return;
            me.busy = true;
            me.lockOptions = true;
            let parameters = me.getDefaultParameters();

            parameters.QuestionID = me.currentQuestion.QuestionID;
            parameters.QuestionOptionID = me.currentQuestion.SelectedOption;
            if ((me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion) || (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined && me.currentQuestion.Options.length == 0)) {

                if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.Text) {
                    parameters.AnswerText = me.getHtml();

                    if (parameters.AnswerText == null || parameters.AnswerText.trim() == '') {
                        showTopErrorMsg('O campo resposta não pode ser vazio.', ERROR_DISPLAY_TIME, null, false);
                        me.busy = false;
                        return;
                    }
                }
                else if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.File) {
                    if (me.currentQuestion.FileName == undefined || me.currentQuestion.FileName.FileUrl == '') {
                        showTopErrorMsg('Não há nenhum arquivo vinculado.', ERROR_DISPLAY_TIME, null, false);
                        me.busy = false;
                        return;
                    }

                    parameters.FileName = me.currentQuestion.FileName;
                    parameters.FileUrl = me.currentQuestion.FileUrl;
                }
                else {
                    let hasText = true;
                    let hasFile = true;

                    parameters.AnswerText = me.getHtml();

                    if (parameters.AnswerText == null || parameters.AnswerText.trim() == '') {
                        hasText = false;
                    }
                    if (me.currentQuestion.FileName == undefined || me.currentQuestion.FileName.FileUrl == '') {
                        hasFile = false;
                    }
                    else {
                        parameters.FileName = me.currentQuestion.FileName;
                        parameters.FileUrl = me.currentQuestion.FileUrl;
                    }

                    if (!hasText && !hasFile) {
                        showTopErrorMsg('Não há nenhum arquivo ou texto como resposta.', ERROR_DISPLAY_TIME, null, false);
                        me.busy = false;
                        return;
                    }
                }

            }
            else {
                parameters.AnswerText = '';
            }
            parameters.AnswerType = 1;

            me.$parent.action(type, parameters, function (result) {
                if (result.Success) {
                    for (let i in result.Result) {
                        me.currentQuestion[i] = result.Result[i];
                    }

                    if (result.Status == me.enums.StudentExerciseSetDetailStatus.Result) {
                        me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Result;
                    }
                    if (typeof (fn) == 'function')
                        fn(result);

                    resolve(result);
                } else {
                    reject(new Error('Requisição falhou'));
                }
                me.busy = false;
                me.lockOptions = false;
            }, me.onError);
        });
    };

    me.postAnswer = function (type, fn) {
        if (me.busy || me.studentIsSuspendedOnDiscipline)
            return;
        me.busy = true;
        me.lockOptions = true;
        let parameters = me.getDefaultParameters();

        parameters.QuestionID = me.currentQuestion.QuestionID;
        parameters.QuestionOptionID = me.currentQuestion.SelectedOption;
        if ((me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion) || (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined && me.currentQuestion.Options.length == 0)) {

            if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.Text) {
                parameters.AnswerText = me.getHtml();

                if (parameters.AnswerText == null || parameters.AnswerText.trim() == '') {
                    showTopErrorMsg('O campo resposta não pode ser vazio.', ERROR_DISPLAY_TIME, null, false);
                    me.busy = false;
                    return;
                }
            }
            else if (me.exercisedto.OpenQuestionTypeID == me.enums.OpenQuestionType.File) {
                if (me.currentQuestion.FileName == undefined || me.currentQuestion.FileName.FileUrl == '') {
                    showTopErrorMsg('Não há nenhum arquivo vinculado.', ERROR_DISPLAY_TIME, null, false);
                    me.busy = false;
                    return;
                }

                parameters.FileName = me.currentQuestion.FileName;
                parameters.FileUrl = me.currentQuestion.FileUrl;
            }
            else {
                let hasText = true;
                let hasFile = true;

                parameters.AnswerText = me.getHtml();

                if (parameters.AnswerText == null || parameters.AnswerText.trim() == '') {
                    hasText = false;
                }
                if (me.currentQuestion.FileName == undefined || me.currentQuestion.FileName.FileUrl == '') {
                    hasFile = false;
                }
                else {
                    parameters.FileName = me.currentQuestion.FileName;
                    parameters.FileUrl = me.currentQuestion.FileUrl;
                }

                if (!hasText && !hasFile) {
                    showTopErrorMsg('Não há nenhum arquivo ou texto como resposta.', ERROR_DISPLAY_TIME, null, false);
                    me.busy = false;
                    return;
                }
            }

        }
        else {
            parameters.AnswerText = '';
        }
        parameters.AnswerType = 1;

        me.$parent.action(type, parameters, function (result) {
            if (result.Success) {
                for (let i in result.Result) {
                    me.currentQuestion[i] = result.Result[i];
                }

                if (result.Status == me.enums.StudentExerciseSetDetailStatus.Result) {
                    me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Result;
                    return;
                }
                if (typeof (fn) == 'function')
                    fn(result);
            }
            me.busy = false;

            me.lockOptions = false;
        }, me.onError);
    };

    me.currentAnswerIsEmpty = function () {
        return (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion || me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined) && me.getHtml() == '';
    }
    me.skipQuestion = function () {
        if (me.currentQuestion == null || me.studentIsSuspendedOnDiscipline)
            return;

        if (me.currentQuestion.IsAnswered || me.exercisedto.ResultSummary != null || me.exercisedto.ReadOnly || me.currentAnswerIsEmpty()) {
            me.currentQuestion.Status = me.enums.StudentQuestionStatus.Skipped;
            me.nextQuestion();
            return;
        }

        if (me.busy)
            return;

        me.postAnswer('skipQuestion', function (result) {
            me.currentQuestion.Status = me.enums.StudentQuestionStatus.Skipped;
            if (me.currentQuestion.last) {
                me.showCloseLayer()
            } else {
                me.nextQuestion();
                me.busy = false;
            }

        }, me.onError);
    };

    me.discardQuestion = function () {
        if (me.currentQuestion == null || me.studentIsSuspendedOnDiscipline || me.discardedQuestionCount >= me.exercisedto.DiscardedQuestionQuantity)
            return;

        if (me.busy)
            return;

        me.postAnswer('discardQuestion', function (result) {
            me.currentQuestion.Status = me.enums.StudentQuestionStatus.Discarded;
            me.currentQuestion.IsDiscarded = true;
            me.countQuestionStatus();
            if (me.currentQuestion.last) {
                me.showCloseLayer();
            } else {
                me.nextQuestion();
                me.busy = false;
            }

        }, me.onError);
    }

    me.includeQuestion = function () {
        if (me.currentQuestion == null || me.studentIsSuspendedOnDiscipline || me.answeredQuestionCount >= me.countMustAnswerQuestion())
            return;

        if (me.busy )
            return;

        me.postAnswer('includeQuestion', function (result) {
            if (me.currentQuestion.IsAnswered) {
                me.currentQuestion.Status = me.enums.StudentQuestionStatus.Answerered;
            }
            else {
                me.currentQuestion.Status = me.enums.StudentQuestionStatus.Unanswered;
            }
            me.busy = false;
            me.countQuestionStatus();
        }, me.onError);
    }

    me.saveAnswerQuestion = function () {
        if (me.currentQuestion == null || me.studentIsSuspendedOnDiscipline  || !me.exercisedto.SaveQuestionClickOption)
            return;

        if (me.busy )
            return;

        me.postAnswer('answerQuestion', function (result) {
            me.busy = false;
        }, me.onError);
    }

    me.answer = async function () {
        if (me.busy)
            return;

        if (me.$parent.serverParameters.parameters.enablePlayerAsync) {
            await me.postAnswerAsync('answerQuestion', function (result) {
                if (result.Success) {
                    me.currentQuestion.Status = me.enums.StudentQuestionStatus.Answerered;
                    me.currentQuestion.IsAnswered = true;
                    if (me.exercisedto.DiscardedQuestionEnabled) {
                        me.currentQuestion.IsDiscarded = false;
                    }
                    me.currentQuestion.AnsweredQuestionOptionID = me.currentQuestion.SelectedOption;
                    me.countQuestionStatus();

                    if (isSafeToUse(me, 'onAfterAnswer')) {
                        me.onAfterAnswer();
                    }
                    me.busy = false;

                    let isVivae = me.exercisedto.isHabilitarFeedbackUltimaQuestao && me.exercisedto.isVivae;

                    if ((!isVivae && result.Result.ShowExerciseSetClose && (!me.currentQuestion.CanAnswerAgain || me.currentQuestion.IsCorrect || me.exercisedto.GoToNextQuestionAfterAnswering)) && !me.isNotNextQuestion) {
                        me.showCloseLayer();
                    }
                    else if ((me.exercisedto.GoToNextQuestionAfterAnswering || me.exercisedto.DiscardedQuestionEnabled) && !me.isNotNextQuestion) {
                        me.nextQuestion();
                    }
                    me.isNotNextQuestion = false;
                }
            });
        } 

        if (!me.$parent.serverParameters.parameters.enablePlayerAsync) {
            me.postAnswer('answerQuestion', function (result) {
                if (result.Success) {
                    me.currentQuestion.Status = me.enums.StudentQuestionStatus.Answerered;
                    me.currentQuestion.IsAnswered = true;
                    if (me.exercisedto.DiscardedQuestionEnabled) {
                        me.currentQuestion.IsDiscarded = false;
                    }
                    me.currentQuestion.AnsweredQuestionOptionID = me.currentQuestion.SelectedOption;
                    me.countQuestionStatus();

                    if (isSafeToUse(me, 'onAfterAnswer')) {
                        me.onAfterAnswer();
                    }
                    me.busy = false;

                    let isVivae = me.exercisedto.isHabilitarFeedbackUltimaQuestao && me.exercisedto.isVivae;

                    if ((!isVivae && result.Result.ShowExerciseSetClose && (!me.currentQuestion.CanAnswerAgain || me.currentQuestion.IsCorrect || me.exercisedto.GoToNextQuestionAfterAnswering)) && !me.isNotNextQuestion) {
                        me.showCloseLayer();
                    }
                    else if ((me.exercisedto.GoToNextQuestionAfterAnswering || me.exercisedto.DiscardedQuestionEnabled) && !me.isNotNextQuestion) {
                        me.nextQuestion();
                    }
                    me.isNotNextQuestion = false;
                }
            }, me.onError);
        }
    };

    me.verifyQuestionAnswered = function () {
        return me.exercisedto.QuestionList.every(item => item.AnswerText);
    };

    me.pushOnGoogleAnalytics = function () {
        if (!isSafeToUse(me, 'root'))
            me.root = getRootScope($scope);
        me.root.pushOnGoogleAnalytics(me.allprops);
    };

    me.updateAssignmentPost = function () {
        if (me.studentIsSuspendedOnDiscipline)
            return

        if (isSafeToUse(me, 'assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
            me.assignmentPostInstance.refresh();
        }
    };

    me.tryAgain = function () {
        if (me.busy  || me.studentIsSuspendedOnDiscipline)
            return;
        me.answeredQuestionCount--;
        me.currentQuestion.AnsweredQuestionOptionID = null;
        me.currentQuestion.Status = me.enums.StudentQuestionStatus.Unanswered;
        me.currentQuestion.IsAnswered = false;
    };
    me.start = function () {
        if (me.busy  || me.studentIsSuspendedOnDiscipline )
            return;

        if (me.exercisedto.CompareFacesEnabled && me.compareFaceSuccess) {
            if (me.streaming) {
                me.stopCam();
            }

            if (me.exercisedto.CompareFaceContinuous  && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
                me.startupVideo(videoExercise, false);
            }
        }

        if (me.exercisedto.DenyCopyCutText) {
            me.denyCopyText();
        }

        if (!me.compareFaceSuccess && me.exercisedto.CompareFacesEnabled  && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
            me.compareFaceDialog = true;
        }
        else {
            me.busy = true;
            let parameters = me.getDefaultParameters();

            me.$parent.action('startExerciseSet', parameters, function (result) {
                if (result.Success) {
                    me.exercisedto = result.Result;
                    if (me.exercisedto.LimitTime > 0 && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
                        me.exercisedto.StartDate = new Date();
                        me.startCountDown(me.allprops.LessonCategoryID, me.exercisedto.StartDate);
                    }
                    me.setCurrentQuestion();
                    if (isSafeToUse(me, 'currentQuestion'))
                        me.currentQuestion.Active = true;
                    me.updateAssignmentPost();
                }
                me.busy = false;
            }, me.onError);
        }
    };
    me.restart = function () {
        if (me.exercisedto.ResultSummary.TryAgainResult != me.enums.ExerciseSetTakeTryAgainResult.Allow)
            return;

        if (me.busy || me.studentIsSuspendedOnDiscipline)
            return;

        if (me.exercisedto.CompareFacesEnabled && me.compareFaceSuccess) {
            if (me.streaming) {
                me.stopCam();
            }

            if (me.exercisedto.CompareFaceContinuous && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
                me.startupVideo(videoExercise, false);
            }
        }

        if (me.exercisedto.DenyCopyCutText) {
            me.denyCopyText();
        }

        if (!me.compareFaceSuccess && me.exercisedto.CompareFacesEnabled && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
            me.compareFaceDialog = true;
        }
        else {
            me.busy = true;
            let parameters = me.getDefaultParameters();

            me.$parent.action('newExerciseSetTake', parameters, function (result) {
                if (result.Success) {
                    me.allprops.StudentTrackID = result.Result.StudentTrackID;
                    me.exercisedto = result.Result;
                    if (me.exercisedto.LimitTime > 0 && me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
                        me.exercisedto.StartDate = new Date();
                        $('#countdounTimer' + me.allprops.LessonCategoryID).css('visibility', '');
                        me.startCountDown(me.allprops.LessonCategoryID, me.exercisedto.StartDate);
                    }
                    me.setCurrentQuestion();
                    if (isSafeToUse(me, 'currentQuestion'))
                        me.currentQuestion.Active = true;
                    me.updateAssignmentPost();
                }
                me.busy = false;
            }, me.onError);
        }
    };
    me.setCurrentQuestion = function () {
        if (me.exercisedto.QuestionList.length == 0)
            return;

        let c = me.exercisedto.QuestionList.first({ Active: true });
        if (c != null) {
            me.currentQuestion = c;
        } else if (me.exercisedto.QuestionList.length > 0) {
            me.currentQuestion = me.exercisedto.QuestionList[0];
            if (me.currentQuestion != null)
                me.currentQuestion.Active = true;
        }
        me.currentQuestion.SelectedOption = me.currentQuestion.AnsweredQuestionOptionID;
        let opt = me.currentQuestion.Options.first({ Selected: true });
        if (opt != null) {
            me.currentQuestion.SelectedOption = opt.QuestionOptionID
        } else {
            me.currentQuestion.SelectedOption = null;
        }
        me.CurrentQuestionIndex = me.getIndexOfCurrentQuestion();
        if (me.CurrentQuestionIndex >= me.lastCard) {
            me.lastCard = me.CurrentQuestionIndex + 1;
            me.firstCard = me.lastCard - me.maxCards;
        }
        me.showResultTab = false;
        me.closeSlideIsOn = false;
        me.resultPageIsOn = false;
        me.setLastQuestion();
        if (isSafeToUse(me, 'changeQuestion'))
            me.changeQuestion();
    };
    me.changeExecerciseSet = function () {
        let c = me.exercisedto.ExerciseSetTakeList.first({ StudentTrackID: me.exercisedto.selectedExerciseSet });
        if (c != null) {
            let parameters = me.getDefaultParameters();
            parameters.StudentLessonID = c.StudentLessonID;
            parameters.StudentTrackID = c.StudentTrackID;

            me.$parent.action('getExerciseSetTake', parameters, function (result) {
                if (result.Success) {
                    me.exercisedto = result.Result;
                    me.$parent.$emit('gotExerciseSetTake', { result });
                    me.resultPageIsOn = me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Result;

                    me.setCurrentExerciseSetTake();
                }
            });
        }
        me.countQuestionStatus();
    };
    me.setCurrentExerciseSetTake = function () {
        if (me.exercisedto.ExerciseSetTakeList != null && me.exercisedto.ExerciseSetTakeList.length > 0) {
            let c = me.exercisedto.ExerciseSetTakeList.first({ StudentTrackID: me.exercisedto.StudentTrackID });
            if (c != null) {
                me.exercisedto.selectedExerciseSet = c.StudentTrackID;
            } else {
                me.exercisedto.selectedExerciseSet = me.exercisedto.ExerciseSetTakeList[0].StudentTrackID;
            }
        }
    };
    me.setLastQuestion = function () {
        if (me.exercisedto.QuestionList != null && me.exercisedto.QuestionList.length > 0) {
            me.exercisedto.QuestionList.set({ last: false, userTyping: false });
            let last = me.exercisedto.QuestionList.last();
            if (last != null)
                last.last = true;

        }
    };
    me.onload = function () {
        if (typeof (me.exercisedto) == 'undefined' || me.exercisedto == null)
            return;

        me.setLastQuestion();
        me.countQuestionStatus();

        if (me.exercisedto.DenyCopyCutText) {
            me.denyCopyText();
        }

        if (me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise && me.exercisedto.LimitTime > 0 && me.exercisedto.StartDate != null) {
            me.startCountDown(me.allprops.LessonCategoryID, me.exercisedto.DateNow);
        }

        if (me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise && me.exercisedto.StartDate != null && me.exercisedto.CompareFaceContinuous && me.exercisedto.Status != me.enums.StudentExerciseSetDetailStatus.Result) {
            me.startupVideo(videoExercise, false);
        }

        if (me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Question && me.currentQuestion == null) {
            me.setCurrentQuestion();
        }
        else if (me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Result) {
            if (
                me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion &&
                me.exercisedto.WaitingCorrection  &&
                me.allprops.Mode == me.serverParameters.enums.StudentCategoryViewMode.Performance) {
                me.exercisedto.Status = me.enums.StudentExerciseSetDetailStatus.Question;
                me.setCurrentQuestion();
                me.showResultTab = true;
            } else {
                me.showResultTab = true;
                me.goToResultPage();
            }

        } else if (me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Close) {
            me.showCloseLayer();
        }

        me.setCurrentExerciseSetTake();
        me.getChart();
        if (isSafeToUse(me, 'onAfterLoad'))
            me.onAfterLoad();

        me.updateExerciseSetOpenQuestionGrade();
    };
    me.onError = function (error) {
        console.error('internal error: ', error);
        me.lockOptions = false;
        me.busy = false;
        me.answering = false;
        me.generalError(error);
    }
    me.ifOpenQuestion = function (ifYes, ifNo) {
        if (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion) {
            return ifYes
        } else {
            return ifNo;
        }
    };
    me.ifMultipleChoice = function (ifYes, ifNo) {
        if (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Objective) {
            return ifYes
        } else {
            return ifNo;
        }
    };
    me.chart = {
        points: '',
        width: 270,
        height: 110
    };
    me.getChart = function () {
        if (typeof (me.exercisedto.ResultSummary) != 'undefined' &&
            me.exercisedto.ResultSummary != null &&
            typeof (me.exercisedto.ResultSummary.ChartValues) != 'undefined' &&
            me.exercisedto.ResultSummary.ChartValues != null &&
            me.exercisedto.ResultSummary.ChartValues.length > 0) {
            me.chart.max = me.exercisedto.ResultSummary.ChartValues.max('Value');
            me.chart.min = me.exercisedto.ResultSummary.ChartValues.min('Value');

            me.exercisedto.ResultSummary.ChartValues.foreach(function (a) {
                a.w = ((a.Interval / 100) * me.chart.width).toFixed(0);
                a.h = ((1 - (a.Value / me.chart.max)) * me.chart.height).toFixed(0);
                if (!isNaN(a.h) && !isNaN(a.w))
                    me.chart.points += a.w.toString() + ',' + a.h.toString() + ' ';
            });

            me.chart.points += (me.chart.width + 3).toString() + ',0 ';
            me.chart.points += (me.chart.width + 3).toString() + ',' + (me.chart.height + 3).toString() + ' ';
            me.chart.points += '-3,' + (me.chart.height + 3).toString() + ' ';
        }
    };
    me.updateExerciseSetOpenQuestionGrade = function () {
        if (me.studentIsSuspendedOnDiscipline)
            return

        if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance && window.parent.UpdateExerciseSetOpenQuestionGrade && (me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.OpenQuestion || me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OfflineAssessment || me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined)) {
            if (me.currentQuestion != null && me.currentQuestion.Status == me.enums.StudentQuestionStatus.Unanswered && me.exercisedto.QuestionType == me.enums.ExerciseSetQuestionType.Combined) {
                window.parent.showHideGrades(false);
            } else {
                try {
                    let questionSelected = (me.currentQuestion != null);
                    let obj = {
                        questionSelected: questionSelected,
                        questionID: questionSelected ? me.currentQuestion.QuestionID : 0,
                        openQuestionPercCorrect: me.exercisedto.OpenQuestionPercCorrect,
                        grade: questionSelected ? me.currentQuestion.Grade : -1,
                        studentTrackID: me.exercisedto.StudentTrackID,
                        lessonCategoryType: me.allprops.LessonCategoryType,
                        isMultipleChoice: (me.currentQuestion.Options != null && me.currentQuestion.Options.length > 0),
                        isCancelled: me.currentQuestion.IsCancelled
                    };
                    window.parent.UpdateExerciseSetOpenQuestionGrade(obj);
                } catch (e) {

                }
            }

        }
    }
    me.startCountDown = function (lessonCategoryId, dateNow) {
        me.countDownManager[lessonCategoryId] = setInterval(function () { me.updateCountDownDate(lessonCategoryId, dateNow) }, 1000);
    }
    me.updateCountDownDate = function (lessonCategoryId, dateNow) {
        if (me.exercisedto.StartDate == null || (me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Result)) {
            me.setDefaultLimitTime(lessonCategoryId);
        }
        else {
            let countDownDate = new Date(me.exercisedto.StartDate).getTime();

            if (!me.dateNowLT[lessonCategoryId] || me.dateNowLT[lessonCategoryId] == undefined || me.dateNowLT[lessonCategoryId] == '') {
                me.dateNowLT[lessonCategoryId] = new Date(dateNow).getTime();
            }

            let diffDate = me.getLimitTime(countDownDate) - me.dateNowLT[lessonCategoryId];
            me.dateNowLT[lessonCategoryId] = me.dateNowLT[lessonCategoryId] + 1000;

            me.setTextLimitTime(diffDate, lessonCategoryId);

            if (diffDate < 1) {
                me.stopCountLimitTime();
                me.goToResultPage(true);
                $('#countdounTimer' + lessonCategoryId).css('visibility', 'hidden');
                me.exercisedto.WaitingCorrection = true;
                if (me.allprops.Mode != me.enums.StudentCategoryViewMode.Performance) {
                    location.reload(true);
                }
            }
        }
    }
    me.getLimitTime = function (addDate) {
        if (me.enums.LimitTimeType.Days == me.exercisedto.LimitTimeType) {
            addDate = addDate + (me.exercisedto.LimitTime * 24) * 3600000;
        }
        else if (me.enums.LimitTimeType.Hours == me.exercisedto.LimitTimeType) {
            addDate = addDate + (me.exercisedto.LimitTime * 3600000);
        }
        else if (me.enums.LimitTimeType.Minutes == me.exercisedto.LimitTimeType) {
            addDate = addDate + (me.exercisedto.LimitTime * 60000);
        }

        return addDate;
    }
    me.setTextLimitTime = function (diffDate, lessonCategoryId) {

        let days = Math.floor(diffDate / (1000 * 60 * 60 * 24));
        let hours = Math.floor((diffDate % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((diffDate % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((diffDate % (1000 * 60)) / 1000);
        let limitTimeInMs = 0;

        if (me.countMSecondsManager[lessonCategoryId] || me.countMSecondsManager[lessonCategoryId] == undefined || me.countMSecondsManager[lessonCategoryId] == '') {
            limitTimeInMs = me.getLimitTime(0);
            me.countMSecondsManager[lessonCategoryId] = true;
            $('#pieSpinner' + lessonCategoryId).css('animation-duration', limitTimeInMs + 'ms')
            $('#pieFiller' + lessonCategoryId).css('animation-duration', limitTimeInMs + 'ms')
            $('#mask' + lessonCategoryId).css('animation-duration', limitTimeInMs + 'ms')
            $('#timeDetail' + lessonCategoryId).css('animation-duration', limitTimeInMs + 'ms')
            $('#limitTime' + lessonCategoryId).css('animation-duration', limitTimeInMs + 'ms')

            $('#pieSpinner' + lessonCategoryId).css('animation-delay', (diffDate - limitTimeInMs) + 'ms')
            $('#pieFiller' + lessonCategoryId).css('animation-delay', (diffDate - limitTimeInMs) + 'ms')
            $('#mask' + lessonCategoryId).css('animation-delay', (diffDate - limitTimeInMs) + 'ms')
            $('#timeDetail' + lessonCategoryId).css('animation-delay', (diffDate - limitTimeInMs) + 'ms')
            $('#limitTime' + lessonCategoryId).css('animation-delay', (diffDate - limitTimeInMs) + 'ms')
        }

        if ((limitTimeInMs * 0.1) >= diffDate) {
            $('#countdounTimer' + lessonCategoryId).addClass('criticalTimer');
        }

        if (hours > 0 && minutes < 10) {
            minutes = '0' + minutes;
        }
        if (minutes > 0 && seconds < 10) {
            seconds = '0' + seconds;
        }

        if (hours > 0) {
            if (days > 0) {
                hours = (days * 24) + hours;
            }
            $('#limitTime' + lessonCategoryId).text(hours + ':' + minutes + ':' + seconds);
        }
        else if (minutes > 0) {
            $('#limitTime' + lessonCategoryId).text(minutes + ':' + seconds);
        }
        else {
            $('#limitTime' + lessonCategoryId).text(seconds);
        }

    }
    me.setDefaultLimitTime = function (lessonCategoryId) {
        $('#countdounTimer' + lessonCategoryId).css('visibility', 'hidden');
        me.stopCountLimitTime();
    }
    me.stopCountLimitTime = function () {
        clearInterval(me.countDownManager[me.allprops.LessonCategoryID]);
    }

    //CAM
    me.startupVideo = function (video, firstTime) {
        me.canvas = document.getElementById('canvas');
        me.photo = document.getElementById('photo');

        navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then(function (stream) {
                me.streaming = true;
                me.localstream = stream;
                me.video = video;
                me.video.srcObject = stream;
                me.video.play();
                me.setTimer(4, firstTime);
            })
            .catch(function (err) {
                console.log('An error occurred: ' + err);
                if (me.exercisedto.CompareFaceAcceptDisabledCam) {
                    me.compareFaceSuccess = true;
                    $('#startOnlineExercise').removeClass('disabled');
                }
                else {
                    me.capturePictureError = true;
                }
            });
        me.capturePicture = false;
        me.clearPhoto();
    }

    me.showCountDown = true;

    me.setTimer = function (t, firstTime) {
        if (firstTime)
            me.timerCountDown = t;
        if (t == 0 || !firstTime) {
            me.showCountDown = false;
            me.takePicture(firstTime);
            if (me.exercisedto.CompareFaceContinuous && !firstTime) {
                me.SetTimeCompareFace();
            }
            else {
                me.compareFaceStudent(true);
            }
        } else {
            me.timeout(function () {
                me.setTimer(t - 1, firstTime);
            }, 1000)
        }

    }

    me.stopCam = function () {
        me.video.pause();
        me.video.src = '';
        me.localstream.getTracks().forEach(track => track.stop());
        me.streaming = false;
        me.targetStudentPicture = '';
    }

    me.clearPhoto = function () {
        let context = me.canvas.getContext('2d');
        context.fillStyle = '#AAA';
        context.fillRect(0, 0, me.canvas.width, me.canvas.height);
    }

    me.takePicture = function (firstTime) {
        let context = me.canvas.getContext('2d');
        if (me.videoWidth && me.videoHeight) {
            me.canvas.width = me.videoWidth;
            me.canvas.height = me.videoHeight;
            context.drawImage(me.video, 0, 0, me.videoWidth, me.videoHeight);

            if (firstTime) {
                me.video.pause();
            }

            me.targetStudentPicture = me.canvas.toDataURL('image/png', 0.1);

            if (!me.CheckTimeCompareFace()) {
                return false;
            }
            if (me.compareTargetStudentPicture != me.targetStudentPicture) {
                me.compareTargetStudentPicture = me.targetStudentPicture;
            }
            else {
                me.startupVideo(videoExercise, false);
                return false;
            }

        } else {
            me.clearPhoto();
        }

        return true;
    }

    me.CheckTimeCompareFace = function () {
        let currentDate = new Date();

        if (me.dateLastPicture == null) {
            me.dateLastPicture = currentDate;
            return true;
        }

        let lastDate = new Date(me.dateLastPicture);
        let timer = me.exercisedto.CompareFaceContinuousTime / 2;
        lastDate.setSeconds(lastDate.getSeconds() + timer);

        if (currentDate >= lastDate) {
            me.dateLastPicture = currentDate;
            return true;
        }
        else {
            me.SetTimeCompareFace();
            return false;
        }
    }

    me.CompareFaceTimer = function () {
        if (me.takePicture(false)) {
            me.compareFaceStudent(false);
            me.SetTimeCompareFace();
        }
    }

    me.SetTimeCompareFace = function () {
        if (me.streaming) {
            setTimeout(function () {
                me.CompareFaceTimer();
            }, me.GetIntervalTime());
        }
    }

    me.GetIntervalTime = function () {
        let time1 = me.exercisedto.CompareFaceContinuousTime;
        let time2 = time1 / 2;
        let random = Math.random() * (time1 - time2) + time2;

        return random * 1000;
    }

    me.compareFaceStudent = function (compareNow) {
        let parameters = me.getDefaultParameters();
        parameters.TargetStudentPicture = me.targetStudentPicture;
        parameters.CompareNow = compareNow;

        me.$parent.action('compareFaceStudent', parameters, function (result) {
            if (result.Success && parameters.CompareNow) {
                $('#assmtPicCont').addClass('success');
                $('#assmtCountDown').addClass('ng-binding img imgV');
                $('#assmtCountDown').removeClass('ng-hide');
                me.timerCountDown = '';
                me.compareFaceSuccess = true;
                me.compareFaceDialog = false;
                $('#startOnlineExercise').removeClass('disabled');
            }
        });
    }

    me.confirmOnlineExerciseDialog = function () {
        me.confirmStartDialog = true;
        me.startDialog = false;
    };

    me.hideConfirmationOnlineExercise = function () {
        me.startDialog = true;
        me.confirmStartDialog = false;
    };

    me.showStartButtonAgain = function () {
        $('#limitTime' + me.allprops.LessonCategoryID).text('');
        me.exercisedto.Status = 1;
        me.exercisedto.ShowStartButton = true;
        me.showCountDown = true;
        me.lockOptions = false;
        me.showResultTab = false;
        me.resultPageIsOn = false;
        me.closeSlideIsOn = false;
        me.answering = false;
        me.skippedOrUnansweredQuestionCount = false;
        me.answeredQuestionCount = false;
        me.discardedQuestionCount = false;
        me.mustDiscardedQuestionCount = false;
        me.firstCard = 1;
        me.lastCard = me.maxCards;
        me.CurrentQuestionIndex = 0;
        me.currentQuestion = null;
        me.starting = false;
        me.countMSecondsManager = [];
        me.dateNowLT = [];
        me.compareFaceDialog = false;
        me.compareFaceSuccess = false;
        me.streaming = false;
        me.localstream = null;
        me.video = null;
        me.canvas = null;
        me.photo = null;
        me.targetStudentPicture = null;
        me.compareTargetStudentPicture = null;
        me.dateLastPicture = null;
        me.confirmStartDialog = false;
        me.startDialog = true;
        me.confirmStartDialogCam = false;
        me.capturePicture = false;
        me.capturePictureError = false;
    };

    me.confirmOnlineExerciseDialogCam = function () {
        me.confirmStartDialogCam = true;
        me.startDialog = false;
        me.startupVideo(video, true);
    };

    me.hideConfirmationOnlineExerciseCam = function () {
        me.startDialog = true;
        me.confirmStartDialogCam = false;
        $('#assmtCountDown').removeClass('ng-binding img imgV');
        $('#assmtPicCont').removeClass('success');
        $('#startOnlineExercise').addClass('disabled');
        me.timerCountDown = '';
        me.showCountDown = true;
        me.compareFaceSuccess = false;
        me.capturePictureError = false;
        me.stopCam();
    };

    me.isErrorCameraOnlineExercise = function () {
        if (me.allprops.LessonCategoryType == me.enums.LessonCategoryType.OnlineExercise) {
            return me.capturePictureError && !me.exercisedto.CompareFaceAcceptDisabledCam && me.exercisedto.CompareFaceContinuous && !me.exercisedto.AllowTurnOffCamDuringActivity;
        }
        else {
            return false;
        }
    }

    me.reloadPage = function () {
        location.reload(true);
    }

    me.denyCopyText = function () {
        $(document).ready(function () {
            $('body').bind('copy cut', function (e) {
                e.preventDefault();
            });
        });
    };

    me.changeShowGradeClassAverage = function () {
        me.showGradeClassAverage = !me.showGradeClassAverage;
    }

    me.selectQuestionDescriptors = function (descriptor) {
        me.exercisedto.QuestionList.map(x => {
            if (x.DescriptorID == descriptor.DescriptorID && me.selectQuestionWithDescriptorID != descriptor.DescriptorID) {
                x.SelectedDescriptor = true;
            }
            else {
                x.SelectedDescriptor = false;
            }
        });

        me.exercisedto.FeedbackDescriptors.map(x => {
            x.SelectedDescriptor = false;
        });

        if (me.selectQuestionWithDescriptorID != descriptor.DescriptorID) {
            me.selectQuestionWithDescriptorID = descriptor.DescriptorID;

        }
        else {
            descriptor.SelectedDescriptor = !descriptor.SelectedDescriptor;
            me.selectQuestionWithDescriptorID = 0;
        }
    }
}

function getCategoryAngularTemplate(template) {
    return getStaticUrl('/Templates/Category/CategoryContentTypes/' + template + '.html');
}
