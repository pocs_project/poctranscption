﻿ILangApp.directive('tratmeManager', function () {
    return {
        template: '<div id="tratmeManager"></div>',
        controller: function ($scope) {
            const KEY_TRATME_AREA_ABBREVIATION = 'tratme-area-abbreviation';
            const me = $scope;

            me.setAreaAbbreviation = function (abbreviation) {
                return sessionStorage.setItem(KEY_TRATME_AREA_ABBREVIATION, abbreviation);
            };

            me.getAreaAbbreviation = function () {
                return sessionStorage.getItem(KEY_TRATME_AREA_ABBREVIATION);
            };

            me.configureCategoryMenu = function (scope) {
                const categoryScope = scope.getCategoryScope();

                const getTratmeLessonSpecialties = function (list) {
                    const LessonIds = list.map(x => x.LessonID);

                    categoryScope.action('tratmeGetLessonSpecialties', { LessonIds },
                        (specialyList) => {
                            const specialtyMap = {};
                            specialyList.forEach(item => specialtyMap[item.LessonId] = item.Specialties);

                            list.forEach((item) => {
                                const specialty = specialtyMap[item.LessonID];
                                if (specialty) {
                                    item.Specialties = specialty.slice(0, 1).map(sp => sp.toLowerCase());
                                }
                            });
                        });
                };

                scope.$on('onLeftMenuLessonsLoaded', (event, { result, base }) => {
                    getTratmeLessonSpecialties(result.Result.List);
                });

                scope.bigContext.tratmeAbbreviation = me.getAreaAbbreviation();
            };

            me.configureExeciseController = function (scope) {

                const playerScope = scope.$parent;

                const shouldCalculateExercise = () => {
                    return scope.serverParameters.fixedParameters.FT_ResultMultipleChoice == true &&
                        me.getAreaAbbreviation() === 'PR' &&
                        scope.allprops.LessonCategoryType == scope.enums.LessonCategoryType.OnlineExercise &&
                        scope.exercisedto.Status == scope.enums.StudentExerciseSetDetailStatus.Result;
                };

                scope.tratmeGoToQuestionPage = (sequencialNumber) => {
                    const questionSelected = scope.exercisedto.QuestionList.filter((question) => question.SequentialNumber == sequencialNumber);
                    scope.select(questionSelected[0]);
                }

                scope.tratmeUpdateResultExerciseTo = () => {

                    if (!shouldCalculateExercise()) {
                        return;
                    }
                    const questionList = scope.exercisedto.QuestionList
                        .filter(question => question.Specialty!==null && question.Specialty!==undefined);

                    const uniqueIds = [];
                    const listSpecialty = questionList.map((question) => {
                        return question.Specialty
                    }).filter(element => {
                        const isDuplicate = uniqueIds.includes(element.SpecialtyId);

                        if (!isDuplicate) {
                            uniqueIds.push(element.SpecialtyId);

                            return true;
                        }

                        return false;
                    }).map((item) => {
                        return {
                            ...item,
                            SpecialtyName: item.SpecialtyName.toLowerCase(),
                            correctList: [],
                            incorrectList: [],
                        }
                    })
                    questionList.forEach((item) => {
                        const finded = listSpecialty.filter((itemSpec) => {
                            return itemSpec.SpecialtyId == item.Specialty.SpecialtyId
                        });
                        if (item.IsCorrect) {
                            finded[0].correctList.push(item.SequentialNumber);
                        } else {
                            finded[0].incorrectList.push(item.SequentialNumber);
                        }

                    })
                    scope.listCardSpecItens = listSpecialty.map((itemSpec) => {
                        const totalScore = (itemSpec.correctList.length / (itemSpec.correctList.length + itemSpec.incorrectList.length) * 100);
                        return {
                            ...itemSpec,
                            totalScore: Math.trunc(totalScore)
                        }
                    });

                    const uniqueIdAreas = [];
                    const listAreas = questionList.map((question) => {
                        return question.Specialty
                    }).filter(element => {
                        const isDuplicate = uniqueIdAreas.includes(element.AreaId);

                        if (!isDuplicate) {
                            uniqueIdAreas.push(element.AreaId);

                            return true;
                        }

                        return false;
                    }).map((item) => {
                        return {
                            ...item,
                            SpecialtyName: item.SpecialtyName.toLowerCase(),
                            correctList: [],
                            incorrectList: [],
                        }
                    })

                    questionList.forEach((item) => {
                        const finded = listAreas.filter((itemSpec) => {
                            return itemSpec.AreaId == item.Specialty.AreaId
                        });
                        if (item.IsCorrect) {
                            finded[0].correctList.push(item.SequentialNumber);
                        } else {
                            finded[0].incorrectList.push(item.SequentialNumber);
                        }

                    })
                    scope.listCardAreaItens = listAreas.map((itemArea) => {
                        const totalScore = (itemArea.correctList.length / (itemArea.correctList.length + itemArea.incorrectList.length) * 100);
                        return {
                            ...itemArea,
                            totalScore: Math.trunc(totalScore)
                        }
                    });
                }

                scope.tratmeUpdateResultExerciseTo();

                playerScope.$on('closedExerciseSet', scope.tratmeUpdateResultExerciseTo);

                playerScope.$on('gotExerciseSetTake', scope.tratmeUpdateResultExerciseTo);
            };

            me.calculateComsuptionPercent = function (numerator, denominator) {
                if (denominator <= 0) {
                    return 0;
                }

                var percent = ((numerator / denominator) * 100);

                if (percent > 0 && percent < 1) {
                    return Math.ceil(percent);
                }
                return Math.floor(percent);
            };
        }
    }
});