﻿var FeedHub;
$(function () {
    FeedHub = $.connection.feedHub;

    if (FeedHub == null) {
        return;
    }

    FeedHub.client.NewFeedPost = function () {
        $("body").trigger("NewFeedPost");
    };

    FeedHub.client.NewFeedComment = function (FeedId, CommentId, Comment, UserID, UserName, UserImageUrl, ElapsedTime, CreatedDateString) {
        $("body").trigger("NewFeedComment", [FeedId, CommentId, Comment, UserID, UserName, UserImageUrl, ElapsedTime, CreatedDateString]);
    };

    FeedHub.client.NewFeedPositiveFeedBack = function (FeedId, WasNegative) {
        $("body").trigger("NewFeedPositiveFeedBack", [FeedId, WasNegative]);
    };

    FeedHub.client.NewFeedNegativeFeedBack = function (FeedId) {
        $("body").trigger("NewFeedNegativeFeedBack", [FeedId]);
    };

    FeedHub.client.NewCommentPositiveFeedBack = function (FeedId, CommentId, WasNegative) {
        $("body").trigger("NewCommentPositiveFeedBack", [FeedId, CommentId, WasNegative]);
    };

    FeedHub.client.NewCommentNegativeFeedBack = function (FeedId, CommentId) {
        $("body").trigger("NewCommentNegativeFeedBack", [FeedId, CommentId]);
    };

    FeedHub.client.NewFeedView = function (userId, FeedId) {
        $("body").trigger("NewFeedView", [userId, FeedId]);
    };

    FeedHub.client.NewFeedViews = function (userId, feedIdList) {
        $("body").trigger("NewFeedViews", [userId, feedIdList]);
    };
});


$(function () {

    $("body").on("NewFeedPost", function (event) {
        AlertNewPost();
    });

    $("body").on("NewFeedView", function (event, userId, feedId) {
        ShowNewFeedView(userId, feedId);
    });

    $("body").on("NewFeedViews", function (event, userId, feedIdList) {
        ShowNewFeedViews(userId, feedIdList);
    });

    $("body").on("NewFeedComment", function (event, feedId, commentId, comment, userID, userName, userImageUrl, elapsedTime, createdDateString) {
        ShowNewComment(feedId, commentId, comment, userID, userName, userImageUrl, elapsedTime, createdDateString);
    });

    $("body").on("NewFeedPositiveFeedBack", function (event, feedId, wasNegative) {
        ShowNewPositiveFeedBack(feedId, wasNegative);
    });

    $("body").on("NewFeedNegativeFeedBack", function (event, feedId, comment) {
        ShowNewNegativeFeedBack(feedId);
    });

    $("body").on("NewCommentPositiveFeedBack", function (event, feedId, commentId, wasNegative) {
        ShowNewCommentPositiveFeedBack(feedId, commentId, wasNegative);
    });

    $("body").on("NewCommentNegativeFeedBack", function (event, feedId, commentId) {
        ShowNewCommentNegativeFeedBack(feedId, commentId);
    });

    var AlertNewPost = function () {
        $("a.topAlert").show();
    };

    var ShowNewFeedView = function (userId, feedId) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewFeedView(userId, feedId);
        else
            if (FeedListInstance.receiveNewFeedView)
                FeedListInstance.receiveNewFeedView(userId, feedId);
    };

    var ShowNewFeedViews = function (userId, feedIdList) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewFeedViews(userId, feedIdList);
        else
            if (FeedListInstance.receiveNewFeedViews)
                FeedListInstance.receiveNewFeedViews(userId, feedIdList);
    };

    var ShowNewPositiveFeedBack = function (feedId, wasNegative) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewPositiveFeedBack(feedId, wasNegative);
        else
            if (FeedListInstance.receiveNewPositiveFeedBack)
                FeedListInstance.receiveNewPositiveFeedBack(eedId, wasNegative);
    };

    var ShowNewNegativeFeedBack = function (feedId) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewNegativeFeedBack(feedId);
        else
            if (FeedListInstance.receiveNewNegativeFeedBack)
                FeedListInstance.receiveNewNegativeFeedBack(feedId);
    };

    var ShowNewComment = function (feedId, commentId, comment, userID, userName, userImageUrl, elapsedTime, createdDateString) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewComment(feedId, commentId, comment, userID, userName, userImageUrl, elapsedTime, createdDateString);
        else
            if(FeedListInstance.receiveNewComment)
                FeedListInstance.receiveNewComment(feedId, commentId, comment, userID, userName, userImageUrl, elapsedTime, createdDateString);
    };

    var ShowNewCommentPositiveFeedBack = function (feedId, commentId, wasNegative) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewCommentPositiveFeedBack(feedId, commentId, wasNegative);
        else
            if (FeedListInstance.receiveNewCommentPositiveFeedBack)
                FeedListInstance.receiveNewCommentPositiveFeedBack(feedId, commentId, wasNegative);
    };

    var ShowNewCommentNegativeFeedBack = function (feedId, commentId) {
        if (getAngularScope('feedListCtrl'))
            getAngularScope('feedListCtrl').receiveNewCommentNegativeFeedBack(feedId, commentId);
        else
            if (FeedListInstance.receiveNewCommentNegativeFeedBack)
                FeedListInstance.receiveNewCommentNegativeFeedBack(feedId, commentId);
    };
});
