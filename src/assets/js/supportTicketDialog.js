﻿document.domain = ILangSettings.JavaScriptDomainName;

ILangApp.controller('supportTicketDialogController', ['$scope', '$http', function ($scope, $http) {
    baseController($scope, $http, {
    });
    $scope.ticket = {
        id: 0,
        detailUrl: '',
        category: {
            value: null,
            valid: true
        },
        title: {
            value: '',
            valid: true
        },
        description: {
            valid: true
        }
    };
    $scope.saving = false;
    $scope.scriptsLoaded = false;
    $scope.optionsLoaded = false;
    $scope.encryptedUserOrganizationID = null;
    $scope.stages = { Create: 0, Feedback: 1 };
    $scope.moment = $scope.stages.Create;
    $scope.categoryList = [];
    $scope.showTinyMCE = function () {
        InitializeTinyMCEStudentMode('txtDesc');
    };
    $scope.loadCategoryList = function () {
        if (this.categoryList.length > 0) {
            return;
        }

        $scope.action('listSupportCategory', { organizationId: ILangSettings.OrganizationID, userOrganizationId: ILangSettings.UserOrganizationID }, function (result) {
            angular.forEach(result.Result, function (item) {
                $scope.categoryList.push(item);
            });

            $scope.optionsLoaded = true;
        });
    };
    $scope.openDialog = function (encryptedUserOrganizationID) {

        if (typeof (encryptedUserOrganizationID) != 'undefined') {
            $scope.encryptedUserOrganizationID = encryptedUserOrganizationID;
        }

        this.loadCategoryList();
        this.df__toggleVisibility(true, true);
    };
    $scope.saveTicket = function () {

        if (!$scope.canSubmit()) {
            return;
        }

        var supportUrl = '';

        if (document.location.pathname.indexOf("error500.aspx") > -1) {
            supportUrl = document.referrer;
        }
        else {
            supportUrl = document.URL;
        }

        $scope.saving = true;

        $scope.action('createSupportTicket', {
            title: $scope.ticket.title.value.trim(),
            description: $scope.getTinyMCEValue(),
            category: $scope.ticket.category.value,
            cookies: document.cookie,
            fullStoryURL: (typeof (FS) != 'undefined' ? FS.getCurrentSessionURL() : ''),
            supportUrl: supportUrl,
            encryptedUserOrganizationID: $scope.encryptedUserOrganizationID
        }, function (result) {

            if (result.Success) {
                $scope.ticket.id = result.Result.id;
                $scope.ticket.detailUrl = result.Result.name;
                $scope.moment = $scope.stages.Feedback;
            } else {
                alert('Erro ao criar chamado de suporte');
            }

            $scope.saving = false;
        });
    };
    $scope.canSubmit = function () {
        $scope.validateCategory();
        $scope.validateTitle();
        $scope.validateDescription();

        return $scope.ticket.category.valid == true
        && $scope.ticket.title.valid == true
        && $scope.ticket.description.valid == true;
    };
    $scope.closeAndResetDialog = function () {
        $scope.ticket.id = 0;

        $scope.ticket.detailUrl = '';

        $scope.ticket.category.value = null;
        $scope.ticket.category.valid = true;

        $scope.ticket.title.value = '';
        $scope.ticket.title.valid = true;

        $scope.ticket.description.value = '';
        $scope.ticket.description.valid = true;

        $scope.moment = $scope.stages.Create;

        $scope.df__toggleVisibility(false);

        $scope.encryptedUserOrganizationID = null;

        try {
            tinymce.editors['txtDesc'].setContent('');
            tinymce.EditorManager.execCommand('mceRemoveEditor', true, 'txtDesc');
        } catch (e) {

        }
    }
    $scope.validateCategory = function () {
        $scope.ticket.category.valid = $scope.ticket.category.value != null;
    }
    $scope.validateTitle = function () {
        $scope.ticket.title.valid = $scope.ticket.title.value.trim().length > 0;
    }
    $scope.validateDescription = function () {
        $scope.ticket.description.valid = $scope.getTinyMCEValue().length > 0;

        if ($scope.ticket.description.valid) {
            $('.mce-tinymce').removeClass('req');
            $('#txtDesc').removeClass('req');
        } else {
            $('.mce-tinymce').addClass('req');
            $('#txtDesc').addClass('req');
        }
    };
    $scope.getTinyMCEValue = function () {
        if (typeof (tinymce.editors['txtDesc']) == 'undefined') {
            return '';
        } else {
            return tinymce.editors['txtDesc'].getContent({ format: 'html' }).trim();
        }
    }
}]);