﻿class ContentPlayerService {
    constructor() {
        this._baseUrl = ILangSettings.Sites.StudentAPI();
        this._httpClient = new HttpClient();
    }

    async getClassDisciplineProgress(parameters) {
        const endpoint = '/class-discipline-student/progress';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.get(uri, parameters);
        return response.json();
    }

    async getLeftMenu(parameters) {
        const endpoint = '/Lesson/GetLeftMenuContentPlayer';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.get(uri, parameters);
        return response.json();
    }

    async getHasTracking(parameters) {
        const endpoint = '/Lesson/HasTracking';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.get(uri, parameters);
        return response.json();
    }

    async getNextLesson(parameters) {
        const endpoint = '/StudentCategory/GetNextLessonCategory';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.get(uri, parameters);
        return response.json();
    }

    async postAssignmentPost(parameters) {
        const endpoint = '/assignment-post';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.post(uri, parameters, 'application/x-www-form-urlencoded', false);
        return response.json();
    }
}