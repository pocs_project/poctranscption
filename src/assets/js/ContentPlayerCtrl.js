

ILangApp.controller('ContentPlayerCtrl', ($http, $scope, $timeout, $location, $anchorScroll, $element) => {
    $element[0].className += ' dc';
    baseController($scope, $http, {
        infiniteScrolling: false,
        rewriteHash: false,
        trackPageIndex: false,
        onBeforeLoad: (base) => {
            base.bigContext.hasMorePages = true;
            me.getExternalDirectives()?.categoryTopItemsScope?.doAfterContentPlayerLoaded(me.serverParameters.parameters.ViewTeacherMessage);
        },
        onAfterLoad: (result) => {
            if (result.Success) {

                me.loadFooterData(result);
                me.ActivityGroupID = result.LessonCategoryList[0].Detail.ActivityGroupID;

                let lessonCategoryResult = result.LessonCategoryList[0];
                me.showCommentsForLessonCategory = false;
                me.showEmojisForLessonCategory = false;
                me.closeFooter();

                if (lessonCategoryResult && lessonCategoryResult.LessonCategoryType !== me.serverParameters.enums.LessonCategoryType.Forum) {
                    me.action('getFeedListFromLessonCategories', { LessonCategoryIdList: lessonCategoryResult.LessonCategoryID }, (actionResult) => {
                        $(result.LessonCategoryList).each((lessonCategoryIndex, lessonCategory) => {
                            if (lessonCategory.Detail.ShowStudentLessonCategoryPost) {
                                me._showCommentsForLessonCategory(lessonCategory);
                            }

                            $(actionResult.Result).each((actionResultIndex, feed) => {

                                if (lessonCategory.LessonCategoryID === feed.ParentID) {
                                    lessonCategory.Feed = feed;
                                    me.feed = feed;
                                    me.mode = lessonCategory.Mode;
                                    me._showCommentsForLessonCategory(lessonCategory);
                                    me._showEmojisForLessonCategory(lessonCategory.LessonCategoryType);
                                }

                            });
                        });
                    });
                }

                me._studentIsSuspendedOnDiscipline = compareIfSafe(me, 'bigContext.dto.StudentClassDisciplineStatus', 5);
                me._studentImageUrl = result.StudentImageUrl;
            }
        },
        onLoadError: (base, result, httpStatus) => {
            let msg = getPropertyByName(result  , 'ErrorMessage');
            if (msg === 'Tipo de questão não suportado pelo ULife') {
                window.location.href = ILangSettings.Sites.Student() + '/Home/Index';
                return;
            }
            let type = getPropertyByName(result, 'ErrorType')
            if (type !== null && type !== base.serverParameters.enums.ServiceResultErrorType.Application) {
                throw type;
            }

            me.currentContext.dto.ClassDisciplineID = getPropertyByNameOrDefault(result, 'ClassDisciplineID', 0);
            me.currentContext.dto.DisciplineContentID = getPropertyByNameOrDefault(result, 'DisciplineContentID', 0);
            me.currentContext.dto.LessonID = getPropertyByNameOrDefault(result, 'LessonID', 0);
            me.currentContext.dto.OrganizationID = getPropertyByNameOrDefault(result, 'OrganizationID', 0);

            showTopErrorMsg(msg, 30000, null, false);

            me.busy = false;
            me._loadingCategoryList = false;
            me.sendMessageWhenContentLoaded();
        }
    }, { location: $location, element: $element });

    let me = $scope;
    me.mode = null;
    me.menuScope = null;
    me.feed = null;
    me.mode = null;
    me.showCommentsForLessonCategory = false;
    me.showEmojusForLessonCategory = false;
    me.contentPlayerService = new ContentPlayerService();
    me.categoryType = { Page: 0, Exercise: 1, Openended: 2 };
    me._currentLessonCategory = null;
    me._loadingCategoryList = true;
    me._studentIsSuspendedOnDiscipline = null;
    me._studentImageUrl = null;
    me._externalDirectivesScopes = { categoryTopItemsScope: null, tratme: null };
    me._flagLoadLessonCategoryTracking = true;
    me._showLoaderMessage = false;
    me._loaderMessage = null;

    me.showModalDocumentationCollection = false;

    me.warningDocumentation05percent = false;
    me.warningDocumentation10percent = false;
    me.warningDocumentation15percent = false;
    me.warningDocumentation20percent = false;
    me.warningDocumentation25percent = false;


    me._notCommentAllowedByOrganizations = function () {
        const organization = [6287]; //inspirali 6287
        return organization.some((organizationID) => organizationID === ILangSettings.OrganizationID);
    };

    me._showCommentsForLessonCategory = function (lessonCategory) {

        let lessonCategoryType = lessonCategory.LessonCategoryType;

        if (me._notCommentAllowedByOrganizations()) {
            me.showCommentsForLessonCategory = false;
            return;
        }

        me.showCommentsForLessonCategory =
            lessonCategoryType === 11 || //Exercise
            lessonCategoryType === 15 || //OpenEndedQuestion
            (lessonCategoryType === 20 && lessonCategory.Detail.ActivityGroupID > 0) || //ActivityGroupOpenEndedQuestion and Group exists
            (lessonCategoryType === 25 && lessonCategory.Detail.Status === 4) || //OnlineExercise and StudentExerciseSetDetailStatus 4 (Result)
            lessonCategoryType === 27; //LTI com retorno de notas
    };

    me._showEmojisForLessonCategory = function (lessonCategoryType) {
        me.showEmojisForLessonCategory =
            lessonCategoryType === 1 || //Link
            lessonCategoryType === 9 || //Page
            lessonCategoryType === 24; //InteractiveHtml
    }

    me.initTinymce = () => {
        if ($('.tinymceElement').length === 0) {
            return;
        }

        $('.tinymceElement').each((_, item) => {
            try {
                let id = $(item).attr('id');
                InitializeTinyMCEStudentSimpleMode(id, true);

                let txt = $(item).attr('text');
                if (txt != null && txt != 'null' && txt != '')
                    me.setEditorContent(id, txt);

                $(item).removeAttr('text');
            } catch (e) { console.log(e.message); }
        });
    };

    me.getExternalDirectives = () => {

        if (me._externalDirectivesScopes.categoryTopItemsScope === null) {
            me._externalDirectivesScopes.categoryTopItemsScope = getAngularScopeByElementId('categoryTopItems') || null;
        }

        if (me._externalDirectivesScopes.tratme === null) {
            me._externalDirectivesScopes.tratme = getAngularScopeByElementId('tratmeManager') || null;
        }


        return me._externalDirectivesScopes;
    }

    me.sendMessageWhenContentLoaded = () => {
        me.$emit('categoryLoaded', { origin: 'player', hasLoadContent: true });
    };

    me.cleanAllEditors = () => {
        $(tinymce.editors).each((_, item) => {
            if (item.id !== 'txtTitle') {
                item.remove();
            }
        });
    };

    me.getEditorById = (id) => {
        let k = $(tinymce.editors).filter((_, item) => item.id === id);
        if (k !== null) {
            return k[0];
        } else {
            return null;
        }
    };

    me.getEditorContent = (id, format) => {
        let _format = ILang.Util.isValidObject(format) ? format : 'html';

        let editor = me.getEditorById(id);
        if (editor !== null) {
            let html = editor.getContent({ format: _format });
            html = html.replace(new RegExp('<span class="AMedit">`', 'g'), '<span class="AM">`');
            return html;
        } else {
            return '';
        }
    };

    me.setEditorContent = (id, content) => {
        let editor = me.getEditorById(id);
        if (editor !== null) {
            editor.setContent(content);
        }
    };

    me.onload = (last) => {
        me.basicParameters = {
            ClassDisciplineID: me.bigContext.dto.ClassDisciplineID,
            LessonID: me.bigContext.dto.LessonID
        };
        me.mode = me.serverParameters.actions.load.data.Mode;

        if (me._flagLoadLessonCategoryTracking && !me.bigContext.dto.LessonCategoryList[0].StudentTrackID) {
            me.registerViewedLessonCategory(me.bigContext.dto.LessonCategoryList[0].LessonCategoryID);
            me._flagLoadLessonCategoryTracking = false;
        }

        if (last) {
            me._loadingCategoryList = false;
            me.sendMessageWhenContentLoaded();
        }

        me.checkCourseRequiresStudentRegistration();
        me.checkCourseIsBlocked();
    };

    me.checkCourseRequiresStudentRegistration = function () {
        if (isSafeToUse(me, 'serverParameters.parameters.requiresStudentRegistration')
            && isSafeToUse(me, 'serverParameters.parameters.OrganizationCourseID')) {
            me.requiresStudentRegistration = me.serverParameters.parameters.requiresStudentRegistration;

            if (me.requiresStudentRegistration) {
                let param = {};
                param.organizationcourseid = me.serverParameters.parameters.OrganizationCourseID;
                $scope.action('checkCourseRequiresStudentRegistration', param, function (result) {
                    if (result.Success) {
                        if (result.Result) {
                            let url = ILangSettings.Sites.Student() + '/Registration';
                            window.location = url;
                        }
                    }
                }, null, { showLoading: false });
                me.courseConsumptionPercentageModal();
                return false;
            }
        }
    }

    me.checkCourseIsBlocked = function () {
        if (isSafeToUse(me, 'serverParameters.parameters.courseIsBlocked')) {
            me.courseIsBlocked = me.serverParameters.parameters.courseIsBlocked;

            if (me.courseIsBlocked) {
                window.location = ILangSettings.Sites.Student();
            }
        }
    }

    $scope.closeAckStudentCanSendDocsDlg = function () {
        me.updateAcknowledgement();
        window.location.href = me.serverParameters.parameters.redirectUrl;
    }

    $scope.sendDocs = function () {
        me.updateAcknowledgement();
        window.location.href = ILangSettings.Sites.ILang() + "/Community/UserDocuments/UserDocuments.aspx";
    }

    me.updateAcknowledgement = function () {
        let actionBody = {
            Percentage: 0,
            StudentClassDisciplineID: 0
        };
        if (me.warningDocumentation05percent) {
            actionBody.Percentage = 5;
        } else if (me.warningDocumentation10percent) {
            actionBody.Percentage = 10;
        } else if (me.warningDocumentation15percent) {
            actionBody.Percentage = 15;
        } else if (me.warningDocumentation20percent) {
            actionBody.Percentage = 20;
        } else if (me.warningDocumentation25percent) {
            actionBody.Percentage = 25;
        } else {
            return;
        }

        actionBody.StudentClassDisciplineID = me.serverParameters.parameters.StudentClassDisciplineID;
        me.action("updateAcknowledgementConsumption", actionBody, function (result) {
            if (result.Success) {
                if (me.warningDocumentation25percent) {
                    me.showModalDocumentationCollection = true;
                } else {
                    me.showModalDocumentationCollection = false;
                }
            } else {
                showTopErrorMsg(UpdateAcknowledgementConsumptionError, me.CloseAfter);
                me.showModalDocumentationCollection = false;
            }
        });
    }

    me.courseConsumptionPercentageModal = function () {
        let param = {};
        param.studentClassDisciplineID = me.serverParameters.parameters.StudentClassDisciplineID;
        if ($scope.serverParameters.parameters.userAbleToSendDocs) {
            $scope.action('courseConsumptionPercentage', param, function (result) {
                if (parseInt(result.Result) === 5) {
                    me.warningDocumentation05percent = true;
                    me.showModalDocumentationCollection = true;
                } else if (parseInt(result.Result) === 10) {
                    me.warningDocumentation10percent = true;
                    me.showModalDocumentationCollection = true;
                } else if (parseInt(result.Result) === 15) {
                    me.warningDocumentation15percent = true;
                    me.showModalDocumentationCollection = true;
                } else if (parseInt(result.Result) === 20) {
                    me.warningDocumentation20percent = true;
                    me.showModalDocumentationCollection = true;
                } else if (parseInt(result.Result) === 25) {
                    me.warningDocumentation25percent = true;
                    me.showModalDocumentationCollection = true;
                }
            });
        } else {
            return;
        }
    }

    me._removeTinyMceEditors = () => {
        angular.forEach(tinymce.editors, (value) => {
            try {
                tinymce.editors[value.id].remove();
            } catch (e) {
                console.log(e);
            }
        });
    };

    me._stopLoadingAnimate = () => {
        $('html, body').stop().animate({ scrollTop: 0 }, 500, 'swing');
    };

    me._mapSearchLessonCategoryDto = (lessonCategory) => {
        return {
            ClassDisciplineID: getPropertyByNameOrDefault(me, 'basicParameters.ClassDisciplineID', me.currentContext.dto.ClassDisciplineID),
            DisciplineContentID: getPropertyByNameOrDefault(me, 'basicParameters.DisciplineContentID', me.currentContext.dto.DisciplineContentID),
            LessonCategoryID: lessonCategory.LessonCategoryID,
            LessonID: lessonCategory.LessonID,
            DisciplineID: lessonCategory.DisciplineID
        };
    };

    me._setCurrentLessonCategory = (lessonCategory) => {
        me._currentLessonCategory = lessonCategory;
    };

    me.getCurrentLessonCategory = () => {
        return me._currentLessonCategory;
    };

    me.loadLessonCategory = (lessonCategory) => {
        try {
            me._loadingCategoryList = true;

            if (!ILang.Util.isValidObject(lessonCategory))
                throw new Error('LessonCategory inválido.');
            
            let category = getPropertyByNameOrDefault(me, 'currentContext.dto.LessonCategoryList', null);
            if (category != null) {
                category = category[0];
                me.SetHasTrackingLitGrade(category);
            }
            

            me._removeTinyMceEditors();

            let lessonCategoryDto = me._mapSearchLessonCategoryDto(lessonCategory);

            me._setCurrentLessonCategory(lessonCategoryDto);

            me.showCommentsForLessonCategory = false;
            me.showEmojisForLessonCategory = false;

            me.bigSearch(lessonCategoryDto, () => {
                me._stopLoadingAnimate();
                hideTopErrorMsg();
                if (!lessonCategory.HasTracking)
                    me.registerViewedLessonCategory(lessonCategory.LessonCategoryID);
            });
        } catch (e) {
            let lessonCategoryName = getPropertyByNameOrDefault(category, 'LessonCategoryName', null);
            if (lessonCategoryName != null) {
                let message = 'Erro ao carregar a atividade: {0} - {1}'.format(lessonCategoryName, e.message);
                showTopErrorMsg(message, 30000, null, false);
            }
            me._loadingCategoryList = false;
        }
    };

    me.loadFooterData = (lessonCategory) => {
        let parameters = {
            LessonID: lessonCategory.LessonID,
            LessonCategoryID: lessonCategory.LessonCategoryList[0].LessonCategoryID,
            ClassDisciplineID: lessonCategory.ClassDisciplineID
        };

        let errorMessage = 'Erro ao buscar dados da próxima atividade.';
        let timeError = 5000;

        me.contentPlayerService.getNextLesson(parameters).then(response => {
            if (response.Success) {
                me.nextLesson = {
                    LessonCategoryID: response.Result.NextLessonCategoryID,
                    LessonID: response.Result.NextLessonID,
                    HasNextLesson: response.Result.HasNextLessonCategory
                };
            }
            else {
                showTopErrorMsg(errorMessage, timeError, null, false);
                console.log(response);
            }
        }).catch(error => {
            showTopErrorMsg(errorMessage, timeError, null, false);
            console.log(error);
        });
    };

    me.getMenuScope = () => {
        if (!me.menuScope)
            me.menuScope = getAngularScopeByElementId('studentmenu');

        return me.menuScope;
    };

    me.getFeedScope = function () {
        if (!me.feedScope)
            me.feedScope = getAngularScopeByElementId('studentFeed');

        return me.feedScope;
    };

    me.getAssignmentPost = (lessonCategoryID) => {
        if (me.serverParameters.parameters.showStudentLessonCategory) {
            let sc = getAngularScopeByElementId('ap_' + lessonCategoryID);
            if (isSafeToUse(sc, 'assignmentPostInstance.loadPosts'))
                sc.assignmentPostInstance.loadPosts();
        }

    };

    me.registerViewedLessonCategory = (lessonCategoryID, lessonCategoryPageID) => {
        let category = me.bigContext.dto.LessonCategoryList.first({ LessonCategoryID: parseInt(lessonCategoryID) });
        if (category === null)
            return;

        let parameters = me.basicParameters;
        parameters.StudentLessonID = category.StudentLessonID;
        parameters.LessonCategoryID = lessonCategoryID;
        parameters.StudentTrackID = category.StudentTrackID;

        if (isSafeToUse(category, 'ActivityGroupID'))
            parameters.ActivityGroupID = category.ActivityGroupID;

        if (isSafeToUse(category, 'Detail.ForumGroupID'))
            parameters.ForumGroupID = category.Detail.ForumGroupID;

        if (category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.Page) {
            if (lessonCategoryPageID !== null) {
                parameters.LessonCategoryPageID = lessonCategoryPageID;
            } else {
                let c = category.Detail.PageList.first({ Active: true });
                if (c !== null)
                    parameters.LessonCategoryPageID = c.LessonCategoryPageID;
            }
        }

        me.action('checkViewed', parameters, (result) => {
            if (me._lessonCategoryShouldRefreshMenuOnViewed(category)) {
                me.checkLessonOnLeftMenu(category);
            }
        });

        if (me.serverParameters.parameters.showStudentLessonCategory) {
            me.getAssignmentPost(lessonCategoryID);
        }

        if ((category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.LtiGradeResource)
            && (me.currentContext.dto.LessonCategoryList[0].StudentGrade)) {
            me.checkLessonOnLeftMenu(category);
        }
    };

    me._lessonCategoryShouldRefreshMenuOnViewed = (category) => {
        return category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.LtiResource ||
            category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.LessonStructure ||
            category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.InteractiveHtml ||
            (category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.Page && compareIfSafe(category, 'Detail.PageList.length', 1));
    };

    me.pushOnGoogleAnalytics = (childrenCtrl) => {
        if (!isSafeToUse(me, 'root'))
            me.root = getRootScope($scope);

        let sttid = isSafeToUse(childrenCtrl, 'Detail.StudentTrackID') ? childrenCtrl.Detail.StudentTrackID : childrenCtrl.StudentTrackID;

        if (isSafeToUse(ILang, 'Util.GoogleAnalytis')) {
            ILang.Util.GoogleAnalytis.track(
                sttid,
                me.bigContext.dto.OrganizationID,
                me.bigContext.dto.OrganizationName,
                me.bigContext.dto.DisciplineName,
                childrenCtrl.LessonCategoryName);
        }
    };

    me.SetHasTrackingLitGrade = function (category) {
        try {
            if ((category.LessonCategoryType === me.serverParameters.enums.LessonCategoryType.LtiGradeResource)
                && (!me.currentContext.dto.LessonCategoryList[0].StudentGrade)) {

                let parametersHasTracking = { ClassDisciplineId: me.currentContext.dto.ClassDisciplineID, LessonCategoryId: category.LessonCategoryID };
                me.contentPlayerService.getHasTracking(parametersHasTracking).then(result => {
                    if (result.Result.HasTracking)
                        me.checkLessonOnLeftMenu(category);
                }).catch(e => {
                    console.log(e);
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    me.checkLessonOnLeftMenu = (category) => {
        try {
            me.getMenuScope();
            if (me.menuScope != null) {
                me.menuScope.checkLesson(category.LessonCategoryID);
            }
        } catch (e) {
            console.log(e);
        }
    };

    me.searchFeeds = (hashTag) => {
        let encodedCurrentUrl = encodeURIComponent(window.location.href);

        let destinationUrl = ILangSettings.Sites.Social().replace(/\/$/, '') + '/' + '#hashTxt=' + hashTag.Name + '&' + '#hashId=' + hashTag.EntityID + '&categoryReturnUrl=' + encodedCurrentUrl;

        window.location.href = destinationUrl;
    };

    me.studentIsSuspendedOnDiscipline = () => {
        if (me._studentIsSuspendedOnDiscipline == null)
            me._studentIsSuspendedOnDiscipline = compareIfSafe(me, 'bigContext.dto.StudentClassDisciplineStatus', 5);

        return me._studentIsSuspendedOnDiscipline;
    };

    me.studentImageUrl = () => {
        return me._studentImageUrl;
    };

    window.getStudentNavigationContent = async (contentKey) => {
        let FetchUrl = `${ILangSettings.Sites.StudentAPI()}/Lesson/GetStudentNavigationContent?ContentKey=${contentKey}`;

        let result = {};
        if (!contentKey) {
            result.Success = false;
            result.ErrorMessage = 'ContentKey was not defined'
            return result;
        }

        result = await fetch(FetchUrl, { credentials: 'include' })
        return  result.json()
    }

    window.updateStudentNavigationContent = async (contentKey, jsonContent) => {
        let FetchUrl = `${ILangSettings.Sites.StudentAPI()}/Lesson/UpdateStudentNavigationContent`;

        let result = {};
        if (!contentKey || !jsonContent) {
            result.Success = false;
            result.ErrorMessage = 'One or more values was not defined'
            return result;
        }

        result = await fetch(FetchUrl, {
            credentials: 'include',
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: new URLSearchParams({
                'ContentKey': contentKey,
                'JsonNavigation': jsonContent
            })
        });

        return result.json();
    }

    me.goToNextLesson = (nextLesson) => {
        me.getMenuScope();
        me.closeFooter();
        me.showCommentsForLessonCategory = false;
        me.showEmojisForLessonCategory = false;
        me.menuScope.goToNextLesson(nextLesson);
    }

    me.isMobile = () => {
        return ILang.Util.isMobile();
    }

    me.footerOpenned = false;
    me.changeFooterState = function () {
        me.footerOpenned ?
            $scope.closeFooter() :
            $scope.openFooter();
    };

    me.openFooter = function () {
        me.footerOpenned = true;
        let commentsButton = $('#commentsbutton');
        let footer = $('.classFooter');
        let assignemtPost = $('#divAssignmentPost');

        commentsButton.addClass('active');
        commentsButton.attr('aria-label', "Ocultar comentários");
        footer.addClass('open');
        assignemtPost.show();
    };

    me.closeFooter = function () {
        $scope.footerOpenned = false;
        let commentsButton = $('#commentsbutton');
        let footer = $('.classFooter');
        let assignemtPost = $('#divAssignmentPost');

        commentsButton.removeClass('active');
        commentsButton.attr('aria-label', "Comentários");
        footer.removeClass('open');
        assignemtPost.hide();
    };
});


