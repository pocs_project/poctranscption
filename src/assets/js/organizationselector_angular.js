﻿ILangApp.controller('organizationSelectorCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
    baseController($scope, $http, { 
        onLoadSuccess: function (result, base) {
        },
        onLoadFail: function (result, base) {
        },
        onLoadError: function (base) {
            $scope.HasError = true;
        }
    });

    $scope.organizationClick = function (selectedOrg, dropDownEl) {

        var orgId = dropDownEl.selectedId; 
        var orgName = dropDownEl.selectedText;
        if (orgName == "Todos")
            return $scope.organizationClick_SelectAll(selectedOrg, dropDownEl);

        $scope.writeCookie('orgSelector_name', encodeURIComponent(orgName));
        $scope.writeCookie('orgSelector_id', orgId);
        $scope.writeCookie('oslorg', orgId + '_' + encodeURIComponent(orgName));

        window.location.href = '/Home/ChangeUserOrganization/' + orgId;
    };
    $scope.organizationClick_Web = function (selectedOrg, dropDownEl) {

        var orgId = dropDownEl.selectedId;
        var orgName = dropDownEl.selectedText;
        if (orgName == "Todos")
            return $scope.organizationClick_SelectAll(selectedOrg, dropDownEl);

        $scope.writeCookie('orgSelector_name', encodeURIComponent(orgName));
        $scope.writeCookie('orgSelector_id', orgId);
        $scope.writeCookie('oslorg', orgId + '_' + encodeURIComponent(orgName));

        window.location.href = ILangSettings.Sites.AdminAPI() + '/Home/ChangeUserOrganization/' + orgId;
    };
    $scope.organizationClick_WWW = function (selectedOrg, dropDownEl) {
        var orgId = dropDownEl.selectedId;
        var orgName = dropDownEl.selectedText;
        if (orgName == "Todos")
            return $scope.organizationClick_SelectAll(selectedOrg, dropDownEl);

        $scope.writeCookie('orgSelector_name', encodeURIComponent(orgName));
        $scope.writeCookie('orgSelector_id', orgId);
        $scope.writeCookie('oslorg', orgId + '_' + encodeURIComponent(orgName));

        var setOrg = $scope.getItemByContextID('setOrg');
        var setAll = 0;
        window.location.reload();
    }
    $scope.organizationClick_SelectAll = function (selectedOrg, dropDownEl) {
        var orgId = dropDownEl.selectedId;
        var orgName = dropDownEl.selectedText;

        $scope.writeCookie('orgSelector_name', encodeURIComponent(orgName));
        $scope.writeCookie('orgSelector_id', orgId);
        $scope.writeCookie('oslorg', orgId + '_' + encodeURIComponent(orgName));

        //$scope.writeCookie('orgSelector_name', "All");
        //$scope.writeCookie('orgSelector_id', "");
        //$scope.writeCookie('oslorg', '_All');

        window.location.reload();
    }
    $scope.getItemByContextID = function (id) {
        return document.getElementById('OrganizationSelector.' + id);
    }
    $rootScope.$on("document_click", function () {
        $scope.serverParameters.drops['organization'].active = false;
    });
    
    $scope.writeCookie = function (name, value) {
        document.cookie = name + "=" + value + "; path=/ ; domain=" + ILangSettings.JavaScriptDomainName;
    };
}]);
