﻿$.extend(ILang.Util, {
    isValidObject: function (objToValidate) {
        return objToValidate !== undefined && objToValidate !== null;
    }
});

const goToUrl = function (url, evt) {
    if (evt.ctrlKey) {
        window.open(url, '_blank').focus();
    } else {
        window.location.href = url;
    }
}