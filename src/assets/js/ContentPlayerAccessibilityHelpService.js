﻿class ContentPlayerAccessibilityHelpService {
    constructor() {
        this._baseUrl = ILangSettings.Sites.StudentAPI();
        this._httpClient = new HttpClient();
    }

    async getAccessibilityHelpIsEnable() {
        const endpoint = '/accessibility/content-player/student-first-access';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.get(uri);
        return response.json();
    }

    async setAccessibilityHelpViewed() {
        const endpoint = '/accessibility/content-player/student-first-access';
        const uri = `${this._baseUrl}${endpoint}`;

        const response = await this._httpClient.post(uri);
        return response.json();
    }
}