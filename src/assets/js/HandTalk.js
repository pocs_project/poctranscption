﻿

const pageDoNotRunAccessibility = () => {
    return $('body[denny-accessibility]').length > 0
}

const tryAddToBody = () => {
    if (document.body && document.readyState === 'complete') {
        if (pageDoNotRunAccessibility())
            return;

        let htScript = document.createElement("script");
        htScript.src = "https://plugin.handtalk.me/web/latest/handtalk.min.js";

        // Instancia classe do Hand Talk Plugin após o download do script
        htScript.onload = function () {

            window.ht = new HT({
                token: "49a25d89233e2aba31dba8fc9994fdcd",
                exceptions: ['.exActions', '.answerItemEmpty', '.ilVTCover', '.actCardNav', '.mcIconLinksCont', '.tmceTextarea', '.hlp-dialog-header', '.hlp-content', '.expandButtonCont', '.sdClose', '.iconCollapseAll', '.iconExpandAll', '.topOrg', '.uOrgControl', '.pic', 'img', 'em.userName', '.ulIcon', '.filterblock ul', '.mcDisImg'],
                mobileEnabled: ILang.Util.isMobile(),
                zIndex: 1000002,
                maxTextSize: 1000,
                avatar: "MAYA",
                side: 'right',
                align:'bottom'
            });

            window.ht.on('activated', () => {
                window.ht.status = 'activated'
            });
            window.ht.on('deactivated', () => {
                window.ht.status = 'deactivated'
            })

        }


        // Adiciona o script na página
        document.body.appendChild(htScript);
    } else {
        window.setTimeout(tryAddToBody, 100);
    }
}


tryAddToBody();



window.applyAccessibility = function (iframe) {
    if (isSafeToUse(window, 'ht.addListenersToIframe') && compareIfSafe(window,'ht.status','activated')) {
        window.setTimeout(() => {
            window.ht.addListenersToIframe(iframe);
        }, 100)
    }
}