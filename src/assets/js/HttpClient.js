﻿class HttpClient {
    constructor() {
        const instance = this.constructor.instance;
        if (instance) {
            return instance;
        }

        this.constructor.instance = this;
    }

    async get(url, params) {
        let options = this.createFetchOptionsGet();

        let completeUrl = url;
        if (params !== undefined) {
            const searchParams = new URLSearchParams(params);
            completeUrl = `${url}?${searchParams}`;
        }

        return fetch(completeUrl, options);
    }

    async post(url, body, contentType, sendAsJson) {
        let options = this.createFetchOptionsPost(body, contentType, sendAsJson);

        return fetch(url, options);
    }

    createFetchOptionsGet() {
        let options = {
            credentials: "include",
            headers: {
                'Origin': ILangSettings.JavaScriptDomainName,
            },
            method: 'GET'
        };            

        return options;
    }

    createFetchOptionsPost(body, contentType, sendAsJson) {
        let options = {
            credentials: "include",
            headers: {
                'Origin': ILangSettings.JavaScriptDomainName,
            },
            method: 'POST'
        };

        if (contentType !== undefined) {
            options.headers['Content-Type'] = contentType;
        }

        if (body !== undefined) {

            if (sendAsJson) {
                options.body = JSON.stringify(body);
            } else {
                options.body = new URLSearchParams(body);
            }
        }

        return options;
    }
}