﻿function registerTinymceFunctions(me) {
    me.getEditorById = function (id) {
        let k = $(tinymce.editors).filter(function (index, item) { return item.id == id });
        if (k != null) {
            return k[0];
        } else {
            return null;
        }
    };
    me.getEditorContent = function (id, format, countTries) {
        try {

            let _format = (typeof (format) == 'undefined') ? 'html' : format;

            let editor = me.getEditorById(id);
            if (editor != null) {
                let html = editor.getContent({ format: _format });
                html = html.replace(new RegExp('<span class="AMedit">`', 'g'), '<span class="AM">`');
                return html;
            } else {
                let elemt = $('#' + id);
                if (elemt != null && elemt.length > 0)
                    return elemt.val();

                return '';
            }
        } catch (e) {
            if (countTries == null) {
                countTries = 0;
            } else {
                countTries += 1;
            }

            if (countTries <= 10) {
                return me.getEditorContent(id, format, countTries)
            } else {
                throw {
                    message: 'Erro detectado por versão desatualizada de navegador ({0}). Tente atualizá-lo ou utilizar um outro.',
                    custom: true,
                    showRefreshBto: true
                };
            }
        }
    };
    me.setEditorContent = (id, content) => {
        try {
            let editor = me.getEditorById(id);
            if (editor != null) {
                editor.setContent(content);
            } else {
                let elemt = $('#' + id);
                if (elemt != null && elemt.length > 0)
                    elemt.val(content);
            }
        } catch (e) {
            throw {
                message: 'Erro detectado por versão desatualizada de navegador ({0}). Tente atualizá-lo ou utilizar um outro.',
                custom: true,
                showRefreshBto: true
            };
        }

    };
    me.checkIsMobile = function (w) {
        try {
            return typeof (w.orientation) != 'undefined';
        } catch (e) {
            return false;
        }

    };
    me.showStudentTestResult = function (item) {
        return me.allprops.Mode == me.enums.StudentCategoryViewMode.Student
            && me.exercisedto.Status == me.enums.StudentExerciseSetDetailStatus.Result
            && me.exercisedto.ResultSubmissionTypeID == '1'
            && !me.exercisedto.EnableResultForAllStudents;
    };
}