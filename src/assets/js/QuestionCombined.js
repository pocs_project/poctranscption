﻿ILangApp.directive('questionCombined', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('QuestionCombined'); },
        scope: {
            allprops: '='
        }, controller: function ($scope, $element, $attrs, $transclude, $interval) {
            registerExerciseController($scope, $element, $attrs, $transclude, $timeout);
            let me = $scope;
            me.compareSuptopic = null;
            me.compareTextLength = 0;
            me.SaveButtonText = 'SALVAR RESPOSTA';
            registerTinymceFunctions(me);

            me.InitTinymce = function () {
                try {
                    InitializeTinyMCEOptionSimpleModeMathOnly(me.txtFieldID(), true);
                } catch (e) { }

            };

            me.txtFieldID = function () {
                return 'if_' + me.allprops.LessonCategoryID;
            };
            me.getText = function () {
                return me.getEditorContent(me.txtFieldID(), 'text');
            };
            me.getHtml = function () {
                return me.getEditorContent(me.txtFieldID(), 'html');
            };
            me.setText = function (txt) {
                me.setEditorContent(me.txtFieldID(), txt);
            };
            me.onChangeQuestion = function () {
                me.setText(me.currentQuestion.AnswerText || '');
                me.cancelTimerAutoSave();
            };
            me.onBeforeChangeQuestion = function () {
                if (me.currentQuestion != null) {
                    me.currentQuestion.AnswerText = me.getHtml();

                }
            };
            me.onAfterAnswer = function () {
                me.currentQuestion.AnswerText = me.getHtml();
                me.currentQuestion.userTyping = false;
            }
            me.startTyping = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return

                me.currentQuestion.userTyping = true;
                me.InitTinymce();
                me.startTimerAutoSave();
            };
            me.edit = function () {

                if (me.exercisedto.ReadOnly || me.studentIsSuspendedOnDiscipline)
                    return;

                me.InitTinymce();
                me.currentQuestion.userTyping = true;
                me.currentQuestion.Status = me.enums.StudentQuestionStatus.Editing
                setTimeout(function () {
                    me.setText(me.currentQuestion.AnswerText || '');
                }, 200);
                if (me.compareTextLength != me.getText().length) {
                    me.startTimerAutoSave();
                }
            };
            me.answerTimerAutoSave = null;
            me.startTimerAutoSave = function () {
                me.cancelTimerAutoSave();
                me.answerTimerAutoSave = $interval(function () {

                    if (me.getText().length <= 1)
                        return;

                    if (me.compareTextLength == me.getText().length)
                        return;

                    me.SaveButtonText = 'SALVANDO...';

                    me.postAnswer('answerQuestion', function (result) {
                        if (result.Result) {
                            setTimeout(function () {

                                me.SaveButtonText = 'SALVAR RESPOSTA';

                                me.compareTextLength = me.getText().length;
                            }, 2 * 1000);
                        }
                    });
                }, 300 * 1000);
            };
            me.cancelTimerAutoSave = function () {
                if (me.answerTimerAutoSave != null) {
                    $interval.cancel(me.answerTimerAutoSave);
                }
            };
            me.beforeAnswer = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                if (isEmptyOrSpaces(me.getText())) {
                    showTopErrorMsg('O campo resposta não pode ser vazio..', 5000, null, false);
                    me.busy = false;
                    return;
                }
                me.answer();
            };
            me.save = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                if (isEmptyOrSpaces(me.getHtml())) {
                    showTopErrorMsg('O campo resposta não pode ser vazio..', 5000, null, false);
                    return;
                }

                me.answer();
                me.cancelTimerAutoSave();
            }

            me.loadMath = function (html) {
                let inputHtml = document.createElement('div');
                $(inputHtml).html(html);

                $(inputHtml).find('.AM').each(function (index, item) {
                    let expression = $(item).text().replace('`', '').replace('`', '');
                    let image = AMTparseMath(expression);

                    $(item).html(image);
                });

                return $(inputHtml).html();
            }
            me.setGradeClassName = function (param) {
                let question = me.exercisedto.QuestionList.first({ QuestionID: param.id });
                if (question != null) {
                    question.GradeClassName = param.className;
                    if (!me.$$phase)
                        me.$apply();
                }
            }

            me.saveFinalStudentGrade = function (gn, gv) {
                parent.saveStudentGrade(gn, gv);
                if (gv == '0') {
                    $('#FinalSatisfatorioButton').removeClass('active');
                    $('#FinalMelhorarButton').addClass('active');
                } else {
                    $('#FinalMelhorarButton').removeClass('active');
                    $('#FinalSatisfatorioButton').addClass('active');
                }
            };
            me.SaveStudentSubTopicGrade = function (subTopicID, studentTrackID, studentSubTopicGrade) {
                if (me.exercisedto.ResultSummarySubTopic && me.exercisedto.ResultSummarySubTopic.length > 0) {
                    let selectedSubTopicObj = me.exercisedto.ResultSummarySubTopic.filter(subtopic => subtopic.SubTopicId == subTopicID);

                    if (!selectedSubTopicObj.length > 0) {
                        me.exercisedto.ResultSummarySubTopic.push(
                            {
                                GradeSubTopic: studentSubTopicGrade,
                                StudentTrackID: studentTrackID,
                                SubTopicId: subTopicID
                            }
                        )
                    }
                } else {
                    me.exercisedto.ResultSummarySubTopic = [];
                    me.exercisedto.ResultSummarySubTopic.push(
                        {
                            GradeSubTopic: studentSubTopicGrade,
                            StudentTrackID: studentTrackID,
                            SubTopicId: subTopicID
                        }
                    )
                }

                parent.SaveStudentSubTopicGrade(subTopicID, studentTrackID, studentSubTopicGrade)
                if (studentSubTopicGrade == '0') {
                    $('#buttonS' + subTopicID).removeClass('active');
                    $('#buttonP' + subTopicID).addClass('active');
                } else {
                    $('#buttonP' + subTopicID).removeClass('active');
                    $('#buttonS' + subTopicID).addClass('active');
                }
                console.log(me.exercisedto.ResultSummarySubTopic);
            };
            me.isGradeRegistrySubTopic = function (subTopicID, gradeSubTopic) {
                let retorno = '';
                if (me.exercisedto.ResultSummarySubTopic != null && me.exercisedto.ResultSummarySubTopic != undefined) {
                    for (let item of me.exercisedto.ResultSummarySubTopic) {
                        if (item.SubTopicId == subTopicID && gradeSubTopic == item.GradeSubTopic) {
                            retorno = 'active';
                        }
                    }
                }
                return retorno;
            };
            me.savefeedbackAnswer = function () {
                let url = ILangSettings.Sites.ILang() + '/Handlers/StudentPerformance/StudentAnswerFeedback.ashx'
                let msg = document.getElementById("feedBackTxt").value;
                if ($("#feedBackTxt").val().trim().length < 1) {
                    alert("Feedback Vazio!");
                    return;
                }
                $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {
                            "msg": msg,
                            "stid": me.exercisedto.StudentTrackID,
                            "uoid": ILangSettings.UserOrganizationID,
                            "qid": me.currentQuestion.QuestionID,
                            "act": "REGISTER"
                        },
                        datatype: 'json'
                    }).done(function (result) {
                        if (result != undefined && result != "null") {
                            me.enableFeedbackAnswer('get', JSON.parse(result));
                            me.enableResponseFeedbackAnswer(false, '');
                        }

                    });
            };
            me.getFeedbackAnswer = function () {
                let url = "";
                if (me.currentQuestion.Options.length == 0 && me.currentQuestion.IsAnswered && me.exercisedto.StudentTrackID > 0) {
                    if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance) {
                        url = ILangSettings.Sites.ILang() + '/Handlers/StudentPerformance/StudentAnswerFeedback.ashx';
                    } else if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Student && !me.exercisedto.WaitingCorrection) {
                        url = ILangSettings.Sites.Student() + '/FeedbackAnswerQuestion/GetFeedbackAnswer';
                    } else {
                        return;
                    }
                } else {
                    return;
                }

                $.ajax(
                    {
                        url: url,
                        type: 'GET',
                        dataType: "json",
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        data: {
                            "stid": me.exercisedto.StudentTrackID,
                            "qid": me.currentQuestion.QuestionID,
                            "act": "GET"
                        },
                        success: function (result) {
                            if ((result != undefined) && (result != "null")) {
                                me.enableFeedbackAnswer('get', result);
                                me.enableResponseFeedbackAnswer(false, '');

                            } else if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance) {
                                me.enableResponseFeedbackAnswer(true, 'register');
                                me.enableFeedbackAnswer('empty', 'empty');
                            } else {
                                me.enableFeedbackAnswer('empty', 'empty');
                            }
                        }
                    });
            };
            me.deletarfeedbackAnswer = function () {
                $("#questionCombinedLoad").show();
                let url = ILangSettings.Sites.ILang() + '/Handlers/StudentPerformance/StudentAnswerFeedback.ashx'
                $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        data: {
                            "stid": me.exercisedto.StudentTrackID,
                            "qid": me.currentQuestion.QuestionID,
                            "act": "ERASE"
                        },
                        datatype: 'json'
                    }).done(function (result) {
                        if (result != undefined) {
                            me.enableResponseFeedbackAnswer(true, 'register');
                            me.enableFeedbackAnswer('empty');
                        }
                    });
                $("#questionCombinedLoad").hide();
            };
            me.editarfeedbackAnswer = function () {
                me.enableResponseFeedbackAnswer(true, 'edit');
            };
            me.cancelarfeedbackAnswer = function () {
                me.enableResponseFeedbackAnswer(false, '');
            };
            me.enableResponseFeedbackAnswer = function (e, a) {
                if (e) {
                    $("#feedBackTxt").show();

                    if (a == 'edit') {
                        $("#btnFeedbackDeixar").hide();
                        $("#btnfeedbackSal").show();
                        $("#btnfeedbackCan").show();
                    } else if (a == 'register') {
                        $("#feedBackTxt").val('');
                        $("#btnFeedbackDeixar").show();
                        $("#btnfeedbackSal").hide();
                        $("#btnfeedbackCan").hide();
                    }

                } else {
                    $("#feedBackTxt").hide();
                    $("#btnfeedbackSal").hide();
                    $("#btnfeedbackCan").hide();
                    $("#btnFeedbackDeixar").hide();
                }

            };
            me.enableFeedbackAnswer = function (a, feedbackAnswer) {
                if (a == 'empty') {
                    $("#divFeedbackAnswer").hide();
                } else if (a == 'get') {
                    if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance) {
                        $("#btnfeedbackEdit").show();
                        $("#btnfeedbackDel").show();
                    }

                    if (me.allprops.Mode == me.enums.StudentCategoryViewMode.Performance || (!me.exercisedto.WaitingCorrection)) {
                        $("#txtFeedbackAnswer").html('');
                        $("#userFeedbackAnswer").html('');
                        $("#dtFeedbackAnswer").html('');
                        $("#feedBackTxt").val('');

                        $("#txtFeedbackAnswer").html(feedbackAnswer.FeedbackMessage);
                        $("#userFeedbackAnswer").html(feedbackAnswer.CommentatorName);
                        $("#dtFeedbackAnswer").html(convertDate(feedbackAnswer.DateRegister));
                        $("#feedBackTxt").val(feedbackAnswer.FeedbackMessage);

                        $("#divFeedbackAnswer").show();
                    }
                }

            };

            const convertDate = (d) => {
                const checkDate = (/^\/Date/gim);
                if (checkDate.test(d)) {
                    const rawDate = d;
                    const ticks = parseInt(rawDate.replace(/\D/g, '').substr(0, 13));
                    return new Date(ticks).toLocaleString('pt-BR');
                }
                else {
                    return d;
                }
            }

            me.activeResultGrade = function (gv) {
                let retorno = '';

                if (parent.spInstance != null && parent.spInstance != undefined) {
                    if ((gv == '0' && parent.spInstance._currentAssignment.gv == 0 && parent.spInstance._currentAssignment.gt != '-') || (gv == '100' && parent.spInstance._currentAssignment.gv == 100 && parent.spInstance._currentAssignment.gt != '-')) {
                        retorno = 'active';
                    }
                }

                return retorno;
            };
            changeFocusKeyDowIframe();
        }
    };
});