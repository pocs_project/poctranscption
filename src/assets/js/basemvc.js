﻿//Google Analytics      
if (isSafeToUse(ILang, 'Util')) {
    ILang.Util.GoogleAnalytis = {
        props:{
            googleAnalyticsKey: null,
            studentTrackID: null,
            organizationID: null,
            disciplineName: null,
            lessonCategoryName: null,
            organizationName: null
        },
        addTrans: function () {
            _gaq.push(['_setAccount', this.props.googleAnalyticsKey]);
            _gaq.push(['_trackPageview']);
            _gaq.push([
             '_addTrans',
              this.props.studentTrackID,  // transaction ID - required
              this.props.organizationID,  // affiliation or store name
              1                // total - required
            ]);
        },
        addItem: function () {
            _gaq.push([
               '_addItem',
               this.props.studentTrackID,           // transaction ID - required
               this.props.disciplineName,           // SKU/code - required
               this.props.lessonCategoryName,        // product name
               this.props.organizationName,   // category or variation
               '1',          // unit price - required
               '1'               // quantity - required
            ]);
            _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
        },
        track: function (studentTrackID, organizationID, organizationName, disciplineName, lessonCategoryName) {
            if (typeof (_gaq) == 'undefined')  //|| !isSafeToUse(ILangSettings,'GoogleAnalyticsKey')
                return;

            this.props.googleAnalyticsKey = ILangSettings.GoogleAnalyticsKey || 1234;       //1
            this.props.studentTrackID = studentTrackID;                             //2
            this.props.organizationID = organizationID;                             //3
            this.props.disciplineName = disciplineName;                             //4
            this.props.lessonCategoryName = lessonCategoryName;                     //5
            this.props.organizationName = organizationName;                         //6

            try {
                this.addTrans()
                this.addItem();
            } catch (e) {
                if(console)
                    console.warn('Impossível logar dados no G.Analytics');
            }
           
        },
        trackEvent:function(){
            //_gaq('send', 'event', 'wod', 'start', me.currentWod.name);
        }
    };
}
