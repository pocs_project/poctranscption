﻿/*!
 * ASP.NET SignalR JavaScript Library v2.2.0
 * http://signalr.net/
 *
 * Copyright Microsoft Open Technologies, Inc. All rights reserved.
 * Licensed under the Apache 2.0
 * https://github.com/SignalR/SignalR/blob/master/LICENSE.md
 *
 */

/// <reference path="..\..\SignalR.Client.JS\Scripts\jquery-1.6.4.js" />
/// <reference path="jquery.signalR.js" />
(function ($, window, undefined) {
    /// <param name="$" type="jQuery" />
    "use strict";

    if (typeof ($.signalR) !== "function") {
        throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/js.");
    }

    var signalR = $.signalR;

    function makeProxyCallback(hub, callback) {
        return function () {
            // Call the client hub method
            callback.apply(hub, $.makeArray(arguments));
        };
    }

    function registerHubProxies(instance, shouldSubscribe) {
        var key, hub, memberKey, memberValue, subscriptionMethod;

        for (key in instance) {
            if (instance.hasOwnProperty(key)) {
                hub = instance[key];

                if (!(hub.hubName)) {
                    // Not a client hub
                    continue;
                }

                if (shouldSubscribe) {
                    // We want to subscribe to the hub events
                    subscriptionMethod = hub.on;
                } else {
                    // We want to unsubscribe from the hub events
                    subscriptionMethod = hub.off;
                }

                // Loop through all members on the hub and find client hub functions to subscribe/unsubscribe
                for (memberKey in hub.client) {
                    if (hub.client.hasOwnProperty(memberKey)) {
                        memberValue = hub.client[memberKey];

                        if (!$.isFunction(memberValue)) {
                            // Not a client hub function
                            continue;
                        }

                        subscriptionMethod.call(hub, memberKey, makeProxyCallback(hub, memberValue));
                    }
                }
            }
        }
    }

    $.hubConnection.prototype.createHubProxies = function () {
        var proxies = {};
        this.starting(function () {
            // Register the hub proxies as subscribed
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, true);

            this._registerSubscribedHubs();
        }).disconnected(function () {
            // Unsubscribe all hub proxies when we "disconnect".  This is to ensure that we do not re-add functional call backs.
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, false);
        });

        proxies['activityGroupHub'] = this.createHubProxy('activityGroupHub'); 
        proxies['activityGroupHub'].client = { };
        proxies['activityGroupHub'].server = {
            addGroup: function (groupName) {
                return proxies['activityGroupHub'].invoke.apply(proxies['activityGroupHub'], $.merge(["AddGroup"], $.makeArray(arguments)));
             },

            send: function (groupName, sender, message) {
                return proxies['activityGroupHub'].invoke.apply(proxies['activityGroupHub'], $.merge(["Send"], $.makeArray(arguments)));
             },

            sendAll: function (sender, message) {
                return proxies['activityGroupHub'].invoke.apply(proxies['activityGroupHub'], $.merge(["SendAll"], $.makeArray(arguments)));
             }
        };

        proxies['baseHub'] = this.createHubProxy('baseHub'); 
        proxies['baseHub'].client = { };
        proxies['baseHub'].server = {
        };

        proxies['feedHub'] = this.createHubProxy('feedHub'); 
        proxies['feedHub'].client = { };
        proxies['feedHub'].server = {
            joinGroup: function (userID, parentID, feedType) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["JoinGroup"], $.makeArray(arguments)));
             },

            joinGroupsAndViewFeeds: function (jsonMessage) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["JoinGroupsAndViewFeeds"], $.makeArray(arguments)));
             },

            newComment: function (parentID, feedID, commentID, comment, feedtype, userID, userName, userImageUrl, elapsedTime, createdDateString) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewComment"], $.makeArray(arguments)));
             },

            newCommentNegativeFeedBack: function (parentID, feedID, feedCommentID, feedtype) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewCommentNegativeFeedBack"], $.makeArray(arguments)));
             },

            newCommentPositiveFeedBack: function (parentID, feedID, feedCommentID, feedtype, wasNegative) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewCommentPositiveFeedBack"], $.makeArray(arguments)));
             },

            newFeedNegativeFeedBack: function (parentID, feedID, feedtype) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewFeedNegativeFeedBack"], $.makeArray(arguments)));
             },

            newFeedPositiveFeedBack: function (parentID, feedID, feedtype, wasNegative) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewFeedPositiveFeedBack"], $.makeArray(arguments)));
             },

            newFeedView: function (parentID, userID, feedID, feedtype) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewFeedView"], $.makeArray(arguments)));
             },

            newFeedViews: function (parentID, userID, feedIDList, feedtype) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewFeedViews"], $.makeArray(arguments)));
             },

            newPost: function (parentID, feedtype) {
                return proxies['feedHub'].invoke.apply(proxies['feedHub'], $.merge(["NewPost"], $.makeArray(arguments)));
             }
        };

        proxies['iLangHub'] = this.createHubProxy('iLangHub'); 
        proxies['iLangHub'].client = { };
        proxies['iLangHub'].server = {
            aPAProcessFinished: function (userMediaId, result) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["APAProcessFinished"], $.makeArray(arguments)));
             },

            applyDefaultGradeFinished: function (classDisciplineId, disciplineContentId, lessonCategoryId, organizationId) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["ApplyDefaultGradeFinished"], $.makeArray(arguments)));
             },

            assessmentBatchProcessFinished: function (userID, assessmentBatchID, processStatus, successCount, errorCount, warningCount, detachedCount, failMessage) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["AssessmentBatchProcessFinished"], $.makeArray(arguments)));
             },

            assessmentGroupSheetProcessFinished: function (userID, assessmentGroupID, questionSheetUrl) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["AssessmentGroupSheetProcessFinished"], $.makeArray(arguments)));
             },

            assessmentSheetProcessFinished: function (userID, assessmentID, assessmentClassDisciplineID, questionSheetUrl) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["AssessmentSheetProcessFinished"], $.makeArray(arguments)));
             },

            changeUserStatus: function (userId, userLoginId, statusId) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["ChangeUserStatus"], $.makeArray(arguments)));
             },

            closeChatRoom: function (chatRoomId, userID) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["CloseChatRoom"], $.makeArray(arguments)));
             },

            fileExportFinished: function (userId, serviceFileExportId, filePathUrl) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["FileExportFinished"], $.makeArray(arguments)));
             },

            fileImportFinished: function (userId, batchId) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["FileImportFinished"], $.makeArray(arguments)));
             },

            getAddChatRoom: function (targetId, type) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["GetAddChatRoom"], $.makeArray(arguments)));
             },

            getCurrentChatRooms: function () {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["GetCurrentChatRooms"], $.makeArray(arguments)));
             },

            getGroupUsersStatus: function (userIds) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["GetGroupUsersStatus"], $.makeArray(arguments)));
             },

            newUserNotification: function (userList) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["NewUserNotification"], $.makeArray(arguments)));
             },

            notificationRead: function (userId) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["NotificationRead"], $.makeArray(arguments)));
             },

            reportProcessed: function (reportID, userScheduleID, filePath, success, notifyCompletelyFail, failMessage) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["ReportProcessed"], $.makeArray(arguments)));
             },

            sendChatRoomMessage: function (chatRoomId, lastMessageId, message) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["SendChatRoomMessage"], $.makeArray(arguments)));
             },

            setMessagesViewed: function (chatRoomId, userID) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["SetMessagesViewed"], $.makeArray(arguments)));
             },

            startOnline: function (userId, userName, organizationId, browser, clientIP, operatingSystem, isMobile) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["StartOnline"], $.makeArray(arguments)));
             },

            videoEncodingFinished: function (userMediaId, result) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["VideoEncodingFinished"], $.makeArray(arguments)));
            },

            newCalendarIntegration: function (id, isGroup) {
                return proxies['iLangHub'].invoke.apply(proxies['iLangHub'], $.merge(["NewCalendarIntegration"], $.makeArray(arguments)));
            }
        };

        return proxies;
    };

    signalR.hub = $.hubConnection("/signalr", { useDefaultPath: false });
    $.extend(signalR, signalR.hub.createHubProxies());

}(window.jQuery, window));