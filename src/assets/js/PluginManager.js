﻿const getPluginList = async () => {
    let request = await fetch(ILangSettings.Sites.StudentAPI() + '/plugin', { credentials: "include" })
    return request.json();
    
}


(async () => {
    
    let plugInResult = await getPluginList();
    if (plugInResult != null && plugInResult.Success && plugInResult.Result != null && plugInResult.Result.length > 0) {
        for (const plugInItem of plugInResult.Result) {
            if (plugInItem.RelativeJsFilePath != null && plugInItem.RelativeJsFilePath.length > 0) {
                let scriptTag = document.createElement("script");
                scriptTag.setAttribute("type", "text/javascript");
                scriptTag.setAttribute("src", ILangSettings.Sites.Static() + plugInItem.RelativeJsFilePath + '?v=' + ILangSettings.Version);
                document.getElementsByTagName("head")[0].appendChild(scriptTag);

            }
        }
    }
})();

