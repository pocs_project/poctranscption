﻿ILangApp.directive('categoryMasterLoader', function () {
    return {
        templateUrl: () => {
            return getStaticUrl('/Templates/Category/CategoryMasterLoader.html');
        },
        controller: function ($scope, $http, $element, $attrs, $sce, $timeout) {
            baseController($scope, $http, {});
            var me = $scope;
            me.menuLoaded = false;
            me.topBarLoaded = true;
            me.footerLoaded = true;
            me.contentLoaded = false;
            me.contentAccessibilityLoaded = false;
            me.showContent = false;
            me.accessibilityHelpScope = null;

            //Functions definitions
            me.controlHtmlElementsOutOfAngularBeforeLoad = function () {

                $('.znv-chat').hide();
                
            };
            me.controlHtmlElementsOutOfAngularAfterLoad = function () {

                $('.znv-chat').show();
                $("#mainContentId").focus();
            }

            me.getAccessibilityHelpScope = () => {
                if (!me.accessibilityHelpScope) {
                    me.accessibilityHelpScope = getAngularScopeByElementId('accessibilityHelpDialog');
                }
            }

            //Listener para mostrar o conteudo carregado
            me.$on('categoryLoaded', function (event, data) {

                if (data.hasLoadMenu != undefined) {
                    me.menuLoaded = data.hasLoadMenu;
                }
                else if (data.hasLoadFooter != undefined) {
                    me.footerLoaded = data.hasLoadFooter;
                }
                else if (data.hasLoadTop != undefined) {
                    me.topBarLoaded = data.hasLoadTop;
                }
                else if (data.hasLoadContent != undefined) {
                    me.contentLoaded = data.hasLoadContent;
                }
                else if (data.hasLoadAccessibility != undefined) {
                    me.contentAccessibilityLoaded = data.hasLoadAccessibility;
                }

                me.showContent = me.menuLoaded && me.topBarLoaded && me.footerLoaded && me.contentLoaded && me.contentAccessibilityLoaded;
                
                if (me.showContent) {
                    me.controlHtmlElementsOutOfAngularAfterLoad();
                    me.getAccessibilityHelpScope();
                    me.accessibilityHelpScope.openIfAccessibilityHelpEnable();                    
                }

            });

            if (!me.showContent) {
                me.controlHtmlElementsOutOfAngularBeforeLoad();
            }
        }
    };
});


