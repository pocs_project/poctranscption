﻿RegisterNamespace('ILang');
RegisterNamespace('ILang.Online');
RegisterNamespace('ILang.OnlineBar');
RegisterNamespace('ILang.UserStatus');

ILang.Online.GetStatusElements = function () {
    //console.log('GetStatusElements');
}

ILang.UserStatus.UpdateUserStatus = function (user, status) {
    if (ILang.OnlineBar.lastStudentId == user.UserID + '_' + status)
        return;

    ILang.OnlineBar.lastStudentId = user.UserID + '_' + status;

    if (BuildILangOnlineBarchatScope()) {
        ILang.OnlineBar.chatScope.updateUserStatus(user, status);
    }
}

ILang.OnlineBar.chatStarted = function (obj) {
    if (BuildILangOnlineBarchatScope()) {
        ILang.OnlineBar.chatScope.openChatCallBack(obj);
    }
}

ILang.OnlineBar.handleChatRoom = function (obj) {
    if (BuildILangOnlineBarchatScope()) {
        ILang.OnlineBar.chatScope.manageMessage(obj);
    }
}

ILang.OnlineBar.handleChatRooms = function (obj) {
    if (BuildILangOnlineBarchatScope()) {
        angular.forEach(obj, function (value, key) {
            ILang.OnlineBar.chatScope.manageMessage(value);
        });
    }
}

ILang.OnlineBar.messageSent = function (userId, status) {
}


ILang.OnlineBar.messageHasNotBeenSent = function (chatRoomId, lastMessageId, message) {
    if (typeof (ILang.OnlineBar.chatScope) == 'undefined') {
        ILang.OnlineBar.chatScope = getAngularScopeByElementId('ilangChatElement');
    }
    ILang.OnlineBar.chatScope.messageHasNotBeenSent({ chatRoomId: chatRoomId, lastMessageId: lastMessageId, message: message });
};


function BuildILangOnlineBarchatScope() {

    if (typeof (ILang.OnlineBar.chatScope) == 'undefined') {
        ILang.OnlineBar.chatScope = getAngularScopeByElementId('ilangChatElement');
    }

    if (typeof (ILang.OnlineBar.chatScope) == 'undefined') {
        console.log("Erro ao montar ILang.OnlineBar.chatScope: " + ILang.OnlineBar.chatScope);
        return false;
    }
    return true;
}


function SetLoginStatusImage(userId, status) {
}

function SucceededUpdateGroupStatusCallback(userOnlineIds) {
}

function SucceededGroupUsersOnlineCallback(users) {
    var scope = getAngularScopeByElementId('ilangChatElement');
    if (scope != null) {
        var list = eval(users);
        scope.setInitialListIds(list, true);
    }
}

function SuccessNotificationCounter(count) {
    var scope = getAngularScopeByElementId('notifiationPageDiv');
    if (isSafeToUse(scope, 'SetNotificationCount')) {
        scope.SetNotificationCount(count);
    }
}

ILangApp.directive('notificationPage', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: true,
        templateUrl: getStaticUrl('/Templates/Notification/Page.html'),
        controller: function ($scope, $http, $rootScope) {
            baseController($scope, $http, {});
            $scope.LoadedNotificationList = false;
            $scope.NotificationCount = 0;
            $scope.ShowNotificationCount = false;
            $scope.ShowNotificationList = false;
            $scope.ShowEmptyMessage = false;
            $scope.ShowNotificationViewAllLink = false;
            $scope.NotificationList = [];
            $scope.ShowLoading = false;

            $scope.NotificationClick = function (event) {
                if (typeof (closeProfilePane) == 'function')
                    closeProfilePane();

                $scope.ShowNotificationList = !$scope.ShowNotificationList;
                if ($scope.ShowNotificationList && isSafeToUse(window, 'ShowHideUserMenu')) {
                    window.ShowHideUserMenu(true);
                }
                if ($scope.ShowNotificationList && !$scope.LoadedNotificationList) {
                    $scope.ShowLoading = true;
                    var url = ILangSettings.Sites.API() + '/Notification/NotificationPageList';
                    $scope.ajax.get(url, null, function (result) {
                        if (result.Success) {
                            angular.forEach(result.Result, function (value, key) {
                                $scope.NotificationList.push(value);
                            });
                            $scope.ShowEmptyMessage = result.Result.length == 0;
                            $scope.ShowNotificationCount = false;
                        }
                        else {
                            $scope.NotificationList = [];
                            $scope.ShowEmptyMessage = true;
                        }
                        $scope.ShowNotificationViewAllLink = !$scope.ShowEmptyMessage;
                        $scope.LoadedNotificationList = true;
                        $scope.ShowLoading = false;
                    }, undefined, { showLoading: true });
                }
            };

            $scope.NotificationViewAllClick = function () {
                window.location.href = ILangSettings.Sites.ILang() + '/Community/Notification.aspx';;
            };

            $scope.SetNotificationCount = function (count) {
                $scope.NotificationCount = count;
                $scope.ShowNotificationCount = count > 0;
                $scope.$apply();
            };
            
            $scope.close = function (event) {
                $scope.ShowNotificationList = false;
            }
        }
    }
});


//###################################################
//                  C H A T
//###################################################


ILangApp.directive('ilangChat', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        scope: true,
        templateUrl: function () {
            var h = window.location.href;
            if (h.indexOf('student') >= 0 || h.indexOf('admin') >= 0 || h.indexOf('social') >= 0) {
                return '/Chat_Chat.tpl';
            } else {
                return getStaticUrl('/Templates/Chat/Chat.html');
            }
        },
        controller: function ($scope, $http, $element, $attrs) {
            baseController($scope, $http, {});
            var me = $scope;

            me.studentSearch = '';

            me.currentUserId = ILangSettings.UserID;
            me.blockType = { Received: 0, Sent: 1, UserStatus: 2 };
            me.userStatus = { On: 1, Off: 0 };
            me.chatStatus = { Hidden: 0, Opened: 1, Collapsed: 2, Closed: 3 };
            me.chatType = { Friends: 1, ActivityGroup: 2 };
            //ui properties
            me.maxOpnedChats = parseInt(($('body').width() - $('.sideMenu').width() - $('.ilChatOnUsers').width() - $('.ilChatHidden').width()) / 250);
            if (me.maxOpnedChats <= 0) { me.maxOpnedChats = 1; }
            me.chatStudenListIsVisible = false;
            me.sinalIRisOff = false;
            var student = function (ID, Name, Img) {
                this.name = Name || "";
                this.userId = ID;
                this.img = Img;
                this.status = null;
                this.chatId = null;
                this.visible = true;
            };
            var chat = function (ChatTitle, ChatStatus, ID, ChatSubtitle, ChatUrl) {
                this.chatTitle = ChatTitle || "";
                this.chatSubtitle = ChatSubtitle || "";
                this.chatUrl = ChatUrl || "";
                this.status = ChatStatus || me.chatStatus.Closed;
                this.chatId = ID;
                this.studentId = null;
                this.typingText = '';
                this.blocks = [];
                this.lastIteration = null;
                this.lastMessageId = 0;
                this.hasUnreadMsg = false;
                this.focusOnTextField = false;
                this.type = null;
                this.groupUsersList = null;
                this.lockKeyboard = false;
                this.userStatus = me.userStatus.Off;
                this.turnOff = function (student) {
                    this.userStatus = me.userStatus.Off;
                    var b = new block(me.blockType.UserStatus);
                    b.statusMsg = student.name + ' está offline';
                    this.blocks.push(b);
                }
            };
            var block = function (Type, Time) {
                this.order = null;
                this.type = Type;
                this.time = Time;
                this.elapsedTime = '';
                this.lines = [];
                this.last = false;
                this.dateString = '';
                this.userId = null;
                this.img = '';
                this.userName = null;
                this.offLine = false;
                this.statusMsg = '';
            };
            var textLine = function (Txt) {
                this.text = Txt || '';
            };
            var group = function (id, name) {
                this.groupId = id || null;
                this.name = name || null;
                this.chatId = null;
            };
            me.chats = [];
            me.students = [];
            me.groups = [];
            me.maxMembersPictures = 7;
            me.visible = false;
            me.showChatMainList = false;
            me.activeGroup = {
                List: [],
                Title: '',
                loading: false,
                showDialog: false
            };

            //Filter
            me.filterList = function (item) {
                return item.status == me.chatStatus.Collapsed || item.status == me.chatStatus.Opened;
            };
            me.filterHidden = function (item) {
                return item.status == me.chatStatus.Hidden;
            }
            //Events
            me.collapseChat = function (chat) {
                if (chat.status == me.chatStatus.Collapsed) {
                    chat.status = me.chatStatus.Opened;
                    chat.hasUnreadMsg = false;
                    chat.focusOnTextField = true;
                    me.scrollChatDown();
                } else {
                    chat.status = me.chatStatus.Collapsed;
                }
            };
            me.closeChat = function (chat) {
                if (isSafeToUse(ILangHub, 'server.closeChatRoom'))
                    ILangHub.server.closeChatRoom(chat.chatId, me.currentUserId);
                chat.status = me.chatStatus.Closed;
                return true;
            };
            me.openChat = function (student, chatType) {
                if (student == null)
                    return;

                var fn = function () {
                    me.reconnectIfNecessary(function () {
                        if (isSafeToUse(ILangHub, 'server.getAddChatRoom'))
                            ILangHub.server.getAddChatRoom(student.userId, chatType == null ? me.chatType.Friends : chatType);
                    });
                }

                if (student.chatId != null && me.chats != null) {
                    var c = me.chats.first({ chatId: student.chatId });
                    if (c != null) {
                        c.lastIteration = new Date().getTime();
                        c.status = me.chatStatus.Opened;
                        c.hasUnreadMsg = false;
                        me.verifyLayout(student.chatId);
                        me.scrollChatDown();
                    } else {
                        fn();
                    }

                } else {
                    fn();
                }

            };
            me.openGroupChat = function (groupInfo) {
                //var item = me.groups.first({ groupId: groupInfo.id });
                me.visible = true;
                me.openChat({ userId: groupInfo.id, chatId: null }, me.chatType.ActivityGroup);
                //if (item != null) {
                //    me.openChat({ chatId: item.chatId, userId: null }, me.chatType.ActivityGroup);
                //} else {
                //    me.groups.push(new group(groupInfo.id))
                //    me.openChat({ userId: groupInfo.id, chatId: null }, me.chatType.ActivityGroup);
                //}
            };
            me.toggleChatStatus = function () {
                //chat.status
            }
            me.openChatCallBack = function (obj, status) {
                var id = obj.ChatRoomID;
                var _status = status || me.chatStatus.Opened;
                if (me.chats.any({ chatId: id })) {
                    var c = me.chats.first({ chatId: id });
                    c.lastIteration = new Date().getTime();
                    c.status = _status
                    c.lastMessageId = 0;
                    c.hasUnreadMsg = _status == me.chatStatus.Collapsed;
                    me.verifyLayout(c.chatId);
                    me.scrollChatDown();
                    c.focusOnTextField = true;
                } else {
                    if (obj.Type == me.chatType.Friends) {
                        var student = obj.ChatUsers.notEquals({ UserID: ILangSettings.UserID }).first();
                        if (student != null) {
                            me.students.setTo({ userId: student.UserID }, { chatId: id });
                            var c = new chat(student.User, _status, id, null, null);
                            c.lastIteration = new Date().getTime();
                            c.hasUnreadMsg = _status == me.chatStatus.Collapsed;
                            c.focusOnTextField = true;
                            c.type = me.chatType.Friends;
                            c.userStatus = student.StatusID;
                            me.chats.push(c);
                            me.addMesages(c, obj.ChatMessages, obj.ChatUsers);
                            me.verifyLayout(c.chatId);
                        }
                    } else {
                        //group
                        var c = new chat(obj.ChatName || id, _status, id, obj.ChatSourceName || '', obj.ChatSourceUrl);
                        c.lastIteration = new Date().getTime();
                        c.hasUnreadMsg = _status == me.chatStatus.Collapsed;
                        c.focusOnTextField = true;
                        c.type = me.chatType.ActivityGroup;
                        c.groupUsersList = obj.ChatUsers;
                        me.chats.push(c);
                        me.addMesages(c, obj.ChatMessages, obj.ChatUsers);
                        me.verifyLayout(c.chatId);
                        me.scrollChatDown();
                        me.groups.push(new group(id, id));
                    }

                }
                me.$apply('chats');

            };
            me.scrollChatDown = function () {

                $timeout(function () {
                    $('#ilangChatElement .ilChatCont[singlechatdialog] ').each(function (index, chat) {
                        var block = $(chat).find('.ilChatText').last();
                        var offset = block.offset();
                        var top = getPropertyByName(offset, 'top') || 2000;
                        if (isSafeToUse(block, 'offset')) {
                            $(chat).animate({
                                scrollTop: top
                            }, 600);
                        }

                    });
                }, 150);

            };
            me.verifyLayout = function (chatId) {
                var count = me.chats.count({ status: OR([me.chatStatus.Opened, me.chatStatus.Collapsed]) });
                if (count > me.maxOpnedChats) {
                    var last = me.chats.notEquals({ chatId: chatId }).equals({ status: OR([me.chatStatus.Opened, me.chatStatus.Collapsed]) }).objWithMin('lastIteraion');
                    if (last != null) {
                        last.status = me.chatStatus.Hidden;
                    }
                }
            };
            me.messageHasNotBeenSent = function (obj) {
                var chat = me.chats.first({ chatId: obj.chatRoomId });
                if (chat != null) {
                    chat.lockKeyboard = true;
                    me.addMesages(chat, [{ offline: true, ElapsedTime: '', Text: obj.message }], null, true);
                    chat.typingText = '';
                    if (typeof (ILang) != 'undefined' && typeof (ILang.ErrorHandling) != 'undefined') {
                        var fsID = ILang.ErrorHandling.HTTPHandler.Send('chat parou de funcionar', document.location.href, 0, true);
                        logErrorOnHiddenField('chat parou de funcionar', fsID, 'chat');
                    }


                    me.$apply('chats');
                }
            }
            me.tryToSendAgain = function (chat) {
                me.reconnectIfNecessary(function () {
                    var txt = chat.typingText;
                    chat.typingText = '';
                    ILangHub.server.sendChatRoomMessage(chat.chatId, chat.lastMessageId || 0, txt);
                });
            };
            me.userTyping = function (event, chat) {
                if (event.keyCode == window.keyCode.KEY_ENTER && !isNullOrWhitespace(chat.typingText)) {
                    me.reconnectIfNecessary(function () {
                        var txt = chat.typingText;
                        chat.typingText = '';
                        ILangHub.server.sendChatRoomMessage(chat.chatId, chat.lastMessageId || 0, txt);
                    });
                }
            };
            me.manageMessage = function (obj) {
                var chat = me.chats.first({ chatId: obj.ChatRoomID });
                if (chat == null) {
                    me.openChatCallBack(obj, me.chatStatus.Collapsed);
                    return me.manageMessage(obj);
                }
                chat.hasUnreadMsg = chat.status != me.chatStatus.Opened;
                if (chat.status == me.chatStatus.Closed)
                    chat.status = me.chatStatus.Collapsed;
                else
                    ILangHub.server.setMessagesViewed(chat.chatId, me.currentUserId);

                var last = obj.ChatMessages.objWithMax('ChatMessageID');
                chat.lastMessageId = (last) == null ? 0 : last.ChatMessageID;

                if (last.UserID == me.currentUserId)
                    chat.typingText = '';

                me.addMesages(chat, obj.ChatMessages.equals({ ChatMessageID: BiggerOrEqualThan(chat.lastMessageId) }), obj.ChatUsers);
                if (chat.status == me.chatStatus.Collapsed && chat.type == me.chatType.ActivityGroup)
                    me.openGroupChat(chat.chatId);

                me.$apply('chats');
            };
            me.addMesages = function (chat, msgs, users, forceNewBlock) {
                for (var i = 0; i < msgs.length; i++) {
                    var msg = msgs[i];
                    var _blockType = (me.currentUserId == msg.UserID) ? me.blockType.Sent : me.blockType.Received;
                    var lastBlock = (chat.blocks != null ? chat.blocks.first({ dateString: msg.DateString, userId: msg.UserID, last: true }) : null);
                    if (lastBlock != null && forceNewBlock != true) {
                        var l = new textLine(msg.Text);
                        lastBlock.lines.push(l);
                    } else {
                        var b = new block(_blockType, new Date().getTime());
                        var ElapsedTimeCompare = msg.ElapsedTime.toLowerCase();
                        b.elapsedTime = ElapsedTimeCompare == 'em menos de 1 minuto' ? 'Há menos de 1 minuto' : msg.ElapsedTime;
                        b.time = msg.Time;
                        b.last = true;
                        b.dateString = msg.DateString;
                        b.userId = msg.UserID;
                        if (chat.type == me.chatType.ActivityGroup)
                            b.userName = msg.User;

                        if (users != undefined && users != null && users.any({ UserID: msg.UserID })) {
                            b.img = users.first({ UserID: msg.UserID }).UserMiniatureUrl;
                        } else if (me.students.any({ userId: msg.UserID })) {
                            b.img = me.students.first({ userId: msg.UserID }).UserMiniatureUrl;
                        }
                        var offline = getPropertyByName(msg, 'offline');
                        if (offline != null)
                            b.offLine = offline;

                        var l = new textLine(msg.Text);
                        b.lines.push(l);
                        chat.blocks.set({ last: false });
                        chat.blocks.push(b);
                    }
                }


                chat.lastIteration = new Date().getTime();
                $('[singleChatDialog]').each(function (index, item) {
                    $(item).animate({ scrollTop: $(item).height() * 20000 }, 50);
                });
            };
            me.showMembersDialog = function (chat) {
                me.activeGroup.List = chat.groupUsersList.select({
                    UserImageUrl: 'UserMiniatureUrl',
                    UserName: 'User',
                    Online: function (item) {
                        return item.StatusID == 1
                    }
                });
                me.activeGroup.Title = chat.chatTitle;
                me.activeGroup.showDialog = true;
                if (me.userList == undefined || me.userList == null)
                    return;
                me.userList.title(chat.chatTitle);
                me.userList.viewCount(me.activeGroup.List.count());
                me.userList.show();
            }
            //SinalR
            me.setInitialListIds = function (list, clean) {
                try {
                    if (list == null || !Array.isArray(list) || list.length == 0)
                        return;

                    me.bindOnlineUserList(clean, list.select({ UserID: 'UserID', Name: 'UserName', MiniatureImageUrl: 'UserMiniatureUrl' }), false);

                } catch (e) {
                    console.log("Problemas na exibição dos usuários online: " + e.message);
                }

            }
            me.bindOnlineUserList = function (clean, userList, callApply) {
                ILang.OnlineBar.lastStudentId = null;

                if (clean == null || clean == true) {
                    me.students = [];
                }

                for (var i = 0; i < userList.length; i++) {
                    var item = userList[i];
                    if (item.UserID != ILangSettings.UserID && !me.students.any({ userId: item.User })) {
                        var s = new student(item.UserID, item.Name, item.MiniatureImageUrl);
                        s.status = me.userStatus.On;
                        me.students.push(s);
                    }
                }

                if (callApply) {
                    me.$apply('chats');
                }
            }
            me.updateUserStatus = function (user, status) {
                if (user.UserID == ILangSettings.UserID)
                    return;

                var student = me.students.first({ userId: user.UserID });
                if (student != null) {
                    if (status == 1) {
                        student.status = me.userStatus.On;
                    } else {
                        student.status = me.userStatus.Off;
                        me.students.remove({ userId: student.userId });
                        //chats
                        if (student.chatId != null) {
                            var openedChat = me.chats.first({ chatId: student.chatId });
                            openedChat.turnOff(student);
                        }
                    }
                    me.updateUserStatusOnGroups(user.UserID, status);
                    me.$apply();
                } else {
                    me.setInitialListIds([{ UserID: user.UserID, UserName: user.UserName, UserMiniatureUrl: user.UserMiniatureUrl, status: 1 }], false);
                }
            };
            me.updateUserStatusOnGroups = function (userId, status) {
                var userOnGroup = me.chats.findMany(function (item) {
                    if (item.groupUsersList != null)
                        return item.groupUsersList.first({ UserID: userId });
                }, ['group', 'user']);

                if (userOnGroup != null) {
                    userOnGroup.forEach(function (item) {
                        item.user.StatusID = status;
                        item.group.turnOff(userid);
                    });
                }

            };
            me.reconnectIfNecessary = function (callback) {

                if (typeof (callback) == 'undefined') {
                    return;
                }

                if ($.connection.hub.state == $.connection.connectionState.disconnected) {
                    $.connection.hub.start().done(function () {
                        window.setTimeout(function () {
                            ILangHub.server.startOnline(ILangSettings.UserID, ILangSettings.UserName, ILangSettings.OrganizationID, GetBrowserName(), ILangSettings.ClientIP, ILangSettings.Browser.OS, ILangSettings.Browser.IsMobile);
                            callback();
                        }, 1000)
                    })
                } else {
                    callback();
                }
            };


        }
    };
}]);
ILangApp.filter('chatActiveList', function () {
    return function (items) {
        return items.filter(function (item, index, array) {
            return item.status == 1 || item.status == 2;
        });
    };
});

//###################################################
//                  F I M   C H A T
//###################################################

//###################################################
//                  M E S S A G E
//###################################################

ILangApp.controller('composeMessageDialogCTRL', function ($http, $scope) {
    baseController($scope, $http, {});
    var me = $scope;
    me.title = '';

    me.addUser = function (user) {
        me.addItem(user);//{ EntityID, Name, Type, ImageUrl}
        me.emptyField = false;
    };
    me.send = function () {
        var list = me.getList();
        if (list == null || list.length == 0) {
            me.emptyField = true;
            return;
        }

        if (!me.isValidadeForm()) {
            return;
        }

        var param = {
            Recipients: list.select({ EntityID: 'EntityID', Type: 'Type' }),
            Subject: me.title,
            Body: me.getText(),
            UserRecipientCount: list.length
        };
        me.action('saveComposeMsg', param, function (result) {
            console.log(result);
            me.close();
            window.showTopMsg("Mensagem enviada com sucesso!", 4000);
        }, function (a, b) {
            me.close();
            window.showTopErrorMsg('Ocorreu um erro. A Mensagem não foi enviada.', 4000);
        });
    };
    me.close = function () {
        me.clean();
        me.df__toggleVisibility(false, false);
    };
    me.open = function (param, apply) {
        if (param == null)
            return;

        me.clean();

        if (Array.isArray(param)) {
            param.forEach(function (item) {
                me.addUser(item);
            });
        } else {
            me.addUser(param);
        }
        me.df__toggleVisibility(true, apply);
    };
    me.clean = function () {
        me.setEditorContent(me.inputID, '');
        me.title = '';
        me.cleanSearch();

    };
    me.getText = function () {
        return me.getEditorContent(me.inputID);
    };
    me.getEditorContent = function (id, format) {
        var _format = (typeof (format) == 'undefined') ? 'html' : format;

        var editor = me.getEditorById(id);
        if (editor != null) {
            var html = editor.getContent({ format: _format });
            html = html.replace(new RegExp('<span class="AMedit">`', 'g'), '<span class="AM">`');
            return html;
        } else {
            return "";
        }
    };
    me.getEditorById = function (id) {
        var k = $(tinymce.editors).filter(function (index, item) { return item.id == id });
        if (k != null) {
            return k[0];
        } else {
            return null;
        }
    };
    me.setEditorContent = function (id, content) {
        var editor = me.getEditorById(id);
        if (editor != null) {
            editor.setContent(content);
        }
    };

    me.isValidadeForm = function () {
        var title = me.checkTitle();
        var body = me.checkBody();

        if (!title || !body) {
            return false;
        } else {
            return true;
        }
    }
    me.checkTitle = function () {
        var elem = angular.element('[ng-model="title"');

        if (me.title.length == 0) {
            elem.addClass('req');
            return false;
        } else {
            elem.removeClass('req');
            return true;
        }
    };
    me.checkBody = function () {

        var elem = angular.element('.mce-tinymce');

        if (me.getText().length == 0) {
            elem.addClass('req');
            return false;
        } else {
            elem.removeClass('req');
            return true;
        }
    };

});
ILangApp.directive('composeMessage', function () {
    return {
        replace: true,
        restrict: 'E',
        //templateUrl: getStaticUrl('/Templates/ComposeMessage/Default.html'),
        templateUrl: function () { return getStaticUrl('/Templates/ComposeMessage/Default.html')},
        link: function ($scope, $element, $attrs) {
            var input = $($element).find('textarea');
            $scope.inputID = 'composeMsgInput_' + $scope.$id
            input.attr('id', $scope.inputID);
            window.setTimeout(function () {
                InitializeTinyMCEStudentMode($scope.inputID);
            }, 1000);

        }
    };
});

function openComposeMesageDialog(param, apply) {
    var scope = getAngularScopeByElementId('composeMessageDialog');
    if (isSafeToUse(scope, 'open')) {
        scope.open(param, apply == null ? true : apply);
    }
}

//###################################################
//                  F I M   M E S S A G E
//###################################################
