﻿

//####################################################
//                   COMMON FUNCTIONS ->
//####################################################
window.callSupport = function (text, encryptedUserOrganizationID) {
    var tinyMCECore = ILangSettings.Sites.Static() + "/src.jq/tinymce/tinymce.min.js?v=" + ILangSettings.Version;
    var ilangTinyMCE = ILangSettings.Sites.Static() + "/src.jq/tinymce/ilangtinymce.js?v=" + ILangSettings.Version;
    var AsciiMath = ILangSettings.Sites.Static() + "/src.jq/tinymce/plugins/asciimath/js/ASCIIMathMLwFallback.js?v=" + ILangSettings.Version;

    var _supportTicketDialogController = getAngularScopeByElementId('supportTicketDialogController')
    if (_supportTicketDialogController != null)
        _supportTicketDialogController.openDialog(encryptedUserOrganizationID);

    require([tinyMCECore, ilangTinyMCE, AsciiMath], function () {
        var __supportTicketDialogController = getAngularScopeByElementId('supportTicketDialogController');
        if (__supportTicketDialogController != null) {
            __supportTicketDialogController.scriptsLoaded = true;
            __supportTicketDialogController.$apply();
        }

    });
};
(function () {

    window.tryFlash = function (params) {
        try {

            if (Alert.getBody() && $(Alert.getBody()).parents('div.alert').find('p[bodyalert=need_flash]')) {
                return;
            }

            var minVersion = params.minVersion || 9;
            var flash = Browser.Plugins.Flash || { version: 0, build: 0 };
            if (flash.version < minVersion) {
                var message = ILang.Resources.Messages.NeedFlash;
                if (message) {
                    Alert(message);
                    var alertBody = document.createElement('p');
                    Alert.getBody().after(alertBody);
                    Alert.getBody().after(document.createElement('br'));
                    $(alertBody).attr({ 'bodyalert': 'need_flash' });
                    var clickHere = document.createElement('a');
                    $(clickHere).attr({
                        bodyalert: 'need_flash',
                        href: 'javascript:void(0);',
                        text: ILang.Resources.Messages.ClickHereToInstall,
                        'events': {
                            'click': function () {
                                var flash = window.open('http://get.adobe.com/flashplayer/', 'getflash');
                                try { flash.focus } catch (ex) { }
                            }
                        }
                    });
                    $(alertBody).append(clickHere);
                }
            }
        }
        catch (_err) {
            return;
        }
    };

}());
// Gera uma sequencia de caracteres aleatorios 
function _jsguid(len) {
    var len = len || $.Random(8, 13);
    function getChar() { return String.fromCharCode($.Random(65, 90)); } //A-Z
    function getNumber() { return String.fromCharCode($.Random(48, 57)); } //0-9
    var guid = '';
    for (var i = 0; i < len; i++) guid += $.Random(0, 1) ? getChar() : getNumber();
    return guid;
}
jQuery.Random = function (m, n) {
    m = parseInt(m);
    n = parseInt(n);
    return Math.floor(Math.random() * (n - m + 1)) + m;
}


//WriteEmail
var writeEmailDialogClientID = null;

function WriteEmail(query, lquery, _setPosition) {
    _setPosition = _setPosition == null ? true : _setPosition;
    try {
        var url = ILangSettings.Sites.ILang() + '/Controls/Community/ComposeMessageIFrame.aspx';

        if (query != undefined && query != '') {
            if (query.indexOf(';') > 0)
                url = url + '?UserIdList=' + query;
            else
                url = url + '?UserId=' + query;

            if (lquery != undefined && lquery != '')
                url = url + '&ULID=' + lquery;
        }
        else
            if (lquery != undefined && lquery != '')
                url = url + '?ULID=' + lquery;

        $('#divIFrame').html("<iframe id='iMessage' src='" + url + "' width='100%' height='100%' frameborder='0' marginheight='0' marginwidth='0' scrolling='no' style='display:none' onload='WriteEmailIFrameLoaded();'></iframe>");
        if (!$('divIFrame').hasClass('containeriframe')) {
            $('divIFrame').addClass('containeriframe');
        }

        if (_setPosition == true)
            SetPosition(1, '#' + writeEmailDialogClientID);

        Show('#' + writeEmailDialogClientID);
    }
    catch (_err1) {
        return _err1;
    }
}

function CloseWriteEmailDialog() {
    Hide(writeEmailDialogClientID);
}

sentMessageDialogClientID = null;
function ShowSentMessageDialog() {
    CloseWriteEmailDialog();
    if (typeof (AfterSendMessage) != 'undefined') {
        AfterSendMessage();
    }
    SetPosition(1, sentMessageDialogClientID);
    Show(sentMessageDialogClientID);
    window.setTimeout(HideSentMessageDialog, 3000);
    return false;
}

function HideSentMessageDialog() {
    Hide(sentMessageDialogClientID);
    return false;
}

function WriteEmailIFrameLoaded() {
    $('#iMessage').css('display', 'block');
    $('#divIFrame').removeClass('containeriframe');
}

function Show(divId) {

    if (typeof divId != 'string')
        divId = $(divId).attr('id');

    if (divId[0] != '#')
        divId = '#' + divId;

    $(divId).css('display', '');
    if (DialogOverlay) {
        DialogOverlay.show(divId);
    }
}

function Hide(divId) {

    if (typeof divId != 'string')
        divId = $(divId).attr('id');

    if (divId[0] != '#')
        divId = '#' + divId;

    $(divId).css('display', 'none');
    if (DialogOverlay) {
        DialogOverlay.hide(divId);
    }
}

function SetPosition(position, divId) {

    if (typeof divId != 'string')
        divId = $(divId).attr('id');

    if (divId[0] != '#')
        divId = '#' + divId;

    if (position == 0) {
    }
    else if (position == 1) {
        $(divId).css("position", "absolute");
        $(divId).css("top", Math.max(0, (($(window).height() - $(divId).outerHeight()) / 2) + $(window).scrollTop()) + "px");
        $(divId).css("left", Math.max(0, (($(window).width() - $(divId).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    }
    else if (position == 2) {
    }
    else if (position == 3) {
        var div = $('#' + divId);
        div.css('top', '0px');
        div.css('left', '0px');
    }
    else if (position == 4) {
    }
    else if (position == 5) {
        $(divId).css('display', 'inline');
        var divBounds = Sys.UI.DomElement.getBounds($(divId));
        $(divId).css('display', 'none');
        var hscroll = document.documentElement.scrollLeft;
        var left = Math.round(hscroll + ((document.documentElement.clientWidth / 2) - (divBounds.width / 2)));
        if (left < 0) {
            left = 0;
        }
        $(divId).css('left', left + 'px');
        $(divId).css('top', '0px');
    }
}

function setTinyMCEIFrameHeightAfterLoad(height, iFrameUrl) {
    var iframe = $('iframe[src="' + iFrameUrl + '"]');
    if (iframe != null) {
        height += 20;
        iframe.css('height', height.toString() + 'px');
        iframe.parent().find('div.bgLoading').attr('class', '');
    }
}

function scrollTinyMCEIframeToTopParent(iFrameUrl) {
    var iframe = $('iframe[src="' + iFrameUrl + '"]');

    if (iframe != null) {
        $([document.documentElement, document.body]).animate({
            scrollTop: iframe.offset().top
        }, 200);
    }
}
//####################################################
//                   UTIL ->
//####################################################
Array.prototype.where = function (property, value) {
    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (item[property] == value)
            a.push(item);
    }

    return a;
};
Array.prototype.first = function (obj) {
    if (obj == null && this.length > 0) {
        return this[0];
    } else if (typeof (obj) == 'string') {
        return this[0][obj];
    }


    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        var b = true;
        for (k in obj) {
            b = b && item[k] == obj[k]
        }
        if (b)
            return item;
    }

    return null;
};
Array.prototype.firstOrFirst = function (obj) {
    var item = this.first(obj);
    if (item != null)
        return item;

    if (item == null && this.length > 0) {
        return this[0];
    } else {
        return null;
    }
}
Array.prototype.last = function (obj) {
    if (obj == null && this.length > 0) {
        return this[this.length - 1];
    } else if (typeof (obj) == 'string') {
        return this[this.length - 1][obj];
    }


    for (var i = this.length - 1; i >= 0; i--) {
        var item = this[i];
        var b = true;
        for (k in obj) {
            b = b && item[k] == obj[k]
        }
        if (b)
            return item;
    }

    return null;
};
Array.prototype.any = function (obj) {
    var a = false;
    this.generalIf(obj, function (item) { a = true; });
    return a;
};
Array.prototype.equals = function (obj) {
    var a = [];
    this.generalIf(obj, function (item) { a.push(item); });
    return a;
};
Array.prototype.generalIf = function (obj, fn, result) {
    result = result == null ? true : false;
    if (typeof (obj) == 'function') {
        for (var i = 0; i < this.length; i++) {
            var aaa = obj(this[i]);
            if (aaa)
                fn(aaa);
        }
    } else if (typeof (obj) == 'number' || typeof (obj) == 'string') {
        for (var i = 0; i < this.length; i++) {
            fn(this[i]);
        }
    } else {
        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            if (typeof (item) != 'function') {
                var b = true;
                for (k in obj) {
                    if (typeof (obj[k]) == 'object' && obj[k].hasOwnProperty('_fntype')) {
                        if (obj[k]._fntype == 'OR') {
                            var c = false;
                            for (e in obj[k].values) {
                                var _item = obj[k].values[e];
                                if (typeof (_item) != 'function')
                                    c = c || item[k] == _item;
                            }
                            b = b && c;
                        } else if (obj[k]._fntype == 'BiggerThan') {
                            b = b && item[k] > obj[k].values;
                        } else if (obj[k]._fntype == 'BiggerOrEqualThan') {
                            b = b && item[k] >= obj[k].values;
                        } else if (obj[k]._fntype == "IsNull") {
                            b = b && item[k] == null;
                        }
                    } else {
                        if (k.indexOf('.') > 0) {
                            var kk = k.split('.');
                            for (var kki = 0; kki < kk.length; kki++) {
                                item = item[kk[kki]];
                            }
                            b = b && item == obj[k]
                        } else {
                            b = b && item[k] == obj[k]
                        }

                    }

                }
                if (b == result)
                    fn(item);
            }
        }
    }

}
Array.prototype.textContains = function (where, what) {
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (typeof (item) == 'object') {
            var b = true;
            for (k in where) {
                var wupper = where[k].toUpperCase();
                b = b && (item[k].toString().indexOf(where[k]) >= 0 || item[k].toString().toUpperCase().indexOf(wupper) >= 0);
            }
            if (b) {
                if (typeof (what) == 'function') {
                    what(item);
                } else if (typeof (what) == 'object') {
                    for (n in what) {
                        item[n] = what[n];
                    }
                }
            }
        }
    }
    return this;
}
Array.prototype.hasText = function (obj) {
    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        for (var k in obj) {
            var y = true;
            if (item.hasOwnProperty(k) && item[k] != null) {
                y = y && item[k].toUpperCase().indexOf(obj[k].toUpperCase()) >= 0;
            }
        }
        if (y)
            a.push(item);
    }
    return a;
};
Array.prototype.hasTextNA = function (obj) {
    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        for (var k in obj) {
            var y = true;
            if (item.hasOwnProperty(k) && item[k] != null) {
                y = y && item[k].toUpperCase().indexOf(obj[k].toUpperCase()) >= 0;
            }
        }
        if (y)
            a.push(item);
    }
    return a;
};
Array.prototype.notEquals = function (obj) {

    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        var b = true;
        for (k in obj) {
            b = b && item[k] != obj[k]
        }
        if (b)
            a.push(item);
    }
    return a;
}
Array.prototype.select = function (obj) {
    if (obj == undefined || obj == null || obj.length == 0)
        return [];

    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        var n = {};
        if (Array.isArray(obj)) {
            for (var j = 0; j < obj.length; j++) {
                if (typeof (obj[j]) == 'string') {
                    if (item.hasOwnProperty(obj[j]))
                        n[obj[j]] = item[obj[j]];
                } else if (typeof (obj[j]) == 'object') {
                    var key = Object.keys(obj[j])[0];
                    var keyToAdd = obj[j][key];
                    if (item.hasOwnProperty(key))
                        n[keyToAdd] = item[key];
                }
                
            }
            a.push(n);
        } else if (typeof (obj) == 'string') {
            if (item.hasOwnProperty(obj)) {
                a.push(item[obj]);
            }

        } else {
            for (k in obj) {
                if (typeof (obj[k]) == 'function') {
                    n[k] = obj[k](item);
                } else if (typeof (obj[k]) == 'number') {
                    n[k] = obj[k];
                }
                else {
                    n[k] = item[obj[k]];
                }
            }
            a.push(n);
        }
    }

    return a;
}
Array.prototype.selectToArray = function (obj) {
    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (item.hasOwnProperty(obj)) {
            a.push(item[obj]);
        }
    }

    return a;
}
Array.prototype.count = function (obj) {
    if (obj == undefined || obj == null)
        return this.length;

    var a = 0;
    this.generalIf(obj, function (item) { a++; });
    return a;
}
Array.prototype.remove = function (obj) {
    if (typeof (obj) == 'object') {
        while (this.any(obj)) {
            for (var i = 0; i < this.length; i++) {
                var item = this[i];
                var b = true;

                for (k in obj) {
                    b = b && item[k] == obj[k]
                }


                if (b)
                    this.splice(i, 1);

            }
        }
    } else {
        var i = this.indexOf(obj);
        if (i >= 0) {
            this.splice(i, 1);
        }
    }

    return this;
}
Array.prototype.removeMany = function (list, index) {
    if (list == null)
        return;
    if (index == null) {
        index = 0;
    }
    if (list.count == 0 || list.length < index)
        return;

    this.remove(list[index]);
    index++;
    return this.removeMany(list, index);


}
Array.prototype.set = function (fn) {
    if (typeof (fn) == 'function') {
        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            fn(item);
        }
    } else if (typeof (fn) == 'object') {
        for (var i = 0; i < this.length; i++) {
            var item = this[i]
            for (o in fn) {
                item[o] = fn[o];
            }
        }
    }
}
Array.prototype.setTo = function (where, what) {
    this.generalIf(where, function (item) {
        if (typeof (what) == 'function') {
            what(item);
        } else if (typeof (what) == 'object') {
            for (n in what) {
                item[n] = what[n];
            }
        }
    });

    return this;
}
Array.prototype.max = function (prop) {
    if (this.length == 0)
        return null;

    var m = null;
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (m == null || item[prop] > m)
            m = item[prop];
    }
    return m;
}
Array.prototype.min = function (prop) {
    if (this.length == 0)
        return null;

    var m = null;
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (m == null || item[prop] < m)
            m = item[prop];
    }
    return m;
}
Array.prototype.objWithMin = function (prop) {
    var min = this.min(prop);
    var t = {};
    t[prop] = min;
    return this.first(t);
};
Array.prototype.objWithMax = function (prop) {
    var max = this.max(prop);
    var k = {};
    k[prop] = max;
    return this.first(k);
};
Array.prototype.take = function (itemsTobeTaken, first) {
    first = (first == undefined || first == null) ? 0 : first;
    return this.slice(first, itemsTobeTaken);
}
Array.prototype.skip = function (length) {
    var a = [];
    for (var i = length; i < this.length; i++) {
        a.push(this[i]);
    }
    return a;
}
Array.prototype.countDistinct = function (prop) {
    var k = {};
    var count = 0;
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (item && !k.hasOwnProperty(item[prop])) {
            k[item[prop]] = 1;
            count++;
        }
    }
    return count;
};
Array.prototype.distinct = function (prop) {
    if (this.length == 0)
        return this;

    var k = {};
    var a = [];
    if (prop == null) {
        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            if (item && !k.hasOwnProperty(item)) {
                k[item] = 1;
                a.push(item);
            }
        }
    } else {
        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            if (item && !k.hasOwnProperty(item[prop])) {
                k[item[prop]] = 1;
                a.push(item);
            }
        }
    }

    return a;
}
Array.prototype.Filter = function (fn) {
    if (this == undefined || this == null || this.length == 0 || typeof (fn) != 'function')
        return null;
    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        if (fn(item, i, a))
            a.push(item);
    }
    return a;
}
Array.prototype.findMany = function (fn, names) {
    if (this == undefined || this == null || this.length == 0)
        return null;

    var a = [];
    for (var i = 0; i < this.length; i++) {
        var item = fn(this[i]);
        if (item != null) {
            if (Array.isArray(names) == true && names.length > 0) {
                var t = {};
                t[names[0]] = this[i];
                t[names[1]] = item;

                a.push(t);
            } else {
                a.push(item);
            }
        }

    }
    return a;
}
Array.prototype.union = function (array2) {
    if (!Array.isArray(array2))
        return this;

    for (var i = 0; i < array2.length; i++) {
        this.push(array2[i]);
    }
    return this;
}
Array.prototype.addRange = function (array2) {
    if (!Array.isArray(array2))
        return this;

    for (var i = 0; i < array2.length; i++) {
        this.push(array2[i]);
    }
    return this;
}
Array.prototype.removeDuplicate = function (array2, fnItem) {
    if (array2 == null || array2 == [] || array2.length == 0 || typeof (fnItem) != 'function')
        return this;
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        var didNotFindEqual = true;
        var thisValue = fnItem(this[i]);
        for (var k = 0; k < array2.length; k++) {
            didNotFindEqual = didNotFindEqual && thisValue != fnItem(array2[k]);
        }
        if (didNotFindEqual) {
            arr.push(this[i]);
        }
    }
    return arr;
};
Array.prototype.include = function (array2) {
    return this.union(array2);
};
Array.prototype.includeBefore = function (array2) {
    if (array2 == null)
        return this;
    if (!Array.isArray(array2) && typeof (array2) == 'object') {
        array2 = [array2];
    }

    for (var i = 0; i < array2.length; i++) {
        this.unshift(array2[i]);
    }
    return this;
};
Array.prototype.sumProduct = function (array2) {
    if (this == null || this.length == 0)
        return 0;
    if (Array.isArray(array2)) {
        if (array2.length != this.length)
            return 0;

        var s = 0;
        for (var i = 0; i < this.length; i++) {
            s += parseFloat(this[i]) * parseFloat(array2[i]);
        }
        return s;
    } else if (typeof (array2) == 'number') {
        var s = 0;
        for (var i = 0; i < this.length; i++) {
            s += parseFloat(this[i]) * array2;
        }
        return s;
    }

}
Array.prototype.sum = function (prop) {
    if (this == null || this.length == 0)
        return 0;

    var s = 0;
    if (prop == null) {
        for (var i = 0; i < this.length; i++) {
            s += parseFloat(this[i]);
        }
    } else {
        for (var i = 0; i < this.length; i++) {
            if (this[i][prop] != null) {
                if (typeof (this[i][prop]) == 'function') {
                    s += parseFloat(this[i][prop]() || 0);
                } else if (isNaN(this[i][prop]) == false) {
                    s += parseFloat(this[i][prop]);
                }

            }

        }
    }

    return s;
};
Array.prototype.avg = function (prop) {
    var c = this.length;
    if (c > 0) {
        return this.sum(prop) / c;
    }
    return 0;
}
Array.prototype.orderBy = function (prop) {
    if (this == null || this.length == 0)
        return this;

    if (prop == null)
        return this.sort(function (a, b) { return a - b });

    if (isNaN(this[0][prop])) {
        return this.sort(function (a, b) {
            return a[prop].localeCompare(b[prop]);
        });
    } else {
        return this.sort(function (a, b) {
            return a[prop] - b[prop];
        });
    }


};
Array.prototype.orderDescBy = function (prop) {
    if (this == null || this.length == 0)
        return this;

    if (prop == null)
        return this;


    if (isNaN(this[0][prop])) {
        return this.sort(function (a, b) {
            return b[prop].localeCompare(a[prop]);
        });
    } else {
        return this.sort(function (a, b) {
            return b[prop] - a[prop];
        });
    }

};
Array.prototype.compare = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}
Array.prototype.convertToInt = function (prop) {
    if (this.length == 0 || typeof (prop) == 'object')
        return

    if (prop == null) {
        for (var i = 0; i < this.length; i++) {
            this[i] = parseInt(this[i]);
        }
        return this;
    }
    if (typeof (prop) == 'string') {
        this.foreach(function (item) { item[prop] = parseInt(item[prop]); });
        return this;
    }

}
Array.prototype.groupBy = function (prop) {
    var k = [];
    for (var i = 0; i < this.length; i++) {
        var item = this[i];
        var search = {};

        var index = k.first({ group: item[prop] });
        if (index != null) {
            index.values.push(item);
        } else {
            var a = { group: item[prop], values: [] };
            a.values.push(item);
            k.push(a);
        }
    }
    return k;
}
Array.prototype.contains = function (array2, prop) {
    if (this.length == 0 || array2.length == 0 || array2[0].hasOwnProperty(prop) == false)
        return [];
    var a = [];
    var k = 0;
    for (var i = 0; i < this.length; i++) {
        if (array2.indexOf(this[i][prop]) >= 0) {
            a[k] = this[i];
            k++;
        }
    }
    return k
}
if (Object.getOwnPropertyDescriptor(Array.prototype, 'foreach') == undefined) {
    Object.defineProperty(Array.prototype, "foreach", {
        enumerable: false,
        value: function (fn) {
            if (this == null || typeof (fn) != 'function')
                return;

            for (var i = 0; i < this.length; i++) {
                fn(this[i], i);
            }
        }
    });
}
if (Object.getOwnPropertyDescriptor(Object.prototype, 'everyEach') == undefined) {
    Object.defineProperty(Object.prototype, "everyEach", {
        enumerable: false,
        value: function (fn) {
            if (this == null || typeof (fn) != 'function')
                return;
            var keys = Object.keys(this);
            for (var i = 0; i < keys.length; i++) {
                fn(this[keys[i]], keys[i])
            }
        }
    });
}
//####################################################
//               String ->
//####################################################
String.prototype.last = function () {
    if (this == null || this == '' || typeof (this) != 'object')
        return null;
    return this[this.length - 1]
}
String.prototype.first = function () {
    if (this == null || this == '' || typeof (this) != 'object')
        return null;
    return this[0];
}
var Latinise = {}; Latinise.latin_map = { "Á": "A", "Ă": "A", "Ắ": "A", "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A", "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A", "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A", "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A", "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A", "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A", "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE", "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU", "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B", "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C", "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C", "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D", "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ", "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E", "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E", "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E", "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET", "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G", "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H", "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I", "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I", "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I", "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S", "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K", "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K", "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L", "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L", "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N", "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N", "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O", "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O", "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O", "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O", "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E", "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q", "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R", "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S", "Ṣ": "S", "Ṩ": "S", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T", "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U", "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U", "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U", "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W", "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y", "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE", "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R", "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N", "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a", "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a", "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a", "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae", "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b", "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c", "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d", "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j", "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e", "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e", "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e", "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g", "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h", "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h", "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i", "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i", "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j", "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k", "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l", "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l", "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m", "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n", "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n", "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o", "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o", "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o", "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o", "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o", "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o", "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o", "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p", "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q", "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r", "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r", "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s", "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t", "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t", "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k", "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y", "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u", "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u", "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v", "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w", "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y", "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z", "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff", "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e", "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x" };
String.prototype.removeAccent = function () { return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) { return Latinise.latin_map[a] || a }) };
String.prototype.startWith = function (txt) {
    if (txt == null || txt == '')
        return false;

    return this.substring(0, txt.length).toLowerCase() == txt.toLowerCase();
}
String.prototype.format = function () {
    if (this == null || this.trim() == '')
        return '';

    var txt = this;
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] != null) {
            txt = txt.replace('{' + i + '}', arguments[i]);
        }
    }
    return txt;
}
String.prototype.trimLeft = function () {
    if (this == '')
        return this;
    var txt = this;
    while (this.first() == ' ') {
        txt = this.substring(1);
    }
    return txt.toString();
}
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
String.prototype.countOccurrencesOf = function (search) {
    var g = this.match(new RegExp(search, 'g'));
    return (g || []).length;
}

//####################################################
//               Common Function ->
//####################################################
function isSafeToUse(obj, prop) {
    try {
        if (obj == undefined || obj == null || prop == null || prop == '')
            return false;

        if (prop.indexOf('.') >= 0) {
            var s = prop.split('.');
            var k = obj;
            for (var i = 0; i < s.length; i++) {
                if (typeof (k[s[i]]) != 'undefined' && k[s[i]] != null) {
                    k = k[s[i]];
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return typeof (obj[prop]) != 'undefined' && obj[prop] != null;
        }
    } catch (e) {
        return false;
    }

}
function isNotSafeToUse(obj, prop) {
    return isSafeToUse(obj, prop) === false;
}
function compareIfSafe(obj, prop, value) {
    try {
        if (typeof (obj) == 'undefined')
            return false;

        var item = getPropertyByName(obj, prop);
        return item != null ? item == value : false;
    } catch (e) {
        return false;
    }

}
function getPropertyByName(obj, prop, runFunction) {
    try {
        if (prop == '' || prop == null || prop == undefined) {
            if (obj != undefined && obj != null) {
                return obj;
            } else {
                return null;
            }
        }
        if (isSafeToUse(obj, prop)) {
            if (prop.indexOf('.') >= 0) {
                var s = prop.split('.');
                var k = obj;
                for (var i = 0; i < s.length; i++) {
                    if (k.hasOwnProperty(s[i])) {
                        k = k[s[i]];
                    } else {
                        return null;
                    }
                }
                return k;
            } else {
                if (typeof (obj[prop]) == 'function' && (runFunction == null || runFunction == true)) {
                    return obj[prop]();
                } else {
                    return obj[prop];
                }
            }
        }
        return null;
    } catch (e) {
        return null;
    }
};
function getPropertyByNameOrDefault(obj, prop, defaultValue) {
    try {
        if (prop == '' || prop == null || prop == undefined) {
            if (obj != undefined && obj != null) {
                return obj;
            } else {
                return defaultValue;
            }
        }
        if (isSafeToUse(obj, prop)) {
            if (prop.indexOf('.') >= 0) {
                var s = prop.split('.');
                var k = obj;
                for (var i = 0; i < s.length; i++) {
                    if (k.hasOwnProperty(s[i])) {
                        k = k[s[i]];
                    } else {
                        return defaultValue;
                    }
                }
                return k;
            } else {
                return obj[prop];
            }
        }
        return defaultValue;
    } catch (e) {
        return null;
    }
};
function setIfSafe(obj, prop, value) {
    try {
        if (obj == undefined || obj == null || prop == null || prop == '')
            return false;

        if (prop.indexOf('.') >= 0) {
            var s = prop.split('.');
            var k = obj;
            for (var i = 0; i < s.length - 1; i++) {
                if (typeof (k[s[i]]) != 'undefined' && k[s[i]] != null) {
                    k = k[s[i]];
                } else {
                    return false;
                }
            }
            if (typeof (k[s.last()]) != 'undefined') {
                k[s.last()] = value;
                return true;
            } else {
                return false;
            }

        } else {
            if (typeof (obj[prop]) != 'undefined') {
                obj[prop] = value;
                return true;
            }
            return false;
        }
    } catch (e) {
        return false;
    }
}
function valueIfUndefined(prop, value) {
    if (prop == undefined || prop == null) {
        return value;
    } else {
        return prop;
    }
}
function falseIfUndefined(prop) {
    return valueIfUndefined(prop, false);
}
function trueIfUndefined(prop) {
    return valueIfUndefined(prop, true);
}
function callIfSafe(obj, fn) {
    var f = getPropertyByName(obj, fn, false);
    if (f != null && typeof (f) == 'function') {
        if (arguments.length > 2) {
            var str = f.toString();
            var param = [];
            for (var i = 2; i < arguments.length; i++) {
                param[i - 2] = arguments[i];
            }
            f.apply(this, param);
        } else {
            f();
        }
    }
}
function singleLookup(obj, value) {
    if (obj == null)
        return null;

    for (i in obj) {
        if (obj[i] == value)
            return i;
    }
    return null;
}
function copyAllPropersTo(base, objToCopy) {
    for (var i in objToCopy) {
        if (typeof (objToCopy[i]) != 'undefined')
            base[i] = objToCopy[i];
    }
};
function copyPropersTo(base, objToCopy, props) {
    for (var i = 0; i < props.length; i++) {
        prop = props[i];
        if (typeof (objToCopy[prop]) != 'undefined')
            base[prop] = objToCopy[prop];
    }
};
function rawCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}
function findFirstArray(obj) {
    for (var i in obj) {
        if (typeof (obj[i]) == 'object' && Array.isArray(obj[i])) {
            return obj[i];
        }
    }
    return null;
}
function objToArray(obj) {
    if (obj == null || Object.keys(obj).length == 0)
        return

    var a = [];
    obj.everyEach(function (item) {
        if (typeof (item) == 'object' || typeof (item) == 'number' || typeof (item) == 'string') {
            a.push(item);
        } else if (typeof (item) == 'function') {
            a.push(item());
        }
    });

    return a;
}
function fullCompare(a, b) {
    var ta = typeof (a);
    var tb = typeof (b);
    if (ta != tb || a.length != b.length)
        return false;

    if (ta == 'number' || ta == 'string') {
        return a == b;
    } else if (ta == 'function') {
        return a() == b();
    } else if (Array.isArray(a) && Array.isArray(b)) {
        var k = true;
        for (var i = 0; i < a.length; i++) {
            k = k && fullCompare(a[i], b[i]);
        }
        return k;
    } else if (ta == 'object') {
        var k = true;
        a.objToArray(function (item, key) { k = k && fullCompare(item, b[key]) })

        return k;
    }

}
function createMockObj(obj, path, value) {
    if (path.indexOf('=') < 0 && value == null)
        return;
    var props = value == null ? path.split('=')[0] : path;
    var value = value == null ? path.split('=')[1] : value;
    if (props.indexOf('.') > 0) {
        var a = props.split('.');
        var item = obj;
        for (var i = 0; i < a.length; i++) {
            var p = a[i].trim();
            if (i == a.length - 1) {
                if (typeof (value) == 'string') {
                    item[p] = eval(value.trim());
                } else {
                    item[p] = eval(value);
                }

            } else {
                if (!item.hasOwnProperty(p))
                    item[p] = {};
                item = item[p];
            }
        }
    } else {
        obj[props.trim()] = eval(value.trim());
    }
    return obj;
}
GetBrowserName = function () {
    var N = navigator.appName, ua = navigator.userAgent, tem;
    var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    return M ? M[1] + ' ' + M[2] : N + ' ' + navigator.appVersion + '-?';
}
function OR(arg) { return { _fntype: 'OR', values: arg }; }
function BiggerThan(arg) { return { _fntype: 'BiggerThan', values: arg }; };
function BiggerOrEqualThan(arg) { return { _fntype: 'BiggerOrEqualThan', values: arg }; };
function IsNull(arg) { return { _fntype: 'IsNull', values: arg }; };
function checkVisible(elm, evalType) {
    evalType = evalType || "visible";

    var vpH = $(window).height(), // Viewport Height
        st = $(window).scrollTop(), // Scroll Top
        y = $(elm).offset().top,
        elementHeight = $(elm).height();

    if (evalType === "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
    if (evalType === "above") return ((y < (vpH + st)));
}
function isScrolledIntoView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    //return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    return elemTop <= docViewBottom && elemTop >= docViewTop || (elemTop < docViewTop && elemBottom > docViewBottom);
}
function isInsideView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    //return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    return elemTop >= docViewTop && elemBottom <= docViewBottom;
}
function isPartialOnView(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    //return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    return elemTop >= docViewTop || elemBottom <= docViewBottom || (elemTop < docViewTop && elemBottom > docViewBottom);
}
function isAboveTheScreen(elem) {
    var $elem = $(elem);
    var $window = $(window);


    var docViewTop = $window.scrollTop();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return elemTop < docViewTop && elemBottom < docViewTop;
}
function isBelowTheScreen(elem) {
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return elemTop > docViewBottom && elemBottom > docViewBottom;
}
function RegisterNamespace(namespace) {
    var names = namespace.split('.');
    var objectstring = 'window';
    var current = null;
    for (var index = 0; index < names.length; index++) {
        objectstring = objectstring + '.' + names[index];
        if (eval('typeof ' + objectstring) == 'undefined') {
            eval(objectstring + ' = new Object();');
        }
        current = eval(objectstring);

    }
    return current;
}
$(function () {
    var _id = 'fileViewerFrame';
    var iframe;
    var em;
    var div;
    if ($('#' + _id).length == 0) {
        div = $('<div></div>', { 'id': _id, 'Class': 'ilIframeViewer' });

        iframe = $('<iframe></iframe>', {});
        iframe.appendTo(div);

        em = $('<em/>', { 'Class': 'ilIframeClose' });
        em.click(function () {
            div.removeClass('ilIframeVisible');
        });
        em.appendTo(div);
        if ($('.pageContent').length > 0) {
            $('.pageContent').append(div);
        } else {
            $('.page-content').append(div);
        }
        
    }
});
window.openDocumentOnViewer = function (file) {

    logDocumentViewer(file);

    if (ILangSettings.IsFlutterApp) {

        window.flutter_inappwebview.callHandler('downloadFile', file);

    }
    else if (ILangSettings.IsAndroidApp || ILangSettings.IsIosApp) {

        const link = document.createElement("a");
        link.style.display = "none";
        link.href = file;

        document.body.appendChild(link);
        link.click();

        link.remove();
    }
    else {

        window.open(file, '_blank');
    }
}

function logDocumentViewer(file) {
    try {
        $.ajax({
            url: ILangSettings.Sites.API() + '/Log/DocumentViewer',
            data: { documentUrl: file },
            type: 'POST',
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            }
        });
    } catch (e) {
    }
}
window.feedHtmlOnLoadFn = function (feedObj) {
    if (typeof (feedObj) == 'object') {
        var _href = getPropertyByName(feedObj, 'location.href');
        if (_href != null && _href.indexOf('?') > 0) {
            var qs = _href.split('?')[1].split('&');
            for (var i = 0; i < qs.length; i++) {
                var item = qs[i].split(':');
                var actionId = item[0];

                switch (actionId) {
                    case "0":
                        var selector = item[1];
                        selector = selector.replaceAll('_', ' ');
                        var content = item[2];

                        if (item.length == 4) {
                            //action:selector:content:index
                            var index = item[3];
                            $(feedObj.document).find(selector).eq(index).addClass(content);
                        } else {
                            //action:selector:content
                            $(feedObj.document).find(selector).addClass(content);
                        }

                        break;
                    case "1":
                        //action:fnName
                        var fnName = item[1];
                        if (isSafeToUse(window, fnName)) {
                            window[fnName](feedObj);
                        }
                        break;
                    default:

                }

            }
        }
    }

}
window.feedHtml_teste_teste = function (feedObj) {
    var a = $('<h1>uepaaaa</h1>');
    $(feedObj.document.body).append(a);
}
//####################################################
//               Session Storage ->
//####################################################
var verifySessionStorage = function () {
    try {
        localStorage.setItem('i_v_s_t', 1);
        localStorage.removeItem('i_v_s_t');
        Storage.prototype.isEnabled = function () { return true; };
    } catch (e) {
        Storage.prototype._setItem = Storage.prototype.setItem;
        Storage.prototype.setItem = function () { };
        Storage.prototype._getItem = Storage.prototype.getItem;
        Storage.prototype.getItem = function () { return null; };
        Storage.prototype.isEnabled = function () { return false; };
    }
};
verifySessionStorage();

//####################################################
//                   TESTE LinqToJavaescrito ->
//####################################################
var linqToJavaescritoUniTest = function () {
    var me = this;
    me.testObj = function (id, name, prop1, prop2, compProp) {
        this.id = id;
        this.name = name || 'name ' + id;
        this.prop1 = prop1 || 'prop1 ' + id;
        this.prop2 = prop2 || 'prop2 ' + id;
        this.compProp = compProp || { a: id, b: Math.floor(100 * Math.random()), c: Math.floor(100 * Math.random()) };
    };
    me.getNumberArray = function () {
        return [17, 11, 83, 8, 9, 38, 24, 27, 13, 99];
    };
    me.getObjArray = function (size) {
        var s = size || 10;
        var list = [];
        for (var i = 0; i < s; i++) {
            list.push(new me.testObj(i));
        }
        return list;
    };
    me.getObj = function (id, name, prop1, prop2, compProp) {
        return new me.testObj(id, name, prop1, prop2, compProp);
    }
    me.getObjId = function (id, compObj) {
        //var obj = me.getObj(id, 'name ' + id, 'prop1 ' + id, 'prop2 ' + prop2);
        return new me.testObj(id, null, null, null, compObj || { a: id });
    }
    me.getSequence = function (start, end, step) {
        var list = [];
        if (end == undefined || end == null) {
            end = start
            start = 0;
        }
        step = (step == undefined || step == null) ? 1 : step;
        for (var i = start; i <= end; i += step) {
            list.push(i);
        }
        return list;
    }
};
var testeLinqToJavaescrito = function () {
    var me = this;
    me.testes = [];
    var ltjTest = function (description, fn, errorMsg) {
        this.description = description;
        this.fn = fn;
        this.tested = null;
        this.result = null;
        this.error = null;
        this.errorMsg = errorMsg || '';
    };
    me.subscribribeTestes = function () {
        me.testes = [];
        me.testes.push(new ltjTest('Where_FindResult', function (list) {
            return list.where('id', 5).length == 1;
        }));
        me.testes.push(new ltjTest('Where_NotFindResult', function (list) {
            return list.where('id', 500).length == 0;
        }));
        me.testes.push(new ltjTest('First_FindResult', function (list) {
            var item = list.first({ id: 3 });
            return item.id == 3 && item.name == "name 3";
        }));
        me.testes.push(new ltjTest('First_NotFindResult', function (list) {
            return list.first({ id: 999 }) == null;
        }));
        me.testes.push(new ltjTest('First_ListWithEqualValueReturnFIRST_Result', function (list) {
            var t = new linqToJavaescritoUniTest();
            list.push(t.getObj(3, 'copy of 3', 3, 3, {}));
            var item = list.first({ id: 3 });
            return item.id == 3 && item.name == "name 3";
        }));
        me.testes.push(new ltjTest('First_ReturnFirstProperty', function (list) {
            return list.first('id') == 0;
        }));
        me.testes.push(new ltjTest('Last_FindResult', function (list) {
            var item = list.last({ id: 3 });
            return item.id == 3 && item.name == 'name 3';
        }));
        me.testes.push(new ltjTest('Last_ListWithEqualValueReturnLAST_Result', function (list) {
            var t = new linqToJavaescritoUniTest();
            list.push(t.getObj(3, 'copy of 3', 3, 3, {}));
            var item = list.last({ id: 3 });
            return item.id == 3 && item.name == "copy of 3";
        }));
        me.testes.push(new ltjTest('Any_FindResult', function (list) {
            return list.any({ id: 3 }) == true;
        }));
        me.testes.push(new ltjTest('Any_NotFindResult', function (list) {
            return list.any({ id: 999 }) == false;
        }));
        me.testes.push(new ltjTest('Equals_Search_1_Criteria_FindResult', function (list) {
            var items = list.equals({ id: 3 });
            return items.length == 1 && items[0].name == 'name 3'
        }));
        me.testes.push(new ltjTest('Equals_Search_1_Criteria_NOT_FindResult', function (list) {
            var items = list.equals({ id: 9999 });
            return items.length == 0;
        }));
        me.testes.push(new ltjTest('Equals_Search_2_Criteria_FindResult', function (list) {
            var items = list.equals({ id: 3, prop1: 'prop1 3' });
            return items.length == 1 && items[0].name == 'name 3'
        }));
        me.testes.push(new ltjTest('Equals_Search_2_Criteria_NOT_FindResult', function (list) {
            var items = list.equals({ id: 3, prop1: 'prop1 5' });
            return items.length == 0;
        }));
        me.testes.push(new ltjTest('Equals_Search_OR_Criteria_FindResult', function (list) {
            var items = list.equals({ id: OR([1, 5]) });
            return items.length == 2 && items.any({ id: 1 }) && items.any({ id: 5 });
        }));
        me.testes.push(new ltjTest('Equals_Search_OR_Criteria_FindOneResult', function (list) {
            var items = list.equals({ id: OR([1, 9999]) });
            return items.length == 1 && items.any({ id: 1 });
        }));
        me.testes.push(new ltjTest('Equals_Search_OR_Criteria_NOT_FindResult', function (list) {
            var items = list.equals({ id: OR([9999, 55555]) });
            return items.length == 0;
        }));
        me.testes.push(new ltjTest('Equals_Search_BiggerThan_Criteria_FindResult', function (list) {
            var items = list.equals({ id: BiggerThan(3) });
            return items.length == 6;
        }));
        me.testes.push(new ltjTest('Equals_Search_BiggerThan_Criteria_NOT_FindResult', function (list) {
            var items = list.equals({ id: BiggerThan(999) });
            return items.length == 0;
        }));
        me.testes.push(new ltjTest('Equals_Search_BiggerOrEqualThan_Criteria_FindResult', function (list) {
            var items = list.equals({ id: BiggerOrEqualThan(3) });
            return items.length == 7;
        }));
        me.testes.push(new ltjTest('Equals_Search_BiggerOrEqualThan_Criteria_NOT_FindResult', function (list) {
            var items = list.equals({ id: BiggerOrEqualThan(999) });
            return items.length == 0;
        }));
        me.testes.push(new ltjTest('TextContains_FindResult_SetProperty', function (list) {
            list.textContains({ prop1: '4' }, { prop2: 'Prop Alterada' });
            return list.first({ id: 4 }).prop2 == 'Prop Alterada' && list.equals({ prop2: 'Prop Alterada' }).length == 1;
        }));
        me.testes.push(new ltjTest('TextContains_2Parameters_FindResult_SetProperty', function (list) {
            list.textContains({ prop1: '4', name: '4' }, { prop2: 'Prop Alterada' });
            return list.first({ id: 4 }).prop2 == 'Prop Alterada' && list.equals({ prop2: 'Prop Alterada' }).length == 1;
        }));
        me.testes.push(new ltjTest('TextContains_FindResult_Function', function (list) {
            list.textContains({ prop1: '4' }, function (item) { item.prop2 = 'Prop Alterada fn' });
            return list.first({ id: 4 }).prop2 == 'Prop Alterada fn' && list.equals({ prop2: 'Prop Alterada fn' }).length == 1;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseFindSingleProperty', function (list) {
            var obj = { prop1: 1 };
            var result = isSafeToUse(obj, 'prop1');
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseFindComplexProperty', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: 2 }
            };
            var result = isSafeToUse(obj, 'prop2.a');
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindSingleProperty', function (list) {
            var obj = { prop1: 1 };
            var result = isSafeToUse(obj, 'prop999');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindComplexProperty', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: 2 }
            };
            var result = isSafeToUse(obj, 'prop999.a');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindComplexProperty2', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: 2 }
            };
            var result = isSafeToUse(obj, 'prop2.b');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseFindSingleFunction', function (list) {
            var obj = { prop1: function () { return 1; } };
            var result = isSafeToUse(obj, 'prop1');
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseFindComplexFunction', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: function () { return 1; } }
            };
            var result = isSafeToUse(obj, 'prop2.a');
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindSingleFunction', function (list) {
            var obj = { prop1: function () { return 1; } };
            var result = isSafeToUse(obj, 'prop999');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindComplexFunction', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: function () { return 1; } }
            };
            var result = isSafeToUse(obj, 'prop999.a');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindComplexFunction2', function (list) {
            var obj = {
                prop1: 1,
                prop2: { a: function () { return 1; } }
            };
            var result = isSafeToUse(obj, 'prop2.b');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseFindDepth_3_Property', function (list) {
            var obj = {
                a: {
                    b: {
                        c: 1
                    }
                }
            };
            var result = isSafeToUse(obj, 'a.b.c');
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_IsSafeToUseNOTFindDepth_3_Property', function (list) {
            var obj = {
                a: {
                    b: {
                        c: 1
                    }
                }
            };
            var result = isSafeToUse(obj, 'a.c.b');
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_HasPropertyEqual', function (list) {
            var obj = { a: 1 }
            var result = compareIfSafe(obj, 'a', 1);
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_HasPropertyNotEquals', function (list) {
            var obj = { a: 1 }
            var result = compareIfSafe(obj, 'a', 999);
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_NOTHasProperty', function (list) {
            var obj = { a: 1 }
            var result = compareIfSafe(obj, 'b', 1);
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_HasComplexPropertyEqual', function (list) {
            var obj = { a: { b: 1 } }
            var result = compareIfSafe(obj, 'a.b', 1);
            return result == true;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_NOTHasComplexPropertyEqual', function (list) {
            var obj = { a: { b: 1 } }
            var result = compareIfSafe(obj, 'b.a', 1);
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_CompareIfSafe_NOTHasComplexPropertyEqual2', function (list) {
            var obj = { a: { b: 1 } }
            var result = compareIfSafe(obj, 'a.b', 9999);
            return result == false;
        }));
        me.testes.push(new ltjTest('Fn_GetPropertyByName_FindSimple', function (list) {
            var obj = { a: 1 };
            var result = getPropertyByName(obj, 'a');
            return result == 1;
        }));
        me.testes.push(new ltjTest('Fn_GetPropertyByName_FindComplex', function (list) {
            var obj = { a: 1, b: { c: 2 } };
            var result = getPropertyByName(obj, 'b.c');
            return result == 2;
        }));
        me.testes.push(new ltjTest('Fn_GetPropertyByName_NOTFindSimple', function (list) {
            var obj = { a: 1, b: { c: 2 } };
            var result = getPropertyByName(obj, 'd');
            return result == null;
        }));
        me.testes.push(new ltjTest('Fn_GetPropertyByName_NOTFindComplex', function (list) {
            var obj = { a: 1, b: { c: 2 } };
            var result = getPropertyByName(obj, 'a.c');
            return result == null;
        }));
        me.testes.push(new ltjTest('Fn_SingleLookup_Find', function (list) {
            var obj = { a: 1 };
            var result = singleLookup(obj, 1);
            return result == 'a';
        }));
        me.testes.push(new ltjTest('Fn_CopyAllPropersTo', function (list) {
            var obj = {};
            var objToCopy = { a: 1, b: 'teste', c: function () { return 3; } };
            copyAllPropersTo(obj, objToCopy);
            return obj.a == objToCopy.a &&
                obj.b == objToCopy.b &&
                obj.c() == objToCopy.c();
        }));
        me.testes.push(new ltjTest('Fn_FindFirstArray_Find', function (list) {
            var a1 = [1, 2, 3];
            var a2 = [3, 4, 5];
            var obj = { a: a1, b: a2, c: '', d: {} };
            var result = findFirstArray(obj);
            return result == a1;
        }));
        me.testes.push(new ltjTest('Fn_FindFirstArray_Find2', function (list) {
            var a1 = [1, 2, 3];
            var a2 = [3, 4, 5];
            var obj = { x: 22, y: 33, a: a1, b: a2, c: '', d: {} };
            var result = findFirstArray(obj);
            return result == a1;
        }));
        me.testes.push(new ltjTest('Fn_FindFirstArray_NotFind', function (list) {
            var obj = { a: 1, b: 2, c: '', d: {} };
            var result = findFirstArray(obj);
            return result == null;
        }));
        me.testes.push(new ltjTest('Fn_ObjToArray', function (list) {
            var obj = { a: 1, b: 2, c: 3 };
            var result = objToArray(obj);
            return fullCompare(result, [1, 2, 3]);
        }));
        me.testes.push(new ltjTest('Fn_ObjToArray_complex', function (list) {
            var obj = { a: 1, b: { d: 4 }, c: 3 };
            var result = objToArray(obj);
            return fullCompare(result, [1, { d: 4 }, 3]);
        }));
        me.testes.push(new ltjTest('Fn_ObjToArray_function', function (list) {
            var obj = { a: 1, b: { d: 4 }, c: function () { return 33; } };
            var result = objToArray(obj);
            return fullCompare(result, [1, { d: 4 }, 33]);
        }));
        me.testes.push(new ltjTest('Select_Find', function (list) {
            var result = list.select('id');
            return result.compare([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        }));
        me.testes.push(new ltjTest('Select_NotFind', function (list) {
            var result = list.select('nada');
            return result.length == 0;
        }));
        me.testes.push(new ltjTest('Select_FindByArray', function (list) {
            var result = list.select(['id', 'name']);
            var cList = [];
            for (var i = 0; i < 10; i++) {
                cList.push({ id: i, name: 'name ' + i });
            }
            return fullCompare(result, cList);
        }));
        me.testes.push(new ltjTest('Select_FindJustOneByArray', function (list) {
            var result = list.select(['id', 'torradeira']);
            var cList = [];
            for (var i = 0; i < 10; i++) {
                cList.push({ id: i });
            }
            return fullCompare(result, cList);
        }));
        me.testes.push(new ltjTest('Select_FindByObject', function (list) {
            var result = list.select({ ID: 'id', Nome: 'name', doubleId: function (item) { return item.id * 2; }, fixed: 1 });
            var cList = [];
            for (var i = 0; i < 10; i++) {
                cList.push({ ID: i, Nome: 'name ' + i, doubleId: i * 2, fixed: 1 });
            }
            return fullCompare(result, cList);
        }));
        me.testes.push(new ltjTest('SelectToArray_ok', function (list) {
            var result = list.selectToArray('id');
            return fullCompare(result, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        }));
        me.testes.push(new ltjTest('SelectToArray_notFind', function (list) {
            var result = list.selectToArray('panela');
            return result.length == 0;
        }));
        me.testes.push(new ltjTest('Count_Simple', function (list) {
            var k = [1, 2, 3, 4, 5];
            return k.count() == 5;
        }));
        me.testes.push(new ltjTest('Count_Obj_Find1', function (list) {
            var result = list.count({ id: 1 });
            return result == 1;
        }));
        me.testes.push(new ltjTest('Count_Obj_BiggerThan', function (list) {
            var result = list.count({ id: BiggerThan(5) });
            return result == 4;
        }));
        me.testes.push(new ltjTest('Count_Obj_BiggerOrEqualThan', function (list) {
            var result = list.count({ id: BiggerOrEqualThan(5) });
            return result == 5;
        }));
        me.testes.push(new ltjTest('Remove_Find', function (list) {
            list.remove({ id: 3 });
            var result = list.first({ id: 3 });
            return result == null;
        }));
        me.testes.push(new ltjTest('Remove_NotFInd', function (list) {
            var count = list.length;
            list.remove({ id: 999 });

            return list.length == count;
        }));
        me.testes.push(new ltjTest('Set_Object', function (list) {
            list.set({ prop1: 'Teste' });
            var result = list.count({ prop1: 'Teste' });
            return result == list.length;
        }));
        me.testes.push(new ltjTest('Set_Function', function (list) {
            list.set(function (item) { item.prop1 = 'Teste'; });
            var result = list.count({ prop1: 'Teste' });
            return result == list.length;
        }));
        me.testes.push(new ltjTest('SetTo_Object_Object', function (list) {
            list.setTo({ id: 4 }, { prop1: 'Teste' });
            var result = list.first({ id: 4 });
            return result.prop1 == 'Teste' && list.count({ prop1: 'Teste' }) == 1;
        }));
        me.testes.push(new ltjTest('SetTo_Object_Object_NotFound', function (list) {
            list.setTo({ id: 999 }, { prop1: 'Teste' });

            return list.count({ prop1: 'Teste' }) == 0;
        }));
        me.testes.push(new ltjTest('SetTo_Object_Function', function (list) {
            list.setTo({ id: 4 }, function (item) { item.prop1 = 'Teste'; });
            var result = list.first({ id: 4 });
            return result.prop1 == 'Teste' && list.count({ prop1: 'Teste' }) == 1;
        }));
        me.testes.push(new ltjTest('Max_OK', function (list) {
            var result = list.max('id');
            return result == 9;
        }));
        me.testes.push(new ltjTest('Min_OK', function (list) {
            var result = list.min('id');
            return result == 0;
        }));
        me.testes.push(new ltjTest('ObjWithMin_Find', function (list) {
            var result = list.objWithMin('id');
            return result.id == 0 && result.name == 'name 0' && result.prop1 == 'prop1 0';
        }));
        me.testes.push(new ltjTest('Take_OK', function (list) {
            var result = list.take(4);
            return result.length == 4;
        }));
        me.testes.push(new ltjTest('Take_Bigger', function (list) {
            var result = list.take(40);
            return result.length == 10;
        }));
        me.testes.push(new ltjTest('CountDistinct_OK', function () {
            var t = new linqToJavaescritoUniTest();
            var list = t.getObjArray();
            list.push(t.getObjId(1));
            list.push(t.getObjId(1));
            var result = list.countDistinct('id');
            return result == 10;
        }));
        me.testes.push(new ltjTest('Union_OK', function (list) {
            var a1 = [1, 2, 3, 4];
            var a2 = [5, 6, 7, 8];
            var union = a1.union(a2);
            return fullCompare(union, [1, 2, 3, 4, 5, 6, 7, 8]);
        }));
        me.testes.push(new ltjTest('Filter_OK', function (list, instance) {
            var l1 = list.filter(function (item) { return item.id >= 5 });
            var l2 = [];
            for (var i = 5; i < 10; i++) {
                l2.push(instance.getObjId(i, list[i].compProp));
            }
            return fullCompare(l1, l2);
        }));
        me.testes.push(new ltjTest('FindMany_OK', function (list, instace) {
            var result = list.findMany(function (item) { if (item.id == 7) { return item.compProp; } }, ['parent', 'obj']);
            return result[0].obj.a == 7 && result[0].parent.id == 7;
        }));
        me.testes.push(new ltjTest('IncludeBefore_OK', function (list, instance) {
            var countBefore = list.length;
            var item = instance.getObj(99);
            list = list.includeBefore(item);
            var result = list.first()
            return countBefore + 1 == list.length && result.id == 99 && result.prop1 == 'prop1 99';
        }));
        me.testes.push(new ltjTest('IncludeBefore_Array', function (list, instance) {
            var countBefore = list.length;
            var arr = [];
            for (var i = 90; i <= 99; i++) {
                arr.push(instance.getObj(i));
            }

            list = list.includeBefore(arr);
            var result = list.first()
            return countBefore + 10 == list.length && result.id == 99 && result.prop1 == 'prop1 99';
        }));
        me.testes.push(new ltjTest('SumProduct_Array', function () {
            var l1 = [];
            var l2 = [];
            for (var i = 1; i <= 10; i++) {
                l1.push(i);
                l2.push(11 - i);
            }
            var result = l1.sumProduct(l2);
            return result == 220;
        }));
        me.testes.push(new ltjTest('SumProduct_Number', function () {
            var l1 = [];
            var l2 = [];
            for (var i = 1; i <= 10; i++) {
                l1.push(i);
            }
            var result = l1.sumProduct(10);
            return result == 550;
        }));
        me.testes.push(new ltjTest('Sum_SimpleArray', function (list, instance) {
            var l1 = instance.getSequence(10);
            var result = l1.sum();
            return result == 55;
        }));
        me.testes.push(new ltjTest('Sum_ComplexArray', function (list, instance) {
            var result = list.sum('id');
            return result == 45;
        }));
    };
    me.testeAll = function () {
        me.subscribribeTestes();

        for (var i = 0; i < me.testes.length; i++) {
            var test = me.testes[i];
            try {
                var t = new linqToJavaescritoUniTest();
                var list = t.getObjArray();
                test.result = test.fn(list, t);

            } catch (e) {
                test.error = e;
                test.result = false;
            }
            test.tested = true;
        }
        return me.testes;
    };

}
function runAllJsUnitTest() {
    console.clear();
    var time1 = new Date();
    console.group('LinqToJavaescrito Unit Test');
    var _testeLinqToJavaescrito = new testeLinqToJavaescrito();
    var list = _testeLinqToJavaescrito.testeAll();

    list.foreach(function (item) {
        if (item.result == true) {
            console.log(item.description, ' OK');
        } else {
            console.error(item.description, item.error || item.errorMsg || "Error");
        }
    })
    console.log('Total tests:', list.length);
    console.log('Done:', list.count({ tested: true }));
    console.log('%cOK:', 'color:green', list.count({ result: true }));
    console.log('Failed:', list.count({ result: false }));
    console.log('items:', list);

    console.groupEnd();
    var time2 = new Date();
    return 'elapsed time: ' + (time2 - time1).toString() + ' ms';
}
function ensureStaticDomainInUrl(url) {
    if (typeof (ILangSettings) != 'undefined' && typeof (ILangSettings.Sites) != 'undefined' && typeof (ILangSettings.Sites.Static()) != 'undefined') {
        return ILangSettings.Sites.Static() + url;
    } else {
        return '';
    }
}

RegisterNamespace('ILang');
RegisterNamespace('ILang.Online');

// SET LEFT MENU SCROLL BAR
var isMobileOrTablet = $('.showCustomScroll').css('display') == 'none';
$(function () {
    if (isMobileOrTablet == true)
        return;

    if ($("#divLeftMenu").length > 0) {
        if ($('#divLeftMenu').customScrollbar) {
            $('#divLeftMenu').customScrollbar({
                hScroll: false,
                vScroll: true,
                preventDefaultScroll: true,
                updateOnWindowResize: true,
                skin: "default-skin"
            });
        }
    }
});

//********************************
// Namespace: ILang.Util
//********************************

$.extend(ILang, {
    Util: {
        Cookie: {
            write: function (name, value, expDays) {
                var c_value = escape(value);

                if (expDays != null) {
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() + expDays);
                    c_value = c_value + "; expires=" + exdate.toUTCString();
                }

                document.cookie = name + "=" + c_value;
            },
            read: function (name) {
                var i, x, y, ARRcookies = document.cookie.split(";");
                for (i = 0; i < ARRcookies.length; i++) {
                    x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                    x = x.replace(/^\s+|\s+$/g, "");
                    if (x == name) {
                        return unescape(y);
                    }
                }
            }
        }
        , QueryString: {
            read: function (name, _url) {
                var url = _url != null ? _url : window.location.href;
                if (url.indexOf('?') >= 0) {
                    var parte = url.split('?')[1].split('#')[0];
                    if (parte.indexOf('&') >= 0) {
                        var retorno = '';
                        $(parte.split('&')).each(function (index, item) {
                            var indexOfEqual = item.indexOf('=')
                            var chaveValor = item.substring(0, indexOfEqual);
                            var valor = item.substring(indexOfEqual+1)
                            if (chaveValor == name) {
                                retorno = valor;
                                return false;
                            }
                        });
                        return retorno;
                    } else {
                        if (parte.indexOf('=') >= 0) {
                            var indexOfEqual = parte.indexOf('=')
                            var chaveValor = parte.substring(0, indexOfEqual);
                            var valor = parte.substring(indexOfEqual + 1)
                            if (chaveValor == name)
                                return valor;
                            return '';
                        }
                    }
                }
            }
        }
    }
});

//**************************************
// Namespace: ILang.ErrorHandling
//**************************************

var ErrorHandling = {
    ErrorHandling: {
        HTTPHandler: {
            Error: function (options) {
                title: this.extractTitle(options.info);
                fullDescription: this.extractFullDescription(options.info);
            }
        }
    }
};
$.extend(ILang, ErrorHandling);

$.extend(ILang.ErrorHandling.HTTPHandler, Send = {
    Send: function (message, url, line, DisplayedToUser, after) {

        if (typeof (ILang.Online.Config) != 'undefined' && !ILang.Online.Config.LogJavaScriptErrors) {
            if (typeof (window.Alert) != 'undefined')
                window.Alert('Error Message: ' + message + '\nURL: ' + url + '\nLine: ' + line);
            else
                alert('Error Message: ' + message + '\nURL: ' + url + '\nLine: ' + line);
            return true;
        }

        var lineString = '';

        if (line) {
            lineString = line.toString();
        }

        var errorRegisterHandlerUrl = ILangSettings.Sites.API() + "/ErrorRegister/log";
        if ('https:' == document.location.protocol) {
            errorRegisterHandlerUrl = errorRegisterHandlerUrl.replace('http://', 'https://');
        }

        var displayed = (DisplayedToUser == undefined || DisplayedToUser == null) ? false : DisplayedToUser;

        $.ajax({
            type: 'post',
            url: errorRegisterHandlerUrl,
            data: { ErrorMessage: escape(message), ErrorUrl: escape(url), ErrorLine: lineString, DisplayedToUser: displayed },
            xhrFields: {
                withCredentials: true
            }
        }).success(function (result) {
            if (result != null) {
                if (typeof (after) == 'function') {
                    after(result);
                } else {
                    return result.Result;
                }
            } else {
                after(null);
            }

        }).error(function (a, b) {
            if (typeof (after) == 'function')
                after(null);
        });
        return true;
    }
});

$.extend(ILang.ErrorHandling.HTTPHandler.Error, extractTitle = {
    extractTitle: function (info) {
        var startregex = info.responseText.indexOf("<title>");
        var endregex = info.responseText.indexOf("</title>");

        if (startregex > 0 && endregex > 0)
            return info.responseText.substring(startregex + '<title>'.length, endregex);
        else
            return info.responseText;
    }
});

$.extend(ILang.ErrorHandling.HTTPHandler.Error, extractFullDescription = {
    extractFullDescription: function (info) {
        var element = document.createElement('div');
        $(element).html(info.responseText);
        return $(element).text();
    }
});


//********************************
// Console
//********************************
var fakeConsole = function () {
    this.log = function () {

    };
};
if (typeof (window.console) == 'undefined') {
    window.console = new fakeConsole();
}


//********************************
// Loading....
//********************************

function ShowLoading() {
    SetLoadingVisibility(true);
}

function HideLoading() {
    SetLoadingVisibility(false);
}
function ToggleLoading(visible) {
    var loadEl = GetLoadingControl();
    if (loadEl) {
        loadEl.css('visibility', (visible) ? 'visible' : 'hidden');
    }
}
function SetLoadingVisibility(visible) {
    var loadEl = GetLoadingControl();
    if (loadEl) {
        loadEl.css('visibility', (visible) ? 'visible' : 'hidden');
    }

    var errorEl = GetErrorControl();
    if (errorEl) {
        errorEl.hide();
    }
}

function GetLoadingControl() {
    var loadEl = $('#topLoad');
    if (loadEl == null && $(window.parent.document) != null) loadEl = $(window.parent.document).find('div[id=topLoad]'); // Try to get element in parent document
    return loadEl;
}
function GetErrorControl() {
    var errorEl = $('#errorFrame');
    if (errorEl == null && $(window.parent.document) != null) errorEl = $(window.parent.document).find('div[id=errorEl]'); // Try to get element in parent document
    return errorEl;
}
function showTopErrorMsg(msg, closeAfter, errorID, showRefreshBto) {
    let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content');
    var txt;
    if (errorID == null) {
        var txt = 'Ocorreu um erro.';
    } else {
        var txt = 'Ocorreu um erro(' + errorID.toString() + '). ';
    }
    if (showRefreshBto == null || showRefreshBto == true) {
        txt = txt + '<a href="javascript:location.reload()">Clique aqui</a> para recarregar a página';
    }
    if (msg != null) {
        if (msg.indexOf('{0}') >= 0) {
            msg = msg.format(errorID);
        }
        txt = msg;
    }
    var topAlert;
    if (pc.find('.topAlert').length <= 0) {
        topAlert = $('<div></div>', { 'class': 'topAlert', id: 'errorPage' });
        var d1 = $('<em></em>', { 'class': 'alert', html: txt }).appendTo(topAlert);
        pc.prepend(topAlert);
    } else {
        pc.find('.topAlert').find('em').html(txt)
        topAlert = pc.find('.topAlert');
        topAlert.show();
    }
    var alertTime = 5000;
    if (closeAfter == null) {
        window.setTimeout(function () { topAlert.addClass('topAlertAnimate') }, alertTime);
    } else {
        window.setTimeout(function () { topAlert.hide(); }, closeAfter);
    }
}

function openNewTab(url) {
    let elementA = document.createElement('a');
    elementA.target = '_blank';
    elementA.href = url;
    elementA.click();
}

function showNegativeMsg(msg, closeAfter) {
    let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content');
    
    let txt;


    if (msg != null) {
        if (msg.indexOf('{0}') >= 0) {
            msg = msg.format(errorID);
        }
        txt = msg;
    }
    let topAlert;
    if (pc.find('.topAlert').length <= 0) {
        topAlert = $('<div></div>', { 'class': 'topAlert', id: 'errorPage' });
        $('<em></em>', { 'class': 'alert', html: txt }).appendTo(topAlert);
        pc.prepend(topAlert);
    } else {
        pc.find('.topAlert').find('em').html(txt)
        topAlert = pc.find('.topAlert');
        topAlert.show();
    }
    let alertTime = 5000;
    if (closeAfter == null || closeAfter > alertTime) {
        window.setTimeout(function () { topAlert.addClass('topAlertAnimate') }, alertTime);
    } else {
        window.setTimeout(function () { topAlert.hide(); }, closeAfter);
    }
}

function hideTopErrorMsg() {
    let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content');
    topAlert = pc.find('.topAlert');
    topAlert.hide();
}

function showTopMsg(msg, closeAfter) {
    let pc = $('.pageContent').length > 0 ? $('.pageContent') : $('.page-content');

    if (msg == null)
        return;

    var topAlert;
    if (pc.find('.topAlert > .alertSuccess').length <= 0) {
        topAlert = $('<div></div>', { 'class': 'topAlert', id: 'topMsgPage' });
        var d1 = $('<em></em>', { 'class': 'alert alertSuccess', html: msg }).appendTo(topAlert);
        pc.prepend(topAlert);
    } else {
        pc.find('.topAlert').hide();
        var em = pc.find('.topAlert > .alertSuccess');
        em.html(msg)
        topAlert = em.parent();
        topAlert.show();
    }
    var alertTime = 5000;
    if (closeAfter == null || closeAfter > alertTime) {
        window.setTimeout(function () { topAlert.addClass('topAlertAnimate') }, alertTime);
    } else {
        window.setTimeout(function () { topAlert.hide(); }, closeAfter);
    }
}

function logErrorOnHiddenField(msg, errorID, type) {
    var inputId = 'fullstoryLogErrorHidden';
    if (type != null) {
        inputId = inputId + '_' + type;
    }
    var element = $('#' + inputId);
    if (element == null || element.length == 0) {
        element = $('<textarea/>', { id: inputId, width: '0px', 'class': 'ng-hide', 'aria-hidden': true, 'role': 'presentation' });
        $('body').prepend(element);
    }
    var current = element.val() || "";

    var txt = ("\n --> ID: + " + errorID);
    txt += ("\n --> " + msg);
    txt += ("\n --> " + (new Date().toString()));
    txt += ("\n --> " + JSON.stringify(ILangSettings));
    txt += "\n--------------------";
    $(element).trigger('change');
    element.val(current + txt);
}


//********************************
// jQuery
//********************************
jQuery.fn.print = function () {

    if (this.size() > 1) {
        this.eq(0).print();
        return;
    } else if (!this.size()) {
        return;
    }

    var strFrameName = ("printer-" + (new Date()).getTime());

    if (window.location.hostname !== document.domain && navigator.userAgent.match(/msie/i)) {
        var iframeSrc = "javascript:document.write(\"<head><script>document.domain=\\\"" + document.domain + "\\\";</script></head><body></body>\")";
        var jFrame = document.createElement('iframe');
        jFrame.name = strFrameName;
        jFrame.id = strFrameName;
        jFrame.className = "MSIE";
        document.body.appendChild(jFrame);
        jFrame.src = iframeSrc;
    } else {

        var jFrame = $("<iframe name='" + strFrameName + "'>");

        jFrame
            .css("width", "1px")
            .css("height", "1px")
            .css("position", "absolute")
            .css("left", "-9999px")
            .appendTo($("body:first"))
            ;
    }

    var objFrame = window.frames[strFrameName];

    var objDoc = objFrame.document;

    var jStyleDiv = $("<div>").append(
        $("style").clone()
    );

    objDoc.open();
    objDoc.write("<!DOCTYPE html>");
    objDoc.write("<html>");

    objDoc.write("<head>");
    objDoc.write("<title>");
    objDoc.write(document.title);
    objDoc.write("</title>");

    objDoc.write("<link href=\"" + ILangSettings.Sites.Static() + "/App_Themes/ILang/default.css\" type=\"text/css\" rel=\"stylesheet\">");
    objDoc.write("<link href=\"" + ILangSettings.Sites.Static() + "/App_Themes/ILang/iLang.css\" type=\"text/css\" rel=\"stylesheet\">");
    objDoc.write("<link href=\"" + ILangSettings.Sites.Static() + "/App_Themes/ILang/iLang2.css\" type=\"text/css\" rel=\"stylesheet\">");
    objDoc.write("<link href=\"" + ILangSettings.Sites.Static() + "/App_Themes/ILang/iLang3.css\" type=\"text/css\" rel=\"stylesheet\">");
    objDoc.write(jStyleDiv.html());
    objDoc.write("</head>");

    if ($(objFrame).hasClass("MSIE")) {
        objDoc.write("<body onload=\"javascript:document.execCommand('print', false, null);\">");
    }
    else {
        objDoc.write("<body onload=\"javascript:window.print();\">");
    }
    objDoc.write(this.html());
    objDoc.write("</body>");
    objDoc.write("</html>");
    objDoc.close();

    if ($(objFrame).hasClass("MSIE")) {
        window.frames[strFrameName].focus();
    }

    setTimeout(
        function () {
            jFrame.remove();
        },
        (60 * 1000)
    );
}

//CONSOLE
if (console != null) {
    console.green = function (text) {
        console.log('%c ' + text, 'color:green');
    }
}

//**************************************
// Fixed element on scroll
//**************************************

function sticky_relocate() {

    if ($('#fixedElementAnchor').length == 0) {
        return;
    }

    var window_top = $(window).scrollTop();
    var div_top = $('#fixedElementAnchor').offset().top;
    if (window_top > div_top) {
        $('#fixedElement').addClass('fixedElement');
        $('#fixedElementAnchor').addClass('fixedElementAnchor');
    } else {
        $('#fixedElement').removeClass('fixedElement');
        $('#fixedElementAnchor').removeClass('fixedElementAnchor');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});

//***********
//TOP MENU
//***********
function ShowHideUserMenu(forceToClose) {
    if ($('#topUserMenu').length == 0) return;

    if ($('#topUserMenu').hasClass('active') || forceToClose == true) {
        $('#topUserMenu').removeClass('active');
    }
    else {
        $('#topUserMenu').addClass('active');
        var scope = getAngularScopeByElementId('notifiationPageDiv');
        if (scope != null) {
            scope.close();
        }
    }
}

if ($('#topUserMenu').length > 0) {
    $('#topUserMenu').click(function (event) {
        event.stopPropagation();
    });

    $(window).click(function () {
        $('#topUserMenu').removeClass('active');
    });
}

/*
    PREVENT BROWSER BACK BUTTON ACTION
*/
var history_api = typeof history.pushState !== 'undefined'

// The previous page asks that it not be returned to
if (location.hash == '#no-back') {
    // Push "#no-back" onto the history, making it the most recent "page"
    if (history_api) history.pushState(null, '', '#stay')
    else location.hash = '#stay'

    // When the back button is pressed, it will harmlessly change the url
    // hash from "#stay" to "#no-back", which triggers this function
    window.onhashchange = function () {
        // User tried to go back; warn user, rinse and repeat
        if (location.hash == '#no-back') {

            if (history_api) history.pushState(null, '', '#stay')
            else location.hash = '#stay'
        }
    }
}


//Content Verification for ANIMA
window.__contentVerification = function () {

    $('.cardPage').each(function (index, card) {
        var cardTitle = $(card).find('h4').text();
        console.group(cardTitle.trim());
        //iframes
        iframeUrlList = [];
        $(card).find('iframe').each(function (_index, _iframe) {
            var src = $(_iframe).attr('src');
            if (typeof (src) != 'undefined' && iframeUrlList.indexOf(src) == -1) {
                iframeUrlList.push($(_iframe).attr('src'));
            }
            window.__verifyElemnt(_iframe, 'img', 'src');
            window.__verifyElemnt(_iframe, 'a', 'href');
        });

        iframeUrlList.foreach(function (item, index) {
            if (item.indexOf('staticcdn.' + ILangSettings.JavaScriptDomainName) >= 0) {
                console.log('%c' + index + ':', 'color:green', item);
            } else {
                console.log('%c' + index + ':', 'color:red', item);
            }

        });
        console.groupEnd();
    });
};
window.__verifyElemnt = function (element, tag, prop) {
    var tags = $(element).contents().find(tag);
    if (tags == null || tags.length == 0)
        return;

    var first = true;
    tags.each(function (index, item) {
        var href = $(item).attr(prop);
        if (typeof (href) != 'undefined' && href != "javascript:void(0);") {
            if (first == true) {
                console.group(tag);
                first = false;
            }
            if (href.indexOf('s3.amazonaws.com/ilang') >= 0) {
                console.log('%c' + index + ':', 'color:green', href);
            } else {
                console.log('%c' + index + ':', 'color:red', href);
            }
        }
    });
    if (first == false)
        console.groupEnd();
}

window.createDomainCookie = function (CookieName, CookieValue, periodType, offset, domain) {

    var expireDate = new Date();
    offset = offset / 1;
    _domain = domain == null ? false : domain;

    var myPeriodType = periodType;
    switch (myPeriodType.toLowerCase()) {
        case "years":
            var year = expireDate.getYear();
            // Note some browsers give only the years since 1900, and some since 0.
            if (year < 1000) year = year + 1900;
            expireDate.setYear(year + offset);
            break;
        case "months":
            expireDate.setMonth(expireDate.getMonth() + offset);
            break;
        case "days":
            expireDate.setDate(expireDate.getDate() + offset);
            break;
        case "hours":
            expireDate.setHours(expireDate.getHours() + offset);
            break;
        case "minutes":
            expireDate.setMinutes(expireDate.getMinutes() + offset);
            break;
        default:
            alert("Invalid periodType parameter for writePersistentCookie()");
            break;
    }

    document.cookie = escape(CookieName) + "=" + escape(CookieValue) + "; expires=" + expireDate.toGMTString() + "; path=/" + (_domain ? ';domain=.' + ILangSettings.JavaScriptDomainName : '');
}

function readCookie(cookieName) {
    var exp = new RegExp(escape(cookieName) + "=([^;]+)");
    if (exp.test(document.cookie + ";")) {
        exp.exec(document.cookie + ";");
        return unescape(RegExp.$1);
    }
    else return false;
}

ILang.Util.MobileApp = function () {
    return $(window).width() <= 500;
}

$(function () {
    var queryStringFromMobile = ILang.Util.QueryString.read("mob");
    if (typeof (queryStringFromMobile) != "undefined" && queryStringFromMobile != null && queryStringFromMobile.trim() != "" && queryStringFromMobile.length > 0 && isNaN(queryStringFromMobile) == false) {
        createDomainCookie('mob', queryStringFromMobile, 'months', 1, true);
    }
});

ILang.Util.isMobile = function () {
    let check = false;
    (function (a) { 
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function handleBiliotecaVirtualLogin() {
    $('#divWelcomeDialog').addClass('bgLoading');
    setTimeout(function () {
        $('.socialDialog').addClass('hide');
    }, 5000);
}

sendMessageTeacher = function (obj) {
    var param = {};
    param.classDisciplineId = obj.serverParameters.fixedParameters.ClassDisciplineID;
    obj.action('sendMessage', param, function (result) {
        if (result.Success) {
            var url = ILangSettings.Sites.ILang() + result.Result;
            window.location.href = url;
        }
    });
};

function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        if (typeof (after) == 'function') { after(true); }
    } catch (err) {
        if (typeof (after) == 'function') { after(false); }
    }

    document.body.removeChild(textArea);
}
function copyTextToClipboard(text, after) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        if (typeof (after) == 'function') { after(true); }
    }, function (err) {
        if (typeof (after) == 'function') { after(false); }
    });
}
function checkIsMobile() {
    return typeof (window.orientation) != 'undefined';
}

function openUrlNewTab(url) {
    window.open(url, '_blank');
}
function isValidIsbn(str) {

    var sum,
        weight,
        digit,
        check,
        i;

    str = str.replace(/[^0-9X]/gi, '');

    if (str.length != 10 && str.length != 13) {
        return false;
    }

    if (str.length == 13) {
        sum = 0;
        for (i = 0; i < str.length; i++) {
            digit = parseInt(str[i]);
            if (i % 2 == 1) {
                sum += 3*digit;
            } else {
                sum += digit;
            }
        }
        return check % 10 == 0;
    }

    if (str.length == 10) {
        weight = 10;
        sum = 0;
        for (i = 0; i < str.length; i++) {
            digit = parseInt(str[i]);
            sum += weight*digit;
            weight--;
        }
        return sum % 11 == 0
    }
}


//***********
// CREATE IFRAME TO WATCH ALL TABS USING BROADCAST
//***********
$(function () {
    if (isNotSafeToUse(ILangSettings, 'IsMasterILang'))
        return;
    if (ILangSettings.IsMasterILang == true)
        return;
    if (ILangSettings.CheckOrganizationChange == false)
        return;

    $('<iframe>', {
        src: ILangSettings.Sites.Static() + '/src.jq/BroadCast/OrganizationWatcher.html',
        id: 'OrganizationWatcher',
        frameborder: 0,
        height: 0, 
        width: 0,
        display:'none',
        scrolling: 'no',
        style: "display:none"
    }).appendTo($('body'));
});

function openDialogOrganizationWatcher(orgLogada) {
    if (typeof (orgLogada) == 'undefined')
        return;

    var smallDlgUrl = ILangSettings.Sites.Static() + '/Templates/Dialog/SmallDialog.html';
    $('#openDialogOrganizationWatcher').remove();
    $.get(smallDlgUrl, function (result) {
        var dlg = $(result);
        dlg.attr('id', 'openDialogOrganizationWatcher');
        var bto = $('<input type="button" value="Ok" class="button" index="0" style="display: inline;">');
        bto.click(function () {
            getCurrentOrgSelectedItem();
        })
        bto.appendTo(dlg.find('.sdBot'));

        var currentSelectedOrg = getSelectedOrganization();
        if (currentSelectedOrg == null)
            return;

        orgLogadaSpace = orgLogada.split('+').join(' ');
        if (orgLogadaSpace == currentSelectedOrg || orgLogada == currentSelectedOrg)
            return;

        $('<h3>Agora você esta logado na Organização <strong>' + orgLogadaSpace + '</strong>. Deseja voltar para ' + currentSelectedOrg + '?</h3>').appendTo(dlg.find('.sdContent'));
        dlg.appendTo($('body'));
    });
}

function getSelectedOrganization() {
    var orgSelectorScope = getAngularScopeByElementId('ctxOrgSelector')
    if (isSafeToUse(orgSelectorScope, 'serverParameters.drops.organization.selectedText'))
        return orgSelectorScope.serverParameters.drops.organization.selectedText;

    //.aspx
    try {
        var selectedText = $("[id^='OrganizationSelector.caption']").find('em').text().trim();
        return selectedText;
    } catch (e) {
        return null;
    }
}
function getCurrentOrgSelectedItem() {
    var orgSelectorScope = getAngularScopeByElementId('ctxOrgSelector')
    if (isSafeToUse(orgSelectorScope, 'serverParameters.drops.organization')) {
        var drop = getPropertyByName(orgSelectorScope, 'serverParameters.drops.organization');
        var id = drop.selectedId;
        var item = drop.list.first({ value: id });
        if (item != null) {
            orgSelectorScope.df__dropSelected(item, drop);
        }
    } else if (isSafeToUse(window, 'OrganizationSelector._Instance')) {
        try {
            var selectedText = $("[id^='OrganizationSelector.caption']").find('em').text().trim();
            var item = $("[id^='OrganizationSelector.list'] a:contains('" + selectedText + "')");
            window.OrganizationSelector._Instance.SetOrganization(item[0]);
        } catch (e) {
            
        }
        
    }
    return null;
}

function aplicarUlifeResources() {
    $("em[ulife-resource]").each(function(index, item) {
        try {
            var _rname = $(item).attr('ulife-resource');
            var _rvalue = eval(_rname);
            $(item).text(_rvalue);
            $(item).removeAttr('ulife - resource');
        }
        catch(ex) { }
    });
}

(function () {
    window.top.CustomJS = window.top.CustomJS || {};
    function TriggeredValueHandler(name, startValue) {
        let index = '#_' + name + '_value';
        let storage = () => window.top.sessionStorage;
        let rxObj = (/(\{.+\})|(\[.+\])/gim);
        let rxNumeric = (/^(\d+([,.]\d+)?)$/);
        let rxBool = (/^(true|false)$/i);
        let triggers = [];
        this.getKey = () => name;
        this.getValue = () => {
            // o storage retorna apenas string
            let rawValue = storage().getItem(index);
            if (typeof rawValue == 'undefined')
                return null;
            try {
                rawValue = JSON.parse(rawValue);
            }
            catch {
                if (rxBool.test(rawValue)) {
                    return rawValue.toLowerCase() == 'true' ? true : false;
                }
                if (rxNumeric.test(rawValue)) {
                    if ((/^\d+$/).test(rawValue))
                        return parseInt(rawValue);
                    return parseFloat(rawValue.replace(',', '.'));
                }
                if (rxObj.test(rawValue)) {
                    try {
                        return eval(rawValue);
                    }
                    catch {
                        return rawValue;
                    }
                }
            }
            return rawValue;
        };
        this.setValue = function (val, skipTrigger) {
            let stringVal = '';
            if (typeof val == 'string')
                stringVal = val;
            else {
                try {
                    stringVal = JSON.stringify(val);
                }
                catch {
                    stringVal = val.toString();
                }
            }
            storage().setItem(index, stringVal);
            if (!skipTrigger) {
                for (let i = 0; i < triggers.length; i++) {
                    let trg = triggers[i];
                    if (trg && typeof trg.handler == 'function') {
                        trg.handler(val);
                    }
                }
            }
            return val;
        };
        this.setTrigger = function (triggerName, handler) {
            let trigger = triggers.filter(trg => trg.name == triggerName);
            if (!!trigger && trigger.length > 0) {
                trigger[0].handler = handler;
            }
            else {
                triggers.push({ 'name': triggerName, 'handler': handler });
            }
            return this;
        };
        this.default = function (val) {
            if (typeof val != 'undefined') {
                storage().setItem(index, val);
                this.setValue(val, true);
            }
            return this;
        };
        
        window.top.CustomJS.Triggers = window.top.CustomJS.Triggers || {};
        window.top.CustomJS.Triggers[name] = (this);
        if (typeof startValue != 'undefined') {
            this.default(startValue);
        }
    };
    window.top.CustomJS.TriggeredValueHandler = TriggeredValueHandler;
}());

(function () {
    
    if (!Function.prototype.SafeExecute) {
        Function.prototype.SafeExecute = function (handleError) {
            const fx = this;
            try {
                return fx();
            } catch (error) {
                if (typeof handleError === 'function') {
                    handleError(error);
                }
            }
            return void (0);
        }
    }

    if (!Function.prototype.Try) {
        Function.prototype.Try = function () {
            const fx = this;
            try {
                return fx.apply(window, arguments);
            } catch (error) {
                return void (0);
            }
        }
    }

    if (!Function.prototype.Wait) {
        Function.prototype.Wait = function () {
            let arg = [];
            for (let i = 1; i < arguments.length; i++) {
                arg.push(arguments[i]);
            }
            var seconds = arguments[0] || 1000;
            if (isNaN(seconds))
                seconds = 1000;
            let fx = this;
            setTimeout(() => {
                fx.apply(window, arg);
            }, seconds);
        }
    }

    if (!Function.prototype.ExecuteWhen) {
        Function.prototype.ExecuteWhen = function (acceptCondition, maxTimeout = 30) {
            const PersistentHandler = (acceptCondition, handler, maxTimeout = 30) => {
                if (typeof acceptCondition !== 'function')
                    return;
                if (typeof handler !== 'function')
                    return;

                const start = new Date();
                var maxTimeout = maxTimeout || 30;
                const fx = () => {
                    const now = new Date();
                    const seconds = (now.getTime() - start.getTime()) / 1000;
                    if (seconds > maxTimeout)
                        return;
                    if (!acceptCondition()) {
                        setTimeout(fx, 333);
                        return;
                    }
                    else {
                        handler();
                    }
                };
                fx();
            }
            PersistentHandler(acceptCondition, this, maxTimeout);
        };
    }

    if (!Array.prototype.FirstOrDefault) {
        Array.prototype.FirstOrDefault = function (selector) {
            try {
                let data = this.filter(selector);
                return data[0];
            }
            catch {
                return void(0);
            }
        }
    }
    
}());