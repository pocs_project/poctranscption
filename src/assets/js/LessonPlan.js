﻿ILangApp.directive('lessonPlan', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('LessonPlan'); },
        scope: { allprops: '=' },
        controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;

            me.dto = me.allprops.Detail.FieldList;
            me.activityType = [{ name: 'Prova', id: 0 },
            { name: 'Atividade Institucional', id: 1 },
            { name: 'Revisão', id: 2 },
            { name: 'Outra', id: 3 }];

            me.setActivityName = function (id) {
                for (const element of me.activityType) {
                    if (element.id == id) {
                        return element.name;
                    }
                }
            }
            me.setTitleEducationUnit = function (id) {
                if (id == 1) {
                    return 'Unidades de ensino'
                }
                else {
                    return 'Descrição da Aula'
                }
            }
        },
    };
});