var TinyMCEUserMediaDialog = {
    init: function () {
    },

    insert: function () {
        top.tinymce.activeEditor.execCommand('mceInsertContent', false, document.forms[0].someval.value);
        top.tinymce.activeEditor.windowManager.close();
    },

    insertUserMedia: function (userMediaID) {
        var embedCode = getUserMediaEmbedCode(userMediaID) + '<p></p>';
        window.parent.tinymce.EditorManager.activeEditor.execCommand('mceInsertContent', false, embedCode);
        window.parent.tinymce.EditorManager.activeEditor.windowManager.close();
    },

    insertHSMUserMedia: function (slug) {
        var embedCode = getHSMContent(slug) + '<p></p>';
        window.parent.tinymce.EditorManager.activeEditor.execCommand('mceInsertContent', false, embedCode);
        window.parent.tinymce.EditorManager.activeEditor.windowManager.close();
    },
    insertLinkCategoryserMedia: function (param) {
        var embedCode = getLinkCategoryContent(param) + '<p></p>';
        window.parent.tinymce.EditorManager.activeEditor.execCommand('mceInsertContent', false, embedCode);
        window.parent.tinymce.EditorManager.activeEditor.windowManager.close();
    },
    insertISBNUserMedia: function (isbn, page) {
        
        getISBNContent(isbn, page, function (result) {
            if (result.Success) {
                var embedCode = getUserMediaEmbedCode(result.Result) + '<p></p>';
                window.parent.tinymce.EditorManager.activeEditor.execCommand('mceInsertContent', false, embedCode);
                window.parent.tinymce.EditorManager.activeEditor.windowManager.close();
            } else {
                showNegativeMsg('Ocorreu um erro inesperado', 4000);
            }
            
        });
        
    }
};