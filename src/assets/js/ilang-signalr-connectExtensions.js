﻿/*!
 * Métodos úteis relacionados a conexão do SignalR
 * Sempre usar esse namespace: ILangSignalRConnectExtensions
 */

RegisterNamespace('ILang.SignalRConnectExtensions');


ILang.SignalRConnectExtensions.ReconnectIfNecessary = function (callback) {

    if (typeof (callback) == 'undefined') {
        return;
    }

    if ($.connection.hub.state == $.connection.connectionState.disconnected) {
        $.connection.hub.start().done(function () {
            window.setTimeout(function () {
                ILangHub.server.startOnline(ILangSettings.UserID, ILangSettings.UserName, ILangSettings.OrganizationID, GetBrowserName(), ILangSettings.ClientIP, ILangSettings.Browser.OS, ILangSettings.Browser.IsMobile);
                callback();
            }, 1000)
        })
    } else {
        callback();
    }
};