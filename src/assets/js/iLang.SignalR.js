﻿$.extend(ILang, {
    SignalR: {}
});

var ILangHub = null;
var ActivityGroupHub = null;

var connectionState = 'disconnected';
var SignalRUserGroupList = new Array();

$.extend(ILang.SignalR, {

    SignalRGroupTypes: { //Sempre 3 caracteres
        ActivityGroup: 'agr',
        ActivityGroupPrivate: 'agp',
        Category: 'cat',
        StudentCategory: 'sca'
    },

    Base: {

        GetGroupName: function (groupType, identity) {
            return groupType + identity;
        },

        GetGroupType: function (groupName) {
            if (groupName == null || groupName.length < 3)
                return 'noGroup';

            return groupName.substring(0, 3);
        },

        GetGroupIdentity: function (groupName) {
            if (groupName == null || groupName.length < 4)
                return '0';

            return groupName.substring(3);
        },

        AddToGroup: function (groupName) {
            if (SignalRUserGroupList.length <= 0 || SignalRUserGroupList.indexOf(groupName) < 0) {
                var i = SignalRUserGroupList.length;
                SignalRUserGroupList[i] = groupName;
            }

            ActivityGroupHub.server.addGroup(groupName);
        },

        AddToAllGroups: function () {
            for (var i = 0; i < SignalRUserGroupList.length; i++) {
                ActivityGroupHub.server.addGroup(SignalRUserGroupList[i]);
            }
        },

        MessageProcessor: function (groupName, sender, message) {

            switch (ILang.SignalR.Base.GetGroupType(groupName)) {
                case ILang.SignalR.SignalRGroupTypes.ActivityGroup:
                    CommentsInstance.MessageReceiver(message);
                    break;
                case ILang.SignalR.SignalRGroupTypes.ActivityGroupPrivate:
                    PrivateDiscussionDlg.MessageReceiver(message);
                    break;
            }
        }
    },

    MemberGroup: {
        RefreshStatus: function () {
            if (typeof SucceededUpdateGroupStatusCallback != 'undefined') {
                var userElementIds = ILang.Online.GetStatusElements();
                ILangHub.server.getGroupUsersStatus(userElementIds);
            }
        }
    }
});

ILang.SignalR.Loader = function (groupType, identity) {
    if (!ILangSettings.SynchronousEnabled) {
        return null;
    }

    var inst = this;

    if (connectionState == 'connected')
        inst.init(groupType, identity);
    else
        $('body').on('SignalRConnected', function () {
            inst.init(groupType, identity);
        });
};

$.extend(ILang.SignalR.Loader.prototype, {
    init: function (groupType, identity) {
        this.groupName = ILang.SignalR.Base.GetGroupName(groupType, identity);
        ILang.SignalR.Base.AddToGroup(this.groupName);
    },

    Send: function (sender, message) {
        ActivityGroupHub.server.send(this.groupName, sender, message);
    }
});

function startSignalR() {
    if (ILangSettings.SynchronousEnabled) {

        ILangHub = $.connection.iLangHub;
        if (ILangHub == null) {
            return;
        }

        ActivityGroupHub = $.connection.activityGroupHub;
        if (ActivityGroupHub != null) {
            ActivityGroupHub.client.receive = function (groupName, sender, message) {
                ILang.SignalR.Base.MessageProcessor(groupName, sender, message);
            };
        }

        ILangHub.client.setStatus = function (user, status) {
            var userData = eval(user);
            if (userData == null)
                return;

            if (typeof SetLoginStatusImage != 'undefined') {
                SetLoginStatusImage(userData.UserID, status);
            }

            if (typeof ILang.UserStatus.UpdateUserStatus != 'undefined') {
                ILang.UserStatus.UpdateUserStatus(userData, status);
            }
            
        };

        ILangHub.client.connected = function () {
            connectionState = 'connected';

            $("body").trigger("SignalRConnected");
        };

        ILangHub.client.onlineStarted = function () {
            $("body").trigger("OnlineStarted");
        };

        ILangHub.client.UpdateGroupStatus = function (userOnlineIds) {
            if (typeof SucceededUpdateGroupStatusCallback != 'undefined') {
                SucceededUpdateGroupStatusCallback(userOnlineIds);
            }
        };

        // Registra servico para obtencao dos usuarios que estao online (nova barra inferior)
        ILangHub.client.GetOnlineFriends = function (users) {
            if (typeof SucceededGroupUsersOnlineCallback != 'undefined') {
                SucceededGroupUsersOnlineCallback(users);
            }
        };

        ILangHub.client.chatStarted = function (chatInfo) {
            ILang.OnlineBar.chatStarted($.parseJSON(chatInfo));
        }

        ILangHub.client.newChatMessage = function (chatInfo) {
            ILang.OnlineBar.handleChatRoom($.parseJSON(chatInfo));
        }

        ILangHub.client.showChatRooms = function (chatInfo) {
            ILang.OnlineBar.handleChatRooms($.parseJSON(chatInfo));
        }

        ILangHub.client.messageSent = function (messages) {
            ILang.Online.Chat.messageSent($.parseJSON(messages));
        }

        ILangHub.client.notificationCountChanged = function (count) {
            SuccessNotificationCounter(count);
        }

        ILangHub.client.logTimelineEvent = function (eventDescription) {
        }

        ILangHub.client.messageHasNotBeenSent = function (chatRoomId, lastMessageId, message) {
            ILang.OnlineBar.messageHasNotBeenSent(chatRoomId, lastMessageId, message);
        }

        // File Import
        ILangHub.client.fileImportProcessFinished = function (batchId) {
            //var iframeID = window.parent.FileImportDialog.getFrame().attr('id');

            if (typeof (SucceededVerifyBatchCallback) != 'undefined') {
                SucceededVerifyBatchCallback('true');
            }
            if (typeof (window.importFileOnSignalIRSiccess) == 'function') {
                window.importFileOnSignalIRSiccess(batchId);
            }
        }

        ILangHub.client.fileExportFinished = function (serviceFileExportId, filePathUrl){
            if (typeof (SucceededVerifyBatchCallback) != 'undefined') {
                SucceededVerifyBatchCallback('true');
            }
            if (typeof (window.exportFileOnSignalrSuccess) == 'function') {
                window.exportFileOnSignalrSuccess(serviceFileExportId, filePathUrl);
            }
        }

        // User Media
        ILangHub.client.videoEncodingFinished = function (userMediaId, result) {
            if (typeof (VideoEncodedCallback) != 'undefined') {
                VideoEncodedCallback(userMediaId, result);
            }
        }

        ILangHub.client.apaProcessFinished = function (userMediaId, result) {
            if (typeof (ApaProcessedCallback) != 'undefined') {
                ApaProcessedCallback(userMediaId, result);
            } else {
                try {
                    $('.mce-container-body > iframe')[0].contentWindow.ApaProcessedCallback(userMediaId, result);
                } catch (e) { }
            }
        }

        // Assessment
        ILangHub.client.assessmentSheetProcessFinished = function (userId, assessmentId, assessmentClassDisciplineId, questionSheetUrl) {

            var scope;

            try {
                scope = getAngularScope('assessmentListCtrl');
            } catch (e) {
            }

            if (!(typeof (scope) == 'undefined')) {
                $(scope.bigContext.list).each(function (index, date) {
                    $(date.List).each(function (a, discipline) {
                        $(discipline.List).each(function (b, course) {
                            $(course.List).each(function (c, assessmentClass) {
                                if (assessmentClass.AssessmentClassDisciplineId == assessmentClassDisciplineId) {
                                    assessmentClass.PDFStatus = scope.serverParameters.enums.AssessmentListItemPDFStatus.Generated;
                                    assessmentClass.QuestionSheetUrl = questionSheetUrl;
                                    scope.$apply();
                                }
                            });
                        });
                    });
                });
            }

            try {
                scope = getAngularScope('assessmentGenerationDialogCtrl');
            } catch (e) {
            }

            if (!(typeof (scope) == 'undefined')) {

                if (scope.currentAssessmentClass && scope.currentAssessmentClass.AssessmentId != assessmentId) {
                    return;
                } else {
                    scope.generationConcluded(questionSheetUrl);
                }
            }
        }

        ILangHub.client.assessmentGroupSheetProcessFinished = function (userId, assessmentGroupId, questionSheetUrl) {

            var scope;

            try {
                scope = getAngularScope('assessmentGroupGenerationDialogCtrl');
            } catch (e) {
            }

            if (typeof (scope) == 'undefined') {
                return;
            }

            if (scope.currentGroup && scope.currentGroup.AssessmentGroupId != assessmentGroupId) {
                return;
            } else {
                scope.generationConcluded(questionSheetUrl);
            }
        }

        ILangHub.client.assessmentBatchProcessFinished = function (userID, assessmentBatchID, processStatus, successCount, errorCount, warningCount, detachedCount, failMessage) {

            var scope;

            try {
                scope = getAngularScope('AnswerSheetCtrl');
            } catch (e) {
            }

            if (!(typeof (scope) == 'undefined')) {
                $(scope.bigContext.list).each(function (index, item) {
                    if (item.ID == assessmentBatchID) {
                        item.AssociatedCount = successCount;
                        item.NotAssociatedCount = errorCount;
                        item.NotIdentifiedCount = warningCount;
                        item.DetachedCount = detachedCount;
                        if (processStatus == 4) {
                            item.AssessmentBatchStatus = scope.serverParameters.enums.AssessmentBatchProcessStatus.Done;
                        } else {
                            item.AssessmentBatchStatus = scope.serverParameters.enums.AssessmentBatchProcessStatus.Error;
                        }
                        scope.$apply();
                    }
                });
            }
        }

        // Grade
        ILangHub.client.applyDefaultGradeFinished = function (classDisciplineId, disciplineContentId, lessonCategoryId, organizationId) {
            if (typeof (UpdateGradeStatus) == 'function') {
                UpdateGradeStatus(classDisciplineId, disciplineContentId, lessonCategoryId, organizationId);
            }
        }

        // Report
        ILangHub.client.reportProcessed = function (reportID, userScheduleID, filePath, success, notifyCompletelyFail, failMessage) {
            if (typeof (ReportProcessDialog) != 'undefined') {
                ReportProcessDialog.ConcludeProcess(reportID, userScheduleID, filePath, success, notifyCompletelyFail, failMessage);
            }
        }

        // Calendar
        ILangHub.client.newCalendarIntegration = function (id, isGroup) {
            if (typeof (window.calendarIntegrationOnSignalIRSiccess) == 'function') {
                window.calendarIntegrationOnSignalIRSiccess(id, isGroup);
            }
        }

        $.connection.hub.url = ILangSettings.Sites.SignalR() + '/signalr';

        if ('https:' == document.location.protocol) {
            $.connection.hub.url = $.connection.hub.url.replace('http://', 'https://');
        }

        try {
            if (ILangSettings.SignalRLogEnabled) {
                $.connection.hub.logging = true;
                $.connection.hub.transportConnectTimeout = 3000;
            }
            var ILangHubReady = $.connection.hub.start()
            .done(function () {
                window.setTimeout(function () {
                    ILangHub.server.startOnline(ILangSettings.UserID, ILangSettings.UserName, ILangSettings.OrganizationID, GetBrowserName(), ILangSettings.ClientIP, ILangSettings.Browser.OS, ILangSettings.Browser.IsMobile);

                }, 1000)
            })
            .fail(function (error) { console.log('SignalR fail: ' + error); })
            .error(function (error) { console.log('SignalR error: ' + error); });
        }
        catch (err) { }
    }
}

$(function () {
    startSignalR();
});