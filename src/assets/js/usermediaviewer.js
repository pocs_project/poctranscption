var LOCALVIDEO = '1';
var YOUTUBEVIDEO = '2';
var LOCALIMAGE = '3';
var SLIDEALBUM = '4';
var RECORDEDAUDIO = '5';
var LOCALDOCUMENT = '6';
var GOOGLEDOCUMENT = '7';
var INTERNETLINK = '8';
var MOVIEREFERENCE = '9';
var BOOKREFERENCE = '10';
var PERIODICREFERENCE = '11';
var SWFFILE = '12';
var SCORMCONTENT = '13';
var VIDEOELEMENT = '14';
var AUDIOELEMENT = '15';
var IMAGEELEMENT = '16';
var APAFILE = '17';
var LOCALAUDIO = '18';
var GENERICFILE = '19';
var VIMEOVIDEO = '20';
var SOUNDCLOUDAUDIO = '21';
var BOOKLINK = '22';
var ZOOMVIDEO = '23';
var LINKINTEGRATION = '24';

var UserMediaViewer = function (rootElementClientID, inputClientID, readOnly, attachSuccessMessage) {
    this.init(rootElementClientID, inputClientID, readOnly, attachSuccessMessage);
}

$.extend(UserMediaViewer.prototype, {

    init: function (rootElementClientID, inputClientID, readOnly, attachSuccessMessage) {
        // Members
        this.list = new Array();
        this.rootElementID = rootElementClientID;
        this.inputElementID = inputClientID;
        this.embedObject = false;
        if (!(readOnly === null) && (typeof readOnly === 'boolean'))
            this.readOnly = readOnly;
        else
            this.readOnly = false;

        this.attachSuccessMessage = attachSuccessMessage;

        //Events
        this.OnUserMediaCreated = null;
    },

    getRootElement: function () {
        return $('#' + this.rootElementID);
    },

    getInputElement: function () {
        return $('#' + this.inputElementID);
    },

    addUserMedia: function (userMediaID) {
        var userMediaIDValue = Number(userMediaID);
        if (this.storeUserMediaID(userMediaIDValue))
            this.showUserMedia(userMediaIDValue);
    },

    addUserMediaList: function (userMediaIDList) {
        this.removeAllUserMedia();
        for (itemIndex = 0; itemIndex < userMediaIDList.length; itemIndex++) {
            this.addUserMedia(userMediaIDList[itemIndex]);
        }
    },

    removeUserMedia: function (e) {
        if (typeof (removeUserMediaCallback) === 'function') {
            if (!removeUserMediaCallback(e)) {
                window.Alert('Não foi possível remover esse item. Favor tentar novamente');
                return;
            }
        }
        var target = $(e).attr('target');
        if ($(target).length > 0) {
            var userMediaID = $(target).attr("userMediaID");
            this.removeUserMediaItem(userMediaID);
        }
    },

    removeAllUserMedia: function () {
        var listToDelete = new Array();
        for (removeIndex = 0; removeIndex < this.list.length; removeIndex++) {
            listToDelete.push(this.list[removeIndex]);
        }
        for (removeIndex = 0; removeIndex < listToDelete.length; removeIndex++) {
            this.removeUserMediaItem(listToDelete[removeIndex]);
        }
    },

    removeUserMediaItem: function (userMediaID) {
        if (!this.deleteUserMediaID(userMediaID))
            return;
        var divToRemove = 'div_' + userMediaID;
        var userMediaElement = this.getRootElement().find('div[id=' + divToRemove + ']');
        userMediaElement.remove();
    },

    storeUserMediaID: function (userMediaID) {
        if ($.inArray(userMediaID, this.list.contains) > -1)
            return false;

        this.list.push(userMediaID);

        var inputElement = this.getInputElement();
        var inputValue = inputElement.val();

        inputElement.val(this.addTextValue(inputValue, userMediaID));

        return true;
    },

    deleteUserMediaID: function (userMediaID) {
        var userMediaIDValue = Number(userMediaID);
        var itemIndex = $.inArray(userMediaIDValue, this.list);
        if (itemIndex == -1) {
            return false;
        }

        this.list.splice(itemIndex, 1);

        var inputElement = this.getInputElement();
        if (typeof (inputElement.val()) == 'undefined') {
            return true;
        }

        var items = inputElement.val().split(';');
        var userMediaIDList = '';
        for (deleteIndex = 0; deleteIndex < items.length; deleteIndex++) {
            if (items[deleteIndex] != userMediaIDValue.toString()) {
                userMediaIDList = this.addTextValue(userMediaIDList, items[deleteIndex]);
            }
        }

        inputElement.val(userMediaIDList);

        $(this).trigger('deletedUserMedia', userMediaID);

        return true;
    },

    showUserMedia: function (userMediaID) {
        var self = this;
        $.post(ILangSettings.Sites.API() + '/UserMedia/Info', { userMediaID: userMediaID.toString() })
            .success(function (userMedia) {
                self.createUserMediaElement(userMedia);
            })
            .error(function (data, status, xhr) {
                if (typeof (deleteUserMediaID) === 'function')
                    deleteUserMediaID(userMediaID);
                alert('Erro ao obter dados do objeto de aprendizagem: ' + status);
            });
    },

    createUserMediaElement: function (userMedia) {
        switch (userMedia.userMediaTypeID.toString()) {
            case LOCALVIDEO:
            case ZOOMVIDEO:
                {
                    this.createLocalVideo(userMedia);
                    break;
                }
            case YOUTUBEVIDEO:
                {
                    this.createYouTubeVideo(userMedia);
                    break;
                }
            case LOCALIMAGE:
                {
                    this.createLocalImage(userMedia);
                    break;
                }
            case SLIDEALBUM:
                {
                    this.createSlideAlbum(userMedia);
                    break;
                }
            case RECORDEDAUDIO:
                {
                    this.createRecordedAudio(userMedia);
                    break;
                }
            case LOCALAUDIO:
                {
                    this.createLocalAudio(userMedia);
                    break;
                }
            case MOVIEREFERENCE:
            case BOOKREFERENCE:
            case PERIODICREFERENCE:
                {
                    this.createBibliographicReference(userMedia);
                    break;
                }
            case INTERNETLINK:
                {
                    this.createInternetLink(userMedia);
                    break;
                }
            case LOCALDOCUMENT:
                {
                    this.createLocalDocument(userMedia);
                    break;
                }
            case GOOGLEDOCUMENT:
                {
                    this.createGoogleDocument(userMedia);
                    break;
                }
            case SWFFILE:
            case APAFILE:
                {
                    this.createInteractiveObject(userMedia);
                    break;
                }
            case SCORMCONTENT:
                {
                    this.createScormContent(userMedia);
                    break;
                }
            case GENERICFILE:
                {
                    this.createGenericFileElement(userMedia);
                    break;
                }
            case VIDEOELEMENT:
            case AUDIOELEMENT:
            case IMAGEELEMENT:
                {
                    this.createMediaElement(userMedia);
                    break;
                }
        }

        if (this.readOnly)
            this.setReadOnly(this.readOnly);

        $(this).trigger('OnUserMediaCreated', [userMedia.userMediaID]);

        this.showCreatedElementSuccessMessage();
    },

    createLocalVideo: function (userMedia) {
        if (this.embedObject)
            this.createEmbeddedLocalVideo(userMedia);
        else
            this.createVideoIcon(userMedia, LOCALVIDEO);
    },

    createYouTubeVideo: function (userMedia) {
        if (this.embedObject)
            this.createEmbeddedYouTube(userMedia);
        else
            this.createVideoIcon(userMedia, YOUTUBEVIDEO);
    },

    createVideoIcon: function (userMedia, mediaType) {

        var divVideoFile = document.createElement('div');
        $(divVideoFile).addClass('file');

        var divDoc = document.createElement('div');
        $(divDoc).addClass('dw_doc');

        var divLeft = document.createElement('div');
        $(divLeft).addClass('dw_lf');
        $(divDoc).append(divLeft);

        var divRight = document.createElement('div');
        $(divRight).addClass('dw_rg video');

        var docImage = document.createElement('img');
        if (mediaType == LOCALVIDEO || mediaType == ZOOMVIDEO) {
            $(docImage).attr('src', '/App_Themes/ILang/img/icon_video_local.gif');
        }
        else {
            $(docImage).attr('src', '/App_Themes/ILang/img/icon_video_youtube.jpg');
            $(docImage).addClass('icon_youtube');
        }

        $(divRight).append(docImage);

        var pDocument = document.createElement('p');
        $(pDocument).addClass('dw_link');
        if (userMedia.title != null && userMedia.title.length > 0) {
            var title = userMedia.title;
            if (title.length > 70) {
                title = title.substring(0, 70) + '...';
            }
            $(pDocument).text(title);
        }

        if (userMedia.fileSize != null && userMedia.fileSize.length > 0) {
            var br = document.createElement('br');
            var label = document.createElement('label');
            $(label).text(userMedia.fileSize)
            $(pDocument).append(br);
            $(pDocument).append(label);
        }

        $(divRight).append(pDocument);

        $(divDoc).append(divRight);

        $(divVideoFile).append(divDoc);

        // User media div
        var userMediaElement = this.createUserMediaDiv(userMedia);
        $(userMediaElement).append(divVideoFile);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createEmbeddedLocalVideo: function (userMedia) {

        // Preview
        var divLocalVideo = document.createElement('div');
        $(divLocalVideo).html(buildLocalVideoFullHtmlEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divLocalVideo);

        this.getRootElement().append(userMediaElement);

        // TODO: Alterar aqui

        var jwplayerJS = buildLocalVideoJavaScriptEmbedCode(userMedia);
        eval(jwplayerJS);
    },

    createEmbeddedYouTube: function (userMedia) {
        // iFrame
        var iFrameID = 'iframeyoutube_' + Math.floor(Math.random() * userMedia.userMediaID).toString();
        var iFrameInfo = GetYouTubeVideoIFrame(iFrameID, userMedia.pathOrUrl);

        // Preview
        var divYouTubeVideo = document.createElement('div');
        $(divYouTubeVideo).addClass('preview');
        $(divYouTubeVideo).html(iFrameInfo.iFrameHTML);

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia)
        var userMediaElement = $(userMediaDiv).append(divYouTubeVideo);

        this.getRootElement().append(userMediaElement);

        // Copiando dimensões do iFrame para a div
        $(divYouTubeVideo).css('width', $('#' + iFrameID).attr('width'));
        $(divYouTubeVideo).css('height', $('#' + iFrameID).attr('height'));
    },

    setBuildImageEmbedCodeOverload: function (fn) {
        this._buildImageEmbedCodeOverload = fn;
    },
    setRemoveUserMediaCallback: function (fn) {
        removeUserMediaCallback = fn;
    },

    createLocalImage: function (userMedia) {

        // Image
        var divImage = document.createElement('div')
        $(divImage).addClass('divIlDrive');
        if (typeof (this._buildImageEmbedCodeOverload) != 'undefined') {
            $(divImage).html(this._buildImageEmbedCodeOverload(userMedia));
        } else {
            $(divImage).html(buildImageEmbedCode(userMedia));
        }


        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divImage);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createSlideAlbum: function (userMedia) {
        // Album url
        var albumUrl = userMedia.pathOrUrl;
        albumUrl = albumUrl.replace(/&/g, '$');

        // iFrame
        var iFrameID = 'iframeslides_' + Math.floor(Math.random() * userMedia.userMediaID).toString();
        var iFrameHTML = '<iframe id="' + iFrameID + '" src="/src/SlideAlbumViewer.aspx?S=' + albumUrl + '" frameborder="0" width="425" height="344" style="display:none" onload="this.style.display=\'block\';"></iframe>';

        // Preview
        var divViewSlide = document.createElement('div')
        $(divViewSlide).addClass('preview');
        $(divViewSlide).html(iFrameHTML);

        // User media div
        var userMediaElement = this.createUserMediaDiv(userMedia);
        $(userMediaElement).append(divViewSlide);

        this.getRootElement().append(userMediaElement);
    },

    createRecordedAudio: function (userMedia) {

        // Preview
        var divRecordedAudio = document.createElement('div');
        $(divRecordedAudio).html(buildRecordedAudioEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divRecordedAudio);

        this.getRootElement().append(userMediaElement);
    },

    createLocalAudio: function (userMedia) {

        // Preview
        var divLocalAudio = document.createElement('div');
        $(divLocalAudio).html(buildLocalAudioHtmlEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divLocalAudio);

        this.getRootElement().append(userMediaElement);

        eval(buildLocalAudioJavaScriptEmbedCode(userMedia));
    },

    createBibliographicReference: function (userMedia) {

        // ABNT
        var divAbnt = document.createElement('div');
        $(divAbnt).addClass('divIlDrive');
        $(divAbnt).html(buildBibliographicReferenceEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divAbnt);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createInternetLink: function (userMedia) {

        // URL
        var divLink = document.createElement('div');
        $(divLink).addClass('divIlDrive');
        $(divLink).html(buildInteretLinkEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divLink);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createMediaElement: function (userMedia) {
        var divMedia;

        if ((userMedia.userMediaTypeID == VIDEOELEMENT) || (userMedia.userMediaTypeID == AUDIOELEMENT)) {
            divMedia = document.createElement('div');
            $(divMedia).addClass('minSize sentenceMedia');

            var divID = this.generateUniqueDivID('dv' + userMedia.userMediaID);
            divMedia.id = divID;

            var flashObject;
            if (userMedia.userMediaTypeID == VIDEOELEMENT)
                flashObject = 'SentencePlayer.swf';
            else
                flashObject = 'SentencePlayer_Audio.swf';

            var objectHTML = '<object id="obj' + userMedia + '" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="100%" height="100%" align="middle">';
            objectHTML = objectHTML + '<param name="allowScriptAccess" value="always" />';
            objectHTML = objectHTML + '<param name="allowFullScreen" value="true" />';
            objectHTML = objectHTML + '<param name="movie" value="/FMS/' + flashObject + '?mediaURL=' + escape(userMedia.pathOrUrl) + '&id=' + divID + '" />';
            objectHTML = objectHTML + '<param name="quality" value="high" />';
            objectHTML = objectHTML + '<param name="wmode" value="Transparent" />';
            objectHTML = objectHTML + '<param name="bgcolor" value="#ffffff" />';
            objectHTML = objectHTML + '<embed src="/FMS/' + flashObject + '?mediaURL=' + escape(userMedia.pathOrUrl) + '&id=' + divID + '" quality="high" wmode="transparent" bgcolor="#ffffff" width="100%" height="100%" name="SentencePlayer" align="middle" allowScriptAccess="always" allowFullScreen="true" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />';
            objectHTML = objectHTML + '</object>';

            divMedia.innerHTML = objectHTML;
        }
        else if (userMedia.userMediaTypeID == IMAGEELEMENT) {
            divMedia = document.createElement('div');
            $(divMedia).addClass('mediaBlock');
            var image = document.createElement('img');
            $(image).attr('src', userMedia.pathOrUrl);
            $(divMedia).append(image);
        }

        var rootElement = this.getRootElement();
        rootElement.addClass('mediaContainer');
        rootElement.append(divMedia);
    },

    createGenericFileElement: function (userMedia) {

        // Preview
        var divGenericFile = document.createElement('div');
        $(divGenericFile).html(buildGenericFileEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divGenericFile);

        this.getRootElement().append(userMediaElement);
    },

    generateUniqueDivID: function (id) {
        while ($('#' + id).length > 0) {
            id = id + 'i';
        }

        return id;
    },

    createLocalDocument: function (userMedia) {

        // Document
        var divLocalDocument = document.createElement('div');
        $(divLocalDocument).addClass('divIlDrive');
        $(divLocalDocument).html(buildLocalDocumentEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divLocalDocument);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createGoogleDocument: function (userMedia) {

        // Document
        var divGoogleDocument = document.createElement('div');
        $(divGoogleDocument).addClass('divIlDrive');
        $(divGoogleDocument).html(buildGoogleDocumentEmbedCode(userMedia));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divGoogleDocument);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createInteractiveObject: function (userMedia) {

        // SWF / APA
        var divSwfFile = document.createElement('div');
        $(divSwfFile).addClass('divIlDrive');
        $(divSwfFile).html(buildInteractiveObjectEmbedCode(userMedia, false));

        // User media div
        var userMediaDiv = this.createUserMediaDiv(userMedia);
        var userMediaElement = $(userMediaDiv).append(divSwfFile);

        // Remove link
        this.createRemoveLink(userMedia.userMediaID, userMediaElement);

        this.getRootElement().append(userMediaElement);
    },

    createUserMediaDiv: function (userMedia) {
        var userMediaElement = document.createElement('div');
        $(userMediaElement).attr('id', 'div_' + userMedia.userMediaID.toString());
        $(userMediaElement).addClass('closetitem');
        return userMediaElement;
    },

    createRemoveLink: function (userMediaID, userMediaDiv) {

        var separator = document.createElement('font');
        $(separator).html('&nbsp;-&nbsp;');
        $(separator).attr('removeLink', '1');
        $(userMediaDiv).append(separator);

        var removeLink = document.createElement('a');
        $(removeLink).attr('href', 'javascript:void(0);');
        var remove = typeof (ILang.Resources) != 'undefined' ? ILang.Resources.Commands.Remove : 'Remove'
        $(removeLink).text(remove);
        $(removeLink).attr({ userMediaID: userMediaID.toString(), removeLink: 1 });
        var context = this;
        $(removeLink).bind('click', function (e) {
            context.removeUserMedia(e);
        });
        $(userMediaDiv).append(removeLink);
    },

    addTextValue: function (text, userMediaID) {
        if (typeof (text) == 'undefined') {
            text = '';
        }
        if (text.indexOf(userMediaID) == -1) {
            if (text.length > 0) {
                text += ';';
            }
            text += userMediaID.toString();
        }
        return text;
    },

    setReadOnly: function (readOnlyValue) {
        if (readOnlyValue === null || !(typeof readOnlyValue === 'boolean'))
            return;

        this.readOnly = readOnlyValue;

        var rootElement = this.getRootElement();
        rootElement.find('a[removeLink=1]').each(function (index, item) {
            item.style.display = readOnlyValue ? 'none' : '';
        });

        rootElement.find('font[removeLink=1]').each(function (index, item) {
            item.style.display = readOnlyValue ? 'none' : '';
        });
    },

    showCreatedElementSuccessMessage: function () {
        if (this.attachSuccessMessage === null || !(typeof this.attachSuccessMessage === 'string'))
            return;
        if (this.readOnly)
            return;
    },

    setEmbed: function (embed) {
        this.embedObject = embed;
    }
});

function OpenUserMediaDialog(iframeClientID, userMediaType, dialogTitle, caller) {
    title = dialogTitle;
    contenturl = ILangSettings.Sites.ILang() + '/UserMedia/UserMediaDialog.aspx?t=' + userMediaType;//UserMediaDialogTinyMCE
    if (caller != null)
        contenturl += '&caller=' + caller;

    Setdialog(iframeClientID, '', false);
    OpenDialog(iframeClientID);
}

function medSize(id) {
    var obj = document.getElementById(id);
    obj.className = 'medSize sentenceMedia';
}

function minSize(id) {
    var obj = document.getElementById(id);
    obj.className = 'minSize sentenceMedia';
}

function getUserMediaInfo(userMediaID, after) {
    var self = this;
    self.embedCode = '';

    $.ajax({
        type: "POST",
        url: ILangSettings.Sites.API() + '/UserMedia/Info',
        data: { 'userMediaID': userMediaID.toString() },
        async: false
    })
        .done(function (userMedia) {
            if (typeof (after) == 'function')
                after(userMedia);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (typeof (after) == 'function')
                after(null);
        });
}
function getAuthenticatedUserMediaLink(userMediaID, after) {
    var self = this;
    self.embedCode = '';

    $.ajax({
        type: "GET",
        url: ILangSettings.Sites.API() + '/UserMedia/GetOnlineClassVideoInfo',
        data: { 'userMediaID': userMediaID.toString() },
        async: false
    })
        .done(function (userMedia) {
            if (typeof (after) == 'function')
                after(userMedia);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (typeof (after) == 'function')
                after(null);
        });
}

function getUserMediaEmbedCode(userMediaID, after) {
    var self = this;
    self.embedCode = '';

    $.ajax({
        type: "POST",
        url: ILangSettings.Sites.API() + '/UserMedia/Info',
        data: { 'userMediaID': userMediaID.toString() },
        async: false
    })
        .done(function (userMedia) {
            self.embedCode = self.buildUserMediaEmbedCode(userMedia);
            if (typeof (after) == 'function')
                after(self.embedCode)
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            self.embedCode = 'Não foi possível obter o código de embed do objeto de aprendizagem.';
        });

    return self.embedCode;
}

function buildUserMediaEmbedCode(userMedia) {
    switch (userMedia.userMediaTypeID.toString()) {
        case YOUTUBEVIDEO:
            {
                return buildYouTubeEmbedCode(userMedia);
            }
        case VIMEOVIDEO:
            {
                return buildVimeoEmbedCode(userMedia);
            }
        case LOCALVIDEO:
        case ZOOMVIDEO:
            {
                return buildLocalVideoHtmlEmbedCode(userMedia);
            }
        case LOCALDOCUMENT:
            {
                return buildLocalDocumentEmbedCode(userMedia);
            }
        case GOOGLEDOCUMENT:
            {
                return buildGoogleDocumentEmbedCode(userMedia);
            }
        case APAFILE:
        case SWFFILE:
            {
                return buildInteractiveObjectEmbedCode(userMedia, true);
            }
        case INTERNETLINK:
            {
                return buildInteretLinkEmbedCode(userMedia);
            }
        case BOOKREFERENCE:
        case PERIODICREFERENCE:
        case MOVIEREFERENCE:
            {
                return buildBibliographicReferenceEmbedCode(userMedia);
            }
        case BOOKLINK:
            {
                return buildBookLinkEmbedCode(userMedia)
            }
        case LINKINTEGRATION: {
            return buildLinkToIntegrationEmbedCode(userMedia)
        }
        case LOCALIMAGE:
            {
                return buildImageEmbedCode(userMedia);
            }
        case RECORDEDAUDIO:
            {
                return buildRecordedAudioEmbedCode(userMedia);
            }
        case LOCALAUDIO:
            {
                return buildLocalAudioEmbedCode(userMedia);
            }
        case SOUNDCLOUDAUDIO:
            {
                return buildSoundCloudAudioEmbedCode(userMedia);
            }
        case GENERICFILE:
            {
                return buildGenericFileEmbedCode(userMedia);
            }
        default:
            {
                return 'Tipo de vídeo desconhecido (' + userMedia.userMediaTypeID.toString() + ') para obter código de embed';
            }
    }
}

function buildYouTubeEmbedCode(userMedia) {

    var iFrameClientID = 'youTubeFrame' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" class="ilVideoPlayerCont" href="##NOTIFICATIONITEMPAGEURL##" id="youTubeDiv' + userMedia.userMediaID.toString() + '" style="background: black url(' + userMedia.coverImageUrl + ') center no-repeat; background-size: 100%; text-align:center; display: block; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">';
    embedCode += GetYouTubeVideoIFrame(iFrameClientID, userMedia.pathOrUrl).iFrameHTML;
    embedCode += '</a>';

    return embedCode;
}
function buildVimeoEmbedCode(userMedia) {
    var iFrameClientID = 'vimeoFrame' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" class="ilVideoPlayerCont" href="##NOTIFICATIONITEMPAGEURL##" id="VimeoDiv' + userMedia.userMediaID.toString() + '" style="background: black url(' + userMedia.coverImageUrl + ') center no-repeat; background-size: 100%; text-align:center; display: block; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">';
    embedCode += GetVimeoIframe(iFrameClientID, userMedia.pathOrUrl).iFrameHTML;
    embedCode += '</a>';

    return embedCode;
}
function buildSoundCloudAudioEmbedCode(userMedia) {
    var iFrameClientID = 'soundcludFrame' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" class="ilVideoPlayerCont ilAudioPlayerCont" href="##NOTIFICATIONITEMPAGEURL##" id="SoundCloudDiv' + userMedia.userMediaID.toString() + '" style="background: black url(' + userMedia.coverImageUrl + ') center no-repeat; background-size: 100%; text-align:center; display: block; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">';
    embedCode += getSoundCloudIFrame(iFrameClientID, userMedia.pathOrUrl).iFrameHTML;
    embedCode += '</a>';

    return embedCode;
}
function buildLocalVideoHtmlEmbedCode(userMedia) {

    var jwplayerData = '';
    jwplayerData += 'usermediaID:' + userMedia.userMediaID.toString();
    jwplayerData += '|hasBitRates:' + userMedia.hasBitRates.toString();
    if (isSafeToUse(ILang, 'Online.Config.ApplicationURLAddress')) {
        jwplayerData += '|hostName:' + ILang.Online.Config.ApplicationURLAddress.replace('http://', '');
    } else {
        jwplayerData += '|hostName:' + ILangSettings.Sites.Student().replace('http://', '');
    }
    jwplayerData += '|height:' + userMedia.videoHeight.toString();
    jwplayerData += '|width:' + userMedia.videoWidth.toString();

    var coverImageClientID = 'coverImage' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<div class="ilVideoPlayerCont" style="text-align:center; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" href="##NOTIFICATIONITEMPAGEURL##" style="position:absolute; display: block; height: 100%; width: 100%; top: 0; left: 0;" jwplayer-data="' + jwplayerData + '">';
    embedCode += '<img id="' + coverImageClientID + '" src="' + userMedia.coverImageUrl + '" style="border: none; max-width: 100%; min-height: 100%;" />';
    embedCode += '</a>';
    embedCode += '</div>';

    return embedCode;
}

function buildLocalVideoFullHtmlEmbedCode(userMedia) {
    var coverImageClientID = 'coverImage' + userMedia.userMediaID.toString();
    var jwplayerClientID = 'jwplayer' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<div userMediaID="' + userMedia.userMediaID.toString() + '" class="ilVideoPlayerCont" style="text-align:center; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">';
    embedCode += '<a href="##NOTIFICATIONITEMPAGEURL##" style="position:absolute; display: block; height: 100%; width: 100%; top: 0; left: 0;">';
    embedCode += '<img id="' + coverImageClientID + '" src="' + userMedia.coverImageUrl + '" style="border: none; max-width: 100%; min-height: 100%;" />';
    embedCode += '</a>';
    embedCode += '<div class="ilVideoPlayer" style="position:absolute; display: block; height: 100%; width: 100%; top: 0; left: 0;">';
    embedCode += '<div id="' + jwplayerClientID + '">&nbsp;</div>';
    embedCode += '</div>';
    embedCode += '</div>';
    return embedCode;
}

function buildLocalVideoJavaScriptEmbedCode(userMedia) {

    var coverImageClientID = 'coverImage' + userMedia.userMediaID.toString();
    var jwplayerClientID = 'jwplayer' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += 'jwplayer("' + jwplayerClientID + '").setup({' + '// userMediaID="' + userMedia.userMediaID.toString() + '"\r\n';
    embedCode += 'modes: [{ type: "html5" }, { type: "flash", src: "/src/jwplayer5/player.swf" }], primary: "html5",';
    if (userMedia.hasBitRates) {
        var handlerUrl = ILang.Online.Config.ApplicationURLAddress + '/Handlers/UserMedia/GetUserMediaPlayListXML.ashx?usermediaid=' + userMedia.userMediaID.toString() + '&imageName=' + userMedia.imageName;
        embedCode += 'playlistfile: "' + handlerUrl + '",';
    }
    else {
        embedCode += 'file: "' + userMedia.pathOrUrl + '",';
        if ($.hasData(userMedia.coverImageUrl)) {
            embedCode += 'image: "' + userMedia.coverImageUrl + '",';
        }
    }
    embedCode += 'provider: "http",';
    embedCode += 'startparam: "start",';
    embedCode += 'height: ' + userMedia.videoHeight.toString() + ',';
    embedCode += 'width: ' + userMedia.videoWidth.toString() + ',';
    embedCode += 'id: "' + jwplayerClientID + '",';
    embedCode += 'autostart: false,';
    embedCode += 'allowfullscreen: true,';
    embedCode += 'allowscriptaccess: "sameDomain",';
    embedCode += 'screencolor: "000000",';
    embedCode += 'skin: "/src/jwplayer5/videoSkin/bekle.xml",';
    embedCode += 'controlbar: "over",';
    embedCode += 'playlist: "none",';
    embedCode += 'bufferlength: "10",';
    embedCode += 'displayclick: "play",';
    embedCode += 'icons: "true",';
    embedCode += 'quality: "High",';
    embedCode += 'repeat: "none",';
    embedCode += 'stretching: "uniform",';
    embedCode += 'bgcolor: "ffffff",';
    embedCode += 'wmode: "transparent",';
    embedCode += 'events: {';
    embedCode += 'onReady: function (event) { window.parent.setTinyMCEIFrameHeightAfterLoad(document.body.offsetHeight, document.location.href); },';
    embedCode += 'onComplete: function (event) { jwplayer("' + jwplayerClientID + '").setFullscreen(false); }';
    embedCode += '}';
    embedCode += '});';
    embedCode += 'document.getElementById("' + coverImageClientID + '").style.display = "none";';
    return embedCode;
}

function buildLocalDocumentEmbedCode(userMedia) {
    var newHtml = getLocalDocument_VIEWER_Html(userMedia);
    return newHtml;
}
var documentExtensionItem = function (className, red, green, blue, opacity1, opacity2) {
    return {
        ClassName: className,
        RED: red,
        GREEN: green,
        BLUE: blue,
        OPACITY1: opacity1 == null ? 0.25 : opacity1,
        OPACITY2: opacity2 == null ? 0.85 : opacity2
    };
}
var DocumentExtensionDictionary = {
    ilVTPdf: documentExtensionItem('ilVTPdf', 191, 20, 18),
    ilVTDoc: documentExtensionItem('ilVTDoc', 41, 116, 184),
    ilVTXls: documentExtensionItem('ilVTXls', 19, 116, 62),
    ilVTPpt: documentExtensionItem('ilVTPpt', 219, 92, 48),
    ilVTImg: documentExtensionItem('ilVTImg', 90, 49, 139),
    ilVTZip: documentExtensionItem('ilVTZip', 113, 80, 41),
    ilVTGenericDownload: documentExtensionItem('ilVTGenericFile', 97, 135, 153),
    ilVTGenericViewer: documentExtensionItem('ilVTGenericFile', 97, 135, 153)
}
function getDocumentExtensionData(cssTemplate) {
    if (DocumentExtensionDictionary.hasOwnProperty(cssTemplate)) {
        var item = DocumentExtensionDictionary[cssTemplate];
        return item;
    } else {
        return getDocumentExtensionData('GENERIC', Title);
    }
}
function getLocalDocument_VIEWER_Template() {
    var html = '<div open-on-viewer usermediaid="{ID}" class="ilViewerThumb {ClassName}" onclick={CLICK} style="width: calc(100% - 20px); max-width: 535px; height: 400px; max-height: 90vw; margin: 20px auto; position: relative; box-shadow: 0px 3px 15px rgba(0,0,0,.2); mix-blend-mode: multiply; border-radius: 8px; overflow: hidden; cursor: pointer; font-family:\'Verdana\'">';
    html += '       <div class="ilVTCoverGeneric" style="width: 100px; opacity: .25; width: 100%; height: 100%; text-align: center; background: rgba({RED}, {GREEN}, {BLUE}, {OPACITY1});">';
    html += '			<svg version="1.1" x="0px" y="0px" viewBox="0 0 49 64" style="enable-background:new 0 0 49 64; height: 100px; margin-top: 23%; font-family:\'Verdana\'; font-weight: bold; font-size:14px;" xml:space="preserve">';
    html += '				<path style="fill: rgba({RED}, {GREEN}, {BLUE}, {OPACITY2});" d="M49,16.8v42.1c0,2.8-2.3,5.1-5.1,5.1H5.1C2.3,64,0,61.7,0,58.9V5.1C0,2.3,2.3,0,5.1,0h27L49,16.8z"></path>';
    html += '				<path style="opacity: .45; fill: #fff;" d="M49,15.9V18H35.2c-2.9,0-4.2-2.3-4.2-5.2V0h2.1L49,15.9z"></path>';
    html += '				<text style="fill: #fff;" transform="matrix(1 0 0 1 24.5956 46.2295)" text-anchor="middle">{EXTENSION}</text>';
    html += '			</svg>	';
    html += '       </div>';
    html += '       <div class="ilVTCover" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background-size: cover; background-position: top center; background-image: url({COVER_PATH});"></div>';
    html += '		<div class="ilVTDetail" style="height: 55px; width: 100%; box-sizing: border-box; padding: 0 8px; position: absolute; bottom: 0; transition: all 0.2s ease-out; overflow: hidden; white-space: nowrap;     background: rgba({RED}, {GREEN}, {BLUE}, {OPACITY2});">';
    html += '		    <em style="height: 100%; width: 1px; display: inline-block; vertical-align: middle;"></em>';
    html += '		    <em class="ilVTIcon" style="height: 30px; line-height: 30px; font-size: 17px; font-weight: bold; color: white; background: rgba(0, 0, 0, .35); padding: 0 10px; border-radius: 8px; display: inline-block; mix-blend-mode: multiply; vertical-align: middle; font-style: normal;">{EXTENSION}</em>';
    html += '		    <strong class="fBold white phs dib pRel vam" style="font-weight: bold; color: white; padding: 0 0 0 5px; max-width: calc(100% - 96px); display: inline-block; vertical-align: middle; max-height: calc(100% - 24px); overflow: hidden; text-overflow: ellipsis; white-space: normal;">{TITLE}</strong>';
    html += '		</div>';
    html += '	</div>';
    return html;
}
function getLocalDocument_DOWLOAD_Template() {

    var html = '<div class="ilViewerThumb ilVTGenericFile" style="width: calc(100% - 20px); max-width: 535px; height: 400px; max-height: 90vw; margin: 20px auto; position: relative; box-shadow: 0px 3px 15px rgba(0,0,0,.2); mix-blend-mode: multiply; border-radius: 8px; overflow: hidden; cursor: pointer; font-family:\'Verdana\'">';
    html += '       <a href="{FILE}" download="{FILE}" style="position: absolute; top: 0; left: 0; height: 100%; width: 100%; z-index: 99; cursor: pointer;"></a>';
    html += '		<div class="ilVTCoverGeneric" style="width: 100px; opacity: .25; width: 100%; height: 100%; text-align: center;     background: rgba(97, 135, 153, .25);">';
    html += '			<svg version="1.1" x="0px" y="0px" viewBox="0 0 49 64" style="enable-background:new 0 0 49 64; height: 100px; margin-top: 23%; font-family:\'Verdana\'; font-weight: bold; font-size:14px;" xml:space="preserve">';
    html += '				<path style="fill: rgba(97, 135, 153, .85);" d="M49,16.8v42.1c0,2.8-2.3,5.1-5.1,5.1H5.1C2.3,64,0,61.7,0,58.9V5.1C0,2.3,2.3,0,5.1,0h27L49,16.8z"></path>';
    html += '				<path style="opacity: .45; fill: #fff;" d="M49,15.9V18H35.2c-2.9,0-4.2-2.3-4.2-5.2V0h2.1L49,15.9z"></path>';
    html += '				<text style="fill: #fff;" transform="matrix(1 0 0 1 24.5956 46.2295)" text-anchor="middle">{EXTENSION}</text>';
    html += '			</svg>';
    html += '		</div>';
    html += '		<div class="ilVTDetail" style="height: 55px; width: 100%; box-sizing: border-box; padding: 0 8px; position: absolute; bottom: 0; transition: all 0.2s ease-out; overflow: hidden; white-space: nowrap;     background: rgba(97, 135, 153, .85);">';
    html += '			<em style="height: 100%; width: 1px; display: inline-block; vertical-align: middle;"></em>';
    html += '			<em class="ilVTIcon" style="height: 30px; line-height: 30px; font-size: 17px; font-weight: bold; color: white; background: rgba(0, 0, 0, .35); padding: 0 10px; border-radius: 8px; display: inline-block; mix-blend-mode: multiply; vertical-align: middle; font-style: normal;">{EXTENSION}</em>';
    html += '			<strong class="fBold white phs dib pRel vam" style="font-weight: bold; color: white; padding: 0 0 0 5px; max-width: calc(100% - 96px); display: inline-block; vertical-align: middle; max-height: calc(100% - 24px); overflow: hidden; text-overflow: ellipsis; white-space: normal;">{TITLE}</strong>';
    html += '		</div>';
    html += '	</div>';
    return html;
}
function getLocalDocument_VIEWER_Html(userMedia) {
    var html = getLocalDocumentTemplate(userMedia);
    var data = getDocumentExtensionData(userMedia.cssTemplate || 'ilVTGenericViewer');
    if (data != null) {
        data["ID"] = userMedia.userMediaID;
        data["CLICK"] = "openOnViewer(\'" + userMedia.pathOrUrl + "\')";
        data["FILE"] = userMedia.pathOrUrl;
        data["TITLE"] = userMedia.title;
        data["EXTENSION"] = userMedia.extension;
        if (userMedia.coverImageUrl != "" && userMedia.coverImageUrl != null) {
            data["COVER_PATH"] = userMedia.coverImageUrl;
        } else {
            data["COVER_PATH"] = "";
        }


        data.everyEach(function (value, prop) {
            html = html.replaceAll('{' + prop + '}', value);
        });
    }
    return html;
}
function getLocalDocumentTemplate(userMedia) {
    if (userMedia.viewerEnabled) {
        return getLocalDocument_VIEWER_Template()
    } else {
        return getLocalDocument_DOWLOAD_Template();
    }
}



function buildGoogleDocumentEmbedCode(userMedia) {

    var iconImageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/ilDriveIcons.svg';

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" href="' + userMedia.pathOrUrl + '" style="text-align: center; width: 280px; border: 1px solid #d4d4d4; box-shadow: 0 1px 8px #e9e9e9; padding: 15px; text-decoration: none; color: #69d; cursor: pointer; font-size: 10px; display: inline-block; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: pointer; max-width: 100%; box-sizing: border-box;">';
    embedCode += '<span style="display: inline-block; background: url(' + iconImageUrl + ') 0 0 no-repeat; width: 51px; height: 65px; background-size: cover; opacity: .6; margin: 10px;"></span>';
    embedCode += '<span style="font-size: 12px; font-weight: bold; margin-bottom:3px; display: block">' + userMedia.title + '</span>';
    embedCode += '<span style="color:#aaa; padding-bottom: 10px; display: inline-block; line-height: 17px;">Clique para abrir o documento</span>';
    embedCode += '</a>';

    return embedCode;
}

function buildInteretLinkEmbedCode(userMedia) {

    var imageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/link.png';

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" href="' + userMedia.pathOrUrl + '" style="color:#69d; text-decoration:none; font-size:11px; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: pointer;" target="_blank">';
    embedCode += '<img src="' + imageUrl + '" style="vertical-align:middle; border:none;margin-right:2px;" />';
    embedCode += '<span style="vertical-align:middle">' + userMedia.pathOrUrl + '</span>';
    embedCode += '</a>';

    return embedCode;
}

function buildBibliographicReferenceEmbedCode(userMedia) {

    var imageName = '';
    switch (userMedia.userMediaTypeID.toString()) {
        case BOOKREFERENCE:
            {
                imageName = 'book.png';
                break;
            }
        case PERIODICREFERENCE:
            {
                imageName = 'news.png';
                break;
            }
        case MOVIEREFERENCE:
            {
                imageName = 'dvd.png';
                break;
            }
    }

    var imageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/' + imageName;

    var embedCode = '';
    embedCode += '<div userMediaID="' + userMedia.userMediaID.toString() + '" style="font-size:11px; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: default;">';
    embedCode += '<img src="' + imageUrl + '" style="vertical-align:middle;margin-right:2px;" />';
    embedCode += '<span style="vertical-align:middle">';
    embedCode += userMedia.abnt;
    embedCode += '</span>';
    embedCode += '</div>';

    return embedCode;
}

function buildImageEmbedCode(userMedia) {

    var embedCode = '';
    embedCode += '<img userMediaID="' + userMedia.userMediaID.toString() + '" src="' + userMedia.pathOrUrl + '" style="max-width: 100%;">';
    embedCode += '</img>';

    return embedCode;
}
function buildBookLinkEmbedCode(userMedia) {

    var html = getLocalDocument_VIEWER_Template();
    var data = getDocumentExtensionData(userMedia.cssTemplate || 'ilVTGenericViewer');
    if (data != null) {
        data["ID"] = userMedia.userMediaID;
        data["CLICK"] = "parent.openUrlNewTab(\'" + ILangSettings.Sites.ILang() + userMedia.pathOrUrl + "\')";
        data["FILE"] = userMedia.pathOrUrl;
        data["TITLE"] = userMedia.title;
        data["EXTENSION"] = "Book";
        if (userMedia.coverImageUrl != "" && userMedia.coverImageUrl != null) {
            data["COVER_PATH"] = userMedia.coverImageUrl;
        } else {
            data["COVER_PATH"] = "";
        }


        data.everyEach(function (value, prop) {
            html = html.replaceAll('{' + prop + '}', value);
        });
    }
    return html;
}

function buildLinkToIntegrationEmbedCode(userMedia) {
    var imageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/link.png';

    var url = ILangSettings.Sites.Social() + userMedia.pathOrUrl + '&userMediaID=' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" href="' + url + '" style="color:#69d; text-decoration:none; font-size:11px; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: pointer;" target="_blank">';
    embedCode += '<span style="vertical-align:middle">' + userMedia.title + '</span>';
    embedCode += '</a>';

    return embedCode;
}


function buildRecordedAudioEmbedCode(userMedia) {

    // Audio url
    var audioUrl = userMedia.pathOrUrl;
    audioUrl = audioUrl.replace(/#/g, '$');

    var url = ILang.Online.Config.ApplicationURLAddress + '/src/AudioPlayerViewer.aspx?A=' + audioUrl;

    // iFrame
    var iFrameID = 'iframeaudio_' + Math.floor(Math.random() * userMedia.userMediaID).toString();
    var embedCode = '';
    embedCode += '<em id="recordedAudio"></em>';
    embedCode += '<iframe userMediaID="' + userMedia.userMediaID.toString() + '" id="' + iFrameID + '" src="' + url + '" frameborder="0" scrolling="no" width="305" height="100"></iframe>';

    return embedCode;
}

function buildLocalAudioEmbedCode(userMedia) {

    var embedCode = '';
    embedCode += buildLocalAudioHtmlEmbedCode(userMedia);
    embedCode += '<script type="text/javascript">';
    embedCode += buildLocalAudioJavaScriptEmbedCode(userMedia);
    embedCode += '</script>';

    return embedCode;
}

function buildLocalAudioHtmlEmbedCode(userMedia) {

    var playImageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/gradVertGray.jpg';
    var audioImageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/ilDriveAudioImage.jpg';
    var coverImageClientID = 'coverImage' + userMedia.userMediaID.toString();
    var jwplayerClientID = 'jwplayer' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += '<div userMediaID="' + userMedia.userMediaID.toString() + '" style="padding: 12px 13px; border-radius: 7px; display:inline-block; border: 1px solid #d4d4d4; box-shadow: 0 1px 3px #e1e1e1; background: url(' + playImageUrl + ') top; cursor:pointer;">';
    embedCode += '<a href="##NOTIFICATIONITEMPAGEURL##" style="display: block">';
    embedCode += '<img id="' + coverImageClientID + '" src="' + audioImageUrl + '" />';
    embedCode += '</a>';
    embedCode += '<div id="' + jwplayerClientID + '"></div>';
    embedCode += '</div>';

    return embedCode;
}

function buildLocalAudioJavaScriptEmbedCode(userMedia) {

    var jwplayerClientID = 'jwplayer' + userMedia.userMediaID.toString();
    var coverImageClientID = 'coverImage' + userMedia.userMediaID.toString();

    var embedCode = '';
    embedCode += 'jwplayer("' + jwplayerClientID + '").setup({' + '// userMediaID="' + userMedia.userMediaID.toString() + '"\r\n';
    embedCode += 'flashplayer: "/src/jwplayer5/player.swf",';
    embedCode += 'id: "' + jwplayerClientID + '",';
    embedCode += 'width: 400,';
    embedCode += 'height: 46,';
    embedCode += 'file: "' + userMedia.pathOrUrl + '",';
    embedCode += 'skin: "/src/jwplayer5/audioSkin.zip",';
    embedCode += 'controlbar: "bottom",';
    embedCode += 'bgcolor: "ffffff",';
    embedCode += 'screencolor: "ffffff",';
    embedCode += 'icons: false,';
    embedCode += 'wmode: "transparent"';
    embedCode += '});';
    embedCode += 'document.getElementById("' + coverImageClientID + '").style.display = "none";';

    return embedCode;
}

function buildGenericFileEmbedCode(userMedia) {

    var iconImageUrl = ILangSettings.Sites.Static() + '/App_Themes/ILang/img/ilDriveIcons.svg';

    var embedCode = '';
    embedCode += '<a userMediaID="' + userMedia.userMediaID.toString() + '" href="' + userMedia.pathOrUrl + '" style="text-align: center; width: 280px; border: 1px solid #d4d4d4; box-shadow: 0 1px 8px #e9e9e9; padding: 15px; text-decoration: none; color: #69d; cursor: pointer; font-size: 10px; display: inline-block; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; cursor: pointer; max-width: 100%; box-sizing: border-box;">';
    embedCode += '<span style="display: inline-block; background: url(' + iconImageUrl + ') -52px 0 no-repeat; width: 51px; height: 65px; background-size: cover; opacity: .6; margin: 10px;"></span>';
    embedCode += '<span style="font-size: 12px; font-weight: bold; margin-bottom:3px; display: block">' + userMedia.fileName + '</span>';
    embedCode += '<span style="color:#aaa; padding-bottom: 10px; display: inline-block; line-height: 17px;">' + userMedia.fileSize + '</span>';
    embedCode += '</a>';

    return embedCode;
}

function buildInteractiveObjectEmbedCode(userMedia, isInsideTinyMCE) {

    var script = '';

    if (isInsideTinyMCE)
        script = 'window.parent.openDialogInteractiveObject(' + userMedia.userMediaID + ');';
    else
        script = 'openDialogInteractiveObject(' + userMedia.userMediaID + ');';

    var embedCode = '';
    embedCode += '<div userMediaID="' + userMedia.userMediaID.toString() + '" class="ilViewerThumb" onclick="' + script + '" style="width: calc(100% - 20px); max-width: 535px; height: 400px; max-height: 90vw; margin: 20px auto; position: relative; box-shadow: 0px 3px 15px rgba(0,0,0,.2); mix-blend-mode: multiply; border-radius: 8px; overflow: hidden; cursor: pointer; font-family: Verdana;">';
    embedCode += '<div class="ilVTCover" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background-size: cover; background-position: top center; background-image: url(http://d1o95oh6yh30t4.cloudfront.net/App_Themes/ILang/img/Objeto_Interativo_Background.jpg);"></div>';
    embedCode += '<div class="ilVTDetail" style="height: 55px; width: 100%; box-sizing: border-box; padding: 0 8px; position: absolute; bottom: 0; transition: all 0.2s ease-out; overflow: hidden; white-space: nowrap; background: rgba(73, 50, 146, .85);">';
    embedCode += '<em style="height: 100%; width: 1px; display: inline-block; vertical-align: middle;"></em>';
    embedCode += '<em class="ilVTIcon" style="height: 30px; line-height: 30px; font-size: 13px; font-weight: bold; color: rgba(255,255,255,.25); background: rgba(0, 0, 0, .35); padding: 0 10px; border-radius: 8px; display: inline-block; vertical-align: middle; font-style: normal;">Objeto Interativo</em>';
    embedCode += '<strong class="fBold white phs dib pRel vam" style="font -weight: bold; color: white; padding: 0 0 0 5px; max-width: calc(100% - 160px); display: inline-block; vertical-align: middle; max-height: calc(100% - 24px); overflow: hidden; text-overflow: ellipsis; white-space: normal;">' + userMedia.title + '</strong>';
    embedCode += '</div>';
    embedCode += '</div>';

    return embedCode;
}

function openDialogInteractiveObject(userMediaID) {

    $.post(ILangSettings.Sites.API() + '/UserMedia/Info', { userMediaID: userMediaID.toString() })
        .success(function (userMedia) {
            var dialogHTML = '';
            dialogHTML += '<div id="divInteractiveHtml" class="socialDialog fullDialog ObjDialog hide">';
            dialogHTML += '<div class="sdBox">';
            dialogHTML += '<a href="javascript:closeDialogInteractiveObject();" class="ilangIcon sdClose" title="Fechar">&nbsp;</a>';
            dialogHTML += '<div class="sdTop">';
            dialogHTML += '<p class="fb phm">';
            dialogHTML += userMedia.title;
            dialogHTML += '</p>';
            dialogHTML += '</div>';
            dialogHTML += '<div class="sdContent">';
            if (userMedia.userMediaTypeID.toString() == APAFILE) {
                dialogHTML += '<iframe src="' + userMedia.pathOrUrl + '" class="contFull iframeDialog"></iframe>';
            }
            else if (userMedia.userMediaTypeID.toString() == SWFFILE) {
                var swfUrl = userMedia.pathOrUrl;
                if (swfUrl.indexOf('?') > 0) {
                    swfUrl = swfUrl.substr(0, swfUrl.length - (swfUrl.length - swfUrl.indexOf('?')));
                }
                var objectID = 'video_' + Math.floor(Math.random() * userMedia.userMediaID).toString();
                var objectHTML = '<object ';
                objectHTML = objectHTML + 'id="' + objectID + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9.0.0.0" width="100%" height="100%" salign="lt" align="left">';
                objectHTML = objectHTML + '<param name="allowScriptAccess" value="sameDomain" />';
                objectHTML = objectHTML + '<param name="allowFullScreen" value="True" />';
                objectHTML = objectHTML + '<param name="quality" value="High" />';
                objectHTML = objectHTML + '<param name="bgcolor" value="#FFFFFF" />';
                objectHTML = objectHTML + '<param name="scale" value="default" />';
                objectHTML = objectHTML + '<param name="movie" value="' + swfUrl + '"></param>';
                objectHTML = objectHTML + '<param name="swliveconnect" value="False"></param>';
                objectHTML = objectHTML + '<param name="menu" value="False"></param>';
                objectHTML = objectHTML + '<param name="scale" value="showAll"></param>';
                objectHTML = objectHTML + '<param name="wmode" value="Transparent"></param>';
                objectHTML = objectHTML + '<embed src="' + swfUrl + '" wmode="Transparent" quality="High" bgcolor="#FFFFFF" name="swfPlayer" salign="lt" align="left" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"  width="100%" height="100%" />';
                objectHTML = objectHTML + '</object>';
                dialogHTML += objectHTML;
            }
            dialogHTML += '</div>';
            dialogHTML += '</div>';
            dialogHTML += '</div>';

            $('#umDialogSwf').html(dialogHTML);
            setTimeout(function () {
                $('#divInteractiveHtml').removeClass('hide');
            }, 250);
        })
        .error(function (data, status, xhr) {
            alert('Erro ao obter dados do objeto de aprendizagem: ' + status);
        });
}
function SetDialogFullScreen(title, iframeUrl) {
    var dialogHTML = '<div id="divInteractiveHtml" class="socialDialog fullDialog ObjDialog hide cardLinkDisDialog">';
    dialogHTML += '<div class="sdBox">';
    dialogHTML += '<a href="javascript:closeDialogInteractiveObject();" class="ilangIcon sdClose" title="Fechar">&nbsp;</a>';
    dialogHTML += '<div class="sdTop">';
    dialogHTML += '<p class="fb phm">';
    dialogHTML += title;
    dialogHTML += '</p>';
    dialogHTML += '</div>';
    dialogHTML += '<div class="sdContent">';
    dialogHTML += '<iframe src="' + iframeUrl + '" class="contFull iframeDialog"></iframe>';
    dialogHTML += '</div>';
    dialogHTML += '</div>';
    dialogHTML += '</div>';
    $('#umDialogSwf').html(dialogHTML);
    setTimeout(function () {
        $('#divInteractiveHtml').removeClass('hide');
    }, 250)
}
function openDialogWebTV(title, pathOrUrl) {
    var dialogHTML = '';
    dialogHTML += '<div id="divInteractiveHtml" class="socialDialog fullDialog ObjDialog hide">';
    dialogHTML += '<div class="sdBox">';
    dialogHTML += '<a href="javascript:closeDialogInteractiveObject();" class="ilangIcon sdClose" title="Fechar">&nbsp;</a>';
    dialogHTML += '<div class="sdTop">';
    dialogHTML += '<p class="fb phm">' + title + '</p>';
    dialogHTML += '</div>';
    dialogHTML += '<div class="sdContent">';
    dialogHTML += '<iframe  src="' + pathOrUrl + '" class="contFull iframeDialog" allowfullscreen allowscriptaccess></iframe>';
    dialogHTML += '</div>';
    dialogHTML += '</div>';
    dialogHTML += '</div>';
    $('#umDialogSwf').html(dialogHTML);
    $('#divInteractiveHtml').removeClass('hide');
}

function closeDialogInteractiveObject() {
    $('#divInteractiveHtml').addClass('hide');
    setTimeout(function () {
        $('#umDialogSwf').html('');
    }, 500);
}

if (typeof (Sys) != 'undefined' && typeof (Sys.Application) != 'undefined')
    Sys.Application.notifyScriptLoaded();
