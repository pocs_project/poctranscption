﻿ILangApp.directive('categoryOffline', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function () { return getCategoryAngularTemplate('Offline'); },
        scope: {
            allprops: '='
        },
        controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;
            me.exercisedto = me.allprops.Detail;
            me.exercisedto.ShowStudentLessonCategoryPost = true;
            registerCommonFunction(me);
            changeFocusKeyDowIframe();
        }
    };
});
