﻿ILangApp.directive('assignmentPost', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function () { return getCategoryAngularTemplate('AssignmentPost'); },
        scope: {
            allprops: '=',
            contenteditableenabled: '='
        },
        controller: function ($scope, $element, $attrs) {
            let me = $scope;
            me.dto = { List: [] };
            me.showPostBto = false;
            me.busy = false;
            me.root = getRootScope($scope);
            me.enums = me.root.enums;
            me.serverParameters = me.root.serverParameters;
            me.commentTextField = ''
            me.commentSelectedIDs = [];
            me.heightTextArea = 0;
            me.contentPlayerService = new ContentPlayerService();
            const assignmentTypeNewGradeLtiId = 45;
            const assignmentTypeChangeGradeId = 46;
            const ltiProviderResourceIdGoMining = 206;
            me.openViewCount = function () {
                if (typeof (me.userList) == 'undefined' || me.userList == null)
                    return;
                me.userList.title(me.title);
                me.userList.viewCount(me.viewCount);
                let param = {};
                me.userList.get('loadForumViewers', param);
            };
            me.comment = function () {
                if (me.busy)
                    return;

                let fn = null;
                if (me.contenteditableenabled && isSafeToUse(me, '_contentEditable')) {
                    me.commentTextField = me._contentEditable.comment;
                    me.commentSelectedIDs = me._contentEditable.selectedList;
                    fn = function () { me._contentEditable.cleanAll(); }
                }
                if (isSafeToUse(me, 'mention.getHtml')) {
                    me.commentTextField = me.mention.getHtml();
                    me.commentSelectedIDs = me.mention.getSelectedsIDs();
                    fn = function () { me.mention.clean(); }
                }

                if (me.commentTextField != null && me.commentTextField != '') {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.Comment = me.commentTextField;
                    param.UserIDList = me.commentSelectedIDs;
                    if (isSafeToUse(me.allprops, 'Detail.ActivityGroupID'))
                        param.ActivityGroupID = me.allprops.Detail.ActivityGroupID;

                    me.root.action('studentLessonCategoryPostComment', param, function (result) {
                        if (result.Success) {
                            me.dto.List = me.dto.List || [];
                            me.dto.List.push(result.Result);
                            me.commentTextField = '';
                            if (fn != null) fn();
                            me.busy = false;

                            feedObserver.observe(document.querySelector(".feed"), { subtree: false, childList: true });
                        }
                    });
                }
            };

            let feedObserver = new MutationObserver(function (mutations_list) {
                mutations_list.forEach(function (mutation) {
                    mutation.addedNodes.forEach(function (added_node) {
                        if (added_node.className) {
                            if (added_node.className.includes("feedLine")) {
                                me.autoScroll();
                                me.textAreaResetHeight();
                                feedObserver.disconnect();
                            }
                        }
                    });
                });
            });

            me.onLoad = function () {
                if (me.serverParameters.parameters.showStudentLessonCategory) {
                    me.root.assignmentPostInstance = me;
                    me.$parent.assignmentPostInstance = me;
                }
                me.loadPosts();
            };
            me.getDefaultParameters = function () {
                return {
                    StudentLessonID: me.allprops.StudentLessonID,
                    StudentTrackID: me.allprops.StudentTrackID || 0,
                    ClassDisciplineID: parseInt(me.root.serverParameters.actions.load.data.ClassDisciplineID),
                    LessonID: me.root.bigContext.dto.LessonID,//parseInt(me.serverParameters.actions.load.data.LessonID),
                    LessonCategoryID: parseInt(me.allprops.LessonCategoryID),
                    StudentID: parseInt(me.root.serverParameters.actions.load.data.StudentID)
                }
            };
            me.refresh = function () {
                let maxid = me.dto.List != null ? me.dto.List.max('StudentLessonCategoryPostID') : null;
                if (maxid != null) {
                    me.loadPosts({ StudentLessonCategoryPostID: maxid, Next: true });
                } else {
                    me.loadPosts({ Next: true });
                }
            };
            me.insertLtiResourceStartedMessage = async function (messageList, ClassDisciplineID, LessonCategoryID) {
                if (!messageList.some(item => item.AssignmentPostTypeID === Enum.assignmentPostTypeEnum.LtiResourceStarted)) {
                    let result = await me.contentPlayerService.postAssignmentPost(
                        {
                            AssignmentPostType: Enum.assignmentPostTypeEnum.LtiResourceStarted,
                            ClassDisciplineID: ClassDisciplineID,
                            LessonCategoryID: LessonCategoryID
                        });
                    if (result.Success) {
                        if (result.Result != 0) {
                            me.refresh();
                        }
                    }
                }
            };
            me.loadPosts = function (parameters) {
                if (me.busy)
                    return;

                me.busy = true;
                let param = me.getDefaultParameters();
                copyAllPropersTo(param, parameters);

                if (isSafeToUse(me.allprops, 'Detail.ActivityGroupID'))
                    param.ActivityGroupID = me.allprops.Detail.ActivityGroupID;

                if (isSafeToUse(parameters, 'StudentLessonCategoryPostID'))
                    param.StudentLessonCategoryPostID = parameters.StudentLessonCategoryPostID;

                me.root.action('studentLessonCategoryPost', param, function (result) {
                    me.busy = false;
                    if (result.Success) {
                        if (parameters == undefined) {
                            if (result.Result == null) {
                                me.dto = { List: [] };
                            } else {
                                me.dto = result.Result;
                            }
                            if (me.allprops.LessonCategoryType == Enum.lessonCategoryTypeEnum.LtiGradeResource) {
                                me.insertLtiResourceStartedMessage(result.Result.List, param.ClassDisciplineID, param.LessonCategoryID);
                            }
                        } else if (isSafeToUse(result, 'Result.List')) {
                            if (parameters.hasOwnProperty('Next') && parameters.Next) {
                                me.dto.List.union(result.Result.List);
                            } else {
                                me.dto.List.includeBefore(result.Result.List);
                            }
                        }
                    }
                });
            };
            me.getMorePosts = function () {
                me.loadPosts({ StudentLessonCategoryPostID: me.dto.List[0].StudentLessonCategoryPostID });
            };
            me.deletePost = function (post, list) {
                if (!post.AllowDelete|| me.busy)
                    return;

                let param = me.getDefaultParameters();
                param.StudentLessonCategoryPostID = post.StudentLessonCategoryPostID;

                me.busy = true;
                me.root.action('studentLessonCategoryPostDelete', param, function (result) {
                    if (result.Success) {
                        list.remove({ StudentLessonCategoryPostID: post.StudentLessonCategoryPostID });
                    }
                    me.busy = false;
                });
            };
            me.onError = function (error) {
                me.busy = false;
                me.generalError(error);
            }
            me.showHistory = function (post) {
                if (me.busy)
                    return;

                if (post.History == undefined) {
                    me.busy = false;
                    post.loadingHistory = true;
                    let param = me.getDefaultParameters();
                    param.HistoryID = post.HistoryID;
                    param.ActivityGroupID = me.allprops.Detail.ActivityGroupID;
                    me.root.action('studentLessonCategoryPostGetHistory', param, function (result) {
                        me.busy = false;
                        post.loadingHistory = false;
                        if (result.Success) {
                            post.historyIsOpen = true;
                            post.History = result.Result;
                        }
                    });
                } else {
                    post.historyIsOpen = true;
                }
            };
            me.hideHistory = function (post) {
                post.historyIsOpen = false;
            };
            me.showPostBtoFn = function () {
                me.showPostBto = true;
            };
            me.openDocument = function (fileUrl) {
                if (isSafeToUse(window, 'openDocumentOnViewer'))
                    window.openDocumentOnViewer(fileUrl);
            }

            me.autoScroll = function () {
                let el = $($element);
                if (el.length > 0) {
                    let h = el[0].scrollHeight;
                    el[0].scrollBy(0, h);
                }

                if (me.heightTextArea === 0)
                    me.heightTextArea = $element.find('textarea').height();
            };

            me.textAreaResetHeight = () => {
                $element.find('textarea').height(me.heightTextArea);
            };

            me.validateAssignmentPostTypeLti = function (assignmentPostTypeID, ltiProviderResourceId) {
                return me.isAssignmentPostTypeLti(assignmentPostTypeID) && ltiProviderResourceId != ltiProviderResourceIdGoMining;
            };

            me.validateAssignmentPostTypeLtiGoMining = function (assignmentPostTypeID, ltiProviderResourceId) {
                return me.isAssignmentPostTypeLti(assignmentPostTypeID) && ltiProviderResourceId == ltiProviderResourceIdGoMining;
            };

            me.isAssignmentPostTypeLti = function (assignmentPostTypeID) {
                return assignmentPostTypeID == assignmentTypeNewGradeLtiId || assignmentPostTypeID == assignmentTypeChangeGradeId;
            };
        }
    };
});
