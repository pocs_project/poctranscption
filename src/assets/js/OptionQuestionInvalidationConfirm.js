﻿ILangApp.directive('optionQuestionInvalidationConfirm', () => {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => {
            return getStaticUrl('/Templates/Dialog/OptionQuestionInvalidationConfirm.html');
        }
    };
});
