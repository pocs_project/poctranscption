﻿
ILangApp.directive('ltiResource', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => getCategoryAngularTemplate('LtiResource'),
        scope: {
            allprops: '='
        },
        controller: function ($scope, $element) {
            const me = $scope;
            const ltiProviderGoMining = 206;

            me.root = getRootScope($scope);

            me.hasError = false;
            me.LtiViewMode = $scope.$parent.serverParameters.parameters.LtiViewMode;
            me._ltiResource = undefined;
            me._currentLessonCategory = undefined;
            me.showContent = false;

            me.isGoMiningProvider = function () {
                return ltiProviderGoMining === me._ltiResource.LtiProviderResourceId;
            };

            me.processLti = function (ltiResource) {
                me.renderForm(ltiResource);

                if (me.isGoMiningProvider()) {
                    me.showContent = !me.hasError;
                    me.loadStyleGoMining();
                } else {
                    me.showContent = !me.hasError && !ltiResource.ExpiredDeliveryDate;
                }
            };

            me.onInit = function () {
                me.service = new LtiService();
                me._currentLessonCategory = me.loadCurrentLessonCategory();
                me._ltiResource = me._currentLessonCategory.Detail;

                if (me._ltiResource.Parameters) {
                    me.processLti(me._ltiResource);
                }
            };

            me.loadStyleGoMining = function () {
                let linkStyleLtiGoMining = document.createElement('link');

                linkStyleLtiGoMining.rel = 'stylesheet';
                linkStyleLtiGoMining.type = 'text/css';
                linkStyleLtiGoMining.href = `${ILangSettings.Sites.Static()}/App_Themes/ILang/css/Category/LtiResource.css`;

                document.head.appendChild(linkStyleLtiGoMining);
            };

            me.reOpenLti = function () {
                me.reLoadLtiResourceDetail();
            };

            me.loadCurrentLessonCategory = function () {
                const currentLessonCategory = me.root.bigContext.dto.LessonCategoryList[0];
                if (currentLessonCategory === undefined || currentLessonCategory === null) {

                    console.log("Não foi possível carregar a LessonCategory");
                    me.onLoadError();
                }
                return currentLessonCategory;
            };

            me.reLoadLtiResourceDetail = function () {

                let parameters = {
                    LtiProviderResourceId: me._currentLessonCategory.Detail.LtiProviderResourceId,
                    DisciplineContentId: me.root.bigContext.dto.DisciplineContentID,
                    LessonCategoryId: me._currentLessonCategory.LessonCategoryID,
                    StudentId: me.root.serverParameters.parameters.StudentId,
                    ClassDisciplineId: me.root.bigContext.dto.ClassDisciplineID,
                    ViewMode: me.LtiViewMode
                }

                me.service.getLtiProviderResource(parameters).then(response => {
                    if (response.Success) {
                        me._ltiResource = response.Result;
                        me.renderForm(response.Result);
                    } else {
                        console.log(response.ErrorMessage);
                        me.hasError = true;
                    }
                }).catch(error => {
                    console.log(error);
                    me.hasError = true;
                });
            }

            me.renderForm = function (ltiResource) {
                let form = angular.element(`<form action="${ltiResource.Url}" method="POST" target="${ltiResource.OpenNewWindow ? "_blank" : "ltiIframe"}"></form>`);

                for (let key in ltiResource.Parameters) {
                    let input = angular.element(`<input type="hidden" name="${key}" value="${ltiResource.Parameters[key]}" >`);
                    form[0].appendChild(input[0]);
                }

                $element.append(form);
                form[0].submit();
                form.remove();
            };

            me.onLoadError = function () {
                me.hasError = true;
            };
        },
    };
});