﻿function registerCommonFunction($scope) {
    $scope.getModeCss = function () {
        let _enum = getPropertyByName($scope, 'serverParameters.enums.StudentCategoryViewMode');
        let mode = getPropertyByName($scope, 'allprops.Mode');
        if (_enum != null && mode != null) {
            return singleLookup(_enum, mode);
        }

    }
    $scope.generalError = function (error) {
        console.error('internal error: ', error);
        throw {
            message: getPropertyByNameOrDefault(error, 'ErrorMessage', 'Ocorreu um erro') + '.',
            custom: compareIfSafe(error, 'ErrorType', 2),
            showRefreshBto: true,
            closeAfter: 5000,
            errorType: getPropertyByNameOrDefault(error, 'ErrorType', 1)
        };
    }
    $scope.openFileViewer = function (fileUrl) {
        if (isSafeToUse(window, 'openDocumentOnViewer'))
            window.openDocumentOnViewer(fileUrl);
    }

    let sc = getScopeByProp($scope, 'IamTheRootScope');
    if (sc != null && typeof (sc.studentIsSuspendedOnDiscipline) == 'function') {
        $scope.studentIsSuspendedOnDiscipline = sc.studentIsSuspendedOnDiscipline();
        $scope.studentImageUrl = sc.studentImageUrl();
        $scope.studentName = sc.bigContext.dto.StudentName;
    } else {
        let rt = getScopeByProp($scope, 'IamTheRootScope');
        $scope.studentIsSuspendedOnDiscipline = compareIfSafe(rt, 'bigContext.dto.StudentClassDisciplineStatus', 5)
    }
}


function removeIframeloading(iframe) {
    $(iframe).removeClass('bgLoading');
}


function changeFocusKeyDowIframe() {

    let iframe = document.getElementsByTagName('iframe');

    for (let componentIframe of iframe) {
        componentIframe.addEventListener('load', function () {

            let iframeWin = componentIframe.contentWindow || componentIframe;
            let iframeDoc = componentIframe.contentDocument || iframeWin.document;

            let script = iframeDoc.createElement("script");

            script.append(`

                        var body = document.getElementsByTagName('body')[0];

                        body.onkeydown = function(e) {
                            if (e.altKey && e.code === 'Digit0') {
                                window.parent.document.getElementById('ucreturnlink').focus();
                            } else if (e.altKey && e.code === 'Digit1') {
                                window.parent.document.getElementById('studentmenu').focus();
                            } else if (e.altKey && e.code === 'Digit2') {
                                window.parent.document.getElementById('topBarIdPlayer').focus();
                            } else if (e.altKey && e.code === 'Digit3') {
                                window.parent.document.getElementById('mainContentId').focus();
                            } else if (e.altKey && e.code === 'Digit4') {
                                window.parent.document.getElementById('contentFooterId').focus();
                            }
                        };
                `);

            iframeDoc.documentElement.appendChild(script);
        });
    }

}
