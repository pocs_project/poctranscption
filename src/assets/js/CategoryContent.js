﻿ILangApp.directive('categoryContent', () => {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => {
            return getStaticUrl('/Templates/Category/CategoryContent.html');
        },
        link: (scope, tElement, attrs) => {
            if (attrs.hasOwnProperty('showpreviousnextbutton') && attrs.showpreviousnextbutton == 'false') {
                $(tElement).find('a[previousNextLink]').hide();
            }
            if (attrs.hasOwnProperty('hideNextLesson') && attrs.hideNextLesson == 'true') {
                scope.hideNextLesson = true
            }
            if (attrs.hasOwnProperty('hideComments') && attrs.hideComments == 'true') {
                scope.hideComments = true
            }
            if (attrs.hasOwnProperty('hideFeedback') && attrs.hideFeedback == 'true') {
                scope.hideFeedback = true
            }
        }
    };
});
