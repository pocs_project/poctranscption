
/* This is an example of how to cancel all the files queued up.  It's made somewhat generic.  Just pass your SWFUpload
object in to this method and it loops through cancelling the uploads. */
function cancelQueue(instance)
{
	if (instance.options.cancelButtonId != null)
		document.getElementById(instance.options.cancelButtonId).disabled = true;

	instance.stopUpload();
	var stats;

	do
	{
		stats = instance.getStats();
		instance.cancelUpload();
	} while (stats.files_queued !== 0);

	if (typeof (instance.options.clientCancelUpload) === 'function')
	{
		instance.options.clientCancelUpload();
	}
}

/* **********************
Event Handlers
These are my custom event handlers to make my
web application behave the way I went when SWFUpload
completes different tasks.  These aren't part of the SWFUpload
package.  They are part of my application.  Without these none
of the actions SWFUpload makes will show up in my application.
********************** */
function fileDialogStart()
{
	/* I don't need to do anything here */
}
function fileQueued(file)
{
	try
	{
		// You might include code here that prevents the form from being submitted while the upload is in
		// progress.  Then you'll want to put code in the Queue Complete handler to "unblock" the form
		var progress = new FileProgress(file, this.customSettings);
		progress.setStatus("Pending...");
		progress.toggleCancel(true, this);

	} catch (ex)
	{
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) 
{
	try
	{
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED)
		{
		    //alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
		    alert('Voc\xEA tentou efetuar o upload de diversos arquivos.\n' + (message === 0 ? "Voc\xEA excedeu o limite de upload." : "Voc\xEA pode selecionar " + (message > 1 ? "no m\xE1ximo " + message + " arquivos." : "um arquivo.")));
			return;
		}

		var progress = new FileProgress(file, this.customSettings);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode)
		{
			case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
				progress.setStatus("File is too big.");
				this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				alert(this.customSettings.fileIsToBigMessage);
				break;
			case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
				progress.setStatus("Cannot upload Zero Byte files.");
				this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				alert('Voc\xEA n\xE3o pode efetuar o upload de um arquivo vazio.');
				break;
			case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
				progress.setStatus("Invalid File Type.");
				this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				if (typeof (invalidFileTypeErrorMessage) != 'undefined') {
				    alert(invalidFileTypeErrorMessage);
				}
				else {
				    alert('Tipo de arquivo n\xE3o permitido para upload.');
				}
				break;
			case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
			    alert("You have selected too many files.  " + (message > 1 ? "You may only add " + message + " more files" : "You cannot add any more files."));
			    alert("Voc\xEA selecionou arquivos demais. " + (message > 1 ? "Voc\xEA  s\xF3 pode adicionar mais " + message + " arquivos" : "Voc\xEA n\xE3o pode adicionar mais arquivos"));
				break;
			default:
				if (file !== null)
				{
					progress.setStatus("Unhandled Error");
				}
				this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
		}
	} catch (ex)
	{
		this.debug(ex);
	}
}

function fileDialogComplete(numFilesSelected, numFilesQueued)
{
	try
	{
		if (this.getStats().files_queued > 0)
		{
		    if ($('#' + this.customSettings.containerId).length > 0) {
		        var object = $('#' + this.customSettings.containerId).find('object');
		        if ($(object).length > 0) {
		            $(object).css('width', '0px');
		        }
		    }
		}
	}
	catch (ex)
	{
		this.debug(ex);
	}
	try
	{
		if (this.getStats().files_queued > 0 && this.customSettings.cancelButtonId != null)
		{
			document.getElementById(this.customSettings.cancelButtonId).disabled = false;
		}
		/* I want auto start and I can do that here */
		this.startUpload();
	} catch (ex)
	{
		this.debug(ex);
	}
}

function uploadStart(file)
{
	try {
	    if (file.name.indexOf('+') > -1 ||
            file.name.indexOf('`') > -1 ||
            file.name.indexOf('�') > -1 ||
            file.name.indexOf('^') > -1 ||
            file.name.indexOf('~') > -1 ||
            file.name.indexOf('�') > -1 ||
            file.name.indexOf('�') > -1 ||
            file.name.indexOf('%', file.name) > -1) {
			alert('Nome do arquivo possui caracteres invalidos.');
			this.cancelQueue(this);
			return;
		}

		/* I don't want to do any file validation or anything,  I'll just update the UI and return true to indicate that the upload should start */
		var progress = new FileProgress(file, this.customSettings);
		progress.setStatus("Uploading...");
		progress.toggleCancel(true, this);
		if (typeof (this.customSettings.clientStartUpload) === 'function') {
			this.customSettings.clientStartUpload(file);
		}
	}
	catch (ex)
	{
	}

	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal)
{
	try
	{
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);
		var progress = new FileProgress(file, this.customSettings);
		progress.setProgress(percent);
		progress.setStatus("Uploading...");
	} catch (ex)
	{
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData)
{
	try
	{
		var progress = new FileProgress(file, this.customSettings);
		var cancelEl = document.getElementById(this.customSettings.cancelButtonId);

		if ($(cancelEl).length > 0)
		{
		    $(cancelEl).css('display', 'none');
		}

		progress.setComplete();
		progress.setStatus("Complete.");
		progress.toggleCancel(false);
		if (typeof (this.customSettings.clienttUploadSuccess) === 'function')
		{
			this.customSettings.clienttUploadSuccess({ file: file, server: eval('(' + serverData + ')') });
		}
		if (typeof (this.customSettings.serverUploadSuccess) === 'function')
		{
			var serverResponse = eval('(' + serverData + ')');
			if (serverResponse.success)
			{
				var containerId = this.customSettings.containerId;
				$('#' + containerId).find('input[object_type="upload.filename"]').val(serverResponse.fileName);
				$('#' + containerId).find('input[object_type="upload.guidfilename"]').val(serverResponse.guidFileName);
				$('#' + containerId).find('input[object_type="upload.filesize"]').val(serverResponse.size);
				$('#' + containerId).find('input[object_type="upload.filepath"]').val(serverResponse.filePath);
				this.customSettings.serverUploadSuccess();
			}
		}
	} catch (ex)
	{
		this.debug(ex);
	}
}

function uploadComplete(file)
{
	try
	{
		var cancelEl = document.getElementById(this.customSettings.cancelButtonId);

		if ($(cancelEl).length > 0)
		{
			$(cancelEl).css('display', 'none');
		}

		var object = $('#' + this.customSettings.containerId).find('object');
		if ($(object).length > 0)
		{
		    $(object).css('width', this.customSettings.button_width + 'px');
		}
	}
	catch (ex)
	{
		this.debug(ex);
	}
	try
	{
		/*  I want the next upload to continue automatically so I'll call startUpload here */
	    if (($(this.getStats()).length == 0 || this.getStats().files_queued === 0) && this.customSettings.cancelButtonId != null)
		{
			document.getElementById(this.customSettings.cancelButtonId).disabled = true;
		} else
		{
			this.startUpload();
		}
	} catch (ex)
	{
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message)
{
	try
	{
		var progress = new FileProgress(file, this.customSettings);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode)
		{
			case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
				progress.setStatus("Upload Error: " + message);
				this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
				progress.setStatus("Configuration Error");
				this.debug("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
				progress.setStatus("Upload Failed.");
				this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.IO_ERROR:
				progress.setStatus("Server (IO) Error");
				this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
				progress.setStatus("Security Error");
				this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
				progress.setStatus("Upload limit exceeded.");
				this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
				progress.setStatus("File not found.");
				this.debug("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
				progress.setStatus("Failed Validation.  Upload skipped.");
				this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
			case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
				if (this.getStats().files_queued === 0 && this.customSettings.cancelButtonId != null)
				{
					document.getElementById(this.customSettings.cancelButtonId).disabled = true;
				}
				progress.setStatus("Cancelled");
				progress.setCancelled();
				break;
			case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
				progress.setStatus("Stopped");
				break;
			default:
				progress.setStatus("Unhandled Error: " + error_code);
				this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
				break;
		}
	} catch (ex)
	{
		this.debug(ex);
	}
}

if (typeof (Sys) != 'undefined' && typeof (Sys.Application) != 'undefined')
	Sys.Application.notifyScriptLoaded();