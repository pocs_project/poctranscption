﻿
ILangApp.directive('categoryTopItems', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => {
            return getStaticUrl('/Templates/Category/CategoryTopItems.html');
        },
        controller: function ($scope, $http) {
            baseController($scope, $http, {});

            var me = $scope;
            me.faleComDocenteResource = '';
            me.viewTeacherMessage = false;
            me._teacherInboxUrl = undefined;

            me.onInit = function () {

                if (me.faleComDocenteResource === '') {
                    me.timeLoadTypeId = me.loadResourcebyLoadTimeLoadType();
                }
            };

            me._createUrlStudentAPI = function (route) {
                var urlBase = ILangSettings.Sites.StudentAPI();
                return `${urlBase}${route}`;
            };

            me.getFaleComDocenteResource = function (timeLoadTypeId) {

                if (timeLoadTypeId === Enum.timeLoadTypeEnum.digitalLearning) {
                    me.faleComDocenteResource = R.student.faleComDocente.dl;
                } else {
                    me.faleComDocenteResource = R.student.faleComDocente.default;
                }
            };

            me.loadResourcebyLoadTimeLoadType = () => {

                var urlApi = me._getTimeLoadTypeUrlApi();

                me.ajax.get(urlApi, {}, function (result) {
                    me.getFaleComDocenteResource(result.Result.TimeLoadTypeId);
                }, () => {
                    me.getFaleComDocenteResource();
                });
            };

            me._getTimeLoadTypeUrlApi = () => {

                var encryptedClassDisciplineId = ILang.Util.QueryString.read('cd');
                var resource = `/class-discipline/time-load-type?EncryptedClassDisciplineId=${encryptedClassDisciplineId}`;

                return me._createUrlStudentAPI(resource);
            };

            me._getTeacherMessageUrlApi = () => {
                var urlResource = "/Student/TeacherMessageCrpt";
                return me._createUrlStudentAPI(urlResource);
            };

            me._getTeacherMessageApiParameters = () => {
                return {
                    classDisciplineCrpt: ILang.Util.QueryString.read('cd'),
                };
            };

            me._showToastMessage = (message) => {
                showNegativeMsg(message, "3000");
            };

            me.sendMessage = () => {

                if (me._teacherInboxUrl) {
                    openNewTab(me._teacherInboxUrl);
                    return;
                }

                var urlTeacherMessageApi = me._getTeacherMessageUrlApi();
                var teacherMessageParameters = me._getTeacherMessageApiParameters();

                me.ajax.get(urlTeacherMessageApi, teacherMessageParameters,
                    (result) => {
                        var urlILang = ILangSettings.Sites.ILang();
                        me._teacherInboxUrl = `${urlILang}${result.Result}`;
                        openNewTab(me._teacherInboxUrl);
                    },
                    (result) => {
                        me._showToastMessage(result.ErrorMessage);
                    });
            };

            me.doAfterContentPlayerLoaded = (viewTeacherMessage) => {
                me.viewTeacherMessage = viewTeacherMessage;
            };
        }
    };
});