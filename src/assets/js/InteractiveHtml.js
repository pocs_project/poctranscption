﻿ILangApp.directive('interactiveHtml', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function () { return getCategoryAngularTemplate('InteractiveHtml'); },
        scope: {
            allprops: '='
        },
        controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;
            me.exercisedto = me.allprops.Detail;
            registerCommonFunction(me);

            changeFocusKeyDowIframe();
        }
    };
});