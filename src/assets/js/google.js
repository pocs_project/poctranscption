function GetGoogleDocIFrame(iFrameID, googleDocURL, documentLoadedScript) {

    var code = googleDocURL;

    if (code == undefined || !code.trim().length > 0)
        return;

    code = code.replace('&amp;', '&');
    code = code.replace('\""', '\'');

    if (code.indexOf('iframe') >= 0) {
        code = code.substring(code.indexOf('src=\'') + 'src=\''.length);
        code = code.substring(0, code.indexOf('\''));
    }

    // Tratamento de URL do Google
    if (code.indexOf('/presentation?') > -1) {
        code = code.replace('/presentation?', '/present/embed?');
    }
    if (code.indexOf('/embedslideshow?') > -1) {
        code = code.replace('/embedslideshow?', '/present/embed?');
    }
    if (code.indexOf('/present/view?') > -1) {
        code = code.replace('/present/view?', '/present/embed?');
    }
    if (code.indexOf('&size=') > -1) {
        code = code.replace('&size=', '&x=');
    }

    var url = code;
    if (url.indexOf('skipauth=') >= 0)
        url = url.replace('skipauth=', 'r0=');
    url += '?skipauth=true';

    if (url.indexOf('presentation') >= 0)
        url = url.replace('presentation', 'present');

    if (!(url.indexOf('docid') >= 0) && (url.indexOf('id=') >= 0)) {
        var id = url.substring(url.indexOf('id=') + 'id='.length);
        if (id.indexOf('&') >= 0)
            id = id.substring(0, id.indexOf('&'));
        url += '&docid=' + id;
    }

    if (documentLoadedScript == undefined || !documentLoadedScript.trim().length > 0)
        documentLoadedScript = '$(\'#' + iFrameID + '\').css(\'display\',\'block\');';

    // ajuste para mudar para modo de visualização
    if (url.indexOf('/edit') >= 0)
        url = url.replace('/edit', '/preview');

    return { iFrameHTML: '<iframe id=\'' + iFrameID + '\' src=\'' + url + '\' frameborder=\'0\' width=\'560\' height=\'315\' top=\'0px\' allowfullscreen wmode=\'transparent\' onload="' + documentLoadedScript + '" style=\'background: transparent;\'></iframe>', documentUrl: url };
}

function GetYouTubeVideoIFrame(iFrameID, youTubeVideoUrl, youTubeLoadedScript) {

    var videoUrl = youTubeVideoUrl;

    if (videoUrl.indexOf('youtube') > -1 && videoUrl.indexOf('v=') > -1) {
        var start = videoUrl.indexOf('v=') + 2;
        videoUrl = videoUrl.substring(start);
        if (videoUrl.indexOf('&') > -1) {
            videoUrl = videoUrl.substring(0, videoUrl.indexOf('&'));
        }
        videoUrl = 'https://www.youtube.com/embed/' + videoUrl;
    }

    if (videoUrl.indexOf('youtu.be/') > -1) {
        var start = videoUrl.indexOf('youtu.be/') + 9;
        videoUrl = videoUrl.substring(start);
        videoUrl = 'https://www.youtube.com/embed/' + videoUrl;
    }

    if (youTubeLoadedScript != null) {
        return { iFrameHTML: '<iframe id="' + iFrameID + '" width="560" height="315" src="' + videoUrl + '" frameborder="0" onload="' + youTubeLoadedScript + '"  class="ilVideoPlayer" allowfullscreen ></iframe>' };
    }
    else {
        return { iFrameHTML: '<iframe id="' + iFrameID + '" width="560" height="315" src="' + videoUrl + '" frameborder="0" class="ilVideoPlayer" allowfullscreen ></iframe>' };
    }
}

function getYoutubeJsonData(youTubeVideoUrl, onSuccessCallback) {
    var videoUrl = youTubeVideoUrl;

    if (videoUrl.indexOf('youtu.be/') > -1) {
        var start = videoUrl.indexOf('youtu.be/') + 9;
        videoUrl = videoUrl.substring(start);
        videoUrl = 'https://www.youtube.com/watch?v=' + videoUrl;
    }

    if (videoUrl.indexOf('youtube') > 0 && videoUrl.indexOf('v=') > 0) {
        var start = videoUrl.indexOf('v=') + 2;
        videoId = videoUrl.substring(start);

        $.ajax({
            type: 'GET',
            url: '/handlers/usermedia/GetYoutubeJsonData.ashx',
            data: { 'youTubeVideoUrl': videoId }
        })
       .done(function (json) {
           var thumbnails = [];

           var json = $.parseJSON(json);

           for (var i = 0; i < json.entry.media$group.media$thumbnail.length; i++) {
               var tn = json.entry.media$group.media$thumbnail[i];
               thumbnails.push({
                   url: tn.url,
                   height: tn.height,
                   width: tn.width,
                   time: tn.time
               });
           }

           onSuccessCallback({
               title: json.entry.media$group.media$title.$t,
               description: json.entry.media$group.media$description.$t,
               keywords: json.entry.media$group.media$keywords.$t,
               coverpath: json.entry.media$group.media$thumbnail[0].url,
               thumbnails: thumbnails,
               success: true
           });
       });
    }
}

function GetVimeoVideoID(vimeoUrl) {
    return vimeoUrl.split('/').last();
}
function GetVimeoJsonData(vimeoUrl, onSuccessCallback) {
    var idvideo = GetVimeoVideoID(vimeoUrl);
    $.ajax({
        type: 'GET',
        url: ILangSettings.Sites.ILang() + '/handlers/usermedia/GetVimeoJsonData.ashx',
        data: { 'videoID': idvideo }
    })
        .done(function (json) {
            var thumbnails = [];

            var json = $.parseJSON(json);

            onSuccessCallback({
                title: json.name,
                description: json.description,
                keywords: json.tags,
                link: json.link,
                coverpath: json.pictures.sizes[3].link,
                thumbnails: json.pictures.sizes[2].link,
                success: true
            });
        }).fail(function () {
            onSuccessCallback({
                title: '',
                description: '',
                keywords: null,
                link: '',
                coverpath: '',
                thumbnails: '',
                success: false
            });
        });
}

function GetVimeoFullScreenIframe(vimeoVideoUrl) {
    var idvideo = GetVimeoVideoID(vimeoVideoUrl);
    var videoUrl = "https://player.vimeo.com/video/" + idvideo + "?color=ef0800&title=0&byline=0&portrait=0&autoplay=1&loop=1"
    return { iFrameHTML: '<iframe  width="100%" height="100%" src="' + videoUrl + '" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>' };
}

if (typeof (Sys) != 'undefined' && typeof (Sys.Application) != 'undefined')
    Sys.Application.notifyScriptLoaded();

function getSoundCloudData(url, after) {
    $.ajax({
        type: "POST",
        url: "https://soundcloud.com/oembed",
        dataType: 'json',
        data: {
            "format": "json",
            "url": url
        },
        success: function (result) {
            after(true, result);
        }, error: function (a,b,c) {
            after(false,a );
        }

    });
}
function getSoundCloudIFrameFromRawUrl(iFrameID, url,after) {
    getSoundCloudData(url, function (ok,result) {
        var soundCloudUrl = $(result.html).attr('src');
        //<iframe id="youTubeFrame" width="560" height="315" ng-src="{{trustSrc(dialogModel.SoundCloudUrl)}}"  scrolling="no" frameborder="no" allow="autoplay"></iframe>
        after(getSoundCloudIFrame(iFrameID, soundCloudUrl, result.title, result.description));
    })
};
function getSoundCloudIFrame(iFrameID, url, title, description) {
    return { iFrameHTML: '<iframe id="' + iFrameID + '" width="560" height="315" src="' + url + '" scrolling="no" frameborder="no" allow="autoplay" ></iframe>', Url: url, Title: title, Description: description };
}