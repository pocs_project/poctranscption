﻿ILangApp.directive('offlineAssessment', function () {
    return {
        restric: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('OfflineAssessment'); },
        scope: {
            allprops: '='
        }, controller: function ($scope, $element, $attrs, $transclude) {
            let me = $scope;
            registerExerciseController(me);
            me.openDialog = false;
            me.funcToBeDeleted = ['tryAgain', 'postAnswer', 'skipQuestion', 'answer'];
            me.eraseFunctions = function () {
                for (const element of me.funcToBeDeleted) {
                    me[element] = function () { return; };
                }
            };
            $scope.loadMath = function (html) {
                let inputHtml = document.createElement('div');
                $(inputHtml).html(html);

                $(inputHtml).find('.AM').each(function (index, item) {
                    let expression = $(item).text().replace('`', '').replace('`', '');
                    let image = AMTparseMath(expression);

                    $(item).html(image);
                });

                return $(inputHtml).html();
            };
            me.eraseFunctions();
            me.getAssessmentFilesUrl = function (after, studentID) {
                let param = {
                    LessonCategoryID: me.allprops.LessonCategoryID,
                    ClassDisciplineID: me.allprops.Detail.ClassDisciplineID
                }
                if (studentID != null)
                    param.StudentID = studentID;

                me.$parent.action('getAssessmentFilesUrl', param, function (result) {
                    if (result.Success) {
                        me.exercisedto.FileList = result.Result;
                    } else {
                        me.exercisedto.FileList = [];
                    }

                    if (typeof (after) == 'function')
                        after(result.Result);
                });
            };
            me.openAssessmentFiles = function () {
                if (me.exercisedto.HasAssessmentBatchSheetFileToShow == null || !me.exercisedto.HasAssessmentBatchSheetFileToShow || me.studentIsSuspendedOnDiscipline)
                    return;
                me.getAssessmentFilesUrl(function () {
                    me.openDialog = true;
                })

            };
            me.setGradeClassName = function (param) {
                let question = me.exercisedto.QuestionList.first({ QuestionID: param.id });
                if (question != null) {
                    question.GradeClassName = param.className;
                    if (!me.$$phase)
                        me.$apply();
                }
            };
            changeFocusKeyDowIframe();
        }
    };
});