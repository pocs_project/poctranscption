﻿
ILangApp.directive('openQuestion', function ($timeout) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('OpenQuestion'); },
        scope: {
            allprops: '='
        }, controller: function ($scope, $element, $attrs, $transclude, $interval) {
            registerExerciseController($scope, $element, $attrs, $transclude, $timeout);
            let me = $scope;
            me.compareTextLength = 0;
            me.SaveButtonText = 'SALVAR RESPOSTA';
            const ERROR_DISPLAY_TIME = 5000;

            registerTinymceFunctions(me);

            me.InitTinymce = function () {
                try {
                    InitializeTinyMCEOptionSimpleModeMathOnly(me.txtFieldID(), true);
                } catch (e) { }

            };

            me.txtFieldID = function () {
                return 'if_' + me.allprops.LessonCategoryID;
            };
            me.getText = function () {
                return me.getEditorContent(me.txtFieldID(), 'text');
            };
            me.getHtml = function () {
                return me.getEditorContent(me.txtFieldID(), 'html');
            };
            me.setText = function (txt) {
                me.setEditorContent(me.txtFieldID(), txt);
            };
            me.onChangeQuestion = function () {
                me.setText(me.currentQuestion.AnswerText || '');
                me.cancelTimerAutoSave();
            };
            me.onBeforeChangeQuestion = function () {
                if (me.currentQuestion != null) {
                    me.currentQuestion.AnswerText = me.getHtml();

                }
            };
            me.onAfterAnswer = function () {
                me.currentQuestion.AnswerText = me.getHtml();
                me.currentQuestion.userTyping = false;
            }
            me.startTyping = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return

                me.currentQuestion.userTyping = true;
                me.InitTinymce();
                me.startTimerAutoSave();
            };
            me.edit = function () {

                if (me.exercisedto.ReadOnly || me.studentIsSuspendedOnDiscipline)
                    return;

                me.InitTinymce();
                me.currentQuestion.userTyping = true;
                me.currentQuestion.Status = me.enums.StudentQuestionStatus.Editing
                setTimeout(function () {
                    me.setText(me.currentQuestion.AnswerText || '');
                }, 200);
                if (me.compareTextLength != me.getText().length) {
                    me.startTimerAutoSave();
                }
            };
            me.answerTimerAutoSave = null;
            me.startTimerAutoSave = function () {
                me.cancelTimerAutoSave();
                me.answerTimerAutoSave = $interval(function () {

                    if (me.getText().length <= 1)
                        return;

                    if (me.compareTextLength == me.getText().length)
                        return;

                    me.SaveButtonText = 'SALVANDO...';
                    me.currentQuestion.AnswerText = me.getText();
                    me.postAnswer('answerQuestion', function (result) {
                        if (result.Result) {
                            me.SaveButtonText = 'SALVAR RESPOSTA';
                            me.compareTextLength = me.getText().length;
                        }
                    });
                }, 300 * 1000);
            };
            me.cancelTimerAutoSave = function () {
                if (me.answerTimerAutoSave != null) {
                    $interval.cancel(me.answerTimerAutoSave);
                }
            };
            me.beforeAnswer = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                if (isEmptyOrSpaces(me.getText())) {
                    showTopErrorMsg('O campo resposta não pode ser vazio..', ERROR_DISPLAY_TIME, null, false);
                    me.busy = false;
                    return;
                }
                me.answer();
            };

            me.save = async function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return;

                if (isEmptyOrSpaces(me.getHtml())) {
                    showTopErrorMsg('O campo resposta não pode ser vazio..', ERROR_DISPLAY_TIME, null, false);
                    return;
                }

                await me.answer();
                me.cancelTimerAutoSave();
            };

            me.loadMath = function (html) {
                let inputHtml = document.createElement('div');
                $(inputHtml).html(html);

                $(inputHtml).find('.AM').each(function (index, item) {
                    let expression = $(item).text().replace('`', '').replace('`', '');
                    let image = AMTparseMath(expression);

                    $(item).html(image);
                });

                return $(inputHtml).html();
            }
            me.setGradeClassName = function (param) {
                let question = me.exercisedto.QuestionList.first({ QuestionID: param.id });
                if (question != null) {
                    question.GradeClassName = param.className;
                    if (!me.$$phase)
                        me.$apply();
                }
            }
            me.onUploadSuccess = function (response) {
                if (response.response.Success) {
                    me.currentQuestion.FileName = response.response.Result.FileName;
                    me.currentQuestion.FileUrl = response.response.Result.VirtualPath;

                    me.answer();
                    response.after();
                }
                else {
                    throw getPropertyByName(response, 'response.ErrorMessage');
                }
            }
            me.deleteFileOpenQuestion = function (file) {
                let param = {};
                param.StudentTrackID = me.exercisedto.StudentTrackID;
                param.QuestionID = me.currentQuestion.QuestionID;

                me.$parent.action('deleteFileOpenQuestion', param, function (result) {
                    if (result.Success) {
                        me.currentQuestion.FileName = null;
                        me.currentQuestion.FileUrl = null;
                    }
                }, me.onError);
            }
            me.removeSpecialCharactersOpenQuestion = function (customName) {
                let cleaner = customName.replace(/[^a-zA-Z0-9 ]/g, '');
                cleaner = cleaner.substr(0, 40);

                return cleaner;
            }
            me.customOpenQuestionFileName = function () {
                let studentName = me.serverParameters.parameters.StudentName;
                let lessonCategoryName = me.allprops.LessonCategoryName;
                lessonCategoryName = me.removeSpecialCharactersOpenQuestion(lessonCategoryName);

                return lessonCategoryName + '_' + studentName + '_' + me.currentQuestion.QuestionID;

            };

            me.showBtnAnswerNowTextUpdate = function () {
                return !me.currentQuestion.IsAnswered &&
                    ((me.currentQuestion.userTyping || me.exercisedto.QuestionList.length == 1) && (me.currentQuestion.Status == me.enums.StudentQuestionStatus.Unanswered || me.currentQuestion.Status == me.enums.StudentQuestionStatus.Skipped))
            }

            changeFocusKeyDowIframe();
        }
    };
});
