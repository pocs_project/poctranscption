﻿ILangApp.directive('categoryForum', function ($window) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('Forum'); },
        scope: {
            allprops: '='
        },
        controller: function ($scope, $element, $attrs) {
            let me = $scope;
            registerCommonFunction(me);
            registerTinymceFunctions(me);
            me.busy = false;
            me.dto = me.allprops.Detail;
            me.serverParameters = me.$parent.serverParameters;
            me.enums = $scope.$parent.serverParameters.enums;
            me.root = getRootScope($scope);
            me.newPostText = '';
            me.commentSelectedIDs = [];
            me.showPostBto = false;
            me.evaluations = { Positive: 1, Negative: 0 };
            me.done = false;
            me.disableAnswerField = false;
            me.default = {
                defaultPostList: null,
                defaultPostCount: 0
            };
            me.uploadingInProgress = false;
            me.lastForumID = null;
            me.showYoutubeDlg = false;
            me.youtubeVideoUrl = '';
            me.tinyMceOnFocus = false;

            me.getDefaultParameters = function () {
                return {
                    StudentLessonID: me.allprops.StudentLessonID,
                    StudentTrackID: me.dto.StudentTrackID || 0,
                    ClassDisciplineID: parseInt(me.root.bigContext.dto.ClassDisciplineID),
                    LessonID: parseInt(me.root.bigContext.dto.LessonID),
                    LessonCategoryID: parseInt(me.allprops.LessonCategoryID),
                    ForumGroupID: me.dto.ForumGroupID,
                    ForumFatherID: 0,
                    ForumID: 0,
                    Evaluation: null,
                    Text: '',
                    StudentID: parseInt(me.root.serverParameters.actions.load.data.StudentID)
                };
            };
            me.addPost = function () {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;
                let f = me.getEditorContent(me.txtFieldID());
                if (f != null)
                    me.newPostText = f;

                if (me.newPostText != null && me.newPostText != '') {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.Text = me.newPostText;
                    me.$parent.action('saveForumPost', param, function (result) {
                        me.dto.PostList.push(result.Result);
                        me.setEditorContent(me.txtFieldID(), '');
                        me.busy = false;
                        me.tinyMceOnFocus = false;
                        me.root.getMenuScope();
                        me.root.menuScope.checkLesson(param.LessonCategoryID);
                    }, me.onError);
                }
                me.newPostText = '';
                me.tinyMceOnFocus = false;
            }
            me.getMorePosts = function () {
                if (me.busy)
                    return;

                if (me.dto.PostList.length < me.dto.PostCount) {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumID = me.dto.PostList[0].ForumID;
                    me.$parent.action('loadForumPosts', param, function (result) {
                        me.dto.PostList.includeBefore(result.Result);
                        me.dto.HasMorePosts = me.dto.PostList.length < me.dto.PostCount;
                        me.busy = false;
                    }, me.onError)
                }
            };
            me.comment = function (post) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;

                if (post.commentText != undefined && post.commentText != null && post.commentText != '') {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumFatherID = post.ForumID;
                    param.Text = post.commentText;
                    me.$parent.action('saveForumComment', param, function (result) {
                        post.CommentList.push(result.Result);
                        post.commentText = '';
                        me.busy = false;
                        if (me.root.checkLessonOnLeftMenu) {
                            me.root.checkLessonOnLeftMenu(me.allprops);
                        }
                    }, me.onError);
                }
            };
            me.getMoreComments = function (post) {
                if (me.busy)
                    return;

                if (post.CommentCount > 0 && post.CommentCount > post.CommentList.length) {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumID = post.ForumID;

                    me.$parent.action('loadForumComments', param, function (result) {
                        post.CommentList = result.Result;
                        post.showCommentTextArea = true;
                        me.busy = false;
                    }, me.onError);
                } else {
                    post.showCommentTextArea = true;
                }
            }
            me.evaluationClick = function (element, value) {

                if (!element.AllowEvaluation || me.studentIsSuspendedOnDiscipline) {
                    return;
                }

                if (!element.UserHasEvaluated || (element.UserHasEvaluated && element.UserEvaluation != value)) {
                    if (value == me.evaluations.Negative) {
                        me.decreaseEvaluation(element);
                    } else {
                        me.increaseEvaluation(element);
                    }
                } else {
                    me.undoEvaluation(element);
                }
            };
            me.undoEvaluation = function (post) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;

                me.busy = true;
                let param = me.getDefaultParameters();
                param.ForumID = post.ForumID;
                me.$parent.action('undoEvaluation', param, function (result) {
                    post.UserHasEvaluated = false;
                    post.UserEvaluation = null;
                    me.updateCounters(post, result);
                    me.busy = false;
                }, me.onError);
            };
            me.updateCounters = function (post, result) {
                post.PositiveEvaluationCount = result.Result.PositiveEvaluationCount;
                post.NegativeEvaluationCount = result.Result.NegativeEvaluationCount;
            };
            me.increaseEvaluation = function (post) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;

                me.evaluation(post, me.evaluations.Positive, function () {
                    post.UserEvaluation = me.evaluations.Positive;
                });
            };
            me.decreaseEvaluation = function (post) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;

                me.evaluation(post, me.evaluations.Negative, function () {
                    post.UserEvaluation = me.evaluations.Negative;
                });
            };
            me.evaluation = function (post, type, fn) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;

                if (me.dto.ForumEvaluationEnabled) {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumID = post.ForumID;
                    param.Evaluation = type;
                    me.$parent.action('saveForumUserValuation', param, function (result) {
                        if (result.Success) {
                            post.UserHasEvaluated = true;
                            me.updateCounters(post, result);
                            if (typeof (fn) == 'function')
                                fn();
                        }
                        me.busy = false;
                    }, me.onError);
                }
            };
            me.onError = function (error, b) {
                me.busy = false;
                me.generalError(error);
            }
            me.onload = function () {
                me.done = true;
            }
            me.searchPost = function (parameters) {
                if (me.busy)
                    return;

                if (parameters.backTodefault && me.default.defaultPostList != null) {
                    me.dto.PostList = me.default.defaultPostList;
                    me.dto.PostCount = me.default.defaultPostCount;
                    me.dto.HasMorePosts = me.dto.PostList.length < me.dto.PostCount;
                    parameters.searchSummaryFn();
                } else {

                    if (me.default.defaultPostList == null) {
                        me.default.defaultPostCount = me.dto.PostCount;
                        me.default.defaultPostList = me.dto.PostList;
                    }

                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.SearchCriteria = parameters.searchCriteria;
                    param.PageSize = 20;
                    param.PageIndex = 1;
                    if (me.dto.PostList.length > 0) {
                        me.lastForumID = me.dto.PostList[0].ForumID;
                    }
                    param.ForumID = me.lastForumID;

                    me.$parent.action('searchForum', param, function (result) {
                        me.dto.PostList = result.Result;
                        me.dto.PostCount = result.TotalCount;
                        me.dto.HasMorePosts = me.dto.PostList.length < me.dto.PostCount;
                        me.dto.PageIndex = result.PageIndex;

                        if (isSafeToUse(parameters, 'searchSummaryFn')) {
                            parameters.searchSummaryFn(result);
                        } else if (isSafeToUse(me, 'root.searchCtrl.bindSearchSummary')) {
                            me.root.searchCtrl.searchTxt = parameters.searchCriteria;
                            me.root.searchCtrl.bindSearchSummary(result);
                        }

                        me.busy = false;
                    }, me.onError)
                }
            };

            me.getUserList = function () {
                if (typeof (me.userList) == 'undefined' || me.userList == null)
                    return;

                let percent = me.getViewPercentage();

                me.userList.total(parseInt(me.dto.TotalCount));
                me.userList.percent(percent);

                let param = me.getDefaultParameters();
                me.userList.get('loadForumViewers', param);

            };

            me.deletePost = function (post, list) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;
                if (post.AllowDelete) {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumID = post.ForumID;
                    me.$parent.action('deleteForum', param, function (result) {
                        me.busy = false;
                        if (result.Success) {
                            list.remove({ ForumID: post.ForumID })
                        }
                    });
                }
            };

            me.invalidatePost = function (post) {
                if (me.busy || me.studentIsSuspendedOnDiscipline)
                    return;
                if (post.AllowInvalidate) {
                    me.busy = true;
                    let param = me.getDefaultParameters();
                    param.ForumID = post.ForumID;
                    me.$parent.action('invalidateForum', param, function (result) {
                        me.busy = false;
                        if (result.Success) {
                            post.CancelInfo = result.Result;
                        }
                    }, function (result, status) {
                        me.busy = false;
                        window.Alert(getPropertyByName(result, 'ErrorMessage'));
                    });
                }
            };
            me.ShowMyComments = function () {
                me.searchPost({ searchCriteria: ILangSettings.UserName });
            }
            me.showPostBtoFn = function () {
                me.showPostBto = true;
            };
            me.showCommentBtoFn = function (post) {
                post.showCommentBto = true;
            };
            me.getViewPercentage = function () {
                if (me.dto.TotalCount == 0)
                    return 0;

                let percent = 0;
                if (me.dto.TotalCount > 0) {
                    percent = parseInt(100 * me.dto.ViewCount / me.dto.TotalCount);
                    if (percent == 0) {
                        percent = parseInt(1000 * me.dto.ViewCount / me.dto.TotalCount) / 10;
                    }
                }
                if (percent > 100) {
                    percent = 100;
                }
                return percent;
            };

            me.txtFieldID = function () {
                return 'if_' + me.allprops.LessonCategoryID;
            };
            me.forumInitTinymce = function () {
                if (me.checkIsMobile($window)) {
                    InitializeTinyMCEStudentMobile(me.txtFieldID(), true, {
                        onYutubeClick: function () {
                            me.youtubeUpload();
                        },
                        focus: function () {
                            me.tinyMceOnFocus = true;
                            if (!me.$$phase)
                                me.$apply();
                        }
                    });
                } else {
                    InitializeTinyMCEStudentForum(me.txtFieldID(), true);
                }

                me.showPostBto = true;
            };

            me.youtubeUpload = function () {
                me.showYoutubeDlg = true;
            };
            me.cancelYoutube = function () {
                me.youtubeVideoUrl = '';
                me.showYoutubeDlg = false;
            };
            me.createYoutube = function (e) {
                me.addUserMedia('youtubeStudent', me.youtubeVideoUrl, me.serverParameters.enums.UserMediaType.YouTubeVideo);
            }
            me.getUploadingType = function (file) {
                let tryGetByName = function (f) {
                    let ext = f.split('.').last();
                    if (ext.index('jpg') >= 0 ||
                        ext.index('png') >= 0 ||
                        ext.index('jpeg') >= 0 ||
                        ext.index('bmp') >= 0 ||
                        ext.index('tiff') >= 0 ||
                        ext.index('gif') >= 0
                    ) {
                        return 3;
                    } else {
                        return 1;
                    }
                }
                try {
                    let type = tinymce.EditorManager.activeEditor._uploadingType;
                    switch (type) {
                        case 'image':
                            return 3;
                        case 'video':
                            return 1;
                        default:
                            return tryGetByName(file);
                    }
                } catch (err) {
                    return tryGetByName(file);
                }
            }

            me.OnStartUpload = function () {
                console.log('uploadingInProgress');
                me.uploadingInProgress = true;
            }
            me.addUserMedia = function (name, path, typeID) {
                me.root.action('createUserMedia', {
                    UserMediaTypeID: typeID,
                    UserId: 0,
                    Title: name,
                    Description: '',
                    PathOrUrl: path,
                    Tags: '',
                    Shared: true,
                    ThumbnailPath: '',
                    CoverPath: '',
                    BibliographicReference: null
                }, function (result) {
                    me.uploadingInProgress = false;
                    me.showYoutubeDlg = false;
                    if (result.Success) {
                        TinyMCEUserMediaDialog.insertUserMedia(result.Result.userMediaID);
                    }
                });
            }
            me.UploadVideoSuccess = function (file) {
                me.addUserMedia(file.file.name, file.response.Result.VirtualPath, me.serverParameters.enums.UserMediaType.LocalVideo);
            };
            me.UploadImageSuccess = function (file) {
                me.addUserMedia(file.file.name, file.response.Result.VirtualPath, me.serverParameters.enums.UserMediaType.LocalImage);
            };
            me.UploadDocSuccess = function (file) {
                me.addUserMedia(file.file.name, file.response.Result.VirtualPath, me.serverParameters.enums.UserMediaType.LocalDocument);
            };
            me.UploadAudioSuccess = function (file) {
                me.addUserMedia(file.file.name, file.response.Result.VirtualPath, me.serverParameters.enums.UserMediaType.LocalAudio);
            }
            me.UploadSuccess = function (file) {
                console.log('result', file);
                let typeID = me.getUploadingType(file);
                me.addUserMedia(file.file.name, file.response.Result.VirtualPath, typeID);
            }
            changeFocusKeyDowIframe();
        }
    };
});