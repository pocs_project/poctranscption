﻿ILangApp.directive('categoryalertExpiration', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            show: '=',
            date: '='
        },
        template: '<div class="divAlert phb" ng-show="show == true"><p class="alert"><span class="fmb db fBold">VOCÊ NÃO PODE MAIS RESPONDER ESTA ATIVIDADE</span><span class="fms db blackGray"><em>O prazo de entrega expirou em {{date}}</em></span></p></div>'
    };
});