﻿ILangApp.directive('contentPlayerFeedBack', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getStaticUrl('/Templates/Category/ContentPlayerFeedBack.html'); },
        scope: {
            feed: '=',
            hideCard: '=',
            hashtagList: '=',
            searchFeedEnabled: '=',
            adminView: '=',
            isUnpinnable: '=',
            hideFeedComment: '=',
            justUseFeedEmoticons: '=',
            mode: '=',
            showcomments: '='
        },
        controller: function ($scope, $http, $sce) {
            var parentScope = $scope;


            while (typeof (parentScope.action) == 'undefined')
                parentScope = parentScope.$parent;

            $scope.rootScope = parentScope;
            $scope.SearchText = '';
            $scope.SentSearchText = '';

            
            if (isSafeToUse(parentScope, '$parent.serverParameters.enums')) {
                $scope.enums = parentScope.$parent.serverParameters.enums;
            }
            else {
                $scope.enums = parentScope.serverParameters.enums;
            }

            parentScope._feedCtrl = $scope;

            if ($scope.feed) {

                if ($scope.feed.FeedType == $scope.enums.FeedType.KPI) {
                    $scope.feed.KPIMyDiscApprEdPlan = parentScope.getMetadataDic($scope.feed).KPIMyDiscApprEdPlan;
                    $scope.feed.KPIMyDiscApplCont = parentScope.getMetadataDic($scope.feed).KPIMyDiscApplCont;
                    $scope.feed.KPIDiscMyUnitApprEdPlan = parentScope.getMetadataDic($scope.feed).KPIDiscMyUnitApprEdPlan;
                    $scope.feed.KPIDiscMyUnitApplCont = parentScope.getMetadataDic($scope.feed).KPIDiscMyUnitApplCont;
                    $scope.feed.UserSchedule = parentScope.getMetadataDic($scope.feed).UserSchedule;
                    $scope.feed.NotificationEnabled = parentScope.getMetadataDic($scope.feed).NotificationEnabled;
                }

                if ($scope.feed.FeedType == $scope.enums.FeedType.Report) {
                    $scope.feed.UserSchedule = parentScope.getMetadataDic($scope.feed).UserSchedule;
                    $scope.feed.NotificationEnabled = parentScope.getMetadataDic($scope.feed).NotificationEnabled;
                }
                if ($scope.feed.FeedType == $scope.enums.FeedType.Dashboard) {
                    $scope.feed.UserSchedule = parentScope.getMetadataDic($scope.feed).UserSchedule;
                }

                $(function () {
                    if ($scope.feed.HtmlFile) {

                        var intervalFunction;
                        intervalFunction = function () {
                            clearInterval(intervalFunction);
                        };

                        setTimeout(intervalFunction, 1000);
                    }
                });
            }

            $scope.getMetadataDic = function (feed) {
                if (feed == undefined || feed == null || isNotSafeToUse(feed, 'Metadata'))
                    return null;

                var metadataValues = {};

                if (feed) {
                    for (var i = 0; i < feed.Metadata.length; i++) {
                        var md = feed.Metadata[i];
                        metadataValues[md.Key] = md.Value;
                    }
                }
                return metadataValues;
            }

            $scope.openViewCount = function (feed) {
                if (!feed)
                    return;

                var self = feed;
                var name = '';

                if (self) {
                    var metadataValues = $scope.getMetadataDic(feed);

                    if (self.FeedType == $scope.enums.FeedType.LessonCategory
                        && (self.TaskType == $scope.enums.FeedTaskType.Page || self.TaskType == $scope.enums.FeedTaskType.LessonPlan || self.TaskType == $scope.enums.FeedTaskType.InteractiveHtml)) {
                        if (metadataValues["DisciplineName"])
                            name = metadataValues["DisciplineName"];
                    }
                    else if (self.FeedType == $scope.enums.FeedType.AdHocGroup) {
                        if (metadataValues["GroupName"])
                            name = metadataValues["GroupName"];
                    }
                }

                var me = this;
                var root = getRootScope($scope);
                if (!isSafeToUse(root, 'uniqueUserList'))
                    return;
                _userMediaList = root.uniqueUserList;


                var percent = $scope.getViewPercentage(feed);

                _userMediaList.title = percent + '% visualizaram';
                _userMediaList.viewCount = feed.ViewCount;
                _userMediaList.total = feed.FeedUserCount;
                _userMediaList.evaluationList = feed.EvaluationSummary;

                _userMediaList.percent = percent;

                var param = {
                    FeedId: feed.ID
                };
                _userMediaList.get('feedUsers', param);
            }

            $scope.openDeliveryCount = function (feed) {
                if (!feed)
                    return;

                var self = feed;
                var name = '';

                if (self) {
                    var metadataValues = $scope.getMetadataDic(feed);

                    if (self.FeedType == $scope.enums.FeedType.LessonCategory
                        && (self.TaskType == $scope.enums.FeedTaskType.Page || self.TaskType == $scope.enums.FeedTaskType.LessonPlan || self.TaskType == $scope.enums.FeedTaskType.InteractiveHtml)) {
                        if (metadataValues["DisciplineName"])
                            name = metadataValues["DisciplineName"];
                    }
                    else if (self.FeedType == $scope.enums.FeedType.AdHocGroup) {
                        if (metadataValues["GroupName"])
                            name = metadataValues["GroupName"];
                    }
                }

                var me = this;

                if (typeof (me.conclusionUserList) == 'undefined' || me.conclusionUserList == null)
                    return;
                me.conclusionUserList.feedId(feed.ID);
                me.conclusionUserList.title(name);
                me.conclusionUserList.deliveryCount(feed.DeliveryCount);

                var percent = $scope.getDeliveryPercentage(feed);
                var complementPercent = 100 - percent;
                me.conclusionUserList.total(feed.StudentUserCount);
                me.conclusionUserList.percent(percent);
                me.conclusionUserList.complementPercent(complementPercent);
                me.conclusionUserList.announcementNavigatorUrl(feed.AdminUserAnnouncementNavigatorUrl);
                var param = {
                    FeedId: feed.ID
                };
                me.conclusionUserList.get('feedNonDeliverers', param);
            }

            $scope.searchFeeds = function (searchParameters) {
                var searchFeedEnabled = true;

                if (typeof ($scope.searchFeedEnabled) != 'undefined') {
                    searchFeedEnabled = $scope.searchFeedEnabled;
                }

                if (searchFeedEnabled) {
                    if (getAngularScope('feedListCtrl'))
                        getAngularScope('feedListCtrl').selectHashTag(searchParameters);

                    if (getAngularScope('ContentPlayerCtrl'))
                        getAngularScope('ContentPlayerCtrl').searchFeeds(searchParameters);
                }
            }

            $scope.isHashTagActive = function (hashTag) {
                if (typeof ($scope.hashtagList) == 'undefined' || $scope.hashtagList == null || $scope.hashtagList.length == 0)
                    return true;
                var type = getRootScope($scope).serverParameters.enums.AngularAutoCompleteType.Tag;
                if ($scope.hashtagList.any({ Type: type })) {
                    return $scope.hashtagList.any({ EntityID: hashTag.ID });
                } else {
                    return true;
                }

            };

            $scope.getViewPercentage = function (feed) {
                if (!feed)
                    return 0;

                var percent = 0;
                if (feed) {
                    if ((feed.FeedUserCount || 0) > 0) {
                        percent = parseInt(100 * feed.ViewCount / feed.FeedUserCount);
                        if (percent == 0) {
                            percent = parseInt(1000 * feed.ViewCount / feed.FeedUserCount) / 10;
                        }
                    }
                }
                if (percent > 100) {
                    percent = 100;
                }
                return percent;
            }

            $scope.getDeliveryPercentage = function (feed) {
                if (!feed)
                    return 0;

                var percent = 0;
                var deliveryCount = feed.DeliveryCount;

                if (feed) {
                    if ((feed.FeedUserCount || 0) > 0) {
                        percent = parseInt(100 * deliveryCount / feed.StudentUserCount);

                        if (percent == 0)
                            percent = parseInt(1000 * deliveryCount / feed.StudentUserCount) / 10;
                    }
                }
                return percent;
            }

            $scope.addEvaluation = function (feed, evaluation) {
                parentScope.action('addFeedback', {
                    FeedId: feed.ID,
                    FeedType: feed.FeedType,
                    FeedParentId: feed.ParentID,
                    Evaluation: evaluation
                }, function (result) {
                    if (!result.Success) {
                        alert('Erro ao salvar avaliação.');
                    }
                });
            }

        }
    };
});