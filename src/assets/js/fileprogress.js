/*
A simple class for displaying file information and progress
Note: This is a demonstration only and not part of SWFUpload.
Note: Some have had problems adapting this class in IE7. It may not be suitable for your application.
*/

// Constructor
// file is a SWFUpload file object
// targetID is the HTML element id attribute that the FileProgress HTML structure will be added to.
// Instantiating a new FileProgress object with an existing file will reuse/update the existing DOM elements

var FileProgress = function (file, options) {
    this.init(file, options)
};

$.extend(FileProgress.prototype, {
    init: function (file, options) {
        this.targetID = options.progressTarget;
        this.options = options;
        this.fileProgressID = file.id;

        this.opacity = 100;
        this.height = 0;

        this.loadCSSClasses();

        this.fileProgressWrapper = document.getElementById(this.fileProgressID);
        if (!this.fileProgressWrapper) {
            var fileName = file.name;

            if (this.options.uploadTypeMode != 1)
                fileName = (fileName.length > 17) ? fileName.substring(0, 17) + '...' : fileName;

            var _name = fileName;

            if (this.options.uploadTypeMode != 1) {
                // File info
                var size = file.size / 1024 / 1024;
                size = size.toFixed(3);

                _name = fileName + ' - ' + size + ' MB';
            }

            var info = document.createElement('DIV');

            var img = document.createElement('IMG');
            $(img).attr('src', this.options.resources_url + 'indicator.gif');

            var divFileInfo = document.createElement('DIV');

            if (this.options.uploadTypeMode != 1) {
                $(info).attr('class', this.fileInfoClassName);

                $(info).append(img);
                $(info).append(document.createTextNode(_name));

                $(divFileInfo).css('clear', 'both');
            }

            $(divFileInfo).attr('object_type', 'upload.progress');
            $(divFileInfo).append(info);

            var targetElement = document.getElementById(this.targetID);
            $(targetElement).append(divFileInfo);

            if (this.options.uploadTypeMode == 1) {
                var pInfo = document.createElement('P');
                $(pInfo).attr('class', 'fms fBold actr pbs vam');
                $(pInfo).append(document.createTextNode('Fazendo upload do arquivo "' + _name + '"'));
                $(divFileInfo).append(pInfo);

                var p = document.createElement('P');
                $(p).attr('class', 'phs gray pbb fs');
                $(p).append(document.createTextNode('N\u00E3o feche a janela do seu navegador.'));                
                $(divFileInfo).append(p);
            }

            // Progress bar
            this.fileProgressWrapper = document.createElement("div");
            $(this.fileProgressWrapper).attr('class',  this.progressWrapperClassName);
            $(this.fileProgressWrapper).attr('id', this.fileProgressID);

            this.fileProgressElement = document.createElement("div");
            $(this.fileProgressElement).attr('class',  this.progressContainerClassName);

            var progressCancel = document.createElement("a");
            $(progressCancel).attr('class',  this.progressCancelClassName);
            $(progressCancel).attr('href', '#');
            $(progressCancel).css('visibility', 'hidden');
            $(progressCancel).append(document.createTextNode(" "));

            var progressText = document.createElement("div");
            $(progressText).attr('class',  this.progressNameClassName);
            $(progressText).append(document.createTextNode(file.name));

            var progressBar = document.createElement("div");
            $(progressBar).attr('class',  this.progressBarInProgressClassName);

            var progressStatus = document.createElement("div");
            $(progressStatus).attr('class',  this.progressBarStatusClassName);
            $(progressStatus).html("&nbsp;");

            $(this.fileProgressElement).append(progressCancel);
            $(this.fileProgressElement).append(progressText);
            $(this.fileProgressElement).append(progressStatus);
            $(this.fileProgressElement).append(progressBar);

            $(this.fileProgressWrapper).append(this.fileProgressElement);

            $(targetElement).append(this.fileProgressWrapper);
            $(targetElement).attr('class',  this.swfContainerClassName);
        }
        else {

            this.fileProgressElement = this.fileProgressWrapper.firstChild;
            this.reset();
        }

        this.height = this.fileProgressWrapper.offsetHeight;
        this.setTimer(null);
    },

    setTimer: function (timer)
    {
        this.fileProgressElement["FP_TIMER"] = timer;
    },

    getTimer: function (timer)
    {
        return this.fileProgressElement["FP_TIMER"] || null;
    },

    reset: function () {
        $(this.fileProgressElement).attr('class',  this.progressContainerClassName);

        $(this.fileProgressElement.childNodes[2]).html("&nbsp;");
        $(this.fileProgressElement.childNodes[2]).attr('class',  this.progressBarStatusClassName);

        $(this.fileProgressElement.childNodes[3]).attr('class',  this.progressBarInProgressClassName);
        $(this.fileProgressElement.childNodes[3]).css('width', '0%');

        this.appear();
    },
    
    setProgress: function (percentage)
    {
        $(this.fileProgressElement).attr('class',  this.progressContainerGreenClassName);
        $(this.fileProgressElement.childNodes[3]).attr('class',  this.progressBarInProgressClassName);
        $(this.fileProgressElement.childNodes[3]).css('width', percentage + "%");

        this.appear();
    },
    
    setComplete: function ()
    {
        $(this.fileProgressElement).attr('class',  this.progressContainerBlueClassName);
        $(this.fileProgressElement.childNodes[3]).attr('class',  this.progressBarCompleteClassName);
        $(this.fileProgressElement.childNodes[3]).css('width', '');

        var oSelf = this;
        this.setTimer(setTimeout(function ()
        {
            oSelf.disappear();
        }, 0));
    },

    setError: function ()
    {
        $(this.fileProgressElement).attr('class',  this.progressContainerRedClassName);
        $(this.fileProgressElement.childNodes[3]).attr('class',  this.progressBarErrorClassName);
        $(this.fileProgressElement.childNodes[3]).css('width', '');

        var oSelf = this;
        this.setTimer(setTimeout(function ()
        {
            oSelf.disappear();
        }, 0));
    },

    setCancelled: function ()
    {
        $(this.fileProgressElement).attr('class',  this.progressContainerClassName);
        $(this.fileProgressElement.childNodes[3]).attr('class',  this.progressBarErrorClassName);
        $(this.fileProgressElement.childNodes[3]).css('width', '');

        var oSelf = this;
        this.setTimer(setTimeout(function ()
        {
            oSelf.disappear();
        }, 0));
    },

    setStatus: function (status)
    {
        this.fileProgressElement.childNodes[2].html = status;
    },

    // Show/Hide the cancel button
    toggleCancel: function (show, swfUploadInstance)
    {
        $(this.fileProgressElement.childNodes[0]).css('visibility', show ? "visible" : "hidden");

        if (swfUploadInstance)
        {
            var fileID = this.fileProgressID;
            var cancelEl = document.getElementById(swfUploadInstance.customSettings.cancelButtonId);

            if ($(cancelEl).length > 0)
            {
                $(cancelEl).css('display', show ? "" : "none");
                $(cancelEl).on('click', function () {
                    swfUploadInstance.cancelUpload(fileID);

                    if (typeof (swfUploadInstance.customSettings.clientCancelUpload) === 'function') {
                        swfUploadInstance.customSettings.clientCancelUpload();
                    }

                    return false;
                });
            }

            $(this.fileProgressElement.childNodes[0]).on('click', function ()
            {
                swfUploadInstance.cancelUpload(fileID);

                if (typeof (swfUploadInstance.customSettings.clientCancelUpload) === 'function') {
                    swfUploadInstance.customSettings.clientCancelUpload();
                }

                return false;
            });
        }
    },

    appear: function ()
    {
        if (this.getTimer() !== null)
        {
            clearTimeout(this.getTimer());
            this.setTimer(null);
        }

        if ($(this.fileProgressWrapper).length > 0)
        {
            try
            {
                if (typeof this.fileProgressWrapper.style.filter != 'undefined') {
                    this.fileProgressWrapper.style.filter.alpha.opacity = 100;
                }
            }
            catch (e)
            {
            }
        } else
        {
            $(this.fileProgressWrapper).css('opacity', '1');
        }

        $(this.fileProgressWrapper).css('height', '');

        this.height = this.fileProgressWrapper.offsetHeight;
        this.opacity = 100;
        $(this.fileProgressWrapper).css('display', '');

    },

    // Fades out and clips away the FileProgress box.
    disappear: function ()
    {
        var reduceOpacityBy = 15;
        var reduceHeightBy = 4;
        var rate = 0; // 15 fps

        if (this.opacity > 0)
        {
            this.opacity -= reduceOpacityBy;
            if (this.opacity < 0)
            {
                this.opacity = 0;
            }

            if (this.fileProgressWrapper)
            {
                try
                {
                    $(this.fileProgressWrapper).filters.item("DXImageTransform.Microsoft.Alpha").opacity = this.opacity;
                } catch (e)
                {
                    // If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
                    $(this.fileProgressWrapper).css('filter','progid:DXImageTransform.Microsoft.Alpha(opacity=" + this.opacity + ")');
                }
            } else
            {
                $(this.fileProgressWrapper).css('opacity', 'this.opacity / 100');
            }
        }

        if (this.height > 0)
        {
            this.height -= reduceHeightBy;
            if (this.height < 0)
            {
                this.height = 0;
            }

            $(this.fileProgressWrapper).css('height', this.height + "px");
        }

        if (this.height > 0 || this.opacity > 0)
        {
            var oSelf = this;
            this.setTimer(setTimeout(function ()
            {
                oSelf.disappear();
            }, rate));
        } else
        {
            $(this.fileProgressWrapper).css('display', 'none');
            this.setTimer(null);
        }
        try
        {
            var divTarget = $('#' + this.targetID);
            var divFileInfo = divTarget.find('div[class="fileInfo_lg"]');
            if (divFileInfo) {
                divFileInfo.remove();
            }
            var divProgressBar = divTarget.find('div[class="progressWrapper_lg"]');
            if (divProgressBar) {
                divProgressBar.remove();
            }
            var divUploadProgress = divTarget.find('div[object_type="upload.progress"]');
            if (divUploadProgress) {
                divUploadProgress.remove();
            }
        }
        catch (e) { }
    },

    loadCSSClasses: function ()
    {
        if (this.options.uploadProgressMode == 0)
        {
            this.fileInfoClassName = "fileInfo";
            this.swfContainerClassName = "swf_container";
            this.progressWrapperClassName = "progressWrapper";
            this.progressContainerClassName = "progressContainer";
            this.progressCancelClassName = "progressCancel";
            this.progressNameClassName = "progressName";
            this.progressBarInProgressClassName = "progressBarInProgress";
            this.progressBarStatusClassName = "progressBarStatus";
            this.progressBarErrorClassName = "progressBarError";
            this.progressContainerGreenClassName = "progressContainer green";
            this.progressContainerBlueClassName = "progressContainer blue";
            this.progressContainerRedClassName = "progressContainer red";
        }
        else
        {
            this.fileInfoClassName = "fileInfo_lg";
            this.swfContainerClassName = "swf_container_lg";
            this.progressWrapperClassName = "progressWrapper_lg";
            this.progressContainerClassName = "progressContainer_lg";
            this.progressCancelClassName = "progressCancel_lg";
            this.progressNameClassName = "progressName_lg";
            this.progressBarInProgressClassName = "progressBarInProgress_lg";
            this.progressBarStatusClassName = "progressBarStatus_lg";
            this.progressBarErrorClassName = "progressBarStatus_lg";
            this.progressContainerGreenClassName = "progressContainer_lg green_lg";
            this.progressContainerBlueClassName = "progressContainer_lg blue_lg";
            this.progressContainerRedClassName = "progressContainer_lg red_lg";
        }
    }

});

if (typeof (Sys) != 'undefined' && typeof (Sys.Application) != 'undefined')
	Sys.Application.notifyScriptLoaded();