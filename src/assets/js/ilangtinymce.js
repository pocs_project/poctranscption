﻿$(document).ready(function () {
    if (typeof Sys !== "undefined") {
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(TinyMCEBeginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(TinyMCEEndRequest)
    }
});

var tinyMCEEditors = null;
var executedBeginRequest = false;
function TinyMCEBeginRequest(sender, args) {
    if (tinyMCEEditors == null || executedBeginRequest == true)
        return;

    for (var i = 0; i < tinyMCEEditors.length; i++) {
        if (tinymce.get(tinyMCEEditors[i]) != null)
            $('#' + tinyMCEEditors[i]).val(tinymce.get(tinyMCEEditors[i]).getContent());

        tinymce.execCommand('mceRemoveEditor', false, tinyMCEEditors[i]);

        if ($('#' + tinyMCEEditors[i]) != null)
            $('#' + tinyMCEEditors[i]).css('display', 'none');
    }
    executedBeginRequest = true;
}
function TinyMCEEndRequest(sender, args) {

    if (tinyMCEEditors == null)
        return;

    executedBeginRequest = false;

    for (var i = 0; i < tinyMCEEditors.length; i++) {
        if ($('#' + tinyMCEEditors[i]) != null && tinymce.get(tinyMCEEditors[i]) == null)
            $('#' + tinyMCEEditors[i]).css('display', 'block');
    }

    if (typeof frmOffsetHeightAttempts != 'undefined') {
        frmOffsetHeightAttempts = {};
    }
}
function ValidateTinyMCETExt(textClientId, labelErrorClientId) {
    if (tinymce.get(textClientId) == null)
        return (true);
    var textBody = tinymce.get(textClientId).getContent();
    var acuteCount = 0;
    for (var i = 0; i < textBody.length; i++) {
        if (textBody.charAt(i) == '`')
            acuteCount++;
    }
    if (acuteCount % 2 == 0) {
        $('#' + labelErrorClientId).css('display', 'none');
        return (true);
    }
    else {
        $('#' + labelErrorClientId).css('display', 'block');
        return (false);
    }
}
function RequiresTinyMCETExt(textClientId, labelErrorClientId) {
    var textBody;
    if (tinymce.get(textClientId) == null)
        textBody = $('#' + textClientId).val();
    else
        textBody = tinymce.get(textClientId).getContent();
    if ($chk(textBody)) {
        $('#' + labelErrorClientId).css('display', 'none');
        return (true);
    }
    else {
        $('#' + labelErrorClientId).css('display', 'block');
        return (false);
    }
}

function bugChrome57(editor) {
    tinymce.dom.Event.bind(editor.getWin(), 'focus', function (e) {
        var browserName = GetBrowserName().substr(0, 6);
        if (browserName.toLowerCase() == "chrome") {
            tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.dom.select('html'), 'heightOnChrome');
            tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.dom.select('body'), 'heightOnChrome');
        }
    });

    //tinymce.dom.Event.bind(editor.getWin(), 'blur', function (e) {
    //    $('#' + editorId).trigger('TinyMceBlur');
    //});
}

function InitializeTinyMCE(editorId) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: 'edit format insert table view',
            image_advtab: true,
            valid_elements: '*[*]',
            valid_children: "+body[link|style]",
            keep_styles: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            toolbar_items_size: 'small',            
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality template paste textcolor',
                'userMediaAudio userMediaBibliographicReference userMediaDocument userMediaGenericFile userMediaImage userMediaInternetLink userMediaSwf userMediaVideo asciimath eqneditor hsm isbn'
            ],
            toolbar1: 'fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist indent outdent | forecolor backcolor | code | asciimath eqneditor',
            toolbar2: 'userMediaDocument userMediaImage userMediaVideo userMediaAudio userMediaSwf userMediaInternetLink userMediaBibliographicReference | hsm isbn',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);

                });
            }
        });
    }, 200);
}
function InitializeTinyMCEAdminSimpleMode(editorId) {
    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            valid_elements: '*[*]',
            remove_script_host: false,
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste',
                'userMediaAudio userMediaBibliographicReference userMediaDocument userMediaGenericFile userMediaImage userMediaInternetLink userMediaSwf userMediaVideo'
            ],
            toolbar1: 'userMediaDocument userMediaImage userMediaVideo userMediaAudio userMediaSwf userMediaInternetLink userMediaBibliographicReference | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }

                    bugChrome57(editor);

                    if (tinymce.get(editorId) != undefined) {
                        tinymce.execCommand('mceFocus', false, editorId);
                    }

                });
            }
        });
    }, 200);
}
function InitializeTinyMCEAdminSimpleCustomMode(editorId, customUserMediaPugins) {
    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: true,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                customUserMediaPugins,
                'paste'
            ],
            toolbar1: customUserMediaPugins + ' | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }

                    bugChrome57(editor);
                });
            }
        });
    }, 200);
}
function InitializeTinyMCEStudentMode(editorId) {
    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    if (typeof (ShowLoading) != 'undefined') {
        ShowLoading();
    }

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste userMediaGenericFile wordcount'
            ],
            toolbar1: 'userMediaGenericFile | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }

                    bugChrome57(editor);
                });
            }
        });
    }, 200);
}
function InitializeTinyMCEStudentSimpleMode(editorId, executeSetFocus) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste wordcount'
            ],
            toolbar1: 'bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);
                    tinymce.dom.Event.bind(editor.getWin(), 'focus', function (e) {
                        $('#' + editorId).trigger('TinyMceFocus');
                    });

                    tinymce.dom.Event.bind(editor.getWin(), 'blur', function (e) {
                        $('#' + editorId).trigger('TinyMceBlur');
                    });

                    if (executeSetFocus) {
                        editor.focus();
                    }
                });
            }
        });
    }, 200);
}
function InitializeTinyMCEOptionSimpleMode(editorId, blurFn) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste userMediaImage code asciimath eqneditor'
            ],
            toolbar1: 'userMediaImage | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist | subscript superscript | code | asciimath eqneditor',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);
                });

                if (typeof (blurFn) == 'function') {
                    editor.on('blur', function (e) {
                        blurFn();
                    });
                }
                editor.on('SetContent', function (e) {

                });
                editor.on('BeforeSetContent', function (e) {
                    var rollback = e.content;
                    try {
                        console.log('BeforeSetContent: ', e.content);
                        if (e.content.toString().indexOf('src') > 0) {
                            var regex = /<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>/g;
                            var matchs = regex.exec(e.content);
                            if (matchs != null) {
                                if (matchs != null && matchs.length > 0) {
                                    if (matchs[1].indexOf("?") < 0)
                                        e.content = e.content.replace(matchs[1], matchs[1] + '?v=1');

                                }
                            }
                        }
                    } catch (e) {
                        e.content = rollback;
                    }

                });
            }
        });
    }, 200);
}
function InitializeTinyMCEOptionSimpleModeMathOnly(editorId, setFocus) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste asciimath table wordcount'
            ],
            toolbar1: 'asciimath | bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist | subscript superscript | table',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);
                    tinymce.dom.Event.bind(editor.getWin(), 'focus', function (e) {
                        $('#' + editorId).trigger('TinyMceFocus');
                    });

                    tinymce.dom.Event.bind(editor.getWin(), 'blur', function (e) {
                        $('#' + editorId).trigger('TinyMceBlur');
                    });
                    if (setFocus == true)
                        editor.focus();
                });
            }
        });
    }, 200);
}
function InitializeTinyMCEStudentForum(editorId, executeSetFocus) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: true,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            valid_elements: '*[*]',
            toolbar_items_size: 'small',
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'paste userMediaAudio userMediaDocument userMediaImage userMediaVideo wordcount'
            ],
            toolbar1: 'userMediaAudio userMediaDocument userMediaImage userMediaVideo |  bold italic underline | alignleft aligncenter alignright alignjustify | numlist bullist ',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);
                    tinymce.dom.Event.bind(editor.getWin(), 'focus', function (e) {
                        $('#' + editorId).trigger('TinyMceFocus');
                    });

                    tinymce.dom.Event.bind(editor.getWin(), 'blur', function (e) {
                        $('#' + editorId).trigger('TinyMceBlur');
                    });

                    if (executeSetFocus) {
                        editor.focus();
                    }
                });
            }
        });
    }, 200);
}
function InitializeTinyMCEStudentMobile(editorId, executeSetFocus, events) {

    if (tinymce.get(editorId) != undefined) {
        tinymce.execCommand('mceFocus', false, editorId);
        return;
    }

    ShowLoading();

    setTimeout(function () {
        tinymce.init({
            mode: 'exact',
            elements: editorId,
            width: $('#' + editorId).width(),
            language: 'pt_BR',
            theme: 'modern',
            skin: 'ilang',
            statusbar: false,
            menubar: false,
            image_advtab: true,
            paste_as_text: true,
            force_p_newlines: true,
            forced_root_block: '',
            remove_script_host: false,
            meus_eventos: events,
            valid_elements: '*[*]',
            toolbar_items_size: 'big',
            toolbar1_location : "bottom",
            content_css: ILangSettings.Sites.ILang() + '/Controls/TinyMCE/css/content.css',
            resize: true,
            plugins: [
                'emoticons paste userMediaPhotoUpload userMediaVideoForMobile userMediaYoutubeForMobile userMediaDocumentForMobile userMediaAudioForMobile wordcount'
            ],
            toolbar1: 'emoticons userMediaPhotoUpload userMediaVideoForMobile userMediaYoutubeForMobile userMediaDocumentForMobile userMediaAudioForMobile ',
            setup: function (editor) {
                editor.on('init', function (e) {
                    if (typeof (HideLoading) != 'undefined') {
                        HideLoading();
                    }
                    bugChrome57(editor);
                    tinymce.dom.Event.bind(editor.getWin(), 'focus', function (e) {
                        $('#' + editorId).trigger('TinyMceFocus');
                        if (typeof (events.focus) == 'function')
                            events.focus();
                    });

                    tinymce.dom.Event.bind(editor.getWin(), 'blur', function (e) {
                        $('#' + editorId).trigger('TinyMceBlur');
                    });

                    if (executeSetFocus) {
                        editor.focus();
                    }
                });
            }
        });
    }, 200);
}
var frmOffsetHeightAttempts = {};

function setTinyMCEIFrameHeight(iframeClientID) {
    try {
        var count = 0;
        if ($(frmOffsetHeightAttempts[iframeClientID]).length > 0)
            count = frmOffsetHeightAttempts[iframeClientID];

        if (count < 4) {
            frmOffsetHeightAttempts[iframeClientID] = count + 1;
            var offsetHeight = document.getElementById(iframeClientID).contentWindow.document.body.offsetHeight;
            if (offsetHeight == 0 || isNaN(offsetHeight)) {
                setTimeout(function () { setTinyMCEIFrameHeight(iframeClientID); }, 1000);
            }
            else {
                offsetHeight = offsetHeight + 13;
                $('#' + iframeClientID).height(offsetHeight);
               
            }
        }
    }
    catch (ex) { }
}

function scrollTinyMCEIframeToTopParent(iFrameUrl) {
    var iframe = $('iframe[src="' + iFrameUrl + '"]');

    if (iframe != null) {
        $([document.documentElement, document.body]).animate({
            scrollTop: iframe.offset().top
        }, 200);
    }
}

function setTinyMCEIFrameHeightAfterLoad(height, iFrameUrl) {
    var iframe = $('iframe[src="' + iFrameUrl + '"]');
    if (iframe != null) {
        iframe.css('height', height.toString() + 'px');
        iframe.parent().find('div.bgLoading').attr('class', '');
    }
}

function setTinyMCEIFrameUrl(iFrameID, iFrameUrl) {
    var iframe = $('iframe[id="' + iFrameID + '"]');
    if (iframe != null) {
        if ($(iframe).attr('src') != iFrameUrl)
            iframe.attr('src', iFrameUrl);
    }
}