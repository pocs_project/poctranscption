﻿ILangApp.directive('categoryMenu', () => {
    return {
        restrict: 'E',
        replace: true,
        scope: true,
        templateUrl: function () { return getStaticUrl('/Templates/Category/CategoryMenu.html'); },
        controller: ($scope, $http, $window) => {
            $('body').addClass('stDisBody');
            baseController($scope, $http, {});
            let me = $scope;
            me._categoryScope = null;
            me.menuOpenned = true;
            me.contentPlayerService = new ContentPlayerService();
            me.activeLessonCategoryId = 0;
            me.setResultTypes(4);
            me.disciplineProgressPercentage = 0;
            me.disciplineProgressPercentageVisible = true;
            me.totalLessonCategory = 0;
            me.totalLessonCategoryTracked = 0;

            me.getCategoryScope = () => {
                if (me._categoryScope === null) {
                    me._categoryScope = getAngularScopeByElementId('ContentPlayerCtrl');
                }
                return me._categoryScope;
            };

            me.enums = me.getCategoryScope().serverParameters.enums;
            me.fixedParameters = me.getCategoryScope().serverParameters.fixedParameters;

            if (me.fixedParameters.isFullTratme == true) {
                me.getCategoryScope().getExternalDirectives().tratme.configureCategoryMenu(me);
            }

            me.jsParameters.onAfterLoad = (result, base) => {
                me._bindDataOnContext('list', result.Result.List);
                me._bindDataOnContext('DisciplineName', result.Result.DisciplineName);
                me._bindDataOnContext('DisciplineID', result.Result.DisciplineId);

                if (result.Success && result.Result.List.length > 0) {
                    me.setActiveLessonCategory(me.bigContext.dto.SelectedLessonCategoryId);

                    me.$emit('onLeftMenuLessonsLoaded', { result, base });
                }

                me.GetReturnUrl();

                if (ILang.Util.isMobile())
                    me.changeMenuState();

                me._getDisciplineProgress();

                me.sendMessageWhenMenuLoaded();
            };

            me.sendMessageWhenMenuLoaded = () => {
                me.$emit('categoryLoaded', { origin: 'menu', hasLoadMenu: true });
            };

            me._bindDataOnContext = (key, value) => {
                me.bigContext[key] = value;
            };

            me._createUrlStudentAPI = (route) => {
                var urlBase = ILangSettings.Sites.StudentAPI();
                return `${urlBase}${route}`;
            };

            me._getMenuContentPlayerRoute = () => {
                return me._createUrlStudentAPI('/Lesson/GetLeftMenuContentPlayer');
            };

            me._getMenuContentPlayerParameters = () => {
                return {
                    ClassDisciplineCrpt: ILang.Util.QueryString.read('cd'),
                    ReferenceLessonCrpt: ILang.Util.QueryString.read('l'),
                    LessonCategoryCrpt: ILang.Util.QueryString.read('lc')
                };
            }

            me.setLoadSettings(
                me._getMenuContentPlayerRoute(),
                me._getMenuContentPlayerParameters(),
                'sideMenu', false);

            me.updateLessonsWithoutCategories = () => {
                var parameters = me._getMenuContentPlayerParameters();

                var lessonsToUpdate = me.bigContext.list.filter(lesson => lesson.List.length === 0);
                if (lessonsToUpdate.length === 0)
                    return;

                me.contentPlayerService.getLeftMenu(parameters).then(response => {
                    if (response.Success && response.Result.List.length > 0) {
                        lessonsToUpdate.forEach((lessonUpdate) => {
                            var lesson = response.Result.List.first({ LessonID: lessonUpdate.LessonID });
                            lessonUpdate.List = lesson.List;
                        });
                    }
                }).catch(error => {
                    showTopErrorMsg('Não foi possível atualizar o conteúdo do menu.', 10000, null, false);
                    console.log(error);
                });
            }

            me.showLesson = (lesson) => {
                if (lesson.Active === true) {
                    lesson.Active = false;
                }
                else {
                    lesson.Active = true;
                }
            };           

            me.showLoaderMessage = function (lessonCategoryType) {
                switch (lessonCategoryType) {
                    case me.enums.LessonCategoryType.LtiResource:
                    case me.enums.LessonCategoryType.LtiGradeResource:
                        return true;
                    default:
                        return false;
                }
            };

            me.defineLoaderMessage = function (lessonCategoryType) {
                switch (lessonCategoryType) {
                    case me.enums.LessonCategoryType.LtiResource:
                    case me.enums.LessonCategoryType.LtiGradeResource:
                        return 'Carregando laboratório';
                    default:
                        return null;
                }
            };

            me.loadLessonCategory = (lessonCategoryId, lessonId, hasTracking, lessonCategoryType) => {
                var categoryScope = me.getCategoryScope();

                categoryScope._showLoaderMessage = me.showLoaderMessage(lessonCategoryType);
                categoryScope._loaderMessage = me.defineLoaderMessage(lessonCategoryType);

                if (!ILang.Util.isValidObject(categoryScope))
                    throw new Error('Escopo não localizado');

                var lessonCategoryDto = {
                    LessonCategoryID: lessonCategoryId,
                    LessonID: lessonId,
                    DisciplineID: me.bigContext.DisciplineID,
                    HasTracking: hasTracking,
                    LessonCategoryType: lessonCategoryType
                };

                categoryScope.loadLessonCategory(lessonCategoryDto);
                me.setActiveLessonCategory(lessonCategoryId);

                if (ILang.Util.isMobile()) {
                    me._closeMenu();
                }
            };

            me.checkLesson = (LessonCategoryID) => {
                var selected = me.bigContext.list.find((lesson) => {
                    return lesson.List.first({ LessonCategoryID: LessonCategoryID });
                });               

                if (selected !== null) {
                    var lessonCategory = selected.List.first();

                    if (!lessonCategory.HasTracking) {
                        lessonCategory.HasTracking = true;
                        me.totalLessonCategoryTracked++;
                    }
                }

                me._calculateDisciplineProgressPercentage();
            };

            me._findLesson = (lessonID) => {
                return me.bigContext.list.first({ LessonID: lessonID });
            };

            me._getMenuButtonElement = () => {
                return $('#menubutton');
            };

            me._getMenuElement = () => {
                return $('#studentmenu');
            };

            me._closeMenu = () => {
                var menuButton = me._getMenuButtonElement();
                var menu = me._getMenuElement();

                me.menuOpenned = false;
                menuButton.addClass('closed');
                menu.removeClass('open');
            };

            me._openMenu = () => {
                var menuButton = me._getMenuButtonElement();
                var menu = me._getMenuElement();

                me.menuOpenned = true;
                menuButton.removeClass('closed');
                menu.addClass('open');
            };

            me.changeMenuState = () => {
                me.menuOpenned
                    ? me._closeMenu()
                    : me._openMenu();
            };

            me.GetReturnUrl = () => {
                let classDisciplineCrpt = ILang.Util.QueryString.read('cd')
                let url = me._getPageUrl();
                let returnLink = me._getReturnLinkElement();
                let returnText = me._getReturnTextElement();

                me._setReturnPageLink(url, classDisciplineCrpt)

                returnLink.title = R.student.voltarMenuPlayer;
                returnText.text(R.student.voltarMenuPlayer);
            };

            me._setReturnPageLink = (url, classDisciplineCrpt) => {
                me._getReturnLinkElement().attr('href', url + '/Lesson/index?cd=' + classDisciplineCrpt + '#pageIndex=1');
            };

            me._getReturnTextElement = () => {
                return $('#ucreturntext');
            };

            me._getReturnLinkElement = () => {
                return $('#ucreturnlink');
            };

            me._getPageUrl = () => {
                return $(location).attr('protocol') + '//' + $(location).attr('hostname');
            };

            me.setActiveLessonCategory = (lessonCategoryID) => {
                me.activeLessonCategoryId = lessonCategoryID;
            };

            me.setLessonActive = (lessonId) => {
                var lesson = me._findLesson(lessonId);

                if (lesson) {
                    lesson.Active = true;
                }
            };

            me.scrollToLessonCategory = (lessonId, lessonCategoryId) => {
                me.setActiveLessonCategory(lessonCategoryId);
                me.setLessonActive(lessonId);

                me._getScrollListElement().animate({
                    scrollTop: me._getSelectedElement(lessonCategoryId).offset().top - me._getScrollListElement().offset().top + me._getScrollListElement().scrollTop()
                }, 'slow');
            };

            me._getSelectedElement = (lessonCategoryId) => {
                return $('#' + lessonCategoryId);
            }

            me._getScrollListElement = () => {
                return $('#' + 'listaAtividades');
            };

            me._getTotalAmountLessonCategory = () => {
                return me.totalLessonCategory;
            };

            me._getTotalAmountLessonCategoryTracked = () => {
                return me.totalLessonCategoryTracked;
            };

            me._calculateDisciplineProgressPercentage = () => {
                var totalLessonCategory = me._getTotalAmountLessonCategory();
                var totalLessonCategoryTracked = me._getTotalAmountLessonCategoryTracked();

                var percent = Math.trunc((totalLessonCategoryTracked * 100) / totalLessonCategory);

                me.disciplineProgressPercentage = percent > 100 ? 100 : percent;
            };

            me._getDisciplineProgress = () => {
                var parameters = me._getClassDisciplinaStudentProgressApiParameters();
                me.contentPlayerService.getClassDisciplineProgress(parameters).then(result => {
                    if (result.Result.Progress < 0 || !result.Success) {
                        me.disciplineProgressPercentageVisible = false;
                    } else {
                        me.disciplineProgressPercentage = result.Result.Progress;
                        me.totalLessonCategory = result.Result.LessonCategoryCount;
                        me.totalLessonCategoryTracked = result.Result.LessonCategoryViewedCount;
                    }
                }).catch(_ => {
                    me.disciplineProgressPercentageVisible = false;
                });
            };

            me._getClassDisciplinaStudentProgressApiParameters = () => {
                return {
                    EncryptedClassDisciplineId: ILang.Util.QueryString.read('cd'),
                };
            };

            me.goToNextLesson = (nextLesson) => {
                me.updateLessonsWithoutCategories();
                me.loadLessonCategory(nextLesson.LessonCategoryID, nextLesson.LessonID);
                me.scrollToLessonCategory(nextLesson.LessonID, nextLesson.LessonCategoryID);
            };

            me.getClassIcon = (category) => {
                var defaultIcon = "ulIcon-Page";

                var classIconsItens = [];
                classIconsItens[9] = "ulIcon-Page";
                classIconsItens[24] = "ulIcon-Page";
                classIconsItens[15] = "ulIcon-QuestionsOpen";
                classIconsItens[11] = "ulIcon-QuestionsObjectives";
                classIconsItens[20] = "ulIcon-Group";
                classIconsItens[12] = "ulIcon-Forum";
                classIconsItens[25] = "ulIcon-Prova-Presencial";
                classIconsItens[22] = "ulIcon-Prova-Presencial";
                classIconsItens[16] = "ulIcon-Grade";
                classIconsItens[26] = "ulIcon-Lab-Atividade-Externa";
                classIconsItens[27] = "ulIcon-Lab-Atividade-Externa";                
                
                if (category.LessonCategoryType === 11 && category.ExerciseSetQuestionTypeID === 1) {
                    return classIconsItens[11];
                } else if (category.LessonCategoryType === 11 && category.ExerciseSetQuestionTypeID === 2) {
                    return classIconsItens[15];
                }

                return classIconsItens[category.LessonCategoryType] || defaultIcon;
            };
            window.onload = function () {
                var schLesson = document.querySelector('#SearchLessonID')
                $(schLesson).keyup(function (event) {
                    if (event.keyCode === 13) {
                        $("#btnSearch").click();
                    }
                });
            };
            me.keyWord = "";
            me.KeyWordFilter = false;
            me.searchKeyWord = function () {
                var schLesson = document.querySelector('#SearchLessonID');
                me.keyWord = $(schLesson).val();
                me.bigSearch({ KeyWord: me.keyWord }, undefined, false);
                if (me.keyWord != "") {
                    me.KeyWordFilter = true;
                } else {
                    me.KeyWordFilter = false;
                }
            };

            me.lessonClearTxt = function (e) {
                document.getElementById('SearchLessonID').value = '';
                me.KeyWord = '';
                me.searchKeyWord()
                
            };

            me.returnToUrl = function () {
                $window.location.href = me.getCategoryScope().serverParameters.parameters.returnUrl;
            };
        }
    };
});