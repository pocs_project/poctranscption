﻿ILangApp.component("disciplineProgressBar", {
    templateUrl: () => {
        return getStaticUrl('/Templates/Category/DisciplineProgressBar.html');
    },
    bindings: { percentage: '@' }
});