﻿
ILangApp.controller('StudentCategoryForumCtrl', function ($http, $scope, $timeout, $location, $anchorScroll) {
    baseController($scope, $http, {
        infiniteScrolling: false,
        rewriteHash: false,
        onBeforeLoad: function (base) {
            base.bigContext.hasMorePages = true;
        }
    });
    me.done = false;
    me.props = {};
});
