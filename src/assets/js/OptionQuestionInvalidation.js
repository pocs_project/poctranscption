﻿ILangApp.directive('optionQuestionInvalidation', () => {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: () => {
            return getStaticUrl('/Templates/Dialog/OptionQuestionInvalidation.html');
        }
    };
});