﻿ILangApp.directive('categoryPage', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('Page'); },
        scope: {
            allprops: '=',
            userImageUrl: '=',
            hideFeedComment: '='
        }
        , controller: function ($scope, $element, $attrs, $transclude) {

            let me = $scope;
            me.dto = me.allprops.Detail;
            me.root = getRootScope($scope);
            registerCommonFunction(me);
            if ($('.pageContent').length > 0) {
                me.maxCards = Math.floor($('.pageContent').eq(0).width() / 50) - 2;
            } else {
                me.maxCards = Math.floor($('.page-content').eq(0).width() / 50) - 2;
            }
            me.firstCard = 1;
            me.lastCard = me.maxCards;
            me.CurrentPageIndex = 0;
            me.currentPage = null;

            me.openViewCount = function (feed) {
                if (typeof (me.userList) == 'undefined' || me.userList == null)
                    return;

                let percent = me.getViewPercentage();

                me.userList.total(parseInt(me.dto.TotalCount));
                me.userList.percent(percent);

                let param = me.getDefaultParameters();
                me.userList.get('feedUsers', param);
            }

            me.getQuestions = function () {
                return me.dto.PageList.filter(function (item) {
                    return item.SequentialNumber >= me.firstCard && item.SequentialNumber <= me.lastCard;
                });
            };

            me.scrollRight = function (step) {
                me.lastCard += step || me.maxCards;
                me.firstCard += step || me.maxCards;
            };
            me.scrollLeft = function (step) {
                me.lastCard -= step || me.maxCards;
                me.firstCard -= step || me.maxCards;
            }
            me.select = function (item) {
                me.currentPage = item;
                me.CurrentPageIndex = item.SequentialNumber - 1;
                me.dto.PageList.set({ Active: false });
                item.Active = true;
                item.Viewed = true;
                me.$parent.registerViewedLessonCategory(item.LessonCategoryID, item.LessonCategoryPageID);
                me.verifyAllItemsAreRead();
            };
            me.verifyAllItemsAreRead = function () {
                if (me.dto.PageList.count({ Viewed: true }) == me.dto.PageList.length) {
                    me.root.checkLessonOnLeftMenu(me.allprops);
                }
            };
            me.init = function () {
                if (me.currentPage == null && typeof (me.dto.PageList) != 'undefined' && me.dto.PageList.length > 0) {
                    me.currentPage = me.dto.PageList[0];
                    me.currentPage.Active = true;
                }
            };

            changeFocusKeyDowIframe();
        }
    };
});

