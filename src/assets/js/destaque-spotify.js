// banner destaque
function spotCheck() {
    try {
        var pathname = window.location.pathname;
        var href = window.location.href;

        if ((pathname == '/StudentHome') || (pathname == '/Home/Index') || (window.location.toString().includes('https://student.ulife.com.br/#stay'))) {
            var pathname = window.location.pathname;
            var href = window.location.href;
            var css_destaque = document.createElement('link');
            css_destaque.rel = 'stylesheet';
            css_destaque.type = 'text/css';
            css_destaque.href = "https://ulife-catalogcontent.s3.us-east-1.amazonaws.com/content-cli/Tasting/BannerDestaque/destaque.css?v=" + new Date().getTime();
            document.head.appendChild(css_destaque)

            // seleciona o elemento que será a referência para a nova div
            var cards = document.getElementById('divStudentDisciplines') || document.getElementsByClassName('cardContFlex homeUcCont')[0];

            // cria a nova div e adiciona a classe "spotify"
            var novaDiv = document.createElement('div');
            //    novaDiv.classList.add('spotify');
            novaDiv.classList.add('degustacao');
            novaDiv.style.cursor = 'pointer';
            novaDiv.style.setProperty('cursor', 'pointer', 'important');

            // insere a nova div acima da div de referência
            cards.parentNode.insertBefore(novaDiv, cards);

            // encontra a div com classe "ltTop" dentro da div de referência
            var divLtTop = cards.querySelector('.ltTop');

            // adiciona o ID "spot" na nova div
            novaDiv.setAttribute('id', 'spot');



            // adiciona um evento de clique na nova div
            //novaDiv.addEventListener('click', () => {
                //window.open('https://www.onelearning.com.br/curso/descomplicando-o-podcast-aprenda-com-quem-faz', '_blank'); //chamada do spotify
                //window.open('https://onelink.to/fsw8fx', '_blank');

            //});
            // const user = document.getElementsByClassName('userName')[0];
            // if(user && user.textContent == 'User Teste 02') {
            var modulo_recomendacao = document.createElement('script');
            modulo_recomendacao.src = "https://ulife-catalogcontent.s3.us-east-1.amazonaws.com/content-cli/Tasting/Homes/recomendacoes/scripts/similaridade-proposta-v2.js?v=" + new Date().getTime();
            document.head.appendChild(modulo_recomendacao);

            // }


        }

    } catch (error) {
    }

}

function recursiveCarregou(tentativas) {
    var card = document.querySelector('a[href*="cd="]');
    if (card) {
        spotCheck()
    } else {
        if (tentativas >= 0) {
            setTimeout(function () {
                // chame a função novamente após um intervalo de tempo
                recursiveCarregou(tentativas - 1)
            }, 200);
        }
    }
}

recursiveCarregou(20);