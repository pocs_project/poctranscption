﻿

/***********************************************************************************************************************
Validação de Flash instalado
***********************************************************************************************************************/
window.tryFlash();

CustomUploadInstances = [];

var CustomUpload = function (options) {
    this.init(options)
};

$.extend(CustomUpload.prototype, {
    init: function (options) {
        // Members
        this.uploadControl = null;
        this.uploadControlInstanceName = null;
        this.options = options;

        this.createUploadControl();

        this.subscribeAJAXEvents();

        CustomUploadInstances.push(this);

        var cancel = document.getElementById(this.options.cancelButtonId);

        if (cancel != null) {
            var me = this;

            $(cancel).on('click', function () {
                me.cancelUpload();
                $(cancel).css('display', 'none');
            });
        }
    },

    createUploadControl: function () {

        if (this.uploadControl != null)
            return;

        this.uploadControl = new SWFUpload({
            // Backend Settings
            upload_url: this.options.upload_url,
            post_params: { "ASPSESSID": _jsguid(), "fl": this.options.fileLocation.toString() },

            // File Upload Settings
            file_size_limit: this.options.file_size_limit,
            file_types: this.options.file_types,
            //file_upload_limit: "1",
            file_queue_limit: this.options.file_queue_limit,

            // Event Handler Settings (all my handlers are in the Handler.js file)
            file_dialog_start_handler: fileDialogStart,
            file_queued_handler: fileQueued,
            file_queue_error_handler: fileQueueError,
            file_dialog_complete_handler: fileDialogComplete,
            upload_start_handler: uploadStart,
            upload_progress_handler: uploadProgress,
            upload_error_handler: uploadError,
            upload_success_handler: uploadSuccess,
            upload_complete_handler: uploadComplete,

            // Button Settings
            button_image_url: this.options.button_image_url,
            button_placeholder_id: this.options.button_placeholder_id,
            button_width: this.options.button_width,
            button_height: 22,
            button_cursor: SWFUpload.CURSOR.HAND,

            // Flash Settings
            flash_url: this.options.flash_url,

            custom_settings: {
                containerId: this.options.containerId,
                progressTarget: this.options.progressTarget,
                cancelButtonId: this.options.cancelButtonId,
                clienttUploadSuccess: this.options.clienttUploadSuccess,
                serverUploadSuccess: this.options.serverUploadSuccess,
                clientStartUpload: this.options.clientStartUpload,
                clientCancelUpload: this.options.clientCancelUpload,
                fileIsToBigMessage: this.options.fileIsToBigMessage,
                resources_url: this.options.resources_url,
                uploadProgressMode: this.options.uploadProgressMode,
                uploadTypeMode: this.options.uploadTypeMode,
                button_width: this.options.button_width,
                button_height: 22

            },
        });

        this.uploadControlInstanceName = this.uploadControl.movieName;
    },

    destroyUploadControl: function () {
        this.uploadControl.destroy();
        this.uploadControl = null;
    },

    subscribeAJAXEvents: function () {
        if (typeof (Sys) != 'undefined' && Sys.WebForms.PageRequestManager.getInstance() != null) {
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(uploadControlBeginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(uploadControlEndRequest);
        }
    },

    manageAJAXBeginRequest: function (updatePanelClientIDs) {
        for (index = 0; index < updatePanelClientIDs.length; index++) {
            $('#' + updatePanelClientIDs[index]).find('object').each(function (i, item) {
                if (item.id == this.uploadControlInstanceName)
                    this.destroyUploadControl();
            });
        }
    },

    manageAJAXEndRequest: function (updatePanelClientIDs) {
        this.createUploadControl();
    },

    stopUpload: function () {
        return this.uploadControl.stopUpload();
    },

    getStats: function () {
        return this.uploadControl.getStats();
    },

    cancelUpload: function () {
        try { this.uploadControl.cancelUpload(); } catch (ex) { }
    }
});

function uploadControlBeginRequest(sender, args) {
    for (controlIndex = 0; controlIndex < CustomUploadInstances.length; controlIndex++) {
        CustomUploadInstances[controlIndex].manageAJAXBeginRequest(sender._updatePanelClientIDs);
    }
}

function uploadControlEndRequest(sender, args) {
    for (controlIndex = 0; controlIndex < CustomUploadInstances.length; controlIndex++) {
        CustomUploadInstances[controlIndex].manageAJAXEndRequest(sender._updatePanelClientIDs);
    }
}

if (typeof (Sys) != 'undefined' && typeof (Sys.Application) != 'undefined')
    Sys.Application.notifyScriptLoaded();