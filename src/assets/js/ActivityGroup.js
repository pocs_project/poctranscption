﻿ILangApp.directive('activityGroup', function ($interval) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: function () { return getCategoryAngularTemplate('ActivityGroup'); },
        scope: {
            allprops: '='
        },
        controller: function ($scope, $element, $attrs) {
            let me = $scope;
            registerCommonFunction(me);
            me.busy = false;
            me.savedText = '';
            me.exercisedto = me.allprops.Detail;
            me.serverParameters = me.$parent.serverParameters;

            me.root = getRootScope($scope);
            me.enums = me.root.serverParameters.enums;
            me.confirmPublication = false;
            me.chatScope = null;

            registerTinymceFunctions(me);
            me.getDefaultParameters = function () {
                return {
                    StudentLessonID: me.allprops.StudentLessonID,
                    StudentTrackID: me.exercisedto.StudentTrackID || 0,
                    ClassDisciplineID: parseInt(me.root.bigContext.dto.ClassDisciplineID),
                    LessonID: parseInt(me.root.bigContext.dto.LessonID),
                    LessonCategoryID: parseInt(me.allprops.LessonCategoryID),
                }
            };
            me.cancel = function () {
                if (me.exercisedto.EditStatus != me.enums.StudentOpenEndedquestionEditStatus.Blank) {
                    me.exercisedto.Text = me.savedText;
                    me.setText();
                    me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                }
                me.stopHeartBeat();
                me.$parent.action('CancelWritingTextActivityGroup', me.getDefaultParameters(), null);
            };
            me.save = function () {
                if (!me.exercisedto.AllowStudentToPublish)
                    return;

                if (isEmptyOrSpaces(me.getText())) {
                    return;
                }

                if (me.busy)
                    return

                me.stopHeartBeat();
                me.busy = true;
                let param = me.getDefaultParameters();
                param.Text = me.getText();
                me.$parent.action('saveActGroup', param,
                    function (result) {
                        if (result.Success) {
                            me.exercisedto.Text = param.Text;
                            me.savedText = param.Text;
                            me.exercisedto.StudentTrackID = result.Result;
                            me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                            me.exercisedto.ShowStudentLessonCategoryPost = true;
                            me.updateAssignmentPost();
                        }
                        me.busy = false;
                    }, me.onError);

            };
            me.publish = function () {
                if (!me.exercisedto.AllowStudentToPublish)
                    return;

                if (me.busy)
                    return

                me.busy = true;
                me.stopHeartBeat();
                let param = me.getDefaultParameters();
                me.$parent.action('publishActGroup', param, function (result) {
                    if (result.Success) {
                        me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Published;
                        me.exercisedto.ShowStudentLessonCategoryPost = true;
                        me.confirmPublication = false;
                        me.updateAssignmentPost();
                        me.pushOnGoogleAnalytics();
                        me.root.checkLessonOnLeftMenu(me.allprops);
                    }
                    me.busy = false;
                }, me.onError);
            };
            me.confirmPublish = function () {
                me.confirmPublication = true;
            };
            me.hideConfirmationLayer = function () {
                me.confirmPublication = false;
            };
            me.updateTextIfHasChanged = function (result) {
                if (me.getText() != result.Result.Text) {
                    me.savedText = result.Result.Text;
                    me.setText();
                }

            }
            me.checkAnyoneIsUsing = function (positiveFn, negativeFn) {
                let param = me.getDefaultParameters();
                me.$parent.action('tryStartWritingActivityGroup', param, function (result) {
                    if (result.Success && typeof (positiveFn) == 'function') {
                        me.updateTextIfHasChanged(result);
                        positiveFn();
                        me.startHeartBeat();
                    } else if (typeof (negativeFn) == 'function') {
                        negativeFn();
                        me.stopHeartBeat();
                    }
                }, function (error) {
                    if (typeof (negativeFn) == 'function')
                        negativeFn(error);
                });
            };

            me.keepEditing = function () {
                me.checkAnyoneIsUsing(function () {
                    me.activityGroupInitTinymce();
                    me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Blank;
                    me.busy = false;
                }, function (err) {
                    me.showBlockMessage(err.ErrorMessage, 4000);
                });
            };
            me.getText = function () {
                return me.getEditorContent(me.txtFieldID(), 'html');
            };
            me.setText = function () {
                me.setEditorContent(me.txtFieldID(), me.savedText);
            };
            me.txtFieldID = function () {
                return 'if_' + me.allprops.LessonCategoryID;
            };
            me.onload = function () {
                me.savedText = me.exercisedto.Text;
                if (isSafeToUse(me, 'allprops.LessonCategoryID'))
                    me.serverParameters.fixedParameters['LessonCategoryID'] = parseInt(me.allprops.LessonCategoryID);
                if (isSafeToUse(me, 'allprops.Detail.ActivityGroupID'))
                    me.serverParameters.fixedParameters['ActivityGroupID'] = parseInt(me.allprops.Detail.ActivityGroupID);

                me.serverParameters.drops.dropGroup.selected = me.allprops.Detail.ActivityGroupID;
                me.originalActivityGroupID = me.serverParameters.drops.dropGroup.selected;
                me.exercisedto.isTeacher = (me.exercisedto.TeacherList != null && me.exercisedto.TeacherList.any({ UserID: ILangSettings.UserID }));
            };
            me.onError = function () {
                me.busy = false;
                me.generalError(error);
            };
            me.deleteFile = function (file) {
                let param = me.getDefaultParameters();
                param.ID = file.ID;

                if (me.busy)
                    return

                me.busy = true;
                me.$parent.action('deleteFileActGroup', param, function (result) {
                    if (result.Success) {
                        me.exercisedto.FileList.remove({ ID: file.ID });
                        if (me.exercisedto.FileList.length == 0)
                            me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Blank;

                        me.updateAssignmentPost();
                    }
                    me.busy = false;
                }, me.onError);
            };
            me.refreshAssignmentPost = function () {
                if (isSafeToUse(me, 'assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
                    me.assignmentPostInstance.loadPosts(undefined);
                } else if (isSafeToUse(me, 'root.assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
                    me.root.assignmentPostInstance.loadPosts(undefined);
                }
            };

            me.updateAssignmentPost = function () {

                if (isSafeToUse(me, 'assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
                    me.assignmentPostInstance.refresh();
                } else if (isSafeToUse(me, 'root.assignmentPostInstance') && compareIfSafe(me, 'exercisedto.ShowStudentLessonCategoryPost', true)) {
                    me.root.assignmentPostInstance.refresh();
                }
            };

            me.pushOnGoogleAnalytics = function () {
                if (!isSafeToUse(me, 'root'))
                    me.root = getRootScope($scope);
                me.root.pushOnGoogleAnalytics(me.allprops);
            };

            me.onUploadSuccess = function (response) {
                if (response.response.Success) {
                    let param = me.getDefaultParameters();

                    param.FileName = response.response.Result.FileName;
                    param.FileSize = response.file.size;
                    param.AttachmentPath = response.response.Result.VirtualPath;
                    me.$parent.action('uploadFileActGroup', param, function (result) {
                        me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved;
                        me.exercisedto.FileList.push({
                            FileUrl: response.response.Result.VirtualPath,
                            FileName: response.response.Result.FileName,
                            AttachmentPath: response.response.Result.VirtualPath,
                            ID: result.Result
                        });
                        me.updateAssignmentPost();
                        response.after();
                    });
                } else {
                    throw getPropertyByName(response, 'response.ErrorMessage');
                }
            }
            me.activityGroupInitTinymce = function () {
                if (me.studentIsSuspendedOnDiscipline)
                    return

                me.checkAnyoneIsUsing(undefined, function (err) {
                    me.showBlockMessage(err.ErrorMessage);
                    me.exercisedto.EditStatus = me.enums.StudentOpenEndedquestionEditStatus.Saved
                });

                InitializeTinyMCEStudentSimpleMode(me.txtFieldID(), true);
            };
            me.heartBeatTimer = null;
            me.startHeartBeat = function () {
                me.heartBeatTimer = $interval(function () {
                    me.updateHeartBeat()
                }, 20 * 1000);
            }
            me.stopHeartBeat = function () {
                if (me.heartBeatTimer == null)
                    return;

                $interval.cancel(me.heartBeatTimer);
                me.heartBeatTimer = null;
            };
            me.updateHeartBeat = function () {
                if (me.heartBeatTimer == null)
                    return;

                me.$parent.action('HeartBeatActivityGroup', me.getDefaultParameters(), function (result) {
                    console.log('heartBeatado', result)
                }, null, { showLoading: false });
            };
            me.showBlockMessage = function (err) {
                me.confirmationDialog.title = 'Existe outra pessoa editando essa atividade';
                me.confirmationDialog.btnOkValue = 'Fechar';
                me.confirmationDialog.htmlContent = '<p class=\'pvb fm\'>' + err + '.</p>';
                me.confirmationDialog.showCancelButton = false;
                me.confirmationDialog.open();
                me.confirmationDialog.onBtoClick = function () {
                    me.confirmationDialog.close();
                }
            }

            me.originalActivityGroupID = 0;
            me.dropGroupSelectedID = 0;
            me.getOneActivityGroupDetail = function () {

                if (me.serverParameters.drops.dropGroup.loaded && me.serverParameters.drops.dropGroup.selected != me.exercisedto.ActivityGroupID) {
                    let param = me.getDefaultParameters();
                    param['ActivityGroupID'] = me.serverParameters.drops.dropGroup.selected;
                    param['OriginalActivityGroupID'] = me.originalActivityGroupID;

                    me.root.action('getOneActivityGroupDetail', param, function (result, status, scope) {
                        console.log(result);
                        if (result.Success) {
                            copyPropersTo(scope.allprops.Detail, result.Result, ['Text', 'StudentList', 'HtmlFileUrl', 'FileList', 'IsMyGroup', 'ReadOnly', 'OpenEndedQuestionType', 'ActivityGroupID', 'HasActivityGroup']);
                            scope.refreshAssignmentPost();

                        }
                    }, undefined, undefined, me)
                }
            }

            //chat
            me.openChat = function (chatGroupID) {
                if (me.chatScope == null) {
                    me.chatScope = me.root.df__getChatScope();
                }
                me.chatScope.openGroupChat({ id: chatGroupID == null ? me.exercisedto.ChatGroupID : chatGroupID });
            };

            //msg
            me.sendMsgToAll = function (teachers) {
                if (teachers != null && teachers.length > 0) {
                    openComposeMesageDialog(teachers.select({ EntityID: 'UserID', Name: 'Name', ImageUrl: 'ImageUrl', Type: 1 }), false);
                }
            }

            changeFocusKeyDowIframe();
        }
    };
});