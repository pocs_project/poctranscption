﻿ILangApp.directive('contentPlayerAccessibilityHelp', function () {
    return {
        
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getStaticUrl('/Templates/Category/ContentPlayerAccessibilityHelp.html'); },
        controller: function ($scope, $http) {
            baseController($scope, $http, {});

            var me = $scope;
            me.accessibilityHelpVisibility = false;
            me.responseAccessibilityHelp = false;
            me.accessibilityAriaHidden = true;
            me.accessibilityFirstAccess = true;
            me.contentPlayerAccessbilityHelpService = new ContentPlayerAccessibilityHelpService();

            me.onLoad = function () {
                me.contentPlayerAccessbilityHelpService.getAccessibilityHelpIsEnable().then(response => {
                    if (response.Success) {
                        me.responseAccessibilityHelp = response.Result.FirstAcess;

                        if (me.responseAccessibilityHelp) {
                            me.contentPlayerAccessbilityHelpService.setAccessibilityHelpViewed();
                        }
                    }
                }).catch(error => {
                    me.responseAccessibilityHelp = false;
                    console.log(error);
                });

                me.sendMessageWhenAccessibilityHelpLoaded();
            };

            me.sendMessageWhenAccessibilityHelpLoaded = () => {
                me.$emit('categoryLoaded', { origin: 'accessibilityHelp', hasLoadAccessibility: true });
            };

            me.closeAccessibilityHelp = function () {
                me.accessibilityHelpVisibility = false;
                me.accessibilityAriaHidden = true;
                $("#accessibilityHelpButton").focus();
                HelpHero.startTour('1Bl6vKcSLV', { skipIfAlreadySeen: true, redirectIfNeeded: false  });
            };

            me.openAccessibilityHelp = function () {
                me.accessibilityHelpVisibility = true;               
                me.accessibilityAriaHidden = false;
                $("#accessibilityHelpDialogContent").focus();                
            };

            me.openIfAccessibilityHelpEnable = function () {
                if (me.responseAccessibilityHelp && me.accessibilityFirstAccess) {
                    me.openAccessibilityHelp();
                    me.accessibilityFirstAccess = false;
                }
            };
        }
    }
});