﻿ILangApp.directive('questionMultiplechoice', function ($timeout) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function () { return getCategoryAngularTemplate('MultipleChoice'); },
        scope: {
            allprops: '='
        }
        , controller: function ($scope, $element, $attrs, $transclude) {
            registerExerciseController($scope, $element, $attrs, $transclude, $timeout);

            $scope.loadMath = function (html) {
                let inputHtml = document.createElement('div');
                $(inputHtml).html(html);

                $(inputHtml).find('.AM').each(function (index, item) {
                    let expression = $(item).text().replace('`', '').replace('`', '');
                    let image = AMTparseMath(expression);

                    $(item).html(image);
                });

                return $(inputHtml).html();
            };

            $scope.selectOption = function (option) {
                if ($scope.isDisabled())
                    return;

                $scope.currentQuestion.SelectedOption = option.QuestionOptionID;
            };

            $scope.hideOptionInput = function (option) {
                return $scope.showCorrectAnswerFeedBack(option) ||
                    $scope.showIncorrectAnswerFeedBack(option);
            }

            $scope.showCorrectAnswerFeedBack = function (option) {
                return $scope.showColorfulFeedBack() &&
                    $scope.userHasAnswered() &&
                    $scope.isCorrectAnswer(option);

            };

            $scope.showIncorrectAnswerFeedBack = function (option) {
                return $scope.showColorfulFeedBack() &&
                    $scope.userHasAnswered() &&
                    $scope.optionIsSelected(option) &&
                    !$scope.currentQuestion.IsCorrect &&
                    !$scope.isCorrectAnswer(option);

            };

            $scope.showColorfulFeedBack = function () {
                return $scope.exercisedto.WorkflowCssClassName != 'exNoFeedback' &&
                    ($scope.currentQuestion.ShowCorrectAnswer || $scope.currentQuestion.ShowFeedback);
            };

            $scope.optionIsSelected = function (option) {
                return option.QuestionOptionID == $scope.currentQuestion.SelectedOption;
            };

            $scope.answeredQuestionOptionHasValue = function () {
                return $scope.currentQuestion.AnsweredQuestionOptionID != null &&
                    $scope.currentQuestion.AnsweredQuestionOptionID != 0;
            }

            $scope.userHasAnswered = function () {
                return (
                    $scope.answeredQuestionOptionHasValue() ||
                    $scope.currentQuestion.IsCancelled || $scope.exercisedto.ReadOnly) ||
                    ($scope.exercisedto.DiscardedQuestionEnabled && $scope.currentQuestion.Status == $scope.enums.StudentQuestionStatus.Discarded)
            }

            $scope.isCorrectAnswer = function (option) {
                return ($scope.currentQuestion.IsCorrect && $scope.optionIsSelected(option) && !$scope.exercisedto.DiscardedQuestionEnabled) ||
                    ($scope.currentQuestion.ShowCorrectAnswer && $scope.currentQuestion.CorrectQuestionOptionID == option.QuestionOptionID);
            }

            $scope.isDisabled = function () {
                return $scope.checkAnswerStatus() ||
                    $scope.lockOptions ||
                    $scope.exercisedto.ReadOnly ||
                    $scope.currentQuestion.IsCancelled;
            }

            $scope.checkAnswerStatus = function () {
                return $scope.currentQuestion.Status != $scope.enums.StudentQuestionStatus.Unanswered &&
                    $scope.currentQuestion.Status != $scope.enums.StudentQuestionStatus.Skipped;
            }

            $scope.showStartButton = function () {
                return $scope.exercisedto.ShowStartButton &&
                    $scope.allprops.LessonCategoryType != $scope.enums.LessonCategoryType.OnlineExercise
            };

            $scope.showStartOnlineExercise = function () {
                return !$scope.exercisedto.ResultSummary || $scope.exercisedto.ResultSummary.TryAgainResult != $scope.enums.ExerciseSetTakeTryAgainResult.Allow;
            };

            $scope.showAnswerQuestionButton = function () {
                return ($scope.currentQuestion.SelectedOption != null && $scope.currentQuestion.SelectedOption != 0) &&
                    ($scope.currentQuestion.AnsweredQuestionOptionID == null || $scope.currentQuestion.AnsweredQuestionOptionID == 0) &&
                    !$scope.exercisedto.DiscardedQuestionEnabled;
            };

            $scope.showBtnAnswerQuestion = function () {
                return $scope.answeredQuestionCount < $scope.countMustAnswerQuestion() &&
                    ($scope.currentQuestion.SelectedOption != null && $scope.currentQuestion.SelectedOption != 0) &&
                    ($scope.currentQuestion.AnsweredQuestionOptionID == null || $scope.currentQuestion.AnsweredQuestionOptionID == 0) &&
                    $scope.exercisedto.DiscardedQuestionEnabled &&
                    $scope.currentQuestion.Status != $scope.enums.StudentQuestionStatus.Discarded
            };

            changeFocusKeyDowIframe();
        }
    };
});